include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "ttbar + up to 2 jets in ME+PS. Decay modes: lepton-lepton'."
evgenConfig.keywords = [ "top","SM" ]
evgenConfig.contact  = [ "mcanobre@cern.ch","frank.siegert@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "ttbar5000"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE=DEFAULT;
  EXCLUSIVE_CLUSTER_MODE 1;

  MASSIVE[15]=1
  NJET:=2;
}(run)

(processes){
  # l l'
  Process 93 93 ->  6[a] -6[b] 93{NJET}
  Order_EW 4
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> -11 12
  Decay -24[d] -> 13 -14
  CKKW sqr(30/E_CMS)
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process

  # l' l                                                                                                
  Process 93 93 ->  6[a] -6[b] 93{NJET}                                                                 
  Order_EW 4                                                                                            
  Decay 6[a] -> 24[c] 5                                                                                 
  Decay -6[b] -> -24[d] -5                                                                              
  Decay 24[c] -> -13 14                                                                                 
  Decay -24[d] -> 11 -12                                                                                
  CKKW sqr(30/E_CMS)                                                                                      
  Integration_Error 0.1 {7,8,9,10}                                                                      
  Max_N_Quarks 6 {10};                                                                                  
  End process    
}(processes)

(selector){
  Mass 90 90 5000.0 10000.0
}(selector)
"""

