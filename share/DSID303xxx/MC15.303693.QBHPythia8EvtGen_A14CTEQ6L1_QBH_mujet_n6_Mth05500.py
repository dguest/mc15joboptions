evgenConfig.description = "Quantum black holes (n = 6, M_th = 5.5 TeV) decaying to muon+quark."
evgenConfig.process = "QBH -> mu + q"
evgenConfig.keywords = ["BSM", "exotic", "blackhole", "extraDimensions", "ADD"]
evgenConfig.generators += ["QBH"]
evgenConfig.contact = ["Doug Gingrich <gingrich@ualberta.ca>"]
evgenConfig.inputfilecheck = "QBH_mujet"

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py" )
include("MC15JobOptions/Pythia8_LHEF.py")
