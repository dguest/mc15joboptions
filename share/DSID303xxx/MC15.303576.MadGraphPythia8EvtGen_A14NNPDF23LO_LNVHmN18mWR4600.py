evgenConfig.description="MadGraph on-the-fly lepton number violating Higgs decay with same sign final state"
evgenConfig.keywords+=['BSM','exotic','BSMHiggs','2lepton','4jet']
evgenConfig.contact = ['miha.muskinja@cern.ch']
evgenConfig.process = "LNV Higgs to same sign 2lepton and 4jet"

#mass of the Majorana neutrinos
mN = 18.0

 #add_time_of_flight command for MadGraph
addLifetimes = True

proc_name = "PROC_LNVH" + str(mN)

proc_card = """set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_optimized_output True
set gauge unitary
set complex_mass_scheme False
import model lrsm_mix_1_3_UFO
define nonh2 = a w+ z2 h2 h02 h03 h+ hp2 hl++ hr++ h3 a02 w- h- hm2 hl-- hr-- ve vm vt n1 n3
define nonh1 = a w+ z2 h2 h02 h03 h+ hp2 hl++ hr++ h3 a02 w- h- hm2 hl-- hr-- ve vm vt n2 n3
generate g g > h > n2 n2, n2 > mu+ j j /nonh2 EWL=1 EWR=4 HIG=1 HIW=0 QCD=0 QED=0
add process g g > h > n2 n2, n2 > mu- j j /nonh2 EWL=1 EWR=4 HIG=1 HIW=0 QCD=0 QED=0
add process g g > h > n1 n1, n1 > e+ j j /nonh1 EWL=1 EWR=4 HIG=1 HIW=0 QCD=0 QED=0
add process g g > h > n1 n1, n1 > e- j j /nonh1 EWL=1 EWR=4 HIG=1 HIW=0 QCD=0 QED=0"""

#modifications to the param_card.dat (generated from the proc_card i.e. the specific model)
#if you want to see the resulting param_card, run Generate_tf with this jobo, and look at the param_card.dat in the cwd
#If you want to see the auto-calculated values of the decay widths, look at the one in <proc_name>/Cards/param_card.dat (again, after running a Generate_tf)
param_card_extras = { 
        "DECAY": { 'WN1':'Auto', 'WN2':'Auto', 'WN3':'Auto', 'WW2':'Auto' }, #auto-calculate decay widths and BR
        "MASS" : { 'mn1': mN   , 'mn2': mN   , 'mn3': mN   , 'MW2': 4600. }  #set masses of Majorana neutrinos
                    }

run_card_extras = { 'lhe_version'  :'1.0', 
                    'cut_decays'   :'F', 
                    'auto_ptj_mjj' :'F',
                    'bwcutoff'     :'1e15',
                    'ptj'          :'1',
                    'ptb'          :'1',
                    'pta'          :'0',
                    'ptl'          :'1',
                    'drjj'         :'0.1',
                    'drll'         :'0.1',
                    'draa'         :'0.1',
                    'draj'         :'0.1',
                    'drjl'         :'0.1',
                    'dral'         :'0.1',
                    'xqcut'        :'0'
                  }


include("MC15JobOptions/MadGraphControl_Pythia8_A14_NNPDF23LO_EvtGen_LNVHiggs_Common.py")