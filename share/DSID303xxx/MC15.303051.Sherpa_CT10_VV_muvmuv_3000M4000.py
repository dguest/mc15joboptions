include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Dilepton Mass-Binned Diboson Sample muvmuv 3000M4000"
evgenConfig.keywords = ["diboson", "2lepton", "neutrino", "2jet", "SM" ]
evgenConfig.contact  = [ "daniel.hayden@cern.ch", "frank.siegert@cern.ch", "atlas-generators-sherpa@cern.ch" ]
evgenConfig.minevents = 500

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  SP_NLOCT 1; FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=2; LJET:=0; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=Internal;
  EXCLUSIVE_CLUSTER_MODE=1
  METS_CLUSTER_MODE=16

  % massive b-quarks such that top-quark processes are not included by the 93 container
  MASSIVE[5]=1;
  OL_PARAMETERS=nq_nondecoupled 5 mass(5) 0.0
  MCATNLO_MASSIVE_SPLITTINGS=0

  MASSIVE[11]=1
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 -> 90 91 90 91 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;
}(processes)

(selector){
  "PT" 90 1.0,E_CMS:1.0,E_CMS [PT_UP]
  Mass 13 -13 3000.0 4000.0
}(selector)
"""
