#################################################################################
# job options fragment for PosDs->piphi5->mumu for HF tau3mu study for run2
#################################################################################

include("MC15JobOptions/nonStandard/Pythia8B_A14_NNPDF23LO_Common.py")
evgenConfig.description  = "PosDs->piphi5->mumu production"
evgenConfig.keywords     = [ "charmonium", "muon", "SM" ]
evgenConfig.minevents    = 500
evgenConfig.contact      = [ 'dai.kobayashi@cern.ch' ]
evgenConfig.process      = "pp>Ds>phipi>2mupi"

genSeq.Pythia8B.Commands       += [ 'HardQCD:all = on' ]
genSeq.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 5.' ]
genSeq.Pythia8B.Commands       += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands       += [ 'HadronLevel:all = off' ]

genSeq.Pythia8B.SelectBQuarks      = False
genSeq.Pythia8B.SelectCQuarks      = True
genSeq.Pythia8B.QuarkPtCut         = 5.0
genSeq.Pythia8B.AntiQuarkPtCut     = 5.0
genSeq.Pythia8B.QuarkEtaCut        = 4.5
genSeq.Pythia8B.AntiQuarkEtaCut    = 4.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents = True

genSeq.Pythia8B.NHadronizationLoops   = 10

genSeq.Pythia8B.Commands += [ '431:onMode = 3' ]
genSeq.Pythia8B.Commands += [ '431:offIfAny = 333' ]
genSeq.Pythia8B.Commands += [ '431:onPosIfMatch = -211 333' ]
genSeq.Pythia8B.Commands += [ '333:onMode = 0' ]
genSeq.Pythia8B.Commands += [ '333:onIfMatch = -13 13' ]

genSeq.Pythia8B.SignalPDGCodes  = [   431,   211,   333,   -13,    13 ]
genSeq.Pythia8B.SignalPtCuts    = [   0.0,   0.0,   0.0,   0.0,   0.0 ] # only for phi pt
genSeq.Pythia8B.SignalEtaCuts   = [ 102.5, 102.5, 102.5, 102.5, 102.5 ] # default

genSeq.Pythia8B.BPDGCodes = [511,521,531,541,5122,5132,5232,5332,-511,-521,-531,-541,-5122,-5132,-5232,-5332]

genSeq.Pythia8B.TriggerPDGCode        = 333
genSeq.Pythia8B.TriggerStateEtaCut    = 102.5 # default
genSeq.Pythia8B.TriggerStatePtCut     = [5.0]
genSeq.Pythia8B.MinimumCountPerCut    = [1]
