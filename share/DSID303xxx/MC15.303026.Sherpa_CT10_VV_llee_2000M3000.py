include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Dilepton Mass-Binned Diboson Sample llee 2000M3000 GeV"
evgenConfig.keywords = ["diboson", "2lepton", "4lepton", "2jet", "SM" ]
evgenConfig.contact  = [ "daniel.hayden@cern.ch", "frank.siegert@cern.ch", "atlas-generators-sherpa@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "Sherpa_CT10_VV_massbinned"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  SP_NLOCT 1; FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=0; LJET:=0; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=Internal;
  EXCLUSIVE_CLUSTER_MODE=1
  METS_CLUSTER_MODE=16

  % massive b-quarks such that top-quark processes are not included by the 93 container
  MASSIVE[5]=1;
  OL_PARAMETERS=nq_nondecoupled 5 mass(5) 0.0
  MCATNLO_MASSIVE_SPLITTINGS=0


  SHERPA_LDADD=SherpaTwoParticleMass
}(run)

(processes){
  Process 93 93 -> 11 -11 90 90 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;
}(processes)

(selector){
  "PT" 90 1.0,E_CMS
  TwoParticleMass 11 -11 2000.0 3000.0
  Mass 11 -11 0.2500 E_CMS
  Mass 13 -13 0.4614 E_CMS
  Mass 15 -15 3.8040 E_CMS
}(selector)
"""
