from MadGraphControl.MadGraphUtils import *

mode=0


cmdsps = """set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-; h0->b,bbar;
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio  0.5
"""

cmdspsh = """
do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar;
"""

safefactor=1.1
evgenConfig.description = "X->hh diHiggs production, decay to bbbb, with MG5_aMC@NLO"
evgenConfig.keywords = ["BSM",  "BSMHiggs", "resonance", "bbbar", "bottom"]
run_number_min=343394
run_number_max=343414

include("MC15JobOptions/MadGraphControl_HerwigppEvtGen_UEEE5_CTEQ6L1_CT10ME_Xhh.py")

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
#from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
#filtSeq += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
#filtSeq += ParentChildFilter("hWWFilter", PDGParent = [25], PDGChild = [24])
#filtSeq.Expression = "hbbFilter and hWWFilter"
