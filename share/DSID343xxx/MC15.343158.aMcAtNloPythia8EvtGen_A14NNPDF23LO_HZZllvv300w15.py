from MadGraphControl.MadGraphUtils import *

mode=0

#---------------------------------------------------------------------------------------------------
# Setting higgs mass
#---------------------------------------------------------------------------------------------------
higgsMass={'25':'3.000000e+02'} #MH
higgsDecay={'25':'DECAY 25 4.500000e+01'} #MH 15%

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'3.0', 
           'cut_decays':'F', 
           'pdlabel':"'nn23lo1'",
           'scale':'900',
           'dsqrt_q2fact1':'900',
           'dsqrt_q2fact2':'900',      
           'parton_shower':'PYTHIA8',
           'ptj':'0',
           'ptb':'0',
           'pta':'0',
           'ptjmax':'-1',
           'ptbmax':'-1',
           'ptamax':'-1',
           'etaj':'-1',
           'etab':'-1',
           'etaa':'-1',
           'etajmin':'0',
           'etabmin':'0',
           'etaamin':'0',
           'mmaa':'0',
           'mmaamax':'-1',
           'mmbb':'0',
           'mmbbmax':'-1',
           'drjj':'0',
           'drbb':'0',
           'draa':'0',
           'drbj':'0',
           'draj':'0',
           'drab':'0',
           'drjjmax':'-1',
           'drbbmax':'-1',
           'draamax':'-1',
           'drbjmax':'-1',
           'drajmax':'-1',
           'drabmax':'-1',
	   'maxjetflavor':'5' }

#---------------------------------------------------------------------------------------------------
# Generating Heavy Higgs resonance with MadGraph
#---------------------------------------------------------------------------------------------------
if (runArgs.runNumber == 343158):
    fcard = """
    import model heft_truncated-no_b_mass
    define p = p b b~
    define j = p
    generate p p > h > z z [real=QCD]
    output -f"""
else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
# Filter efficiency is ~50%
# Thus, setting the number of generated events to
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1

safefactor=1.1
nevents=(int)(5000*safefactor)
if runArgs.maxEvents > 0:
    nevents=(int)(runArgs.maxEvents*safefactor)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
process_dir = new_process(fcard)

build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras,xqcut=0.0)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
build_param_card(param_card_old='aMcAtNlo_param_card_HeavyHiggs.dat',param_card_new='param_card_new.dat',masses=higgsMass,decays=higgsDecay)  

# copy local version of fks_singular.f
file_loc='fks_singular.ggHrwgt.232.f'

mglog.info('Getting '+str(file_loc)+' via get_files')

get_file = subprocess.Popen(['get_files','-data',file_loc])
get_file.wait()

if not os.access(file_loc,os.R_OK):
    RuntimeError('Could not find '+str(file_loc))

shutil.copy(file_loc,process_dir+"/SubProcesses/fks_singular.f") 

#
# Build madspin card
#
newcard = open('madspin_card.dat','w')
newcard.write('#************************************************************\n')
newcard.write('#*                        MadSpin                           *\n')
newcard.write('#*                                                          *\n')
newcard.write('#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *\n')
newcard.write('#*                                                          *\n')
newcard.write('#*    Part of the MadGraph5_aMC@NLO Framework:              *\n')
newcard.write('#*    The MadGraph5_aMC@NLO Development Team - Find us at   *\n')
newcard.write('#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *\n')
newcard.write('#*                                                          *\n')
newcard.write('#************************************************************\n')

newcard.write('set max_weight_ps_point 400  # number of PS to estimate the maximum for each event\n')
newcard.write('decay t > w+ b, w+ > all all\n')
newcard.write('decay t~ > w- b~, w- > all all\n')
newcard.write('decay w+ > all all\n')
newcard.write('decay w- > all all\n')
newcard.write('decay z > l+ l-\n')
newcard.write('decay z > vl vl~\n')
newcard.write('# running the actual code\n')
newcard.write('launch\n')
newcard.close()

print_cards()

runName='run_01'     

generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName,madspin_card_loc='madspin_card.dat')

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

#---------------------------------------------------------------------------------------------------
# Pythia8 Showering with A14_NNPDF23LO
#---------------------------------------------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

# set globalRecoil on
genSeq.Pythia8.Commands += ["TimeShower:globalRecoil=On"]

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------

evgenConfig.generators += ["aMcAtNlo", "Pythia8", "EvtGen"]
if (runArgs.runNumber == 343158):
    evgenConfig.description = "H large width production, decay to ZZ, with MG5_aMC@NLO, ZZ to llvv with MadSpin, mH = 300 GeV, width = 15%."
    evgenConfig.keywords = ["BSM",  "BSMHiggs", "resonance", "ZZ"]

evgenConfig.contact = ['Lailin Xu <lailin.xu@cern.ch>','Robert Harrington <roberth@cern.ch>']

evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
