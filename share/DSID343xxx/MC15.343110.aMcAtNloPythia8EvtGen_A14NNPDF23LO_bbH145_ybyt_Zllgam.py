evgenConfig.description = 'MadGraph5_aMC@NLO+Pythia8 bbH production H->Zllgam'
evgenConfig.keywords    = [ 'Higgs', 'BSMHiggs', 'MSSM', 'photon', '2lepton' ]
evgenConfig.contact     = [ 'lorenz.hauswald@cern.ch', 'niklaos.rompotis@cern.ch', 'samina.jabbar@cern.ch', 'xifeng.ruan@cern.ch' ]

include("MC15JobOptions/aMcAtNloPythia8EvtGenControl_bbHZllgam.py")
evgenConfig.generators  = [ "aMcAtNlo", "Pythia8", "EvtGen"] 
