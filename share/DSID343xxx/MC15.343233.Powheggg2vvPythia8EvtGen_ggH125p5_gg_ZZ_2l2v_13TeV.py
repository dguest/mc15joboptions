##########################################################################################################
#
# gg2VV 3.1.6/Powheg/Pythia8 gg -> (H) -> ZZ, with ZZ -> 2(e/mu/tau)2v(different flavor to charged lepton)
#
# generator cuts on LHE level:
# pt(l+/l-) > 3GeV
# fabs( eta(l+/l-) ) < 5
# m(l+l-) > 4GeV ; pt(l+l-) > 2GeV
# charge leptons' flavor always differes from neutrino flavor (i.e. no WW interference)
# Higgs removed from LHE events
#

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += [ 'Powheg', 'gg2vv', 'Pythia8' ]
evgenConfig.description = 'gg2VV, (H->)ZZ-> 2l2v using CT10 PDF, PowhegPythia8 , m(l+l-)>4GeV - filter see below, Higgs removed from LHE file to avoid ME matched Pythia shower, interference kept, charged lepton flavor always differs to neutrino flavor -> no WW'
evgenConfig.keywords = ['diboson', '4lepton', 'electroweak', 'Higgs', 'ZZ']
evgenConfig.contact = ['jochen.meyer@cern.ch']
evgenConfig.inputfilecheck = 'gg2VV0316.343233.ggH125p5_gg_ZZ_2l2v'

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg.py')

# boson decays already done in the lhe file
genSeq.Pythia8.Commands += [ '25:onMode = off' ]
genSeq.Pythia8.Commands += [ '24:onMode = off' ]
genSeq.Pythia8.Commands += [ '23:onMode = off' ]

# no power shower, just wimpy showers
genSeq.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 1' ]

# lepton filter (since tau is involved)
include("MC15JobOptions/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut = 5000.
filtSeq.MultiLeptonFilter.Etacut = 5.0
filtSeq.MultiLeptonFilter.NLeptons = 2
