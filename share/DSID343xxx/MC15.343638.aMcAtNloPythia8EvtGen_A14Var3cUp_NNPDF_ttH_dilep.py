evgenConfig.generators += ["aMcAtNlo","Pythia8"]
evgenConfig.description = 'MG5_aMC@NLO_Pythia8'
evgenConfig.keywords+=['Higgs','top']
evgenConfig.inputfilecheck = 'ttH'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")
include("MC15JobOptions/Pythia8_SMHiggs125_inc.py")

include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0
