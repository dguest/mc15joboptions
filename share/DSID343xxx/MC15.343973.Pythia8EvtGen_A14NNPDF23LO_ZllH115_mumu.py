evgenConfig.description = "PYTHIA8+EVTGEN, ZH125, Z->ll, H->mumu"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZHiggs", "2muon" ]
evgenConfig.contact     = [ 'xin.chen@cern.ch' ]
evgenConfig.process     = "ZH, H->mumu, Z->ll"

#Higgs mass (in GeV)
H_Mass = 115.0

#Higgs width (in GeV)
H_Width = 0.00312

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:onMode = off',
                             '25:doForceWidth = true',
                             '25:onIfMatch = 13 13', # Higgs decay
                             'HiggsSM:ffbar2HZ = on',
                             '23:onMode = off',
                             '23:onIfAny = 11 13 15'
                             ]
evgenConfig.generators  = [ "Pythia8", "EvtGen"] 
