evgenConfig.description = "PYTHIA8+EVTGEN, WH125, W->qq, H->mumu"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WHiggs", "2muon", "mH125" ]
evgenConfig.contact     = [ 'xin.chen@cern.ch' ]
evgenConfig.process     = "WH, H->mumu, W->qq"

#Higgs mass (in GeV)
H_Mass = 125.0

#Higgs width (in GeV)
H_Width = 0.00407

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:onMode = off',
                             '25:doForceWidth = true',
                             '25:onIfMatch = 13 13', # Higgs decay
                             'HiggsSM:ffbar2HW = on',
                             '24:onMode = off',
                             '24:onIfAny = 1 2 3 4 5'
                             ]
evgenConfig.generators  = [ "Pythia8", "EvtGen"] 
