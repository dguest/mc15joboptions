Mchi =  200.
Mxi =  700.
gxichi = 1.0 
gxiU = 0.25 
Wxi = 38.528471665
evgenConfig.description = "Wimp pair monoZ with dmV"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kenji Hamano <kenji.hamano@cern.ch>"]
include("MC15JobOptions/MadGraphControl_Pythia8EvtGen_A14NNPDF30_dmV_Zll_MET40.py")
