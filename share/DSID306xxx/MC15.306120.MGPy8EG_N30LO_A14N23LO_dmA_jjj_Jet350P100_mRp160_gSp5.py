model='dmA'
mR  = 160
mDM= 10000
gSM= 0.5
gDM= 1.0
widthR = 15.8981
xptj=100
filteff = 0.01
jetminpt=350

evgenConfig.description = "Zprime sample mR160 - model dmA"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Karol Krizka <@cern.ch>", "Chase Shimmin <cshimmin@cern.ch>", "Yvonne Ng <yvonne.ng@cern.ch>"]
evgenConfig.minevents = 500

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetjet.py")
