include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "Mass-Binned XX to YY GeV, Sherpa ttbar production with tt+2j@LO in the dileptonic channel."
evgenConfig.keywords = ["SM", "top", "ttbar", "2lepton"]
evgenConfig.contact  = [ "Daniel.Hayden@cern.ch", "frank.siegert@cern.ch", "atlas-generators-sherpa@cern.ch" ]
evgenConfig.minevents = 100
evgenConfig.inputconfcheck = "Sherpa_221_NNPDF30NNLO_ttbar"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{sqr(172.5)+0.5*(PPerp2(p[2]+p[3]+p[4])+PPerp2(p[5]+p[6]+p[7]))};
  EXCLUSIVE_CLUSTER_MODE 1;
  
  %tags for process setup
  MASSIVE[15]=1
  NJET:=2; QCUT:=30.;

  %me generator settings
  INTEGRATION_ERROR=0.05;
}(run)

(processes){
  # l l
  Process 93 93 ->  6[a] -6[b] 93{NJET}
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> -13 14
  Decay -24[d] -> 13 -14
  CKKW sqr(QCUT/E_CMS)
  Order (*,4);
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process
}(processes)

(selector){
  Mass 90 90 250.0 500.0
  DecayMass 24 40.0 120.0
  DecayMass -24 40.0 120.0
  DecayMass 6 150.0 200.0
  DecayMass -6 150.0 200.0
}(selector)
"""
