evgenConfig.description = "Herwig7 multijets with pdf NNPDF30nlo, slice JZ1"
evgenConfig.generators  = ["Herwig7", "EvtGen"]
evgenConfig.keywords+=['QCD', 'jets']
evgenConfig.contact = ['jdickinson@lbl.gov']
evgenConfig.minevents = 5000

name = runArgs.jobConfig[0]
name_info = name.split("_JZ")[1].split(".py")
slice = int(name_info[0])

minkT = {0:0,1:0,2:15,3:50,4:150,5:350,6:600,7:950,8:1500,9:2200,10:2800,11:3500,12:4200}

# initialize Herwig7 generator configuration for built-in matrix elements
include("MC15JobOptions/Herwig7_BuiltinME.py")

# configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
command = """
##################################################
# MEMinBias Matrix Element
##################################################

### Note to users - Release 7.1:
### This currently uses parameters tuned for the
### default shower.

# MPI model settings
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0

## Report the correct cross section
cd /Herwig/Generators
create Herwig::MPIXSecReweighter MPIXSecReweighter
insert EventGenerator:EventHandler:PostSubProcessHandlers 0 MPIXSecReweighter
set EventGenerator:EventHandler:CascadeHandler NULL

clear EventGenerator:EventHandler:SubProcessHandlers[0]

##################################################
# Create separate SubProcessHandler for MinBias
##################################################
cd /Herwig/MatrixElements/
cp SubProcess QCDMinBias

set QCDMinBias:CascadeHandler /Herwig/DipoleShower/DipoleShowerHandler
set QCDMinBias:CascadeHandler:MPIHandler /Herwig/UnderlyingEvent/MPIHandler
set QCDMinBias:DecayHandler /Herwig/Decays/DecayHandler

# Due to numerics the pomeron could be seen as timelike.
set /Herwig/Shower/ShowerHandler:SplitHardProcess No
set /Herwig/DipoleShower/DipoleShowerHandler:SplitHardProcess No

insert QCDMinBias:MatrixElements[0] MEMinBias

cd /Herwig/Generators
# MinBias parameters used for the new kinematics of soft MPI
set /Herwig/Cuts/MinBiasCuts:X1Min 0.11
set /Herwig/Cuts/MinBiasCuts:X2Min 0.11

# Needed to get the correct fraction of diffractive events
set /Herwig/MatrixElements/MEMinBias:csNorm 4.5584

set EventGenerator:EventHandler:Cuts /Herwig/Cuts/MinBiasCuts

cd /Herwig/MatrixElements/
insert /Herwig/Generators/EventGenerator:EventHandler:SubProcessHandlers[0] QCDMinBias
set /Herwig/Cuts/JetKtCut:MinKT """+str(minkT[slice])+"""*GeV

cd /Herwig/EventHandlers
set EventHandler:CascadeHandler /Herwig/DipoleShower/DipoleShowerHandler
read snippets/DipoleShowerFiveFlavours.in
cd /Herwig/DipoleShower
do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=2.0 2.0 2.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=1.0 2.0 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=0.5 2.0 0.5 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=2.0" 1.0 2.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.5" 1.0 0.5 All
do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=2.0" 0.5 2.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=1.0 0.5 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=0.5 0.5 0.5 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.75_fsr:muRfac=1.0 1.75 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.5_fsr:muRfac=1.0 1.5 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.25_fsr:muRfac=1.0 1.25 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=0.625_fsr:muRfac=1.0" 0.625 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=0.75_fsr:muRfac=1.0 0.75 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=0.875_fsr:muRfac=1.0 0.875 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.75 1.0 1.75 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.5 1.0 1.5 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.25 1.0 1.25 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.625 1.0 0.625 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.75 1.0 0.75 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.875 1.0 0.85 All

###  END
"""
print command

Herwig7Config.add_commands(command)

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")
#Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")

# run Herwig7
Herwig7Config.run()

include("MC15JobOptions/JetFilter_JZ1.py")
