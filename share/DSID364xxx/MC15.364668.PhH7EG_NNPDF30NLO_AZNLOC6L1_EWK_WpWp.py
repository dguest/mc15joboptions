include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3ME_LHEF_EvtGen_Common.py")
include("MC15JobOptions/Herwig7_701_StripWeights.py")

from Herwig7_i import config as hw
genSeq.Herwig7.Commands += hw.powhegbox_cmds().splitlines()
cmds1 = """ set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001 """
genSeq.Herwig7.Commands += cmds1.splitlines()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+HERWIG7+EVTGEN, VBS WpWp + 2 jets"
evgenConfig.generators += ["Powheg", "Herwig7"]
evgenConfig.keywords    = [ "SM", "VBS", "electroweak", "WW", "2jet", "NLO" ]
evgenConfig.contact     = [ "Jochen Meyer <Jochen.Meyer@cern.ch>", "Stefanie Todt <Stefanie.Todt@cern.ch>" ]
evgenConfig.process     = "VBS ssWW"
evgenConfig.inputfilecheck = 'EWK_WpWp'
evgenConfig.minevents   = 5000


