from MadGraphControl.MadGraphUtils import *

# General settings
minevents=5000
nevents=20000 #this most often generate the 5000 needed

run_name='run_01'
mode=0
gridpack_dir=None
gridpack_mode=False

# MG Particle cuts
mllcut=40

# Shower/merging settings
maxjetflavor=5
parton_shower='PYTHIA8'
nJetMax=3
qCut=30.

Zmumu=[364856]
name='Zmumu'
keyword=['SM','Z'] 
stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

if runArgs.runNumber in Zmumu:
   gridpack_mode=True
   gridpack_dir='madevent/'
   
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model loop_sm-no_b_mass
define p = p b b~
define j = p
generate p p > mu+ mu- [QCD] @0
add process p p > mu+ mu- j [QCD] @1                                                                                                    
add process p p > mu+ mu- j j [QCD] @2                                                                                                     
add process p p > mu+ mu- j j j [QCD] @3 
output -f
""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

lhaid=325100    #NNPDF31nnlo, luxQED and alphaS = 0.118 
pdflabel='lhapdf'
pdfErrSize=100
lhe_version=3.0

#Fetch default LO run_card.dat and set parameters
extras = { 'lhaid'         : lhaid,
           'pdlabel'       : "'"+pdflabel+"'",
           'maxjetflavor'  : maxjetflavor,
           'parton_shower' : parton_shower,
           'ickkw'         : 3,
           'reweight_scale': '.true.',
           'rw_Rscale_down':  0.5,
           'rw_Rscale_up'  :  2.0,
           'rw_Fscale_down':  0.5,  
           'rw_Fscale_up'  :  2.0, 
           'reweight_PDF'  : '.true.',
           'PDF_set_min'   : lhaid+1, 
           'PDF_set_max'   : lhaid+pdfErrSize, 
           'jetradius'     : 1.0,
           'ptj'           : 10,
           'etaj'          : 10,
           'mll_sf'        : mllcut,
           'mll'           : mllcut
}

process_dir = new_process(grid_pack=gridpack_dir)
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', 
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
               extras=extras)
print_cards()

param_card_loc='param_card.Torrielli.dat'
paramcard = subprocess.Popen(['get_files','-data',param_card_loc])
paramcard.wait()
if not os.access(param_card_loc,os.R_OK):
    raise RuntimeError("ERROR: Could not get %s"%param_card_loc)

generate(run_card_loc='run_card.dat',param_card_loc=param_card_loc,mode=mode,proc_dir=process_dir,
         run_name=run_name,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=runArgs.randomSeed,gridpack_compile=True)
arrange_output(run_name=run_name,proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',lhe_version=lhe_version)

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

#### Shower 
evgenConfig.description = 'aMcAtNloPythia8EvtGen_'+str(name)
evgenConfig.keywords+=keyword 
#evgenConfig.inputfilecheck = stringy
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch"]
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

PYTHIA8_nJetMax=nJetMax
PYTHIA8_qCut=qCut
include("MC15JobOptions/Pythia8_FxFx_vjets.py")
