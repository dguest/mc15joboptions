include("MC15JobOptions/Sherpa_2.2.5_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> qq + 1,2,3,4j@LO with 1000 GeV < pTV < E_CMS."
evgenConfig.keywords = ["SM", "Z", "jets" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "Zqq_PTV1000_E_CMS"

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; QCUT:=20.;

  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0
  HDH_STATUS[23,1,-1]=2
  HDH_STATUS[23,2,-2]=2
  HDH_STATUS[23,3,-3]=2
  HDH_STATUS[23,4,-4]=2
  HDH_STATUS[23,5,-5]=2
}(run)

(processes){
  Process 93 93 -> 23 93 93{NJET};
  Cut_Core 1;
  Order (*,1); CKKW sqr(QCUT/E_CMS);
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.05 {5,6,7,8};
  End process;
}(processes)

(selector){
  PT 23 1000.0 E_CMS
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[23]=0" ]
genSeq.Sherpa_i.NCores = 96

