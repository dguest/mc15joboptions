include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa taunugamma + 0,1j@NLO + 2,3j@LO with 35<pT_y<70."
evgenConfig.keywords = ["SM", "tau", "photon", "neutrino", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "Sherpa_222_NNPDF30NNLO_taunugamma_pty_35_70"

evgenConfig.process="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % tags for process setup
  NJET:=3; LJET:=3,4; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0
  NUM_ACCURACY=1e-6
  DIPOLE_AMIN=1e-8

  % tau settings
  SOFT_SPIN_CORRELATIONS=1
  # DECAYFILE=HadronDecaysTauL.dat # first tau in event decays leptonically
}(run)

(processes){
  Process 93 93 -> 22 15 -16 93{NJET}
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.99 {5,6}
  End process

  Process 93 93 -> 22 -15 16 93{NJET}
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.99 {5,6}
  End process
}(processes)

(selector){
  PTNLO  22  35  70
  IsolationCut  22  0.1  2  0.10
  DeltaRNLO  22  90  0.1 1000.0
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=redlib1=5=redlib2=5=write_parameters=1" ]

# take tau BR into account
genSeq.Sherpa_i.CrossSectionScaleFactor=0.352
