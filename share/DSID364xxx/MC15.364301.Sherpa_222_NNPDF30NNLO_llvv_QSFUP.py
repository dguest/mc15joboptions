include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "2 charged leptons + 2 neutrinos with 0,1j@NLO + 2,3j@LO and mll>4GeV, pTl1>5 GeV, pTl2>5 GeV, QSFUP variation."
evgenConfig.keywords = ["SM", "diboson", "2lepton", "neutrino", "jets", "NLO", "systematic" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 200
evgenConfig.inputconfcheck = "llvv_QSFUP"

Sherpa_iRunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=4.0;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=4,5; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  PARTICLE_CONTAINER 900 lminus 11 13 15
  PARTICLE_CONTAINER 901 lplus -11 -13 -15

  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0
  SOFT_SPIN_CORRELATIONS=1

  % massive b-quarks such that b-quarks are not included in the 93 container
  % and hence top-quark processes are not considered
  MASSIVE[5]=1;
  OL_PARAMETERS=nq_nondecoupled 5 mass(5) 0.0
  MCATNLO_MASSIVE_SPLITTINGS=0
}(run)

(processes){
  Process 93 93 -> 900 91 901 91 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  Enhance_Factor  3 {5};
  Enhance_Factor 10 {6};
  Enhance_Factor 10 {7};
  End process;
}(processes)

(selector){
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
  Mass 11 -11 4.0 E_CMS
  Mass 13 -13 4.0 E_CMS
  Mass 15 -15 4.0 E_CMS
}(selector)
"""

Sherpa_iOpenLoopsLibs = [ "ppllll", "ppllllj" ]
Sherpa_iNCores = 96

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

