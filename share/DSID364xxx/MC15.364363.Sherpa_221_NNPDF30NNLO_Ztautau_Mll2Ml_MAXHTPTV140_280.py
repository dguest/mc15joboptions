include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> tau tau + 0,1,2j@NLO + 3,4j@LO with 140GeV<max(HT,pTV)<280GeV and mll>2*ml+250 MeV."
evgenConfig.keywords = ["SM", "Z", "drellYan", "2tau", "jets", "NLO",  ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "Ztautau_Mll2Ml_MAXHTPTV140_280"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=2,3; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops
  SOFT_SPIN_CORRELATIONS=1

  %settings for MAX(HT,PTV) slicing
  SHERPA_LDADD=SherpaFastjetMAXHTPTV
  HTMIN:=140
  HTMAX:=280
}(run)

(processes){
  Process 93 93 -> 15 -15 93 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  # mll > 2*ml+250 MeV
  Mass 15 -15 3.804 E_CMS

  # pT_lep1>2 GeV, pT_lep2>2 GeV
  "PT" 90 2.0,E_CMS:2.0,E_CMS [PT_UP]

  # either (mll < 10 GeV) or (pT_lep2 < 5 GeV)
  # to make it complementary to the 10<mll<40 samples
  MinSelector {
    Mass 15 -15 0.0 10.0
    "PT" 90 0.0,E_CMS:0.0,5.0 [PT_UP]
  }

  # 140 GeV < max(HT,pTV) < 280 GeV
  FastjetMAXHTPTV  HTMIN  HTMAX  antikt  20.0  0.0  0.4
}(selector)
"""
