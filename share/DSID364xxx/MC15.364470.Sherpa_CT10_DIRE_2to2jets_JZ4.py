include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "QCD: 2 jets in ME (no electro-weak processes included). The CT14NNLO PDF is used with the CT10 tune. The DIRE shower is used."
evgenConfig.keywords = ["jets", "multijet", "QCD", "SM"]
evgenConfig.contact  = ["amoroso@cern.ch", "stef.von.b@cern.ch"]
evgenConfig.minevents = 1000

# Top mass set high to avoid g -> tt splitting in high pT jets
genSeq.Sherpa_i.Parameters += ["MASS[6]=100000.0"]

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  PDF_LIBRARY LHAPDFSherpa; PDF_SET CT14nnlo;
  CORE_SCALE QCD;
  CSS_FS_AS_FAC 1; CSS_FS_PT2MIN 3;
  CSS_IS_AS_FAC 1; CSS_IS_PT2MIN 3;
  SHOWER_GENERATOR=Dire
}(run)

(processes){
  Process 93 93 -> 93 93
  Order (*,0);  
  Integration_Error 0.02 {2};
  End process;
}(processes)

(selector){
  NJetFinder  2  10.0  0.0  0.4  -1  999.0  10.0
  NJetFinder  1  200.0  0.0  0.4  -1  999.0  10.0
}(selector)
"""

include("MC15JobOptions/JetFilter_JZ4.py")
