#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal top mass, A14 tune var 3c down, at least one lepton filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch' ]

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.topdecaymode = 22222
PowhegConfig.hdamp = 172.5
PowhegConfig.PDF = 260000
#Information on how to run with multiple weights: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PowhegForATLAS#Running_with_multiple_scale_PDF
#PDFs - you can see a listing here: https://lhapdf.hepforge.org/pdfsets.html; picked these three as they are the inputs to the PDF4LHC2015 prescription (http://arxiv.org/pdf/1510.03865v2.pdf).

# Weight group configuration
# tested mu_F, muF, mu_R, muR and no change
PowhegConfig.define_event_weight_group( group_name='hdamp_pdf', parameters_to_vary=['hdamp','mu_F','mu_R','PDF'] )
PowhegConfig.define_event_weight_group( group_name='hdamp_scales', parameters_to_vary=['hdamp','mu_F','mu_R','PDF'] )

# hdamp variation with NNPDF
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_0p5mtop_NNPDF',     parameter_values=[86.25, 1.0, 1.0,260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_mtop_NNPDF',        parameter_values=[172.5, 1.0, 1.0,260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_1p5mtop_NNPDF',     parameter_values=[258.75, 1.0, 1.0,260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_2mtop_NNPDF',       parameter_values=[345.0, 1.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_3mtop_NNPDF',       parameter_values=[517.5, 1.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_4mtop_NNPDF',       parameter_values=[790.0, 1.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_5mtop_NNPDF',       parameter_values=[962.5, 1.0, 1.0, 260000] )
# PDF variation, MMHT2014nlo68clas118
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_0p5mtop_MMHT',      parameter_values=[86.25, 1.0, 1.0,25200] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_mtop_MMHT',         parameter_values=[172.5, 1.0, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_1p5mtop_MMHT',      parameter_values=[258.75, 1.0, 1.0,25200] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_2mtop_MMHT',        parameter_values=[345.0, 1.0, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_3mtop_MMHT',        parameter_values=[517.5, 1.0, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_4mtop_MMHT',        parameter_values=[790.0, 1.0, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_5mtop_MMHT',        parameter_values=[962.5, 1.0, 1.0, 25200] )
# PDF variation, CT14nlo_as_0118
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_0p5mtop_CT14',      parameter_values=[86.25, 1.0, 1.0,13165] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_mtop_CT14',         parameter_values=[172.5, 1.0, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_1p5mtop_CT14',      parameter_values=[258.75, 1.0, 1.0,13165] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_2mtop_CT14',        parameter_values=[345.0, 1.0, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_3mtop_CT14',        parameter_values=[517.5, 1.0, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_4mtop_CT14',        parameter_values=[790.0, 1.0, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_5mtop_CT14',        parameter_values=[962.5, 1.0, 1.0, 13165] )
# PDF variation, PDF4LHC15_nlo_30
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_0p5mtop_PDF4LHC',   parameter_values=[86.25, 1.0, 1.0,90900] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_mtop_PDF4LHC',      parameter_values=[172.5, 1.0, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_1p5mtop_PDF4LHC',   parameter_values=[258.75, 1.0, 1.0,90900] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_2mtop_PDF4LHC',     parameter_values=[345.0, 1.0, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_3mtop_PDF4LHC',     parameter_values=[517.5, 1.0, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_4mtop_PDF4LHC',     parameter_values=[790.0, 1.0, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='hdamp_pdf', weight_name='hdamp_5mtop_PDF4LHC',     parameter_values=[962.5, 1.0, 1.0, 90900] )

# Scale variations, hdamp = 0p5mtop, NNPDF
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_0p5mtop_2muF',           parameter_values=[86.25, 2.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_0p5mtop_0p5muF',         parameter_values=[86.25, 0.5, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_0p5mtop_2muR',           parameter_values=[86.25, 1.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_0p5mtop_0p5muR',         parameter_values=[86.25, 1.0, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_0p5mtop_0p5muF_0p5muR',  parameter_values=[86.25, 0.5, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_0p5mtop_2muF_2muR',      parameter_values=[86.25, 2.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_0p5mtop_0p5muF_2muR',    parameter_values=[86.25, 0.5, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_0p5mtop_2muF_0p5muR',    parameter_values=[86.25, 2.0, 0.5, 260000] )
# Scale variations, hdamp = mtop, NNPDF
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_mtop_2muF',              parameter_values=[172.5, 2.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_mtop_0p5muF',            parameter_values=[172.5, 0.5, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_mtop_2muR',              parameter_values=[172.5, 1.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_mtop_0p5muR',            parameter_values=[172.5, 1.0, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_mtop_0p5muF_0p5muR',     parameter_values=[172.5, 0.5, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_mtop_2muF_2muR',         parameter_values=[172.5, 2.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_mtop_0p5muF_2muR',       parameter_values=[172.5, 0.5, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_mtop_2muF_0p5muR',       parameter_values=[172.5, 2.0, 0.5, 260000] )
# Scale variations, hdamp = 1p5mtop, NNPDF
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_1p5mtop_2muF',           parameter_values=[258.75, 2.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_1p5mtop_0p5muF',         parameter_values=[258.75, 0.5, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_1p5mtop_2muR',           parameter_values=[258.75, 1.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_1p5mtop_0p5muR',         parameter_values=[258.75, 1.0, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_1p5mtop_0p5muF_0p5muR',  parameter_values=[258.75, 0.5, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_1p5mtop_2muF_2muR',      parameter_values=[258.75, 2.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_1p5mtop_0p5muF_2muR',    parameter_values=[258.75, 0.5, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_1p5mtop_2muF_0p5muR',    parameter_values=[258.75, 2.0, 0.5, 260000] )
# Scale variations, hdamp = 2mtop, NNPDF
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_2mtop_2muF',             parameter_values=[345.0, 2.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_2mtop_0p5muF',           parameter_values=[345.0, 0.5, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_2mtop_2muR',             parameter_values=[345.0, 1.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_2mtop_0p5muR',           parameter_values=[345.0, 1.0, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_2mtop_0p5muF_0p5muR',    parameter_values=[345.0, 0.5, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_2mtop_2muF_2muR',        parameter_values=[345.0, 2.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_2mtop_0p5muF_2muR',      parameter_values=[345.0, 0.5, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_2mtop_2muF_0p5muR',      parameter_values=[345.0, 2.0, 0.5, 260000] )
# Scale variations, hdamp = 3mtop, NNPDF
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_3mtop_2muF',             parameter_values=[517.5, 2.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_3mtop_0p5muF',           parameter_values=[517.5, 0.5, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_3mtop_2muR',             parameter_values=[517.5, 1.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_3mtop_0p5muR',           parameter_values=[517.5, 1.0, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_3mtop_0p5muF_0p5muR',    parameter_values=[517.5, 0.5, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_3mtop_2muF_2muR',        parameter_values=[517.5, 2.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_3mtop_0p5muF_2muR',      parameter_values=[517.5, 0.5, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_3mtop_2muF_0p5muR',      parameter_values=[517.5, 2.0, 0.5, 260000] )
# Scale variations, hdamp = 4mtop, NNPDF
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_4mtop_2muF',             parameter_values=[690.0, 2.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_4mtop_0p5muF',           parameter_values=[690.0, 0.5, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_4mtop_2muR',             parameter_values=[690.0, 1.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_4mtop_0p5muR',           parameter_values=[690.0, 1.0, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_4mtop_0p5muF_0p5muR',    parameter_values=[690.0, 0.5, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_4mtop_2muF_2muR',        parameter_values=[690.0, 2.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_4mtop_0p5muF_2muR',      parameter_values=[690.0, 0.5, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_4mtop_2muF_0p5muR',      parameter_values=[690.0, 2.0, 0.5, 260000] )
# Scale variations, hdamp = 5mtop, NNPDF
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_5mtop_2muF',             parameter_values=[862.5, 2.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_5mtop_0p5muF',           parameter_values=[862.5, 0.5, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_5mtop_2muR',             parameter_values=[862.5, 1.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_5mtop_0p5muR',           parameter_values=[862.5, 1.0, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_5mtop_0p5muF_0p5muR',    parameter_values=[862.5, 0.5, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_5mtop_2muF_2muR',        parameter_values=[862.5, 2.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_5mtop_0p5muF_2muR',      parameter_values=[862.5, 0.5, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='hdamp_scales', weight_name='hdamp_5mtop_2muF_0p5muR',      parameter_values=[862.5, 2.0, 0.5, 260000] )

PowhegConfig.nEvents     *= 3. # compensate filter efficiency
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_Var3cDown_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
