# based on the JobOptions MC15.426042 and MC15.429312

evgenConfig.description = "Herwig7 BuiltinME QCD dijet production JZ2W with MMHT2014lo68cl PDF and H7-UE-MMHT tune."
evgenConfig.generators  = ["Herwig7", "EvtGen"] 
evgenConfig.keywords    = ["QCD", "jets"]
evgenConfig.contact     = ['paolo.francavilla@cern.ch', "Haifeng.Li@cern.ch", "daniel.rauch@desy.de"]
evgenConfig.minevents   = 500

# initialize Herwig7 generator configuration for built-in matrix elements
include("MC15JobOptions/Herwig7_BuiltinME.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="LO", name="MMHT2014lo68cl")
Herwig7Config.tune_commands()
Herwig7Config.add_commands("""
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 15*GeV
""")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()

include("MC15JobOptions/JetFilter_JZ2W.py")
