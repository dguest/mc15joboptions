evgenConfig.description = "Sherpa Z/gamma* -> mu mu + 0,1,2j@NLO + 3,4j@LO with 0 GeV < max(HT, pTV) < 70 GeV with charm-jet (b-jet veto) filter taking input from existing unfiltered input file"
evgenConfig.keywords = ["SM", "Z", "2muon", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch","mdshapiro@lbl.gov" ]
evgenConfig.minevents = 5000

if runArgs.trfSubstepName == 'generate' :
   print "ERROR: These JO require an input file.  Please use the --afterburn option"

if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]

## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below 
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False

## Disable TestHepMC for the time being, cf.   
## https://its.cern.ch/jira/browse/ATLMCPROD-1862 
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   include("MC15JobOptions/AntiKt4TruthJets.py")
   include("MC15JobOptions/BHadronFilter.py")
   include("MC15JobOptions/CHadronPt4Eta3_Filter.py")
   filtSeq += HeavyFlavorBHadronFilter
   filtSeq += HeavyFlavorCHadronPt4Eta3_Filter
   filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (HeavyFlavorCHadronPt4Eta3_Filter)"

