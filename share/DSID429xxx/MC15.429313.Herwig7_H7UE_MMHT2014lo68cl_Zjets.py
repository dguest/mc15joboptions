include ( 'MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_Common.py' )

## Add to commands
cmds = """

## Z+jet
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEZJet

"""

## Set commands
genSeq.Herwig7.Commands += cmds.splitlines()

evgenConfig.description = "Z + jets with MMHT2014 LO PDF and H7UE tune"
evgenConfig.keywords = ["SM", "Z", "jets"]
evgenConfig.minevents = 5000
evgenConfig.contact = ["Orel Gueta"]
