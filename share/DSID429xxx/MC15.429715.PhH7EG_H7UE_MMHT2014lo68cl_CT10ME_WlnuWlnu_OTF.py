# based on the JobOptions MC15.361591 and MC15.429308

# Provide config information
evgenConfig.generators    += ["Powheg", "Herwig7", "EvtGen"] 
evgenConfig.tune           = "H7-UE-MMHT"
evgenConfig.description    = "PowhegBox/Herwig7 Diboson WW->lvlv production with H7-UE-MMHT tune"
evgenConfig.keywords       = ['electroweak', 'diboson', 'WW', 'neutrino', '2lepton']
evgenConfig.contact        = ['james.robinson@cern.ch', 'christian.johnson@cern.ch', 'paolo.francavilla@cern.ch', 'daniel.rauch@desy.de']
evgenConfig.minevents      = 5000

# PowhegBox WW setup starting from ATLAS defaults
include('PowhegControl/PowhegControl_WW_Common.py')
#PowhegConfig.decay_mode = 'WWlvlv'
PowhegConfig.decay_mode = "w+ w- > l+ vl l'- vl'~"
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.PDF = range( 11000, 11053 )+[ 21100, 260000 ] # CT10nlo 0-52, MSTW2008nlo68cl, NNPDF3.0
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.nEvents *= 2.0     # Dependent on filter efficiency
PowhegConfig.generate()

# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()
