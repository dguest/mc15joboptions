include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")

evgenConfig.description = "Z to tau tau gamma production with up to three jets in ME+PS and 35<pT_gamma<70 GeV."
evgenConfig.keywords = [ "electroweak", "2tau", "photon", "SM" ]
evgenConfig.inputconfcheck = "tautaugammaPt35_70"
evgenConfig.contact  = [ "frank.siegert@cern.ch", "stefano.manzoni@cern.ch","atlas-generators-sherpa@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.process="""
(run){
  ACTIVE[25]=0

  SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
  Process 93 93 ->  15 -15 22 93{3}
  Order (*,3)
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {6}
  Integration_Error 0.05 {6}
  End process;
}(processes)

(selector){
  PT 22  35 70
  Mass 90 90 10 7000
  DeltaR 22 90 0.1 1000
  IsolationCut  22  0.3  2  0.025
}(selector)
"""
