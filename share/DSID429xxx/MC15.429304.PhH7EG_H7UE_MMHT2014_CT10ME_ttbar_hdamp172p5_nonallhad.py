#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 ttbar production with Powheg hdamp equal top mass, H7-UE-MMHT tune, at least one lepton filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','onofrio@liverpool.ac.uk','andrea.helen.knue@cern.ch', 'orel.gueta@cern.ch' ]
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.topdecaymode = 22222
PowhegConfig.hdamp        = 172.5
# compensate filter efficiency
PowhegConfig.nEvents     *= 3.
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Showering with Herwig7, H7-UE-MMHT tune
#--------------------------------------------------------------
#include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_CT10_LHEF_Common.py")
include('MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_CT10_LHEF_EvtGen_Common.py')
 
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
