evgenConfig.description = "Low-pT inelastic minimum bias events using EPOS"
evgenConfig.keywords = ["QCD", "minBias" , "SM"]
evgenConfig.contact  = [ "deepak.kar@cern.ch" ]
evgenConfig.minevents = 1000

include("MC15JobOptions/Epos_Base_Fragment.py")

