evgenConfig.description = "Low-pT inelastic minimum bias events using EPOS"
evgenConfig.keywords = ["QCD", "minBias" , "SM"]
evgenConfig.contact  = [ "deepak.kar@cern.ch" ]

evgenConfig.saveJets = True

include("MC15JobOptions/Epos_Base_Fragment.py")

#include("MC15JobOptions/JetFilter_MinbiasLow.py")
include("MC15JobOptions/JetFilter_MinbiasHigh.py")

#include ("GeneratorFilters/FindJets.py")
#CreateJets(prefiltSeq,filtSeq,runArgs.ecmEnergy, 0.6)

#from AthenaCommon.SystemOfUnits import GeV
#filtSeq.QCDTruthJetFilter.MaxPt = 35.*GeV

# This will remove the TestHepMC temporarily, so that it will not crash comparing 900 GeV vs. 900.002 GeV
del testSeq.TestHepMC

evgenConfig.minevents = 1000
