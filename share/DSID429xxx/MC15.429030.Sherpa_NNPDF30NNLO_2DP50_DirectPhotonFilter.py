include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")

evgenConfig.description = "gamma gamma + 0,1,2,3 jets"
evgenConfig.keywords = ["SM", "diphoton"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.inputconfcheck = "2DP"
evgenConfig.minevents = 2000

evgenConfig.process="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])}

  % tags for process setup
  NJET:=3; QCUT:=10;
}(run)

(processes){
  Process 21 21 -> 22 22
  ME_Generator Internal;
  Loop_Generator gg_yy
  Scales VAR{Abs2(p[2]+p[3])};
  End process;

  Process 93 93 -> 22 22 93{NJET};
  Order (*,2);
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/Abs2(p[2]+p[3]));
  Scales LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {7,8};
  End process;
}(processes)

(selector){
  PT      22      40.0 E_CMS
  IsolationCut  22  0.3  2  0.025
  DeltaR 22 22 0.2 1000.0
}(selector)
"""


## Filter
if not hasattr( filtSeq, "DirectPhotonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
    filtSeq += DirectPhotonFilter()
    pass


DirectPhotonFilter = filtSeq.DirectPhotonFilter
DirectPhotonFilter.Ptcut = 50000.
DirectPhotonFilter.Etacut = 2.7
DirectPhotonFilter.NPhotons = 2

