include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> nu nu + 0,1,2j@NLO + 3,4j@LO with 1000 GeV < ptV < 2000 GeV with b-jet filter."
evgenConfig.keywords = ["SM", "Z", "neutrino", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 20
evgenConfig.inputconfcheck = "Sherpa_CT10_Znunu_Pt1000_2000"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=0.25;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
}(run)

(processes){
  Process 93 93 -> 91 91 93 93{3};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Scales LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 91 91 2.0 E_CMS
  "PT" 91,91  1000,2000
}(selector)
"""


# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter

