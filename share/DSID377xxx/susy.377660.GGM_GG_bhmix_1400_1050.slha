#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.04500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.05000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05405947E+01   # W+
        25     1.25000000E+02   # h
        35     2.00410066E+03   # H
        36     2.00000000E+03   # A
        37     2.00157623E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013294E+03   # ~d_L
   2000001     5.00002536E+03   # ~d_R
   1000002     4.99989242E+03   # ~u_L
   2000002     4.99994929E+03   # ~u_R
   1000003     5.00013294E+03   # ~s_L
   2000003     5.00002536E+03   # ~s_R
   1000004     4.99989242E+03   # ~c_L
   2000004     4.99994929E+03   # ~c_R
   1000005     4.99965305E+03   # ~b_1
   2000005     5.00050665E+03   # ~b_2
   1000006     4.99158024E+03   # ~t_1
   2000006     5.01286460E+03   # ~t_2
   1000011     5.00008223E+03   # ~e_L
   2000011     5.00007607E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008223E+03   # ~mu_L
   2000013     5.00007607E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99979938E+03   # ~tau_1
   2000015     5.00035953E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.40000000E+03   # ~g
   1000022     1.00239707E+03   # ~chi_10
   1000023    -1.05009580E+03   # ~chi_20
   1000025     1.08955914E+03   # ~chi_30
   1000035     3.00313959E+03   # ~chi_40
   1000024     1.04698064E+03   # ~chi_1+
   1000037     3.00313814E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.14711958E-01   # N_11
  1  2    -2.71044768E-02   # N_12
  1  3     4.96479453E-01   # N_13
  1  4    -4.91894621E-01   # N_14
  2  1     4.16755413E-03   # N_21
  2  2    -3.83353599E-03   # N_22
  2  3    -7.06934144E-01   # N_23
  2  4    -7.07256709E-01   # N_24
  3  1     6.99405917E-01   # N_31
  3  2     2.90076306E-02   # N_32
  3  3    -5.03099499E-01   # N_33
  3  4     5.06834109E-01   # N_34
  4  1     9.00955260E-04   # N_41
  4  2    -9.99204288E-01   # N_42
  4  3    -2.53606625E-02   # N_43
  4  4     3.07703813E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.58455846E-02   # U_11
  1  2     9.99357341E-01   # U_12
  2  1     9.99357341E-01   # U_21
  2  2     3.58455846E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.34980292E-02   # V_11
  1  2     9.99053513E-01   # V_12
  2  1     9.99053513E-01   # V_21
  2  2     4.34980292E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.08050427E-01   # cos(theta_t)
  1  2     7.06161874E-01   # sin(theta_t)
  2  1    -7.06161874E-01   # -sin(theta_t)
  2  2     7.08050427E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.61047825E-01   # cos(theta_b)
  1  2     7.50343770E-01   # sin(theta_b)
  2  1    -7.50343770E-01   # -sin(theta_b)
  2  2     6.61047825E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03207563E-01   # cos(theta_tau)
  1  2     7.10984615E-01   # sin(theta_tau)
  2  1    -7.10984615E-01   # -sin(theta_tau)
  2  2     7.03207563E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90203542E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.05000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52097276E+02   # vev(Q)              
         4     3.52128165E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52813954E-01   # gprime(Q) DRbar
     2     6.27372472E-01   # g(Q) DRbar
     3     1.08619156E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02546599E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71872485E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79828993E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.04500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.81553448E+06   # M^2_Hd              
        22    -6.49780106E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37136831E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.72812571E-06   # gluino decays
#          BR         NDA      ID1       ID2
     6.35100448E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     9.52545963E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.58016273E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.21978230E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.45009798E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.38912901E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.59480570E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.96989377E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     5.27517924E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.21978230E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.45009798E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.38912901E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.59480570E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.96989377E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     5.27517924E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.24646586E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.14365002E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     1.44293718E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     6.71498366E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.18869007E-05    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     9.92238261E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     9.92238261E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     9.92238261E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     9.92238261E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     4.96662401E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     4.96662401E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.40156376E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.56103011E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.87572326E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.82997634E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.00903972E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.02947238E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.00432085E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.54251859E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.31940116E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.47980324E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.81411113E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.84674102E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.32361061E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.40699034E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.63258594E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.54961577E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.57059015E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.40525604E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.60105645E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.43426166E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.20956375E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.00303319E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.43364314E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.56349084E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.75681494E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.15630939E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.93037026E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.74624213E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.67960079E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.46124535E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.38024579E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.98275144E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.43994860E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.15956300E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.60147259E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80375444E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.16932941E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.68282225E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.33889555E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.02029097E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.29821559E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.17826660E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     7.34618285E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.05439460E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.53863661E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.57672638E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.43998586E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.85371922E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.50904926E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.33469886E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.17191894E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.82197428E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.34364369E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.02073478E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.22530285E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.62419568E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.89675499E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.30436578E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.97282217E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89071245E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.43994860E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.15956300E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.60147259E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80375444E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.16932941E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.68282225E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.33889555E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.02029097E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.29821559E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.17826660E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     7.34618285E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.05439460E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.53863661E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.57672638E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.43998586E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.85371922E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.50904926E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.33469886E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.17191894E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.82197428E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.34364369E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.02073478E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.22530285E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.62419568E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.89675499E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.30436578E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.97282217E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89071245E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.97058135E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.52937049E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.33654662E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.06644130E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.68600672E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.54826190E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.37911895E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.26448545E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.14624744E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.73556168E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.85357538E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.62757391E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.97058135E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.52937049E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.33654662E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.06644130E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.68600672E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.54826190E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.37911895E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.26448545E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.14624744E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.73556168E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.85357538E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.62757391E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62156711E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.78731449E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47918236E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.66953437E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.50165396E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.83560717E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.00728981E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.37210807E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.62267642E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.62889052E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.88930271E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.73585793E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.54130740E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.89497229E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.08666535E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.97058859E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.11742235E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.29756555E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.93525225E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69103196E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.27974603E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.37499325E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.97058859E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.11742235E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.29756555E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.93525225E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69103196E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.27974603E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.37499325E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.97352707E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11631810E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.29529506E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.92741053E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.68837264E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.26473208E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.36969135E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.10623713E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.40768689E-03    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.32036814E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.30388384E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10679073E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10675984E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09812059E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.55729768E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53025010E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.13650278E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46354400E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.33021790E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53940644E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.87797626E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     5.55089332E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00992340E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.87916361E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.10912990E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.25607367E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.35692413E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.59832620E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.19175505E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.31944722E-03    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18010385E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52797085E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17205063E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52775269E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38954530E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.48923165E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.48910579E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.45370194E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.96793618E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.96793618E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.96793618E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     8.62161800E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     8.62161800E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.12672283E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.12672283E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.87387286E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.87387286E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.85745275E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.85745275E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.53728139E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.53728139E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.29030325E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.61561405E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.31810742E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.00394233E-03    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.59951992E-03    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.28727603E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18398341E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.42226041E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.39091663E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.42613567E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.14949019E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.55139848E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.55136891E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.68051507E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.90798953E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.90798953E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.90798953E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.50141248E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.23877591E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.47813151E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.23811791E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.82613803E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.39600075E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.39564808E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.29615789E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.47695765E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.47695765E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.47695765E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31135286E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31135286E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30420471E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30420471E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.37116934E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.37116934E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.37103534E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.37103534E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.33358243E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.33358243E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.55733534E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.07446959E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.50710114E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.24634998E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46338337E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46338337E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.16091720E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.11529021E-07    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.29073812E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.35901544E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89038547E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.98675311E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.14722362E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     5.45805025E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.16560454E-12    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08446009E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17631715E-01    2           5        -5   # BR(h -> b       bb     )
     6.37680259E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25715326E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78311106E-04    2           3        -3   # BR(h -> s       sb     )
     2.06352390E-02    2           4        -4   # BR(h -> c       cb     )
     6.70033483E-02    2          21        21   # BR(h -> g       g      )
     2.30529516E-03    2          22        22   # BR(h -> gam     gam    )
     1.53793691E-03    2          22        23   # BR(h -> Z       gam    )
     2.00789569E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56248434E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78119862E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47200198E-03    2           5        -5   # BR(H -> b       bb     )
     2.46426093E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71207984E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11545748E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667572E-05    2           4        -4   # BR(H -> c       cb     )
     9.96061557E-01    2           6        -6   # BR(H -> t       tb     )
     7.97645451E-04    2          21        21   # BR(H -> g       g      )
     2.70399099E-06    2          22        22   # BR(H -> gam     gam    )
     1.15984450E-06    2          23        22   # BR(H -> Z       gam    )
     3.35119090E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67102541E-04    2          23        23   # BR(H -> Z       Z      )
     9.04230785E-04    2          25        25   # BR(H -> h       h      )
     8.17932312E-24    2          36        36   # BR(H -> A       A      )
     3.30876845E-11    2          23        36   # BR(H -> Z       A      )
     7.12848613E-12    2          24       -37   # BR(H -> W+      H-     )
     7.12848613E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382901E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47490827E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897317E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62265124E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677208E-06    2           3        -3   # BR(A -> s       sb     )
     9.96174838E-06    2           4        -4   # BR(A -> c       cb     )
     9.96994345E-01    2           6        -6   # BR(A -> t       tb     )
     9.43674089E-04    2          21        21   # BR(A -> g       g      )
     3.25324954E-06    2          22        22   # BR(A -> gam     gam    )
     1.35242965E-06    2          23        22   # BR(A -> Z       gam    )
     3.26608455E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74484481E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36619893E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237739E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81145441E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50437872E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45692164E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731232E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99401812E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34760044E-04    2          24        25   # BR(H+ -> W+      h      )
     3.41668908E-13    2          24        36   # BR(H+ -> W+      A      )
