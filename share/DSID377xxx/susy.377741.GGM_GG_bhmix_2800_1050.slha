#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.04500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.05000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05405902E+01   # W+
        25     1.25000000E+02   # h
        35     2.00409967E+03   # H
        36     2.00000000E+03   # A
        37     2.00158357E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013294E+03   # ~d_L
   2000001     5.00002536E+03   # ~d_R
   1000002     4.99989241E+03   # ~u_L
   2000002     4.99994929E+03   # ~u_R
   1000003     5.00013294E+03   # ~s_L
   2000003     5.00002536E+03   # ~s_R
   1000004     4.99989241E+03   # ~c_L
   2000004     4.99994929E+03   # ~c_R
   1000005     4.99965335E+03   # ~b_1
   2000005     5.00050635E+03   # ~b_2
   1000006     4.99157817E+03   # ~t_1
   2000006     5.01286981E+03   # ~t_2
   1000011     5.00008223E+03   # ~e_L
   2000011     5.00007607E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008223E+03   # ~mu_L
   2000013     5.00007607E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99979938E+03   # ~tau_1
   2000015     5.00035953E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     2.80000000E+03   # ~g
   1000022     1.00239657E+03   # ~chi_10
   1000023    -1.05009581E+03   # ~chi_20
   1000025     1.08955956E+03   # ~chi_30
   1000035     3.00313967E+03   # ~chi_40
   1000024     1.04698056E+03   # ~chi_1+
   1000037     3.00313822E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.14711542E-01   # N_11
  1  2    -2.71048510E-02   # N_12
  1  3     4.96479769E-01   # N_13
  1  4    -4.91894885E-01   # N_14
  2  1     4.16759841E-03   # N_21
  2  2    -3.83358778E-03   # N_22
  2  3    -7.06934140E-01   # N_23
  2  4    -7.07256712E-01   # N_24
  3  1     6.99406342E-01   # N_31
  3  2     2.90080107E-02   # N_32
  3  3    -5.03099176E-01   # N_33
  3  4     5.06833823E-01   # N_34
  4  1     9.00976870E-04   # N_41
  4  2    -9.99204266E-01   # N_42
  4  3    -2.53610030E-02   # N_43
  4  4     3.07707946E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.58460654E-02   # U_11
  1  2     9.99357323E-01   # U_12
  2  1     9.99357323E-01   # U_21
  2  2     3.58460654E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.34986130E-02   # V_11
  1  2     9.99053487E-01   # V_12
  2  1     9.99053487E-01   # V_21
  2  2     4.34986130E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.08050135E-01   # cos(theta_t)
  1  2     7.06162167E-01   # sin(theta_t)
  2  1    -7.06162167E-01   # -sin(theta_t)
  2  2     7.08050135E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.61012548E-01   # cos(theta_b)
  1  2     7.50374847E-01   # sin(theta_b)
  2  1    -7.50374847E-01   # -sin(theta_b)
  2  2     6.61012548E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03207038E-01   # cos(theta_tau)
  1  2     7.10985135E-01   # sin(theta_tau)
  2  1    -7.10985135E-01   # -sin(theta_tau)
  2  2     7.03207038E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90204802E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.05000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52099672E+02   # vev(Q)              
         4     3.45866573E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52814340E-01   # gprime(Q) DRbar
     2     6.27374967E-01   # g(Q) DRbar
     3     1.07511151E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02580724E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71673101E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79827199E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.04500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.81665462E+06   # M^2_Hd              
        22    -6.28787973E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37137941E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.14453050E-02   # gluino decays
#          BR         NDA      ID1       ID2
     2.91366316E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     5.77312740E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.84482113E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.39719896E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.36161642E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     3.08294639E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.34349364E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.50685335E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.17075437E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     4.39719896E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.36161642E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     3.08294639E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.34349364E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.50685335E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.17075437E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.47347283E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.68907022E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.17249852E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.12238006E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.08359165E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.26052183E-01    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     1.32335377E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.32335377E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.32335377E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.32335377E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.25457956E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.25457956E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.49408940E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.02206473E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.33407716E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.17189458E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.37818057E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.38860294E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.73764283E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     5.32648337E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.36046691E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.82648794E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.36225595E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.09084018E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.85514611E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.30788309E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.68993446E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.20186394E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.62333452E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.01468589E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.18866659E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.96057643E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.81925696E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.45622479E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.65975477E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     7.88493275E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.85250778E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.10872593E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.45719804E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.94261969E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.57953605E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.25027351E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.19157509E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.90064472E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.51780071E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.30248160E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.05395789E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.88001862E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.08970908E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     4.28320829E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.01797983E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.42693052E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.37568780E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.61347098E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.21882938E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.40882231E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.56794053E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.29775823E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.51787126E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.95897823E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.40918699E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.17106430E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.09370941E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.90874948E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.01871069E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.42768510E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.30328836E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.53568284E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.21640146E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.99563442E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.77681718E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.81468354E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.51780071E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.30248160E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.05395789E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.88001862E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.08970908E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     4.28320829E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.01797983E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.42693052E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.37568780E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.61347098E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.21882938E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.40882231E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.56794053E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.29775823E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.51787126E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.95897823E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.40918699E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.17106430E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.09370941E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.90874948E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.01871069E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.42768510E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.30328836E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.53568284E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.21640146E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.99563442E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.77681718E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.81468354E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.97060168E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.52930025E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.33660847E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.06643980E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.68600936E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.54830517E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.37912440E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.26449039E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.14624189E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.73559857E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.85358092E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.62774773E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.97060168E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.52930025E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.33660847E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.06643980E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.68600936E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.54830517E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.37912440E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.26449039E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.14624189E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.73559857E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.85358092E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.62774773E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62157908E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.78730565E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47904590E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.66953514E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.50165662E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.83562299E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.00729522E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.37210180E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.62268956E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.62887694E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.88916483E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.73585167E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.54131399E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.89594183E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.08667864E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.97060894E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.11741837E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.29762010E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.93520486E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69103470E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.27980977E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.37499858E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.97060894E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.11741837E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.29762010E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.93520486E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69103470E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.27980977E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.37499858E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.97354736E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11631416E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.29534962E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.92736340E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.68837546E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.26476935E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.36969682E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.10631223E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.40724574E-03    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.32036945E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.30388546E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10679116E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10676028E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09812119E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.55739664E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53026454E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.13651169E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46356169E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.33022676E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53935653E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.87775741E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     5.55075938E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00992478E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.87916355E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.10911669E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.25616430E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.35686858E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.59814819E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.19159722E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.31906635E-03    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18010493E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52797151E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17205184E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52775336E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38954851E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.48922977E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.48910391E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.45370061E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.96793013E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.96793013E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.96793013E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     8.62237942E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     8.62237942E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.12719533E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.12719533E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.87412667E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.87412667E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.85770605E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.85770605E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.53837171E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.53837171E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.29047569E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.61515032E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.31811162E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.00373110E-03    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.59932284E-03    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.28664468E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18418317E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.42251407E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.39111156E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.42638927E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.14863638E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.55193677E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.55190718E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.68103233E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.90915693E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.90915693E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.90915693E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.50139354E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.23874981E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.47811312E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.23809184E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.82612278E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.39593393E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.39558128E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.29609356E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.47694383E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.47694383E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.47694383E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31135631E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31135631E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30420831E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30420831E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.37118083E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.37118083E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.37104684E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.37104684E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.33359472E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.33359472E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.55743428E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.07447629E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.50711546E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.24635620E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46340108E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46340108E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.16089532E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.12118657E-07    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.29066347E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.35898819E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89032462E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.98659574E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.14717623E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     5.45806815E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.16560022E-12    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08602285E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17777752E-01    2           5        -5   # BR(h -> b       bb     )
     6.37438767E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25629847E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78129934E-04    2           3        -3   # BR(h -> s       sb     )
     2.06273119E-02    2           4        -4   # BR(h -> c       cb     )
     6.69775884E-02    2          21        21   # BR(h -> g       g      )
     2.30441476E-03    2          22        22   # BR(h -> gam     gam    )
     1.53734861E-03    2          22        23   # BR(h -> Z       gam    )
     2.00712905E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56150426E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78120363E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46944603E-03    2           5        -5   # BR(H -> b       bb     )
     2.46425228E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71204927E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11545361E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667774E-05    2           4        -4   # BR(H -> c       cb     )
     9.96063531E-01    2           6        -6   # BR(H -> t       tb     )
     7.97647407E-04    2          21        21   # BR(H -> g       g      )
     2.70400566E-06    2          22        22   # BR(H -> gam     gam    )
     1.15984093E-06    2          23        22   # BR(H -> Z       gam    )
     3.35501965E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67293456E-04    2          23        23   # BR(H -> Z       Z      )
     9.04237286E-04    2          25        25   # BR(H -> h       h      )
     8.24313894E-24    2          36        36   # BR(H -> A       A      )
     3.30477214E-11    2          23        36   # BR(H -> Z       A      )
     7.01165512E-12    2          24       -37   # BR(H -> W+      H-     )
     7.01165512E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382077E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47238106E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897843E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62266983E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677453E-06    2           3        -3   # BR(A -> s       sb     )
     9.96176986E-06    2           4        -4   # BR(A -> c       cb     )
     9.96996495E-01    2           6        -6   # BR(A -> t       tb     )
     9.43676124E-04    2          21        21   # BR(A -> g       g      )
     3.25325893E-06    2          22        22   # BR(A -> gam     gam    )
     1.35243105E-06    2          23        22   # BR(A -> Z       gam    )
     3.26983233E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74484553E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36070591E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238605E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81148504E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50086317E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693790E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731555E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99401430E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.35147118E-04    2          24        25   # BR(H+ -> W+      h      )
     3.49697183E-13    2          24        36   # BR(H+ -> W+      A      )
