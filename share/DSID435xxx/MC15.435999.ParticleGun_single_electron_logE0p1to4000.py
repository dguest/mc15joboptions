evgenConfig.description = "Single Photons with flat eta-phi and log E in [0.1, 4000] GeV"
evgenConfig.keywords = ["singleParticle", "electron"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-11,11)
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(100., 4000000.), eta=[-5.0, 5.0])
