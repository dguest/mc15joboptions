#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------

include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 125.
PowhegConfig.width_H = 0.00407

# CPS for the SM Higgs
PowhegConfig.complexpolescheme = 1

#
PowhegConfig.withdamp = 1

# Increase number of events requested to compensate for filter efficiency
#PowhegConfig.nEvents *= 4.

# Generate Powheg events
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]
#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:oneChannel = 1 0.5   100 15 -13',
                             '25:addChannel = 1 0.5   100 13 -15'
]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "VBF H->taumu"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->taumu"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "VBF", "mH125" ]
evgenConfig.contact     = [ 'Luca.Fiorini@cern.ch' ]
evgenConfig.generators  = [ "Powheg", "Pythia8", "EvtGen"] 
