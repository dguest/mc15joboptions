include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")

evgenConfig.description = "5FS PP-> bA+X with A-> b ~b with up to 4 jets in ME+PS"
evgenConfig.keywords = [ "BSMHiggs", "bHiggs","bbbar"]
evgenConfig.contact  = [ "timb@slac.stanford.edu" ] 
evgenConfig.inputconfcheck = "bhbb" 

evgenConfig.process="""
(run){
  MASS[25]=0125.; WIDTH[25]=0.;
  YUKAWA[5] 4.8;
  PARTICLE_CONTAINER 98 B 5 -5;
  CSS_IS_AS_FAC 1;
  HARD_DECAYS On;
  HDH_STATUS[25,5,-5]=2;
  HDH_BR_WEIGHTS=1;
}(run);

(processes){
  Process 93 93 -> 25 98 93{1};
  CKKW sqr(20.0/E_CMS);
  Order (*,1);
  End process;
}(processes);

(selector){
  PT 98 1 E_CMS;
}(selector);
"""

