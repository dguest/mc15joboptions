evgenConfig.description = "PYTHIA8, WH, W->any, H->UpsilonGamma"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WHiggs", "mH125" ]
evgenConfig.contact     = [ 'J.Broughton@cern.ch' ]
evgenConfig.minevents = 5000

#Higgs mass (in GeV)
H_Mass = 125.0

#Higgs width (in GeV)
H_Width = 0.00407

include( "MC15JobOptions/nonStandard/Pythia8_A14_NNPDF23LO_Common.py" )

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:onMode = off',
                             '25:doForceWidth = true',
                             'HiggsSM:ffbar2HW = on',
                             '24:onMode = off',
                             '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                             ]

genSeq.Pythia8.UserModes += [ 'WeakZ0:gmZmode = 2']
genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:addChannel = 1 0.002 100 553  22',
                             '25:addChannel = 1 0.002 100 100553  22',
                             '25:addChannel = 1 0.002 100 200553  22',
                             '553:onMode = off',
                             '553:onIfMatch = 13 -13',
                             '100553:onMode = off',
                             '100553:onIfMatch = 13 -13',
                             '200553:onMode = off',
                             '200553:onIfMatch = 13 -13'
                             ]
