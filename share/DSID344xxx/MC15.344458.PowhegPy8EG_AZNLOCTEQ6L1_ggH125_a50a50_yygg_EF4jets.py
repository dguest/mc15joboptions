#--------------------------------------------------------------
# Powheg ggH_quark_mass_effects setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggF_H_Common.py')

H_Mass = 125.0
H_Width = 0.00407

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = H_Mass
PowhegConfig.width_H = H_Width

# Turn on the heavy quark effect
PowhegConfig.use_massive_b = True
PowhegConfig.use_massive_c = True

# Complex pole scheme or not (1 for NWA and 3(CPS) for SM)
PowhegConfig.bwshape = 3

# Dynamical scale (sqrt(pT(H)^2+mH^2) real emission contributions)
# Note: r2330 does not support this option. please use newer versions.
PowhegConfig.runningscale = 2

# EW correction
if PowhegConfig.mass_H <= 1000.:
  PowhegConfig.ew = 1
else:
  PowhegConfig.ew = 0

# Set scaling and masswindow parameters
hfact_scale    = 1.2
masswindow_max = 30.

# Calculate an appropriate masswindow and hfact
masswindow = masswindow_max
if PowhegConfig.mass_H <= 700.:
  masswindow = min( (1799.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
else:
  masswindow = min( (1999.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
PowhegConfig.masswindow = masswindow
PowhegConfig.hfact = PowhegConfig.mass_H / hfact_scale

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 10.

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

import os, sys
infile = 'PowhegOTF._1.events'
f1 = open( infile )
newfile = infile+'.temp'
f2 = open(newfile,'w')
for line in f1:
    if line.startswith('      25     1'):
        f2.write(line.replace('      25     1','      35     1'))
    else:
        f2.write(line)
f1.close()
f2.close()
os.system('mv %s %s '%(infile, infile+'.old') )
os.system('mv %s %s '%(newfile, infile) )


#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 1' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 
                             'Higgs:useBSM = on',

                             '35:m0 = '+str(H_Mass),
                             '35:mWidth = '+str(H_Width),
                             '35:doForceWidth = on',
                             '35:onMode = off',
                             '35:onIfMatch = 36 36', # h->aa

                             '36:onMode = off', # decay of the a
                             '36:oneChannel = 1 0.5 100 22 22', #a->gamgam
                             '36:addChannel = 1 0.5 100 21 21', #a->gg
                             '36:m0 50.0', #scalar mass
                             '36:mMin 49.5', #scalar mass
                             '36:mMax 50.5', #scalar mass
                             '36:mWidth 0.01', # narrow width
                             '36:tau0 0', #scalarlife time
                             ]

#--------------------------------------------------------------
# Filter for VBF analysis
#--------------------------------------------------------------
include("MC15JobOptions/QCDTruthMultiJetFilter.py")
filtSeq.QCDTruthMultiJetFilter.Njet = 4
filtSeq.QCDTruthMultiJetFilter.NjetMinPt = 20.*GeV #
filtSeq.QCDTruthMultiJetFilter.MinLeadJetPt = 20.*GeV #
filtSeq.QCDTruthMultiJetFilter.MaxLeadJetPt = 7000.*GeV # LHC kinematic limit...
filtSeq.QCDTruthMultiJetFilter.MaxEta = 5.0
filtSeq.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.QCDTruthMultiJetFilter.DoShape = False

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->aa->yygg"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125"]
evgenConfig.contact     = [ 'rubbo@cern.ch' ]

if not hasattr(genSeq, "XtoVVDecayFilter"):
  from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilter
  genSeq += XtoVVDecayFilter()
  
### Add this filter to the algs required to be successful for streaming
#if "XtoVVDecayFilter" not in StreamEVGEN.RequireAlgs:
#    StreamEVGEN.RequireAlgs += ["XtoVVDecayFilter"]
genSeq.XtoVVDecayFilter.PDGGrandParent = 35
genSeq.XtoVVDecayFilter.PDGParent = 36
genSeq.XtoVVDecayFilter.StatusParent = 22
genSeq.XtoVVDecayFilter.PDGChild1 = [22]
genSeq.XtoVVDecayFilter.PDGChild2 = [21]
