#--------------------------------------------------------------
# Powheg Zj setup starting from ATLAS defaults
#--------------------------------------------------------------
include("PowhegControl/PowhegControl_Zjj_Common.py")
PowhegConfig.nEvents *=	30
PowhegConfig.vdecaymodeZ = 13
PowhegConfig.itmx1    = 1
PowhegConfig.itmx2    = 2
PowhegConfig.ncall1   = 6300000
PowhegConfig.ncall2   = 2520000
PowhegConfig.nubound  = 12600000
PowhegConfig.xupbound = 4
PowhegConfig.foldx    = 5
PowhegConfig.foldy    = 5
PowhegConfig.foldphi  = 5
PowhegConfig.ptborncut = 20.0
PowhegConfig.mllmin    = 60.0
PowhegConfig.PDF = 11000
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the AZNLO CTEQ6L1 Var1Down tune
#--------------------------------------------------------------
include("MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_Var1Down_EvtGen_Common.py")
genSeq.Pythia8.UserModes += ['Main31:NFinal = 3']
include("MC15JobOptions/Pythia8_Powheg.py")

#--------------------------------------------------------------
# VBF Filter:
#--------------------------------------------------------------
include('MC15JobOptions/VBFForwardJetsFilter.py')
filtSeq.VBFForwardJetsFilter.JetMinPt = 20.0*GeV
filtSeq.VBFForwardJetsFilter.JetMaxEta = 4.4
filtSeq.VBFForwardJetsFilter.NJets = 2
filtSeq.VBFForwardJetsFilter.Jet1MinPt = 20.0*GeV
filtSeq.VBFForwardJetsFilter.Jet1MaxEta = 4.4
filtSeq.VBFForwardJetsFilter.Jet2MinPt = 20.0*GeV
filtSeq.VBFForwardJetsFilter.Jet2MaxEta = 4.4
filtSeq.VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2 = False
filtSeq.VBFForwardJetsFilter.MassJJ = 200.0*GeV
filtSeq.VBFForwardJetsFilter.DeltaEtaJJ = 0.0
filtSeq.VBFForwardJetsFilter.UseLeadingJJ = True
filtSeq.VBFForwardJetsFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.VBFForwardJetsFilter.LGMinPt = 1*GeV
filtSeq.VBFForwardJetsFilter.LGMaxEta = 5.0
filtSeq.VBFForwardJetsFilter.DeltaRJLG = 0.0

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 Z+jj production with AZNLO CTEQ6L1 Var1Down tune"
evgenConfig.keywords    = [ "SM", "Z", "2jet" ]
evgenConfig.contact     = [ "christian.johnson@cern.ch" ]
evgenConfig.inputconfcheck = "QCDZjjmm_Var1Down"
evgenConfig.generators  = [ "Powheg", "Pythia8", "EvtGen"]
evgenConfig.minevents   = 50
