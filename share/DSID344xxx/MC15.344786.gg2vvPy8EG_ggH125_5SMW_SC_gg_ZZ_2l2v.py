################################################################################
#
# gg2VV 3.1.6 / Pythia8 
# gg -> (H* ->) ZZ -> 2(e/mu)2nu_(mu,tau/e,tau) (signal/background/interference)
#
# generator cuts on LHE level:
# pt(l1) > 2.GeV ; |eta(l1)| < 5.5
# pt(l2) > 2.GeV ; |eta(l2)| < 5.5
# 40. < m(l1l2)/GeV < 140. ; pt(l1l2) > 2.GeV
# scale : m(ZZ)/2 ; PDF set : CT10nnlo.LHgrid
#
# wH = .020440 GeV (5x SM); couplings scaled accordingly
#

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += [ 'gg2vv', 'Pythia8' ]
evgenConfig.description = 'gg2VV, (H*->)ZZ->2l2v (5x SM width, coupling scaled), CT10nnlo PDF , Pythia8 , generator cuts see shower job options , sig/bkg/int'
evgenConfig.keywords = ['diboson', '2lepton', 'electroweak', 'Higgs', 'ZZ']
evgenConfig.contact = ['jochen.meyer@cern.ch']
evgenConfig.inputfilecheck = 'ggH125_5SMW_SC_gg_ZZ_2l2v'

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_LHEF.py")

# boson decays already done in the lhe file
genSeq.Pythia8.Commands += [ '25:onMode = off' ]
genSeq.Pythia8.Commands += [ '24:onMode = off' ]
genSeq.Pythia8.Commands += [ '23:onMode = off' ]

# no power shower, just wimpy showers
genSeq.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 1' ]
