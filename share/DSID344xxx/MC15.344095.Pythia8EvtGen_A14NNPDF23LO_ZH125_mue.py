evgenConfig.description = "PYTHIA8+EVTGEN, ZH, Z->any, H->taue"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125", "ZHiggs", "inclusive" ]
evgenConfig.contact     = [ 'Luca.Fiorini@cern.ch' ]
evgenConfig.generators  = [ "Pythia8", "EvtGen"] 

#Higgs mass (in GeV)
H_Mass = 125.0

#Higgs width (in GeV)
H_Width = 0.00407

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += [ 'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             '25:doForceWidth = true',
                             'HiggsSM:ffbar2HZ = on',
                             '23:onMode = off',
                             '23:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                             ]


genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:oneChannel = 1 0.5   100 13 -11',
                             '25:addChannel = 1 0.5   100 11 -13'
]

