#--------------------------------------------------------------
# Powheg VBF Z setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_Z_Common.py')
PowhegConfig.decay_mode = "Zmumu"
PowhegConfig.itmx1    = 4
PowhegConfig.itmx2    = 6
PowhegConfig.ncall1   = 1000000
PowhegConfig.ncall2   = 50000
PowhegConfig.nubound  = 100000
PowhegConfig.xupbound = 4
PowhegConfig.foldx    = 5
PowhegConfig.foldy    = 5
PowhegConfig.foldphi  = 5
PowhegConfig.mll_gencut = 60
PowhegConfig.ptj_gencut = 20
PowhegConfig.PDF = 11000
PowhegConfig.mu_F = 2.0
PowhegConfig.mu_R = 1.0
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the AZNLO CTEQ6L1 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
genSeq.Pythia8.UserModes += ['Main31:NFinal = 3']
include('MC15JobOptions/Pythia8_Powheg.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 VBF Z production with AZNLO CTEQ6L1 tune'
evgenConfig.keywords    = [ 'SM', 'VBF', 'Z' ]
evgenConfig.contact     = [ 'christian.johnson@cern.ch' ]
evgenConfig.inputconfcheck = "VBFZmumu_MURnMUFu"
evgenConfig.generators  = [ "Powheg", "Pythia8", "EvtGen"] 
