from MadGraphControl.MadGraphUtils import *

mode = 0

cmdsps = """
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;
set /Herwig/Particles/W+/W+->nu_e,e+;:OnOff On
set /Herwig/Particles/W+/W+->nu_mu,mu+;:OnOff On
set /Herwig/Particles/W+/W+->nu_tau,tau+;:OnOff On
set /Herwig/Particles/W+/W+->u,dbar;:OnOff Off
set /Herwig/Particles/W+/W+->c,sbar;:OnOff Off
set /Herwig/Particles/W+/W+->sbar,u;:OnOff Off
set /Herwig/Particles/W+/W+->c,dbar;:OnOff Off
set /Herwig/Particles/W+/W+->bbar,c;:OnOff Off
##set W- decay
set /Herwig/Particles/W-/W-->nu_ebar,e-;:OnOff On
set /Herwig/Particles/W-/W-->nu_mubar,mu-;:OnOff On
set /Herwig/Particles/W-/W-->nu_taubar,tau-;:OnOff On
set /Herwig/Particles/W-/W-->ubar,d;:OnOff Off
set /Herwig/Particles/W-/W-->cbar,s;:OnOff Off
set /Herwig/Particles/W-/W-->s,ubar;:OnOff Off
set /Herwig/Particles/W-/W-->cbar,d;:OnOff Off
set /Herwig/Particles/W-/W-->b,cbar;:OnOff Off
"""

safefactor = 1.5
evgenConfig.description = "h2->h1h1 diHiggs production with MG5_aMC@NLO, h1 -> W+ W-, W+W- -> 4 leptons."
evgenConfig.keywords = ["BSM",  "BSMHiggs", "resonance", "hh", "WW", "4lepton"] 

evgenConfig.contact = ['Jason Veatch <Jason.Veatch@cern.ch>']
evgenConfig.inputconfcheck = 'aMcAtNloHerwigppEvtGen.343670.Xhh_13TeV'

run_number_min = 344866 
run_number_max = 344866
offset = 2

include("MC15JobOptions/MadGraphControl_HerwigppEvtGen_UEEE5_CT10ME_NLO_h2h1h1.py")

