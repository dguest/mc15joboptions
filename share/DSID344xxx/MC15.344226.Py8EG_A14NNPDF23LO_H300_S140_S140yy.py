# DSID config

## Here will go information determining which SLHA file to use
slha_file = 'H300_S140_SS.slha'
mode = 'S_Syy'

from PyJobTransformsCore.trfutil import get_files
get_files(slha_file, keepDir=False, errorIfNotFound=True)

import os

if not os.path.isfile(slha_file):
    raise IOError("Could not find SLHA file " + slha_file)

evgenConfig.auxfiles += [slha_file]
    
# Pythia

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.contact = ['Stefan von Buddenbrock <stef.von.b@cern.ch>']
evgenConfig.description = "Generation of gg > H > SS where one S goes to yy."
evgenConfig.keywords = ["BSMHiggs", "diphoton", "Higgs", "scalar", "invisible"]

genSeq.Pythia8.Commands += ['Higgs:useBSM = on',
                            'ParticleDecays:mSafety = 0.0',
                            'HiggsBSM:gg2A3 = on',
                            'Higgs:clipWings = off',
                            'SLHA:readFrom = 2',
                            'SLHA:file = ' + slha_file,
                            'SLHA:useDecayTable = on',
                            'SLHA:allowUserOverride = on',
                            '36:mWidth = 0.01',
                            '36:doForceWidth = on',
                            '36:addChannel = 1 1 100 35 25',
                            '36:onMode = off',
                            '36:onIfMatch = 35 25',
                            '36:mayDecay = on',
                            '35:mMin = 50.0',
                            '25:mMin = 50.0'
                            ]

if 'Syy' in mode:
    genSeq.Pythia8.Commands += ['35:onMode = off', '35:onIfMatch = 22 22']
elif 'hyy' in mode:
    genSeq.Pythia8.Commands += ['25:onMode = off', '25:onIfMatch = 22 22']
else:
    raise RuntimeError('Invalid mode! Check JOs')



