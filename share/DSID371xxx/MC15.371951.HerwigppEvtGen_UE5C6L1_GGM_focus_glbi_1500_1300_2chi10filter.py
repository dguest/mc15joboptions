include ( 'MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py' )

slha_file = 'susy.371951.gl_bino_focus_1500_1300.slha'

include ( 'MC15JobOptions/Herwigpp_SUSYConfig.py' )
include ( 'MC15JobOptions/Herwigpp_EvtGen.py' )

cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')

evgenConfig.description = 'GGM gluino-bino grid generation with mgl=1500 mbino=1300'
evgenConfig.keywords = ['SUSY', 'gluino', 'bino']
evgenConfig.contact = [ 'osamu.jinnouchi@cern.ch']

genSeq.Herwigpp.Commands += cmds.splitlines()

from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
filtSeq += ParticleFilter("ParticleFilter")
filtSeq.ParticleFilter.Ptcut = 0.
filtSeq.ParticleFilter.Etacut = 10.0
filtSeq.ParticleFilter.StatusReq = 11
filtSeq.ParticleFilter.PDG = 1000022
filtSeq.ParticleFilter.MinParts = 2 

del cmds
