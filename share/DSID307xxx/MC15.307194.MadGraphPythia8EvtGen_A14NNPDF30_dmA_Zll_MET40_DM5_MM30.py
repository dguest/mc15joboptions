Mchi =  5.
Mxi =  30.
gxichi = 1.0 
gxiU = 0.25 
Wxi = 1.39033367871
evgenConfig.description = "Wimp pair monoZ with dmA"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kenji Hamano <kenji.hamano@cern.ch>"]
include("MC15JobOptions/MadGraphControl_Pythia8EvtGen_A14NNPDF30_dmA_Zll_MET40_DM5_MM30.py")
