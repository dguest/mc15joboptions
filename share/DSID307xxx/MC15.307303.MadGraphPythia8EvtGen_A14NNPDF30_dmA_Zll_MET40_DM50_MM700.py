Mchi =  50.
Mxi =  700.
gxichi = 1.0 
gxiU = 0.25 
Wxi = 37.6897935841
evgenConfig.description = "Wimp pair monoZ with dmA"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kenji Hamano <kenji.hamano@cern.ch>"]
include("MC15JobOptions/MadGraphControl_Pythia8EvtGen_A14NNPDF30_dmA_Zll_MET40.py")
