include('MC15JobOptions/MadGraphControl_ZpHA_Y1toHy.py')

evgenConfig.description = "Dark Matter Zprime model LO Spin-1 qq->Y1->Higgs+gamma 900 GeV narrow width resonance in b-bbar decay model"
evgenConfig.keywords = ["exotic", "Higgs", "photon", "LO", "bbbar", "spin1"]
