Mchi =  225.
Mxi =  480.
gxichi = 1.0 
gxiU = 0.25 
Wxi = 13.2591541136
evgenConfig.description = "Wimp pair monoZ with dmA"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kenji Hamano <kenji.hamano@cern.ch>"]
include("MC15JobOptions/MadGraphControl_Pythia8EvtGen_A14NNPDF30_dmA_Zll_MET40.py")
