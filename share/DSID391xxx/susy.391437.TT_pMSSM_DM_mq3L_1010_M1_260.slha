#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14032286E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.03990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.00990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.96485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04162792E+01   # W+
        25     1.25546935E+02   # h
        35     4.00000789E+03   # H
        36     3.99999707E+03   # A
        37     4.00106962E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02861209E+03   # ~d_L
   2000001     4.02468883E+03   # ~d_R
   1000002     4.02795953E+03   # ~u_L
   2000002     4.02542873E+03   # ~u_R
   1000003     4.02861209E+03   # ~s_L
   2000003     4.02468883E+03   # ~s_R
   1000004     4.02795953E+03   # ~c_L
   2000004     4.02542873E+03   # ~c_R
   1000005     1.07377246E+03   # ~b_1
   2000005     4.02685842E+03   # ~b_2
   1000006     1.05546727E+03   # ~t_1
   2000006     1.97848493E+03   # ~t_2
   1000011     4.00513120E+03   # ~e_L
   2000011     4.00349137E+03   # ~e_R
   1000012     4.00401647E+03   # ~nu_eL
   1000013     4.00513120E+03   # ~mu_L
   2000013     4.00349137E+03   # ~mu_R
   1000014     4.00401647E+03   # ~nu_muL
   1000015     4.00474503E+03   # ~tau_1
   2000015     4.00746857E+03   # ~tau_2
   1000016     4.00521260E+03   # ~nu_tauL
   1000021     1.98412253E+03   # ~g
   1000022     2.50338347E+02   # ~chi_10
   1000023    -3.15059155E+02   # ~chi_20
   1000025     3.25082613E+02   # ~chi_30
   1000035     2.05142919E+03   # ~chi_40
   1000024     3.12278000E+02   # ~chi_1+
   1000037     2.05159411E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.83648960E-01   # N_11
  1  2    -1.20186629E-02   # N_12
  1  3    -3.66740435E-01   # N_13
  1  4    -2.90725850E-01   # N_14
  2  1    -5.77793962E-02   # N_21
  2  2     2.49588937E-02   # N_22
  2  3    -7.02218461E-01   # N_23
  2  4     7.09174047E-01   # N_24
  3  1     4.64569784E-01   # N_31
  3  2     2.80857024E-02   # N_32
  3  3     6.10224961E-01   # N_33
  3  4     6.41101869E-01   # N_34
  4  1    -9.86147483E-04   # N_41
  4  2     9.99221596E-01   # N_42
  4  3    -4.02286179E-03   # N_43
  4  4    -3.92306692E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.69458699E-03   # U_11
  1  2     9.99983786E-01   # U_12
  2  1    -9.99983786E-01   # U_21
  2  2     5.69458699E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.54923206E-02   # V_11
  1  2    -9.98459114E-01   # V_12
  2  1    -9.98459114E-01   # V_21
  2  2    -5.54923206E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.90763321E-01   # cos(theta_t)
  1  2    -1.35602514E-01   # sin(theta_t)
  2  1     1.35602514E-01   # -sin(theta_t)
  2  2     9.90763321E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999514E-01   # cos(theta_b)
  1  2    -9.85900484E-04   # sin(theta_b)
  2  1     9.85900484E-04   # -sin(theta_b)
  2  2     9.99999514E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05713960E-01   # cos(theta_tau)
  1  2     7.08496864E-01   # sin(theta_tau)
  2  1    -7.08496864E-01   # -sin(theta_tau)
  2  2    -7.05713960E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00282916E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.40322855E+03  # DRbar Higgs Parameters
         1    -3.03990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43479002E+02   # higgs               
         4     1.60698794E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.40322855E+03  # The gauge couplings
     1     3.62213539E-01   # gprime(Q) DRbar
     2     6.36362445E-01   # g(Q) DRbar
     3     1.02833849E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.40322855E+03  # The trilinear couplings
  1  1     1.60594898E-06   # A_u(Q) DRbar
  2  2     1.60596428E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.40322855E+03  # The trilinear couplings
  1  1     5.90913814E-07   # A_d(Q) DRbar
  2  2     5.90968581E-07   # A_s(Q) DRbar
  3  3     1.05627250E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.40322855E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.28414168E-07   # A_mu(Q) DRbar
  3  3     1.29736116E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.40322855E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65146592E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.40322855E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.82595449E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.40322855E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03599509E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.40322855E+03  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58030032E+07   # M^2_Hd              
        22    -4.63785386E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.00990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.96485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40497131E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.31366546E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40264645E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40264645E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59735355E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59735355E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.36686874E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     7.42342193E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.33161583E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.87718381E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.04885816E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.12575146E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.31296642E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.21948607E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.01191833E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.38885973E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.54039199E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.11813410E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.18991313E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.40565586E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.32160163E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.87193120E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.69468837E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.91117788E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66081034E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54096537E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80267267E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.64670280E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.08982961E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63634097E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.20200622E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13293491E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     9.61448065E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.27053516E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.12741137E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.94029437E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78260701E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.10871603E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.69806360E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.17010483E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82866751E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.33962383E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.64513569E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51432204E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60481734E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.34606941E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.85018226E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.19519767E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.00554129E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44402281E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78280863E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.81052801E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11855453E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.12432877E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83361685E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.62332935E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.67728081E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51650584E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53725877E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.13405151E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.82780756E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.11871045E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.84060550E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85492489E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78260701E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.10871603E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.69806360E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.17010483E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82866751E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.33962383E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.64513569E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51432204E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60481734E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.34606941E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.85018226E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.19519767E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.00554129E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44402281E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78280863E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.81052801E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11855453E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.12432877E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83361685E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.62332935E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.67728081E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51650584E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53725877E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.13405151E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.82780756E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.11871045E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.84060550E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85492489E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15325584E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.22359914E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.17678501E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.32040651E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77654101E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.27811930E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56717371E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07116210E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.81755609E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.32708840E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.14916769E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.33637885E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15325584E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.22359914E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.17678501E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.32040651E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77654101E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.27811930E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56717371E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07116210E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.81755609E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.32708840E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.14916769E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.33637885E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10469594E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.81993146E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35028724E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.94671834E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39897152E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46346419E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80505004E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10196030E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.92804253E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.24640914E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.66266170E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42394704E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05602437E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85510537E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15430735E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.34552021E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.68990271E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.81869289E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77998342E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11098140E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54461824E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15430735E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.34552021E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.68990271E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.81869289E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77998342E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11098140E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54461824E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.48266400E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.21903004E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53104192E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.55372043E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51970072E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.65094622E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02549215E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.87631172E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33472687E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33472687E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11158770E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11158770E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10737086E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.70615474E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.41405265E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.18099958E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.23714763E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.50451769E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.21791568E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.30196644E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.04130502E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.42111138E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.55972811E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.55483597E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17571649E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52519789E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17571649E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52519789E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.44834353E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49804542E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49804542E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47475851E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99391423E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99391423E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99391406E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.52942730E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.52942730E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.52942730E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.52942730E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.43143368E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.43143368E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.43143368E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.43143368E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     9.80929315E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     9.80929315E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.94285354E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.07661997E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.28949590E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.10681237E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.43264261E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.10681237E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.43264261E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.38718519E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.26031120E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.26031120E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.25122261E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.58097108E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.58097108E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.58096520E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.58389424E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.94670389E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.58389424E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.94670389E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.60633718E-06    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.36403225E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.36403225E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.12488346E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.72671859E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.72671859E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.72671861E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     5.26388036E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     5.26388036E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     5.26388036E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     5.26388036E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.75461155E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.75461155E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.75461155E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.75461155E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.59739916E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.59739916E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.70497190E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.55521286E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.13488510E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.81398361E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.28844867E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.28844867E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.86265517E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.34178059E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.37038232E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.66046253E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.66046253E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.66555168E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.66555168E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.03395748E-03   # h decays
#          BR         NDA      ID1       ID2
     5.93386328E-01    2           5        -5   # BR(h -> b       bb     )
     6.45967816E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28672118E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84943410E-04    2           3        -3   # BR(h -> s       sb     )
     2.10604355E-02    2           4        -4   # BR(h -> c       cb     )
     6.87921603E-02    2          21        21   # BR(h -> g       g      )
     2.37778775E-03    2          22        22   # BR(h -> gam     gam    )
     1.62712547E-03    2          22        23   # BR(h -> Z       gam    )
     2.19673553E-01    2          24       -24   # BR(h -> W+      W-     )
     2.77722131E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.47726279E+01   # H decays
#          BR         NDA      ID1       ID2
     3.50823285E-01    2           5        -5   # BR(H -> b       bb     )
     6.05338937E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14033186E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53164348E-04    2           3        -3   # BR(H -> s       sb     )
     7.10120702E-08    2           4        -4   # BR(H -> c       cb     )
     7.11688794E-03    2           6        -6   # BR(H -> t       tb     )
     8.20270695E-07    2          21        21   # BR(H -> g       g      )
     7.81648906E-10    2          22        22   # BR(H -> gam     gam    )
     1.82822379E-09    2          23        22   # BR(H -> Z       gam    )
     1.86877858E-06    2          24       -24   # BR(H -> W+      W-     )
     9.33741884E-07    2          23        23   # BR(H -> Z       Z      )
     7.26572534E-06    2          25        25   # BR(H -> h       h      )
     2.26449338E-24    2          36        36   # BR(H -> A       A      )
    -1.38729487E-20    2          23        36   # BR(H -> Z       A      )
     1.85380769E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.62254467E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.62254467E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.06039253E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.25612003E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.18922886E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.78455960E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.54603452E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.16122955E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.01912468E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.10483607E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.01037007E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.11010041E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.81021914E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     2.33621149E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.33621149E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.26368496E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.47675393E+01   # A decays
#          BR         NDA      ID1       ID2
     3.50881538E-01    2           5        -5   # BR(A -> b       bb     )
     6.05398255E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14053991E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53230262E-04    2           3        -3   # BR(A -> s       sb     )
     7.14732298E-08    2           4        -4   # BR(A -> c       cb     )
     7.13073727E-03    2           6        -6   # BR(A -> t       tb     )
     1.46518595E-05    2          21        21   # BR(A -> g       g      )
     5.67425179E-08    2          22        22   # BR(A -> gam     gam    )
     1.61037579E-08    2          23        22   # BR(A -> Z       gam    )
     1.86502402E-06    2          23        25   # BR(A -> Z       h      )
     1.86174311E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.62268903E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.62268903E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.78612623E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.03654946E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.89776356E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.42037737E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.25411954E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.21935985E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.12841859E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.94187418E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.10959365E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.51569870E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.51569870E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.48099337E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.62107113E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.05092196E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13945776E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.59748244E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21204126E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49355863E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.58073064E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.86573381E-06    2          24        25   # BR(H+ -> W+      h      )
     3.40763986E-14    2          24        36   # BR(H+ -> W+      A      )
     6.51346585E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.88703580E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.22248656E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.62359496E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.38392383E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60434827E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.21316936E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.14081787E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     4.84773278E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
