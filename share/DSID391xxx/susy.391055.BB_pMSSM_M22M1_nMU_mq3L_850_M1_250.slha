#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15954603E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04000773E+01   # W+
        25     1.25050992E+02   # h
        35     3.00005336E+03   # H
        36     2.99999991E+03   # A
        37     3.00108971E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03875784E+03   # ~d_L
   2000001     3.03358148E+03   # ~d_R
   1000002     3.03784918E+03   # ~u_L
   2000002     3.03518213E+03   # ~u_R
   1000003     3.03875784E+03   # ~s_L
   2000003     3.03358148E+03   # ~s_R
   1000004     3.03784918E+03   # ~c_L
   2000004     3.03518213E+03   # ~c_R
   1000005     9.48023820E+02   # ~b_1
   2000005     3.03163027E+03   # ~b_2
   1000006     9.45768480E+02   # ~t_1
   2000006     3.02376140E+03   # ~t_2
   1000011     3.00653293E+03   # ~e_L
   2000011     3.00154674E+03   # ~e_R
   1000012     3.00514403E+03   # ~nu_eL
   1000013     3.00653293E+03   # ~mu_L
   2000013     3.00154674E+03   # ~mu_R
   1000014     3.00514403E+03   # ~nu_muL
   1000015     2.98610258E+03   # ~tau_1
   2000015     3.02173333E+03   # ~tau_2
   1000016     3.00508753E+03   # ~nu_tauL
   1000021     2.34803865E+03   # ~g
   1000022     2.51764468E+02   # ~chi_10
   1000023     5.30474146E+02   # ~chi_20
   1000025    -2.99694878E+03   # ~chi_30
   1000035     2.99713246E+03   # ~chi_40
   1000024     5.30637400E+02   # ~chi_1+
   1000037     2.99798080E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891061E-01   # N_11
  1  2     7.59039705E-05   # N_12
  1  3    -1.47518212E-02   # N_13
  1  4    -4.93343534E-04   # N_14
  2  1     3.15126743E-04   # N_21
  2  2     9.99646609E-01   # N_22
  2  3     2.63993426E-02   # N_23
  2  4     3.10367510E-03   # N_24
  3  1    -1.00806528E-02   # N_31
  3  2     1.64744223E-02   # N_32
  3  3    -7.06838349E-01   # N_33
  3  4     7.07111393E-01   # N_34
  4  1     1.07771292E-02   # N_41
  4  2    -2.08625272E-02   # N_42
  4  3     7.06728383E-01   # N_43
  4  4     7.07095186E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99303069E-01   # U_11
  1  2     3.73279489E-02   # U_12
  2  1    -3.73279489E-02   # U_21
  2  2     9.99303069E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990376E-01   # V_11
  1  2    -4.38724382E-03   # V_12
  2  1    -4.38724382E-03   # V_21
  2  2    -9.99990376E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98510287E-01   # cos(theta_t)
  1  2    -5.45637861E-02   # sin(theta_t)
  2  1     5.45637861E-02   # -sin(theta_t)
  2  2     9.98510287E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99715739E-01   # cos(theta_b)
  1  2    -2.38420049E-02   # sin(theta_b)
  2  1     2.38420049E-02   # -sin(theta_b)
  2  2     9.99715739E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06946606E-01   # cos(theta_tau)
  1  2     7.07266920E-01   # sin(theta_tau)
  2  1    -7.07266920E-01   # -sin(theta_tau)
  2  2    -7.06946606E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00145543E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.59546033E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43946396E+02   # higgs               
         4     1.03869872E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.59546033E+03  # The gauge couplings
     1     3.62358472E-01   # gprime(Q) DRbar
     2     6.37724900E-01   # g(Q) DRbar
     3     1.02494591E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.59546033E+03  # The trilinear couplings
  1  1     2.42630256E-06   # A_u(Q) DRbar
  2  2     2.42633881E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.59546033E+03  # The trilinear couplings
  1  1     8.55542824E-07   # A_d(Q) DRbar
  2  2     8.55644141E-07   # A_s(Q) DRbar
  3  3     1.75163022E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.59546033E+03  # The trilinear couplings
  1  1     3.73873842E-07   # A_e(Q) DRbar
  2  2     3.73892270E-07   # A_mu(Q) DRbar
  3  3     3.79083524E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.59546033E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51454235E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.59546033E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.82051889E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.59546033E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02044946E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.59546033E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.95441680E+04   # M^2_Hd              
        22    -9.04555347E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41170610E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.68503937E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47941941E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47941941E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52058059E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52058059E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.40797168E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.09745214E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.96511951E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.82513528E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13952847E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.74746606E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.17662182E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.38868323E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.18703341E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.98063847E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.68014142E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.60652403E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.14719714E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.22874940E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.36709831E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.56555414E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.19773603E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.36106219E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.07892795E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.04574601E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.55339801E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.44003098E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.31502381E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.48821799E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.11463104E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.01590869E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.47561373E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.87496198E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.02983585E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.60312393E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.97273225E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.92075964E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.20794954E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.13323110E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.12862732E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16659103E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58914898E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.50706139E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.18406562E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.34073326E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41085062E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.88007142E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.01760312E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.60210188E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.93711468E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.59814044E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.20226372E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.87822032E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.13545323E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65703818E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.52391502E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.29001452E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.10531312E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.51473800E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54760839E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.87496198E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.02983585E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.60312393E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.97273225E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.92075964E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.20794954E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.13323110E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.12862732E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16659103E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58914898E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.50706139E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.18406562E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.34073326E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41085062E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.88007142E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.01760312E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.60210188E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.93711468E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.59814044E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.20226372E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.87822032E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.13545323E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65703818E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.52391502E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.29001452E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.10531312E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.51473800E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54760839E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.80873000E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01665116E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99661630E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.49430803E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.70427940E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98673221E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.87204564E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54580567E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999903E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.45441819E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.66137905E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.01785042E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.80873000E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01665116E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99661630E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.49430803E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.70427940E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98673221E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.87204564E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54580567E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999903E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.45441819E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.66137905E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.01785042E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.70312935E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.57280044E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14412877E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28307079E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.64921347E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.65466997E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.11673751E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.41414660E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.22422551E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.22818700E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.41686715E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.80887431E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01558687E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99279550E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.66766977E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.59057587E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99161752E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.78586751E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.80887431E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01558687E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99279550E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.66766977E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.59057587E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99161752E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.78586751E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.80912856E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01549945E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99253224E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.60339729E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.49775665E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99195448E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.37317884E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.70689943E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.82593554E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.34160878E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.55266467E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.35346616E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.29697283E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.03575767E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.12987079E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.59463068E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.69111073E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.05373154E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.94626846E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.84767347E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.11243410E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.86608768E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.11264544E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.11264544E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.00903638E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.45860811E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.30722072E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.30722072E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.59202586E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.59202586E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.83607776E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.83607776E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.75154374E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.84534280E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.44116535E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.18155185E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.18155185E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.17011579E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.16552247E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.27730199E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.27730199E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.66473085E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.66473085E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.84426624E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.84426624E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.62320884E-03   # h decays
#          BR         NDA      ID1       ID2
     5.62557107E-01    2           5        -5   # BR(h -> b       bb     )
     7.15957700E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.53450898E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.37909765E-04    2           3        -3   # BR(h -> s       sb     )
     2.33734235E-02    2           4        -4   # BR(h -> c       cb     )
     7.59584099E-02    2          21        21   # BR(h -> g       g      )
     2.60135803E-03    2          22        22   # BR(h -> gam     gam    )
     1.73250741E-03    2          22        23   # BR(h -> Z       gam    )
     2.32318096E-01    2          24       -24   # BR(h -> W+      W-     )
     2.90719683E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.57176256E+01   # H decays
#          BR         NDA      ID1       ID2
     8.97327549E-01    2           5        -5   # BR(H -> b       bb     )
     6.96222311E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.46167572E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.02294946E-04    2           3        -3   # BR(H -> s       sb     )
     8.47322035E-08    2           4        -4   # BR(H -> c       cb     )
     8.45257813E-03    2           6        -6   # BR(H -> t       tb     )
     1.09111545E-05    2          21        21   # BR(H -> g       g      )
     5.75095321E-08    2          22        22   # BR(H -> gam     gam    )
     3.46127167E-09    2          23        22   # BR(H -> Z       gam    )
     7.79029660E-07    2          24       -24   # BR(H -> W+      W-     )
     3.89034511E-07    2          23        23   # BR(H -> Z       Z      )
     5.79570452E-06    2          25        25   # BR(H -> h       h      )
     1.98118376E-24    2          36        36   # BR(H -> A       A      )
    -1.02192983E-20    2          23        36   # BR(H -> Z       A      )
     8.20125800E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.25600533E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.09841143E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.67349301E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.24860646E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.21819149E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.49231075E+01   # A decays
#          BR         NDA      ID1       ID2
     9.17771888E-01    2           5        -5   # BR(A -> b       bb     )
     7.12054029E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.51764937E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.09219622E-04    2           3        -3   # BR(A -> s       sb     )
     8.72573944E-08    2           4        -4   # BR(A -> c       cb     )
     8.69969444E-03    2           6        -6   # BR(A -> t       tb     )
     2.56197679E-05    2          21        21   # BR(A -> g       g      )
     6.84032187E-08    2          22        22   # BR(A -> gam     gam    )
     2.52332740E-08    2          23        22   # BR(A -> Z       gam    )
     7.93771727E-07    2          23        25   # BR(A -> Z       h      )
     9.36478673E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.44907482E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.67934194E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.86531599E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.82996055E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46440232E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.49515118E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.29652703E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.37216328E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.34961030E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.77658236E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.21706901E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.25035577E-07    2          24        25   # BR(H+ -> W+      h      )
     5.28087758E-14    2          24        36   # BR(H+ -> W+      A      )
     5.10750873E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.74449702E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.08355294E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
