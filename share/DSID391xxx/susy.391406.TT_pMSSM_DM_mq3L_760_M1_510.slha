#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13068192E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.23990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.59990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.26485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04172693E+01   # W+
        25     1.25481270E+02   # h
        35     4.00001447E+03   # H
        36     3.99999776E+03   # A
        37     4.00100517E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02603086E+03   # ~d_L
   2000001     4.02258765E+03   # ~d_R
   1000002     4.02537646E+03   # ~u_L
   2000002     4.02364732E+03   # ~u_R
   1000003     4.02603086E+03   # ~s_L
   2000003     4.02258765E+03   # ~s_R
   1000004     4.02537646E+03   # ~c_L
   2000004     4.02364732E+03   # ~c_R
   1000005     8.39084250E+02   # ~b_1
   2000005     4.02547870E+03   # ~b_2
   1000006     8.27966313E+02   # ~t_1
   2000006     2.27876607E+03   # ~t_2
   1000011     4.00472975E+03   # ~e_L
   2000011     4.00303106E+03   # ~e_R
   1000012     4.00361454E+03   # ~nu_eL
   1000013     4.00472975E+03   # ~mu_L
   2000013     4.00303106E+03   # ~mu_R
   1000014     4.00361454E+03   # ~nu_muL
   1000015     4.00368288E+03   # ~tau_1
   2000015     4.00830307E+03   # ~tau_2
   1000016     4.00502280E+03   # ~nu_tauL
   1000021     1.98156823E+03   # ~g
   1000022     4.94708170E+02   # ~chi_10
   1000023    -5.35843987E+02   # ~chi_20
   1000025     5.54828828E+02   # ~chi_30
   1000035     2.05233800E+03   # ~chi_40
   1000024     5.33722818E+02   # ~chi_1+
   1000037     2.05250236E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.69548139E-01   # N_11
  1  2    -2.09157625E-02   # N_12
  1  3    -4.69381424E-01   # N_13
  1  4    -4.32480371E-01   # N_14
  2  1    -3.15908341E-02   # N_21
  2  2     2.27689918E-02   # N_22
  2  3    -7.05142800E-01   # N_23
  2  4     7.07995215E-01   # N_24
  3  1     6.37805813E-01   # N_31
  3  2     2.82346216E-02   # N_32
  3  3     5.31392263E-01   # N_33
  3  4     5.56802311E-01   # N_34
  4  1    -1.19427555E-03   # N_41
  4  2     9.99123071E-01   # N_42
  4  3    -8.77343287E-03   # N_43
  4  4    -4.09229831E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.24136834E-02   # U_11
  1  2     9.99922947E-01   # U_12
  2  1    -9.99922947E-01   # U_21
  2  2     1.24136834E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.78820063E-02   # V_11
  1  2    -9.98323431E-01   # V_12
  2  1    -9.98323431E-01   # V_21
  2  2    -5.78820063E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96243736E-01   # cos(theta_t)
  1  2    -8.65934090E-02   # sin(theta_t)
  2  1     8.65934090E-02   # -sin(theta_t)
  2  2     9.96243736E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998590E-01   # cos(theta_b)
  1  2    -1.67928497E-03   # sin(theta_b)
  2  1     1.67928497E-03   # -sin(theta_b)
  2  2     9.99998590E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06291329E-01   # cos(theta_tau)
  1  2     7.07921294E-01   # sin(theta_tau)
  2  1    -7.07921294E-01   # -sin(theta_tau)
  2  2    -7.06291329E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00231071E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30681919E+03  # DRbar Higgs Parameters
         1    -5.23990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43761388E+02   # higgs               
         4     1.62342282E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30681919E+03  # The gauge couplings
     1     3.61848943E-01   # gprime(Q) DRbar
     2     6.35777883E-01   # g(Q) DRbar
     3     1.03009106E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30681919E+03  # The trilinear couplings
  1  1     1.38106467E-06   # A_u(Q) DRbar
  2  2     1.38107771E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30681919E+03  # The trilinear couplings
  1  1     5.11098303E-07   # A_d(Q) DRbar
  2  2     5.11145235E-07   # A_s(Q) DRbar
  3  3     9.12136330E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30681919E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.13057369E-07   # A_mu(Q) DRbar
  3  3     1.14201185E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30681919E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65738289E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30681919E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87075650E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30681919E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02759641E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30681919E+03  # The soft SUSY breaking masses at the scale Q
         1     5.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56190525E+07   # M^2_Hd              
        22    -2.79370523E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.59989996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.26485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40234804E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.70883240E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44935592E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44935592E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55064408E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55064408E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.61638522E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.35023125E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.04030946E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.34926868E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.26019060E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.31357922E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.39967088E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.12581718E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.03315481E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.59959675E-08    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.23671897E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.67755112E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.43171244E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.42438398E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.71862799E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.05476477E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.92073390E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.30798906E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.00831451E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.28109705E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.64025994E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.67048125E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.57388594E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80780461E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.55258547E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.58754573E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62226769E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.14225839E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13007287E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.02992333E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.15929354E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.99206952E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.41795723E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78881630E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.32442044E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.37010191E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.87086373E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79092487E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.64470518E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.56978955E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52609400E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61055961E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.20508669E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.37319313E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.18464100E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.37571595E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.46048947E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78902492E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.35857903E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.96745909E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.22944627E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79658645E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.59645954E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.60311047E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52825868E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54446963E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.35322083E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.40037848E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.69367229E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.14001730E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85939092E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78881630E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.32442044E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.37010191E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.87086373E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79092487E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.64470518E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.56978955E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52609400E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61055961E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.20508669E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.37319313E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.18464100E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.37571595E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.46048947E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78902492E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.35857903E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.96745909E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.22944627E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79658645E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.59645954E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.60311047E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52825868E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54446963E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.35322083E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.40037848E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.69367229E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.14001730E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85939092E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13249168E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.67062830E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.13721187E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.56905669E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78616650E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.52847839E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58822280E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.01567512E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.94136980E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.95865140E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.04866353E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.01854436E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13249168E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.67062830E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.13721187E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.56905669E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78616650E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.52847839E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58822280E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.01567512E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.94136980E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.95865140E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.04866353E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.01854436E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.05541995E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.13311739E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.51411258E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.61005588E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41274106E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.59121390E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.83355302E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.04493935E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.14380520E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.85960391E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.43059569E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45098691E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.78485275E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.91016654E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13353748E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.04905603E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     8.22727377E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.53826902E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79094045E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.32101203E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.56473922E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13353748E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.04905603E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     8.22727377E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.53826902E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79094045E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.32101203E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.56473922E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45198210E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.52636568E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.47112809E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.02927171E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53563845E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.45571763E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.05575492E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.54021083E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33709373E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33709373E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11237862E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11237862E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10105529E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.55907098E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.68168873E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.54878697E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.03819965E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.35879023E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.85877179E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.49675012E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.94273121E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.76048658E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.79367517E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18660549E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53933158E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18660549E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53933158E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.36606206E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53054025E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53054025E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48663667E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.05826531E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.05826531E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.05826520E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.52799888E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.52799888E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.52799888E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.52799888E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.17600045E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.17600045E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.17600045E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.17600045E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.08607153E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.08607153E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.39619015E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.06150367E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.95058732E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.53994904E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.52200348E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.53994904E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.52200348E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.36369473E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     9.83138334E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     9.83138334E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     9.92241314E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.10073368E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.10073368E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.10071755E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.97836971E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.56650780E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.97836971E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.56650780E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.41071379E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.88670054E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.88670054E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.60094074E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.17676020E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.17676020E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.17676021E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.14924571E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.14924571E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.14924571E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.14924571E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.83077024E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.83077024E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.83077024E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.83077024E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.69922636E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.69922636E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.55742830E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.69777963E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.96254673E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.30731332E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.85997119E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.85997119E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.62554017E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.04799138E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.25118253E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.80081899E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.80081899E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.81496628E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.81496628E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.99041844E-03   # h decays
#          BR         NDA      ID1       ID2
     5.90796809E-01    2           5        -5   # BR(h -> b       bb     )
     6.52538380E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30998382E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.89927904E-04    2           3        -3   # BR(h -> s       sb     )
     2.12812709E-02    2           4        -4   # BR(h -> c       cb     )
     6.95987153E-02    2          21        21   # BR(h -> g       g      )
     2.39816203E-03    2          22        22   # BR(h -> gam     gam    )
     1.63542060E-03    2          22        23   # BR(h -> Z       gam    )
     2.20466726E-01    2          24       -24   # BR(h -> W+      W-     )
     2.78481318E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.53417886E+01   # H decays
#          BR         NDA      ID1       ID2
     3.68039611E-01    2           5        -5   # BR(H -> b       bb     )
     5.99114646E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11832428E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.50561179E-04    2           3        -3   # BR(H -> s       sb     )
     7.02672960E-08    2           4        -4   # BR(H -> c       cb     )
     7.04224618E-03    2           6        -6   # BR(H -> t       tb     )
     8.40006612E-07    2          21        21   # BR(H -> g       g      )
     3.21881224E-09    2          22        22   # BR(H -> gam     gam    )
     1.81353732E-09    2          23        22   # BR(H -> Z       gam    )
     1.58536228E-06    2          24       -24   # BR(H -> W+      W-     )
     7.92131970E-07    2          23        23   # BR(H -> Z       Z      )
     6.54069463E-06    2          25        25   # BR(H -> h       h      )
    -1.55285802E-24    2          36        36   # BR(H -> A       A      )
     5.47426354E-20    2          23        36   # BR(H -> Z       A      )
     1.87466568E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.52170478E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.52170478E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.50866415E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.86047147E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.66024634E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.15582481E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.13948121E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.05489471E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.57931118E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.29796636E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.91668385E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     8.02883881E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.16271802E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.16271802E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.42814090E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.53339517E+01   # A decays
#          BR         NDA      ID1       ID2
     3.68117027E-01    2           5        -5   # BR(A -> b       bb     )
     5.99201348E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11862917E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50638169E-04    2           3        -3   # BR(A -> s       sb     )
     7.07416223E-08    2           4        -4   # BR(A -> c       cb     )
     7.05774630E-03    2           6        -6   # BR(A -> t       tb     )
     1.45018784E-05    2          21        21   # BR(A -> g       g      )
     6.97973145E-08    2          22        22   # BR(A -> gam     gam    )
     1.59416637E-08    2          23        22   # BR(A -> Z       gam    )
     1.58225220E-06    2          23        25   # BR(A -> Z       h      )
     1.93401436E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.52153613E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.52153613E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.22222256E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.08622736E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.45834978E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.52436185E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.78067555E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.64735713E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.72480921E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.88568522E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.72965862E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.19856837E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.19856837E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.56459947E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.94425031E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.95991304E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10727924E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.80431699E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19381396E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.45605921E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.78151760E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.57511948E-06    2          24        25   # BR(H+ -> W+      h      )
     2.45381002E-14    2          24        36   # BR(H+ -> W+      A      )
     4.51757439E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.52309559E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.64729477E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.51535360E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.68092204E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.50966652E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.62131130E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.11552250E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.35012128E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
