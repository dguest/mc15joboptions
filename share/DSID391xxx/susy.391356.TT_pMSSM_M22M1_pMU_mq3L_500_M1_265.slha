#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12256355E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.64990000E+02   # M_1(MX)             
         2     5.29990000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04030289E+01   # W+
        25     1.24678418E+02   # h
        35     3.00021762E+03   # H
        36     2.99999954E+03   # A
        37     3.00096238E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02377238E+03   # ~d_L
   2000001     3.01870671E+03   # ~d_R
   1000002     3.02285580E+03   # ~u_L
   2000002     3.02066997E+03   # ~u_R
   1000003     3.02377238E+03   # ~s_L
   2000003     3.01870671E+03   # ~s_R
   1000004     3.02285580E+03   # ~c_L
   2000004     3.02066997E+03   # ~c_R
   1000005     5.70613823E+02   # ~b_1
   2000005     3.01818796E+03   # ~b_2
   1000006     5.67693410E+02   # ~t_1
   2000006     3.00133899E+03   # ~t_2
   1000011     3.00650714E+03   # ~e_L
   2000011     3.00114643E+03   # ~e_R
   1000012     3.00511564E+03   # ~nu_eL
   1000013     3.00650714E+03   # ~mu_L
   2000013     3.00114643E+03   # ~mu_R
   1000014     3.00511564E+03   # ~nu_muL
   1000015     2.98573991E+03   # ~tau_1
   2000015     3.02214792E+03   # ~tau_2
   1000016     3.00521949E+03   # ~nu_tauL
   1000021     2.32929982E+03   # ~g
   1000022     2.67528954E+02   # ~chi_10
   1000023     5.57086386E+02   # ~chi_20
   1000025    -3.00221138E+03   # ~chi_30
   1000035     3.00296430E+03   # ~chi_40
   1000024     5.57244080E+02   # ~chi_1+
   1000037     3.00353823E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886158E-01   # N_11
  1  2    -8.22919792E-04   # N_12
  1  3     1.49255393E-02   # N_13
  1  4    -2.05496265E-03   # N_14
  2  1     1.23824173E-03   # N_21
  2  2     9.99616195E-01   # N_22
  2  3    -2.70033568E-02   # N_23
  2  4     6.06198437E-03   # N_24
  3  1    -9.08602661E-03   # N_31
  3  2     1.48179875E-02   # N_32
  3  3     7.06863862E-01   # N_33
  3  4     7.07136021E-01   # N_34
  4  1    -1.19825986E-02   # N_41
  4  2     2.33925677E-02   # N_42
  4  3     7.06676395E-01   # N_43
  4  4    -7.07048568E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99270024E-01   # U_11
  1  2    -3.82023577E-02   # U_12
  2  1     3.82023577E-02   # U_21
  2  2     9.99270024E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99963214E-01   # V_11
  1  2    -8.57730634E-03   # V_12
  2  1     8.57730634E-03   # V_21
  2  2     9.99963214E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98891105E-01   # cos(theta_t)
  1  2    -4.70803606E-02   # sin(theta_t)
  2  1     4.70803606E-02   # -sin(theta_t)
  2  2     9.98891105E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99919830E-01   # cos(theta_b)
  1  2     1.26622894E-02   # sin(theta_b)
  2  1    -1.26622894E-02   # -sin(theta_b)
  2  2     9.99919830E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06931975E-01   # cos(theta_tau)
  1  2     7.07281544E-01   # sin(theta_tau)
  2  1    -7.07281544E-01   # -sin(theta_tau)
  2  2     7.06931975E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01935616E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.22563552E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44694694E+02   # higgs               
         4     6.55241217E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.22563552E+03  # The gauge couplings
     1     3.61499013E-01   # gprime(Q) DRbar
     2     6.37387471E-01   # g(Q) DRbar
     3     1.03086368E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.22563552E+03  # The trilinear couplings
  1  1     1.38501750E-06   # A_u(Q) DRbar
  2  2     1.38503805E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.22563552E+03  # The trilinear couplings
  1  1     3.63429401E-07   # A_d(Q) DRbar
  2  2     3.63496649E-07   # A_s(Q) DRbar
  3  3     8.09179913E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.22563552E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.56561218E-08   # A_mu(Q) DRbar
  3  3     7.65398092E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.22563552E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.56392226E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.22563552E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.13701375E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.22563552E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06568382E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.22563552E+03  # The soft SUSY breaking masses at the scale Q
         1     2.64990000E+02   # M_1(Q)              
         2     5.29990000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.94323490E+04   # M^2_Hd              
        22    -9.12775729E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41008294E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.22734778E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48255001E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48255001E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51744999E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51744999E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.95522942E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.41599163E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.58400837E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.05009222E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.15071730E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.28158737E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.59360904E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.20302478E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.64132307E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.51791643E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.01278880E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.88717214E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.07841976E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     9.21580243E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.25749571E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.83717707E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.21605487E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.70555651E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.45493577E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     5.37481574E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27149821E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.97482809E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.29213889E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.18014024E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.86252503E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.91209200E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.58503312E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.40420606E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.54939959E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.17065942E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.82437683E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.18518628E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19507983E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55977441E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.27217357E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.31899770E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.71888233E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44022325E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.86775846E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.01328972E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.58290149E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.10926939E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.49212907E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.16497031E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.04635675E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.19199389E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68841779E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.43203283E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.45583639E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.29952584E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.41473658E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55679606E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.86252503E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.91209200E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.58503312E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.40420606E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.54939959E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.17065942E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.82437683E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.18518628E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19507983E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55977441E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.27217357E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.31899770E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.71888233E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44022325E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.86775846E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.01328972E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.58290149E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.10926939E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.49212907E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.16497031E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.04635675E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.19199389E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68841779E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.43203283E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.45583639E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.29952584E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.41473658E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55679606E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.77972662E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01449781E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99943720E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.44843064E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.91214602E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98606494E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.65611612E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53544275E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998547E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.45272203E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.77972662E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01449781E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99943720E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.44843064E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.91214602E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98606494E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.65611612E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53544275E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998547E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.45272203E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.68499509E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.57186640E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14587680E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28225680E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.62823230E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.65658702E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.11771031E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.13319992E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     9.30086524E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.22543655E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     9.17824466E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.77993776E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01985925E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98927180E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.78278217E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     5.99810495E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99086893E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.20732125E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.77993776E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01985925E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98927180E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.78278217E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     5.99810495E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99086893E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.20732125E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.78041824E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01976600E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98900987E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.12993464E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.58987261E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99122412E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.82240760E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.94149616E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.74831173E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.29418110E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.73759901E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.70323849E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.08337028E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.53763014E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.98556101E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.55050128E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.03786457E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.49621354E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.96292862E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.36337315E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.76382020E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.51158960E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.51158960E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.05604630E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.93828182E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.67780911E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.67780911E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.27470855E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.27470855E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.61714185E-12    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     3.61714185E-12    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     3.61714185E-12    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     3.61714185E-12    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     7.08448418E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     7.08448418E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.88025031E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.91329224E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.93252792E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.57541601E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.57541601E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.42107730E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.06978773E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.65163068E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.65163068E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.30082250E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.30082250E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.84924186E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.84924186E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.84924186E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.84924186E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     9.35722707E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     9.35722707E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.86648960E-03   # h decays
#          BR         NDA      ID1       ID2
     6.82148498E-01    2           5        -5   # BR(h -> b       bb     )
     5.35262163E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.89485576E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.02349832E-04    2           3        -3   # BR(h -> s       sb     )
     1.73599818E-02    2           4        -4   # BR(h -> c       cb     )
     5.65769638E-02    2          21        21   # BR(h -> g       g      )
     1.90944631E-03    2          22        22   # BR(h -> gam     gam    )
     1.24760033E-03    2          22        23   # BR(h -> Z       gam    )
     1.65983277E-01    2          24       -24   # BR(h -> W+      W-     )
     2.06561812E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.38887957E+01   # H decays
#          BR         NDA      ID1       ID2
     7.41489167E-01    2           5        -5   # BR(H -> b       bb     )
     1.79053126E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.33089066E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.77430558E-04    2           3        -3   # BR(H -> s       sb     )
     2.19476363E-07    2           4        -4   # BR(H -> c       cb     )
     2.18941946E-02    2           6        -6   # BR(H -> t       tb     )
     3.38490397E-05    2          21        21   # BR(H -> g       g      )
     6.90851579E-08    2          22        22   # BR(H -> gam     gam    )
     8.39510877E-09    2          23        22   # BR(H -> Z       gam    )
     3.51366041E-05    2          24       -24   # BR(H -> W+      W-     )
     1.75466461E-05    2          23        23   # BR(H -> Z       Z      )
     8.84101857E-05    2          25        25   # BR(H -> h       h      )
    -1.18159645E-23    2          36        36   # BR(H -> A       A      )
     3.84367074E-17    2          23        36   # BR(H -> Z       A      )
     2.08765616E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.09856381E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.04109518E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.85454499E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.20473422E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.34896392E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31755788E+01   # A decays
#          BR         NDA      ID1       ID2
     7.81684461E-01    2           5        -5   # BR(A -> b       bb     )
     1.88736576E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.67326498E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.19615524E-04    2           3        -3   # BR(A -> s       sb     )
     2.31283884E-07    2           4        -4   # BR(A -> c       cb     )
     2.30593536E-02    2           6        -6   # BR(A -> t       tb     )
     6.79076010E-05    2          21        21   # BR(A -> g       g      )
     3.99104810E-08    2          22        22   # BR(A -> gam     gam    )
     6.69171833E-08    2          23        22   # BR(A -> Z       gam    )
     3.68950899E-05    2          23        25   # BR(A -> Z       h      )
     2.67024061E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22948260E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.33153691E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.02800918E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.86633112E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.08402019E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.52121250E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.91439245E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.93771824E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.23878961E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.07778748E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.06761985E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.93483032E-05    2          24        25   # BR(H+ -> W+      h      )
     1.10357868E-13    2          24        36   # BR(H+ -> W+      A      )
     2.03290085E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.05499333E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.59219402E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
