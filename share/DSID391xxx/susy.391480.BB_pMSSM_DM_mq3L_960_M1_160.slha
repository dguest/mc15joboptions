#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13004333E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.85900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.78485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04173600E+01   # W+
        25     1.26092007E+02   # h
        35     4.00000364E+03   # H
        36     3.99999652E+03   # A
        37     4.00106793E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02592245E+03   # ~d_L
   2000001     4.02257762E+03   # ~d_R
   1000002     4.02527169E+03   # ~u_L
   2000002     4.02332039E+03   # ~u_R
   1000003     4.02592245E+03   # ~s_L
   2000003     4.02257762E+03   # ~s_R
   1000004     4.02527169E+03   # ~c_L
   2000004     4.02332039E+03   # ~c_R
   1000005     1.01401169E+03   # ~b_1
   2000005     4.02537710E+03   # ~b_2
   1000006     9.86735232E+02   # ~t_1
   2000006     1.80212953E+03   # ~t_2
   1000011     4.00455177E+03   # ~e_L
   2000011     4.00350839E+03   # ~e_R
   1000012     4.00343898E+03   # ~nu_eL
   1000013     4.00455177E+03   # ~mu_L
   2000013     4.00350839E+03   # ~mu_R
   1000014     4.00343898E+03   # ~nu_muL
   1000015     4.00534766E+03   # ~tau_1
   2000015     4.00704567E+03   # ~tau_2
   1000016     4.00488474E+03   # ~nu_tauL
   1000021     1.97719778E+03   # ~g
   1000022     1.45807171E+02   # ~chi_10
   1000023    -1.95995240E+02   # ~chi_20
   1000025     2.09819319E+02   # ~chi_30
   1000035     2.05221243E+03   # ~chi_40
   1000024     1.92214493E+02   # ~chi_1+
   1000037     2.05237729E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.15406696E-01   # N_11
  1  2    -1.34531289E-02   # N_12
  1  3    -4.63990455E-01   # N_13
  1  4    -3.45895636E-01   # N_14
  2  1    -9.37474366E-02   # N_21
  2  2     2.63817597E-02   # N_22
  2  3    -6.95915627E-01   # N_23
  2  4     7.11489185E-01   # N_24
  3  1     5.71246429E-01   # N_31
  3  2     2.51508938E-02   # N_32
  3  3     5.48098085E-01   # N_33
  3  4     6.10437088E-01   # N_34
  4  1    -9.25063032E-04   # N_41
  4  2     9.99244939E-01   # N_42
  4  3    -1.66906153E-03   # N_43
  4  4    -3.88060546E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.36509093E-03   # U_11
  1  2     9.99997203E-01   # U_12
  2  1    -9.99997203E-01   # U_21
  2  2     2.36509093E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.48928364E-02   # V_11
  1  2    -9.98492252E-01   # V_12
  2  1    -9.98492252E-01   # V_21
  2  2    -5.48928364E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.85807583E-01   # cos(theta_t)
  1  2    -1.67879151E-01   # sin(theta_t)
  2  1     1.67879151E-01   # -sin(theta_t)
  2  2     9.85807583E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999822E-01   # cos(theta_b)
  1  2    -5.96657329E-04   # sin(theta_b)
  2  1     5.96657329E-04   # -sin(theta_b)
  2  2     9.99999822E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04659248E-01   # cos(theta_tau)
  1  2     7.09545872E-01   # sin(theta_tau)
  2  1    -7.09545872E-01   # -sin(theta_tau)
  2  2    -7.04659248E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00317609E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30043333E+03  # DRbar Higgs Parameters
         1    -1.85900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43511037E+02   # higgs               
         4     1.60688750E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30043333E+03  # The gauge couplings
     1     3.62066391E-01   # gprime(Q) DRbar
     2     6.36790469E-01   # g(Q) DRbar
     3     1.03007630E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30043333E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.36886030E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30043333E+03  # The trilinear couplings
  1  1     5.01001269E-07   # A_d(Q) DRbar
  2  2     5.01048314E-07   # A_s(Q) DRbar
  3  3     8.98139227E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30043333E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.08156848E-07   # A_mu(Q) DRbar
  3  3     1.09270729E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30043333E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69012195E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30043333E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.81350118E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30043333E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04057894E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30043333E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58554726E+07   # M^2_Hd              
        22     1.49075151E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.78485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40681458E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.70093128E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.36939107E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.36939107E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.61747787E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.61747787E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     1.31310642E-03    2     2000006        -6   # BR(~g -> ~t_2  tb)
     1.31310642E-03    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     1.43788201E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.56280338E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.32339608E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.63399054E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.08633305E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.15071354E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.41859036E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.14581475E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.64630353E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.20126068E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.60127617E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.26137812E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.38378090E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.48324808E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.91204464E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.72035503E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.03544738E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.93321529E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65681272E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.68101251E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.76937442E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53052694E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.33598473E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.61878460E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.54621930E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13847523E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.58104017E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.42795744E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.37823646E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.23173872E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77870865E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.74594245E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.49584791E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.55725865E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83538626E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.27652561E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.65851596E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51223628E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60195341E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.71960710E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.90718407E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.82088346E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.64235930E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44104350E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77890242E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.47550055E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.70643126E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.04401808E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.84013341E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.79573949E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.69036101E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51443531E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53415766E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.70811345E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.28076608E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.75247147E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.89475638E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85411332E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77870865E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.74594245E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.49584791E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.55725865E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83538626E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.27652561E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.65851596E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51223628E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60195341E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.71960710E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.90718407E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.82088346E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.64235930E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44104350E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77890242E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.47550055E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.70643126E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.04401808E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.84013341E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.79573949E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.69036101E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51443531E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53415766E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.70811345E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.28076608E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.75247147E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.89475638E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85411332E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15671116E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03422465E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.69076988E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.23213936E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77507812E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.69815884E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56373553E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08071736E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.65518145E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.77806508E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.25703323E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.66790362E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15671116E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03422465E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.69076988E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.23213936E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77507812E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.69815884E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56373553E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08071736E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.65518145E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.77806508E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.25703323E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.66790362E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11478607E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.50743758E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.15082770E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.34384915E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39503986E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41686677E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79690396E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11797303E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.44474168E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.73790838E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.09986094E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41875401E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.18399446E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84445309E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15777012E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16083446E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.23155390E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.56647361E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77812145E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.06762826E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54140490E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15777012E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16083446E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.23155390E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.56647361E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77812145E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.06762826E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54140490E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.49050110E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05055953E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.92457289E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.13268419E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51549944E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.73868222E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01755866E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.69794024E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33597075E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33597075E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11200080E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11200080E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10405691E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.99150212E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.48671435E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.85929141E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.30381754E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.14635986E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.70993605E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.99078093E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.16789279E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.05111860E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.38169879E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.12391446E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18087920E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53162277E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18087920E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53162277E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40761809E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51116133E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51116133E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47721144E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01996503E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01996503E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01996479E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.52386238E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.52386238E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.52386238E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.52386238E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.17462280E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.17462280E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.17462280E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.17462280E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.00295330E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.00295330E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.02542432E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.60055199E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     5.91520088E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.65348872E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.90016525E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.65348872E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.90016525E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.37808620E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.24849164E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.24849164E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.23203667E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.54767146E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.54767146E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.54766530E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.53508501E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.58536836E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.53508501E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.58536836E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.21180674E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.05137093E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.05137093E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     9.55494135E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.10144738E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.10144738E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.10144742E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.01268971E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.01268971E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.01268971E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.01268971E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.33754599E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.33754599E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.33754599E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.33754599E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.27217643E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.27217643E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.99034978E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.76653886E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.47168763E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.79703892E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.94311259E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.94311259E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.67086513E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.67175843E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.49025130E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.69986334E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.69986334E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     9.96178610E-05    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     9.96178610E-05    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.69610539E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.69610539E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.12429549E-03   # h decays
#          BR         NDA      ID1       ID2
     5.86420404E-01    2           5        -5   # BR(h -> b       bb     )
     6.34656240E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.24665505E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.76044419E-04    2           3        -3   # BR(h -> s       sb     )
     2.06710900E-02    2           4        -4   # BR(h -> c       cb     )
     6.78005628E-02    2          21        21   # BR(h -> g       g      )
     2.37085620E-03    2          22        22   # BR(h -> gam     gam    )
     1.66947725E-03    2          22        23   # BR(h -> Z       gam    )
     2.27854214E-01    2          24       -24   # BR(h -> W+      W-     )
     2.90470617E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.45920397E+01   # H decays
#          BR         NDA      ID1       ID2
     3.43181655E-01    2           5        -5   # BR(H -> b       bb     )
     6.07340517E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14740896E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54001482E-04    2           3        -3   # BR(H -> s       sb     )
     7.12567827E-08    2           4        -4   # BR(H -> c       cb     )
     7.14141314E-03    2           6        -6   # BR(H -> t       tb     )
     1.03191523E-06    2          21        21   # BR(H -> g       g      )
     4.86562887E-11    2          22        22   # BR(H -> gam     gam    )
     1.83225607E-09    2          23        22   # BR(H -> Z       gam    )
     2.06570125E-06    2          24       -24   # BR(H -> W+      W-     )
     1.03213506E-06    2          23        23   # BR(H -> Z       Z      )
     7.79056218E-06    2          25        25   # BR(H -> h       h      )
    -5.40506964E-24    2          36        36   # BR(H -> A       A      )
     6.37877748E-20    2          23        36   # BR(H -> Z       A      )
     1.85159074E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.65738895E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.65738895E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.87906537E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.56015707E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.60456935E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.08704421E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.80060802E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.65784139E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.62258164E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.77756524E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.18783637E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.17339652E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.26265885E-07    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     2.44634905E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.44634905E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.26962385E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.45883399E+01   # A decays
#          BR         NDA      ID1       ID2
     3.43230820E-01    2           5        -5   # BR(A -> b       bb     )
     6.07385538E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14756646E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54061520E-04    2           3        -3   # BR(A -> s       sb     )
     7.17078494E-08    2           4        -4   # BR(A -> c       cb     )
     7.15414478E-03    2           6        -6   # BR(A -> t       tb     )
     1.46999586E-05    2          21        21   # BR(A -> g       g      )
     4.68661191E-08    2          22        22   # BR(A -> gam     gam    )
     1.61596632E-08    2          23        22   # BR(A -> Z       gam    )
     2.06145194E-06    2          23        25   # BR(A -> Z       h      )
     1.85296577E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.65753870E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.65753870E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.49327549E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.93348164E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.29804917E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.66157452E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.56292277E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.51409031E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.82718534E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.31524998E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.78929246E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.73242269E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.73242269E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.45113214E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.47640797E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.08406626E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15117677E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.50489808E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21868035E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.50721738E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.49076538E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.06674700E-06    2          24        25   # BR(H+ -> W+      h      )
     3.40810752E-14    2          24        36   # BR(H+ -> W+      A      )
     5.55957269E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.46100097E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.32823038E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.66203488E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.19089657E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61303453E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00044800E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     6.26136798E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     5.04109291E-04    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
