#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14031834E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.89990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.00990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.96485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04168966E+01   # W+
        25     1.25544361E+02   # h
        35     4.00000913E+03   # H
        36     3.99999726E+03   # A
        37     4.00106221E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02860636E+03   # ~d_L
   2000001     4.02468388E+03   # ~d_R
   1000002     4.02795419E+03   # ~u_L
   2000002     4.02541713E+03   # ~u_R
   1000003     4.02860636E+03   # ~s_L
   2000003     4.02468388E+03   # ~s_R
   1000004     4.02795419E+03   # ~c_L
   2000004     4.02541713E+03   # ~c_R
   1000005     1.07458052E+03   # ~b_1
   2000005     4.02686801E+03   # ~b_2
   1000006     1.05623521E+03   # ~t_1
   2000006     1.97868399E+03   # ~t_2
   1000011     4.00512175E+03   # ~e_L
   2000011     4.00346060E+03   # ~e_R
   1000012     4.00400779E+03   # ~nu_eL
   1000013     4.00512175E+03   # ~mu_L
   2000013     4.00346060E+03   # ~mu_R
   1000014     4.00400779E+03   # ~nu_muL
   1000015     4.00434046E+03   # ~tau_1
   2000015     4.00781361E+03   # ~tau_2
   1000016     4.00519751E+03   # ~nu_tauL
   1000021     1.98411952E+03   # ~g
   1000022     3.47902329E+02   # ~chi_10
   1000023    -4.01640482E+02   # ~chi_20
   1000025     4.15139007E+02   # ~chi_30
   1000035     2.05140474E+03   # ~chi_40
   1000024     3.99245396E+02   # ~chi_1+
   1000037     2.05156951E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.40411031E-01   # N_11
  1  2    -1.55965158E-02   # N_12
  1  3    -4.09470339E-01   # N_13
  1  4    -3.54683083E-01   # N_14
  2  1    -4.35102615E-02   # N_21
  2  2     2.40419869E-02   # N_22
  2  3    -7.03937589E-01   # N_23
  2  4     7.08520085E-01   # N_24
  3  1     5.40199073E-01   # N_31
  3  2     2.81564733E-02   # N_32
  3  3     5.80320768E-01   # N_33
  3  4     6.08785661E-01   # N_34
  4  1    -1.05739742E-03   # N_41
  4  2     9.99192646E-01   # N_42
  4  3    -5.80672664E-03   # N_43
  4  4    -3.97393919E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.21774658E-03   # U_11
  1  2     9.99966234E-01   # U_12
  2  1    -9.99966234E-01   # U_21
  2  2     8.21774658E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.62104681E-02   # V_11
  1  2    -9.98418942E-01   # V_12
  2  1    -9.98418942E-01   # V_21
  2  2    -5.62104681E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.90735356E-01   # cos(theta_t)
  1  2    -1.35806680E-01   # sin(theta_t)
  2  1     1.35806680E-01   # -sin(theta_t)
  2  2     9.90735356E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999189E-01   # cos(theta_b)
  1  2    -1.27357738E-03   # sin(theta_b)
  2  1     1.27357738E-03   # -sin(theta_b)
  2  2     9.99999189E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06042588E-01   # cos(theta_tau)
  1  2     7.08169375E-01   # sin(theta_tau)
  2  1    -7.08169375E-01   # -sin(theta_tau)
  2  2    -7.06042588E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00264169E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.40318342E+03  # DRbar Higgs Parameters
         1    -3.89990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43514166E+02   # higgs               
         4     1.60818698E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.40318342E+03  # The gauge couplings
     1     3.62162973E-01   # gprime(Q) DRbar
     2     6.36086928E-01   # g(Q) DRbar
     3     1.02833786E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.40318342E+03  # The trilinear couplings
  1  1     1.60570980E-06   # A_u(Q) DRbar
  2  2     1.60572503E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.40318342E+03  # The trilinear couplings
  1  1     5.92355102E-07   # A_d(Q) DRbar
  2  2     5.92409669E-07   # A_s(Q) DRbar
  3  3     1.05776131E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.40318342E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.29707948E-07   # A_mu(Q) DRbar
  3  3     1.31039266E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.40318342E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65002964E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.40318342E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84345409E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.40318342E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03292185E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.40318342E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57440445E+07   # M^2_Hd              
        22    -1.06442745E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.00990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.96485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40372800E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.30865578E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40240813E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40240813E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59759187E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59759187E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.19717770E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.14813071E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.28544879E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.49871736E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.06770314E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.10762979E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.95716850E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.19655876E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.25060914E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.34982339E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.56883902E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.13728979E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.22671127E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.23851900E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.69732839E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.97830256E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.42100515E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.89033639E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66168511E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.55694955E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81684581E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62013623E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.20818259E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.65086996E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.39546132E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12804324E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.60497801E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.17039604E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.63137442E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.38184410E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78232140E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.78908632E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.14214727E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.46633822E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82491439E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.43454164E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.63768492E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51553707E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60408559E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.90483342E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.04160613E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.60343010E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.45618103E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44813170E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78252424E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.64892055E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.97055306E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.74895566E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83011798E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.16160605E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.67023482E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51771334E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53705760E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.01858448E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.71704796E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.18257363E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.01330110E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85604402E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78232140E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.78908632E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.14214727E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.46633822E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82491439E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.43454164E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.63768492E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51553707E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60408559E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.90483342E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.04160613E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.60343010E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.45618103E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44813170E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78252424E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.64892055E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.97055306E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.74895566E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83011798E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.16160605E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.67023482E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51771334E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53705760E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.01858448E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.71704796E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.18257363E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.01330110E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85604402E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14677864E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.08113364E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.68286621E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.65023762E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77949078E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.78178322E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57367095E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05398961E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.07627724E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.88713295E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.90484524E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.18490834E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14677864E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.08113364E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.68286621E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.65023762E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77949078E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.78178322E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57367095E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05398961E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.07627724E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.88713295E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.90484524E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.18490834E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08925165E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.55466626E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.44382767E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.23264348E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40327149E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.51065749E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81397025E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08322932E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.61182706E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.04966324E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.98266471E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43246298E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95618356E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87246057E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14783148E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.23098467E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19547235E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.91127277E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78338909E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.17104115E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55083383E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14783148E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.23098467E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19547235E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.91127277E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78338909E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.17104115E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55083383E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47266644E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11617938E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.08398234E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.54650960E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52484639E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.58238650E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03524480E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.36713660E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33546133E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33546133E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11183531E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11183531E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10540672E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.68401715E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.42437974E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.14692455E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.24499260E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.45763054E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.85828802E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.24680655E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.36731744E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.37508713E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.33118461E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.65612588E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17914583E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52973185E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17914583E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52973185E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42218965E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50896516E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50896516E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47927132E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01555438E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01555438E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01555425E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.31215869E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.31215869E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.31215869E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.31215869E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.70720239E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.70720239E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.70720239E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.70720239E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.19494153E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.19494153E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.65294270E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.43734490E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.57301206E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.40454718E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.26706929E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.40454718E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.26706929E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     7.93402360E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.86331003E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.86331003E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.86235132E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.80705839E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.80705839E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.80705056E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.32003600E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.19937141E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.32003600E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.19937141E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.97578929E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.88094992E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.88094992E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.70143819E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.76012583E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.76012583E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.76012586E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.85313306E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.85313306E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.85313306E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.85313306E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.61769435E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.61769435E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.61769435E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.61769435E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.52155308E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.52155308E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.68260254E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.44716874E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.44901698E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.26557223E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.24186538E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.24186538E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14863839E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.97874620E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.22433921E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.66505226E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.66505226E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.67020971E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.67020971E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.02161270E-03   # h decays
#          BR         NDA      ID1       ID2
     5.92242372E-01    2           5        -5   # BR(h -> b       bb     )
     6.47888849E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29352173E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.86388014E-04    2           3        -3   # BR(h -> s       sb     )
     2.11247382E-02    2           4        -4   # BR(h -> c       cb     )
     6.89957263E-02    2          21        21   # BR(h -> g       g      )
     2.38531166E-03    2          22        22   # BR(h -> gam     gam    )
     1.63178484E-03    2          22        23   # BR(h -> Z       gam    )
     2.20266841E-01    2          24       -24   # BR(h -> W+      W-     )
     2.78486015E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.50101823E+01   # H decays
#          BR         NDA      ID1       ID2
     3.57349539E-01    2           5        -5   # BR(H -> b       bb     )
     6.02725159E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13109017E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52071206E-04    2           3        -3   # BR(H -> s       sb     )
     7.07001383E-08    2           4        -4   # BR(H -> c       cb     )
     7.08562589E-03    2           6        -6   # BR(H -> t       tb     )
     7.37311871E-07    2          21        21   # BR(H -> g       g      )
     3.00333901E-09    2          22        22   # BR(H -> gam     gam    )
     1.82194296E-09    2          23        22   # BR(H -> Z       gam    )
     1.76223531E-06    2          24       -24   # BR(H -> W+      W-     )
     8.80507191E-07    2          23        23   # BR(H -> Z       Z      )
     7.00197108E-06    2          25        25   # BR(H -> h       h      )
     1.94126016E-24    2          36        36   # BR(H -> A       A      )
     4.21197954E-20    2          23        36   # BR(H -> Z       A      )
     1.85700642E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.58703589E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.58703589E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.31929235E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.78735138E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.46063050E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.55205517E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     8.67377818E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.55905697E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.24329493E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.21983561E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.14507991E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     7.86796347E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.58840718E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     5.23236018E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.23236018E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.28369773E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.50052808E+01   # A decays
#          BR         NDA      ID1       ID2
     3.57407007E-01    2           5        -5   # BR(A -> b       bb     )
     6.02781657E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13128826E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52135770E-04    2           3        -3   # BR(A -> s       sb     )
     7.11643142E-08    2           4        -4   # BR(A -> c       cb     )
     7.09991740E-03    2           6        -6   # BR(A -> t       tb     )
     1.45885316E-05    2          21        21   # BR(A -> g       g      )
     6.23799532E-08    2          22        22   # BR(A -> gam     gam    )
     1.60358787E-08    2          23        22   # BR(A -> Z       gam    )
     1.75868701E-06    2          23        25   # BR(A -> Z       h      )
     1.87856667E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.58707357E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.58707357E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.01097460E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.37833941E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.23721500E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.08543407E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.12080070E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.77335829E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.38160831E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.55607740E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.58599505E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.63730320E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.63730320E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.51375540E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.74139053E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.01495702E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12674144E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.67448681E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20483751E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47873821E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.65545937E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.75648423E-06    2          24        25   # BR(H+ -> W+      h      )
     3.26906297E-14    2          24        36   # BR(H+ -> W+      A      )
     5.70466873E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.56689288E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.83429056E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.58546670E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.32942254E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57428084E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.07275966E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.06319657E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.08857987E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
