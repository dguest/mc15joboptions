#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15475426E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.99900000E+02   # M_1(MX)             
         2     5.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04017064E+01   # W+
        25     1.25134324E+02   # h
        35     3.00007508E+03   # H
        36     2.99999994E+03   # A
        37     3.00109006E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03699705E+03   # ~d_L
   2000001     3.03183932E+03   # ~d_R
   1000002     3.03608907E+03   # ~u_L
   2000002     3.03360949E+03   # ~u_R
   1000003     3.03699705E+03   # ~s_L
   2000003     3.03183932E+03   # ~s_R
   1000004     3.03608907E+03   # ~c_L
   2000004     3.03360949E+03   # ~c_R
   1000005     8.98456443E+02   # ~b_1
   2000005     3.03024845E+03   # ~b_2
   1000006     8.96062097E+02   # ~t_1
   2000006     3.02340022E+03   # ~t_2
   1000011     3.00656588E+03   # ~e_L
   2000011     3.00136610E+03   # ~e_R
   1000012     3.00517874E+03   # ~nu_eL
   1000013     3.00656588E+03   # ~mu_L
   2000013     3.00136610E+03   # ~mu_R
   1000014     3.00517874E+03   # ~nu_muL
   1000015     2.98614536E+03   # ~tau_1
   2000015     3.02172541E+03   # ~tau_2
   1000016     3.00518255E+03   # ~nu_tauL
   1000021     2.34584100E+03   # ~g
   1000022     3.02235431E+02   # ~chi_10
   1000023     6.32982040E+02   # ~chi_20
   1000025    -2.99802310E+03   # ~chi_30
   1000035     2.99829030E+03   # ~chi_40
   1000024     6.33145240E+02   # ~chi_1+
   1000037     2.99910396E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99890397E-01   # N_11
  1  2    -8.32952086E-07   # N_12
  1  3    -1.47865896E-02   # N_13
  1  4    -7.42547392E-04   # N_14
  2  1     3.98337576E-04   # N_21
  2  2     9.99635866E-01   # N_22
  2  3     2.66770099E-02   # N_23
  2  4     4.03916423E-03   # N_24
  3  1    -9.92788302E-03   # N_31
  3  2     1.60102297E-02   # N_32
  3  3    -7.06848193E-01   # N_33
  3  4     7.07114376E-01   # N_34
  4  1     1.09760228E-02   # N_41
  4  2    -2.17211630E-02   # N_42
  4  3     7.06707383E-01   # N_43
  4  4     7.07087260E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99288266E-01   # U_11
  1  2     3.77221506E-02   # U_12
  2  1    -3.77221506E-02   # U_21
  2  2     9.99288266E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99983697E-01   # V_11
  1  2    -5.71022374E-03   # V_12
  2  1    -5.71022374E-03   # V_21
  2  2    -9.99983697E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98536018E-01   # cos(theta_t)
  1  2    -5.40908565E-02   # sin(theta_t)
  2  1     5.40908565E-02   # -sin(theta_t)
  2  2     9.98536018E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99721340E-01   # cos(theta_b)
  1  2    -2.36059812E-02   # sin(theta_b)
  2  1     2.36059812E-02   # -sin(theta_b)
  2  2     9.99721340E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06950513E-01   # cos(theta_tau)
  1  2     7.07263015E-01   # sin(theta_tau)
  2  1    -7.07263015E-01   # -sin(theta_tau)
  2  2    -7.06950513E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00131549E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.54754258E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44010023E+02   # higgs               
         4     1.05378659E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.54754258E+03  # The gauge couplings
     1     3.62255572E-01   # gprime(Q) DRbar
     2     6.37282441E-01   # g(Q) DRbar
     3     1.02563160E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.54754258E+03  # The trilinear couplings
  1  1     2.28566177E-06   # A_u(Q) DRbar
  2  2     2.28569565E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.54754258E+03  # The trilinear couplings
  1  1     8.12752756E-07   # A_d(Q) DRbar
  2  2     8.12847304E-07   # A_s(Q) DRbar
  3  3     1.64999600E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.54754258E+03  # The trilinear couplings
  1  1     3.49849691E-07   # A_e(Q) DRbar
  2  2     3.49866817E-07   # A_mu(Q) DRbar
  3  3     3.54672185E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.54754258E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52195555E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.54754258E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.81934122E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.54754258E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01632582E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.54754258E+03  # The soft SUSY breaking masses at the scale Q
         1     2.99900000E+02   # M_1(Q)              
         2     5.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.20005886E+04   # M^2_Hd              
        22    -9.05582953E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40967633E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.93142495E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47962114E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47962114E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52037886E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52037886E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.63816037E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     3.58321189E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.51434720E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.12733161E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.15006681E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.65832399E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.22834840E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.49310438E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.04575976E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.96624409E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.68956747E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.61066473E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.15692409E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.42011710E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.42744056E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.95223580E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.60502014E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.37867847E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.04799881E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.75140977E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.92138192E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.79695203E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -7.30916224E-09    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.48068260E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.15678662E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.01828995E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.47977627E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.78679173E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.06088996E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.57865300E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.46308034E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.69519739E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.15888177E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.60826563E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.20185554E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16801376E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57767361E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.34202380E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.98734516E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.20300260E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42232593E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.79197546E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.05830234E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.57754031E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.13013176E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.21209606E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.15321854E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.47678410E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.20865351E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66050063E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.48823397E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.66214849E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.57606804E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.10054203E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55117648E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.78679173E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.06088996E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.57865300E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.46308034E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.69519739E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.15888177E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.60826563E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.20185554E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16801376E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57767361E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.34202380E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.98734516E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.20300260E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42232593E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.79197546E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.05830234E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.57754031E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.13013176E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.21209606E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.15321854E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.47678410E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.20865351E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66050063E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.48823397E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.66214849E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.57606804E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.10054203E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55117648E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.70887227E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03667032E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99015997E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.13336057E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.37227367E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97316945E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.29064069E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53518729E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999851E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.47887636E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.98737625E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.16096540E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.70887227E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03667032E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99015997E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.13336057E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.37227367E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97316945E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.29064069E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53518729E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999851E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.47887636E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.98737625E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.16096540E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.64684368E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.62349100E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.12737466E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.24913435E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.59500671E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.70578773E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.09986158E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.31709823E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.12869124E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.19397507E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.31039708E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.70895039E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03615674E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98575500E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.47236861E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     5.36579007E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.97808817E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.48194355E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.70895039E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03615674E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98575500E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.47236861E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     5.36579007E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.97808817E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.48194355E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.70928759E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03606391E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98548806E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.47574249E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     5.37122541E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.97843783E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.01149016E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.13646248E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.95212557E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.34792060E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.59653534E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.90340025E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.13687001E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.99867872E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.97327424E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.44657852E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.08619941E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.77644135E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     4.22355865E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.97201121E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.12742531E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.96406548E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.95704114E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.95704114E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.55872972E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.19408959E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.32997947E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.32997947E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.62239266E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.62239266E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.47776423E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.47776423E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.87831315E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.32690166E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.18060747E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.02207888E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.02207888E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.18579145E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.28286170E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.29943050E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.29943050E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.69260631E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.69260631E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.43873090E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.43873090E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.62741234E-03   # h decays
#          BR         NDA      ID1       ID2
     5.60489382E-01    2           5        -5   # BR(h -> b       bb     )
     7.15565732E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.53311733E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.37545064E-04    2           3        -3   # BR(h -> s       sb     )
     2.33588616E-02    2           4        -4   # BR(h -> c       cb     )
     7.60061955E-02    2          21        21   # BR(h -> g       g      )
     2.60571839E-03    2          22        22   # BR(h -> gam     gam    )
     1.74352350E-03    2          22        23   # BR(h -> Z       gam    )
     2.34106804E-01    2          24       -24   # BR(h -> W+      W-     )
     2.93420849E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.63249967E+01   # H decays
#          BR         NDA      ID1       ID2
     8.98777936E-01    2           5        -5   # BR(H -> b       bb     )
     6.84586197E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.42053320E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.97242340E-04    2           3        -3   # BR(H -> s       sb     )
     8.33113062E-08    2           4        -4   # BR(H -> c       cb     )
     8.31083588E-03    2           6        -6   # BR(H -> t       tb     )
     9.75502375E-06    2          21        21   # BR(H -> g       g      )
     6.00877950E-08    2          22        22   # BR(H -> gam     gam    )
     3.40632075E-09    2          23        22   # BR(H -> Z       gam    )
     7.28319619E-07    2          24       -24   # BR(H -> W+      W-     )
     3.63710827E-07    2          23        23   # BR(H -> Z       Z      )
     5.64368696E-06    2          25        25   # BR(H -> h       h      )
     3.19323197E-24    2          36        36   # BR(H -> A       A      )
     1.80214568E-19    2          23        36   # BR(H -> Z       A      )
     7.52373408E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.13175173E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.75929370E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.54019126E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.24678170E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.21890971E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.55198229E+01   # A decays
#          BR         NDA      ID1       ID2
     9.19175338E-01    2           5        -5   # BR(A -> b       bb     )
     7.00091882E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.47535413E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.04024889E-04    2           3        -3   # BR(A -> s       sb     )
     8.57915143E-08    2           4        -4   # BR(A -> c       cb     )
     8.55354397E-03    2           6        -6   # BR(A -> t       tb     )
     2.51893685E-05    2          21        21   # BR(A -> g       g      )
     6.71533326E-08    2          22        22   # BR(A -> gam     gam    )
     2.48163329E-08    2          23        22   # BR(A -> Z       gam    )
     7.42021231E-07    2          23        25   # BR(A -> Z       h      )
     9.08284299E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.36007961E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.53768135E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.78607355E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.89822262E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46651536E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.38141485E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.25631264E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.38568678E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.32597730E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.72796170E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.22889226E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.77276027E-07    2          24        25   # BR(H+ -> W+      h      )
     5.19610838E-14    2          24        36   # BR(H+ -> W+      A      )
     4.90621840E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.17732690E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.08177379E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
