﻿include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa VBF-Z production, with Z/gamma* -> tautau."
evgenConfig.keywords = [ "SM", "Z", "tau", "jets", "VBF" ]
evgenConfig.contact  = [ "bill.balunas@cern.ch" ]
evgenConfig.minevents = 10
evgenConfig.inputconfcheck = "Sherpa_CT10_Ztautau2JetsEW1JetQCD15GeVM40"

evgenConfig.process="""

(run){
  %scales, tags for scale variations
  FSCF:=1.0; RSCF:=1.0; QSCF:=1.0;
  QCUT:=20.;
  SCALES=STRICT_METS{FSCF*MU_F2}{RSCF*MU_R2}{QSCF*MU_Q2};
  EXCLUSIVE_CLUSTER_MODE=1;
}(run)

(processes){
  Process 93 93 -> 15 -15 93 93 93{1};
  Order_EW 4;
  CKKW sqr(QCUT/E_CMS);
  Integration_Error 0.05;
  End process;
}(processes)

(selector){
  Mass 15 -15 40 E_CMS
  NJetFinder 2 15.0 0.0 0.4 1
}(selector)

"""
