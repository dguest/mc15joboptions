evgenConfig.description = "UEH subgroup; prompt leptonjet samples"
evgenConfig.keywords = ["BSM", "exotic"]
evgenConfig.generators += ["ParticleDecayer"]
evgenConfig.process = 'FRVZ'
evgenConfig.contact = ["Bernhard.Meirose@cern.ch"]

import ParticleGun as PG
pg = PG.ParticleGun()
genSeq += pg
pg.randomSeed = 123456
pg.sampler.pid = 999
pg.sampler.mom = PG.PtEtaMPhiSampler(pt=[12000, 500000], eta=[-2.5,2.5], phi=[-3.14,3.14])

from ParticleDecayer.ParticleDecayerConf import ParticleDecayer
genSeq += ParticleDecayer()
genSeq.ParticleDecayer.OutputLevel = DEBUG
genSeq.ParticleDecayer.McEventCollection = "GEN_EVENT"


genSeq.ParticleDecayer.LJType = 2
genSeq.ParticleDecayer.OutputLevel = FATAL
genSeq.ParticleDecayer.ScalarMass = 12000
genSeq.ParticleDecayer.ScalarPDGID = 700021
genSeq.ParticleDecayer.ParticleID = 999
genSeq.ParticleDecayer.ParticleMass = 5000
genSeq.ParticleDecayer.ParticleLifeTime = 0
genSeq.ParticleDecayer.ParticlePolarization = 0
genSeq.ParticleDecayer.ParticlePDGID = 700022
genSeq.ParticleDecayer.DecayBRElectrons = 0.33
genSeq.ParticleDecayer.DecayBRMuons     = 0.33
genSeq.ParticleDecayer.DecayBRPions     = 0.34
genSeq.ParticleDecayer.DoUniformDecay               = True
genSeq.ParticleDecayer.DoExponentialDecay           = False
genSeq.ParticleDecayer.ExpDecayDoVariableLifetime   = False
#genSeq.ParticleDecayer.ExpDecayPercentageToKeep     = 0.8
genSeq.ParticleDecayer.ExpDecayDoTruncateLongDecays = False
genSeq.ParticleDecayer.BarrelRadius         = 8.e3
genSeq.ParticleDecayer.EndCapDistance       = 11.e3
genSeq.ParticleDecayer.ThetaEndCapBarrel    = 0.628796286
