mDM=100
Lambda=200

include("MC15JobOptions/MadGraphControl_monoHiggs_xgxFhDh.py")

evgenConfig.description = "xgxFhDh EFT Model for MonoHiggs(h->bb) with Lambda=200GeV and mDM="+str(mDM)+"GeV"
evgenConfig.keywords = [ "Higgs", "BSMHiggs", "BSM"]
evgenConfig.contact = ['Nikola Whallon <alokin@uw.edu>']
