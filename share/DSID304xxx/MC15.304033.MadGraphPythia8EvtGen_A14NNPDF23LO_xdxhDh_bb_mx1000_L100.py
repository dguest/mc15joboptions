mDM=1000
Lambda=100

include("MC15JobOptions/MadGraphControl_monoHiggs_xdxhDh.py")

evgenConfig.description = "xdxhDh EFT Model for MonoHiggs(h->bb) with Lambda=100GeV and mDM="+str(mDM)+"GeV"
evgenConfig.keywords = [ "Higgs", "BSMHiggs", "BSM"]
evgenConfig.contact = ['Nikola Whallon <alokin@uw.edu>']
