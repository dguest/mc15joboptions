evgenConfig.description = "Pythia8 Z'(600 GeV)->qq with A14 tune and NNPDF23LO PDF m=600 GeV"
evgenConfig.keywords = ["exotic","Zprime","BSM"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact = ["Ning.Zhou@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += [
    "NewGaugeBoson:ffbar2gmZZprime = on", #create Z' bosons
    "PhaseSpace:mHatMin = 200.", #lower invariant mass
    "Zprime:gmZmode = 3",
    "32:m0 = 600", #set Z' mass [GeV]
    "32:onMode = off", #switch off all Z' decays
    "32:onIfAny = 1 2 3"] #switch on Z'->uu dd ss decay
