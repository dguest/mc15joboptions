include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

evgenConfig.description = "JHU+Pythia8, Graviton Longitudinal Bulk, mVV = 2000, @LO"
evgenConfig.keywords = ["BSM", "exotic", "spin2", "diboson", "jets" ]
evgenConfig.contact  = [ "sebastian.andres.olivares.pino@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.generators = ["JHU", "Pythia8", "EvtGen"]
evgenConfig.process = "qqbar -> G -> WW -> jet+jet"
evgenConfig.inputfilecheck = 'qqbar_GravitonLongitudinal_WW4q_m2000'
