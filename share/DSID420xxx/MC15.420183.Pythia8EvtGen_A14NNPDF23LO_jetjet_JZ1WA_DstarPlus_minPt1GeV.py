evgenConfig.description = "Dijet truth jet slice JZ1A, with the A14 NNPDF23 LO tune and DstarPlus Filter"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("MC15JobOptions/JetFilter_JZ0W.py")
include("MC15JobOptions/DstarPlusFilter_minPt1GeV.py")


# Standard JZ0 ends at 20 GeV.  We'll increase the slice to 30 so
# we can use hard scattering for slice 1
filtSeq.QCDTruthJetFilter.MinPt = 20*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 40*GeV

evgenConfig.minevents = 100



