###############################################################
#
# Job options file for Hijing (with diffraction) generation of
# p + Pb collisions at 5020 GeV CMS with flow
#
# Andrzej Olszewski
#
# Created: February 2019
#==============================================================

# use common fragment
include("MC15JobOptions/Hijing_Common.py")

evgenConfig.description = "Hijing"
evgenConfig.keywords = ["ND"]
evgenConfig.contact = ["Andrzej.Olszewski@ifj.edu.pl"]

evgenConfig.minevents = 1000

#----------------------
# Hijing Parameters
#----------------------
Hijing = genSeq.Hijing
Hijing.McEventsRW = "HIJING_EVENT"
Hijing.McEventKey = "HIJING_EVENT"
Hijing.Initialize = ["efrm 5020.", "frame CMS", "proj A", "targ P",
                     "iap 208", "izp 82", "iat 1", "izt 1",
# simulation of minimum-bias events
                     "bmin 0", "bmax 10",
# turns OFF jet quenching:
                     "ihpr2 4 0",
# Jan24,06 turns ON decays charm and  bottom but not pi0, lambda, ...
                     "ihpr2 12 2",
# turns ON retaining of particle history - truth information:
                     "ihpr2 21 1",
# turning OFF string radiation
                     "ihpr2 1 0",
# Nov 10,11, set minimum pt for hard scatterings to default, 2 GeV
                     "hipr1 8 2",
# Nov 10,11, turn off diffractive scatterings
                     "ihpr2 13 0"
                     ]

from FlowAfterburner.FlowAfterburnerConf import AddFlowByShifting
genSeq += AddFlowByShifting()

AddFlowByShifting = genSeq.AddFlowByShifting
AddFlowByShifting.McTruthKey    = "HIJING_EVENT"
AddFlowByShifting.McFlowKey     = "FLOW_EVENT"

#"jjia_minbias_new", "jjia_minbias_new_v2only", "fixed_vn", "fixed_v2", "jjia_minbias_old", "ao_test", "custom", "p_Pb_cent_eta_indep"
AddFlowByShifting.FlowFunctionName="p_Pb_cent_eta_indep"
AddFlowByShifting.FlowImplementation="exact" # "approximate" , "exact"

AddFlowByShifting.RandomizePhi  = 0

AddFlowByShifting.FlowEtaSwitch = 0
AddFlowByShifting.FlowMinEtaCut = 0
AddFlowByShifting.FlowMaxEtaCut = 6.5

AddFlowByShifting.FlowPtSwitch  = 0
AddFlowByShifting.FlowMinPtCut  = 0
AddFlowByShifting.FlowMaxPtCut  = 2.0

AddFlowByShifting.custom_v1=0.0000;
AddFlowByShifting.custom_v2=0.0500;
AddFlowByShifting.custom_v3=0.0280;
AddFlowByShifting.custom_v4=0.0130;
AddFlowByShifting.custom_v5=0.0045;
AddFlowByShifting.custom_v6=0.0015;
AddFlowByShifting.FlowBSwitch = 0

from BoostAfterburner.BoostAfterburnerConf import BoostEvent
genSeq += BoostEvent()

boost=genSeq.BoostEvent
boost.BetaZ=-0.4345
boost.McInputKey="FLOW_EVENT"
boost.McOutputKey="GEN_EVENT"
