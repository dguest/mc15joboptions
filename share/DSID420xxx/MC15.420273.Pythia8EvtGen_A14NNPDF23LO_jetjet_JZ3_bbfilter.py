# JO for Pythia 8 jet jet b-flavored J2 slice

evgenConfig.description = "Dijet truth jet slice JZ3, with the A14 NNPDF23 LO tune and di-bijet filter"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 50.",
                            "PhaseSpace:bias2Selection = on",
                            "PhaseSpace:bias2SelectionRef = 160.0",
                            "PhaseSpace:bias2SelectionPow = 5.0"
                            ]

# Makes akt0.4 jets
include("MC15JobOptions/AntiKt4TruthJets_pileup.py")

from GeneratorFilters.GeneratorFiltersConf import DiBjetFilter
filtSeq += DiBjetFilter()
DiBjetFilter = filtSeq.DiBjetFilter
DiBjetFilter.LeadJetPtMin = 160000
DiBjetFilter.LeadJetPtMax = 400000
DiBjetFilter.DiJetPtMin = 0
DiBjetFilter.BottomPtMin = 5.0 * GeV
DiBjetFilter.BottomEtaMax = 3.0
DiBjetFilter.DiJetMassMin = 0
DiBjetFilter.DiJetMassMax = 300 * GeV
DiBjetFilter.JetPtMin = 15.0 * GeV
DiBjetFilter.JetEtaMax = 2.7
DiBjetFilter.DeltaRFromTruth = 0.3
# Use the jets that have been built
DiBjetFilter.TruthContainerName = "AntiKt4TruthJets"
DiBjetFilter.LightJetSuppressionFactor = 10
DiBjetFilter.AcceptSomeLightEvents = False

# Get some events out
evgenConfig.minevents = 500
