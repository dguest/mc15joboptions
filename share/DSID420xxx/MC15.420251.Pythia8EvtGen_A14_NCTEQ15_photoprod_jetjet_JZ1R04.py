evgenConfig.description = "Pythia8 jet photoproduction using nuclear photon flux and NCTEQ15 PDFs with A14 tune"
evgenConfig.keywords = ["QCD","coherent","jets"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# This AthenaTool will be used to set the Photon Flux
# =====================================================
# You can find the code in Generators/Pythia8_i/src/Pythia8Custom/
#  I included one property (Process) that can be set from the JobOptions
#  you can of course add others
from Pythia8_i.Pythia8_iConf import UserPhotonFlux
photonFluxTool=UserPhotonFlux()
photonFluxTool.Process=2
photonFluxTool.NuclearCharge=82;
photonFluxTool.MinimumB=2.*6.62;


from AthenaCommon.AppMgr import ToolSvc
ToolSvc += UserPhotonFlux()
genSeq.Pythia8.CustomInterface = photonFluxTool

#options to set up gamma* p collisions
genSeq.Pythia8.Beam1='MUON'
genSeq.Pythia8.Beam2='PROTON'
genSeq.Pythia8.Commands += [
"Photon:Q2max = 1.0",
"Photon:ProcessType = 0",
"Photon:sampleQ2 = off",
"PDF:lepton2gamma = on",
"PDF:lepton2gammaSet = 2",
"MultipartonInteractions:pT0Ref = 3.0"
]

#use NCTEQ nuclear PDFs
genSeq.Pythia8.Commands += ["PDF:pSet = LHAPDF6:nCTEQ15npFullNuc_208_82"]


#hard scattering details
genSeq.Pythia8.Commands +=["HardQCD:all = on",
                           "PhotonParton:all = on",
                           "PhaseSpace:pTHatMin = 7.0",
                           "Photon:Wmin  = 7.0"] #W>= pThatmin

include("MC15JobOptions/JetFilter_JZ1R04.py")
