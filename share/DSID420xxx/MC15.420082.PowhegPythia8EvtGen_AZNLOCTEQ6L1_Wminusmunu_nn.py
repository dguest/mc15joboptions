#--------------------------------------------------------------
# Powheg W setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_W_Common.py')
PowhegConfig.idvecbos   = -24 # W-
PowhegConfig.beam_1_type = 2  # n
PowhegConfig.beam_2_type = 2  # n
PowhegConfig.vdecaymode = 2  # munu

# Configure Powheg setup
PowhegConfig.withdamp   = 1
PowhegConfig.ptsqmin    = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 1.2 # increase number of generated events by 20%
PowhegConfig.running_width = 1

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 nn -> Wmin->munu production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Andrzej Olszewski <Andrzej.Olszewski@ifj.edu.pl>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'W', 'drellYan', 'muon', 'neutrino' ]
evgenConfig.minevents = 1000
