evgenConfig.description = "SuperChic 3.0 MC gammagamma to gammagamma at 5020 GeV, M_gg > 3.6 GeV, pT(gamma)>1.8GeV, |eta(gamma)|<2.6"
evgenConfig.keywords = ["2photon"]
#evgenConfig.weighting = 0
evgenConfig.contact = ["mateusz.dyndal@cern.ch"]

# TODO: Sort out proper param setting based on runArgs.ecmEnergy
if int(runArgs.ecmEnergy) != 5020:
    evgenLog.error("This JO can currently only be run for a beam energy of 5020 GeV")
    sys.exit(1)


evgenConfig.inputfilecheck = 'ggTOgg_5TeV_M3p6'

include("MC15JobOptions/HepMCReadFromFile_Common.py")

evgenConfig.tune = "none"
