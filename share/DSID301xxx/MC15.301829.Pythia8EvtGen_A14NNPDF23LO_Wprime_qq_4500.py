# W' -> qq                                                                                                                                                               
# Wprime Mass (in GeV)                                                                                                                                                                                     
M_Wprime = 4500.0

evgenConfig.description = "Wprime->qq "+str(M_Wprime)+" GeV with NNPDF23LO PDF and A14 tune"
evgenConfig.keywords = ["exotic", "Wprime", "jets","BSM"]
evgenConfig.contact     = [ 'jsearcy@umich.edu' ]
evgenConfig.process = "pp>Wprime>qq"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on" # Allow Wprime
                           ,"Wprime:coup2WZ = 0."             # Wprime Coupling to WZ
                           ,"34:m0 ="+str(M_Wprime)           # Wprime mass
                           ,"34:onMode = off"                 # Turn off decay modes
                           ,"34:onIfAny = 1 2 3 4 5 6"        # Turn on ->qq 
                           ]
