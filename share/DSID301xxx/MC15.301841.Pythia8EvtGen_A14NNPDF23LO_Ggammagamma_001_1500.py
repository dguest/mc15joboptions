######################################################################
# Graviton to gam gam decay with Pythia 8
######################################################################

evgenConfig.description = "RS Graviton, m = 1500 GeV, kappaMG = 0.054."
evgenConfig.keywords = ["exotic", "graviton","diphoton","RandallSundrum","BSM"]
evgenConfig.contact = ["simone.mazza@mi.infn.it","jan.stark@cern.ch"]
evgenConfig.process = "RS Graviton -> gam gam"

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands +=[
                           "ExtraDimensionsG*:gg2G* = on",
                           "ExtraDimensionsG*:ffbar2G* = on",
                           "5100039:m0 = 1500.",
                           "5100039:onMode = off",
                           "5100039:onIfAny = 22",
                           "ExtraDimensionsG*:kappaMG = 0.054"
                           ]
