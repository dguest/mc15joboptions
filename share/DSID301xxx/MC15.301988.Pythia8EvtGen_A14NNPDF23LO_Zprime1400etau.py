###############################################################
#
# Job options file
# Pythia8 Z'->etau
# May 2015
#===============================================================

evgenConfig.description = "Zprime(1400)->etau production with the A14 NNPDF23LO tune"
evgenConfig.process = "Zprime -> e+- tau-+"
evgenConfig.contact = [ "quli@cern.ch" ]
evgenConfig.keywords = [ "electron", "tau", "exotic", "Zprime" ]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",
				"Zprime:gmZmode = 3",
				"32:m0 = 1400.0", # set Z' mass
				"32:8:products = 11 -15",
				"32:10:products = 15 -11",
				"32:0:onMode = 0",
				"32:1:onMode = 0",
				"32:2:onMode = 0",
				"32:3:onMode = 0",
				"32:4:onMode = 0",
				"32:5:onMode = 0",
				"32:6:onMode = 0",
				"32:7:onMode = 0",
				"32:9:onMode = 0",
				"32:11:onMode = 0",
				"32:12:onMode = 0"]


#==============================================================
#
# End of job options file
#
###############################################################
