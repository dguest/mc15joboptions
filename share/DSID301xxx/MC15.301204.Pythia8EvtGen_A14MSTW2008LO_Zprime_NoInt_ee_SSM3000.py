# Zprime resonance mass (in GeV)
ZprimeMass = 3000

include('MC15JobOptions/nonStandard/Pythia8_A14_MSTW2008LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",  # create Z' bosons
                            "Zprime:gmZmode = 3",                  # Z',Z,g with interference
                            "32:onMode = off",                     # switch off all Z decays
                            "32:onIfAny  = 11",                    # switch on Z->ee decays
                            "32:m0 = "+str(ZprimeMass)]


# EVGEN configuration
evgenConfig.description = 'Pythia 8 Zprime decaying to two electrons'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords    = [ 'BSM', 'Zprime', 'heavyBoson', 'SSM', 'resonance', 'electroweak', '2electron' ]
evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.process = "pp>Zprime>ee"
