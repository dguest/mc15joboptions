include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on",
                            "23:onMode = off",                    # turn off all decays modes
                            "23:onIfAny = 13",               # turn on the mumu decay mode
                            "PhaseSpace:mHatMin = 2000.",       # lower invariant mass
                            "PhaseSpace:mHatMax = 2250."]       # upper invariant mass

evgenConfig.description = "Pythia 8 DY->mumu production with NNPDF23LO tune"
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords = ["SM", "drellYan", "electroweak", "2muon"]
evgenConfig.generators += ["Pythia8"]

