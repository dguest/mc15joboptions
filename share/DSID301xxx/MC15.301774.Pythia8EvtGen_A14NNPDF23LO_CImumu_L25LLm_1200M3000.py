include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["ContactInteractions:QCffbar2mumubar = on"] # process
genSeq.Pythia8.Commands += ["ContactInteractions:Lambda = 25000"] # mass scale
genSeq.Pythia8.Commands += ["ContactInteractions:etaLL = -1"] # interference
genSeq.Pythia8.Commands += ["PhaseSpace:mHatMin = 1200"] # min invariant mass
genSeq.Pythia8.Commands += ["PhaseSpace:mHatMax = 3000"] # max invariant mass

evgenConfig.description = "Pythia 8 CI->mumu production with NNPDF23LO tune"
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords = ["BSM", "contactInteraction", "electroweak", "2muon"]
evgenConfig.generators += ["Pythia8"]

