#--------------------------------------------------------------
# Author 1: S. Ask (Cambrdige U.), 21 May 2012
# Author 2: D. Hayden (RHUL.), 3rd August 2012
# Modifications for Wprime->e,mu flat sample at sqrt(s) = 8 TeV by Nikos Tsirintanis, Mihail Chizhov 18th September 2013
# Modifications for Wprime->e,mu flat sample at sqrt(s) = 13 TeV by Nikos Tsirintanis, Mihail Chizhov 16th January 2015
#--------------------------------------------------------------

# EVGEN configuration
evgenConfig.description = "Wprime->mu + nu production with A14 NNPDF23LO tune"
evgenConfig.process = "W' -> mu + nu"
evgenConfig.contact = ["Nikolaos.Tsirintanis@cern.ch"] 
evgenConfig.keywords    = [ 'BSM', 'Wprime', 'heavyBoson', 'SSM', 'resonance', 'electroweak', 'lepton', 'neutrino' ]
evgenConfig.generators += [ 'Pythia8' ]

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on",  # create W' bosons
                            "PhaseSpace:mHatMin = 25.0",         # minimum inv.mass cut
                            "34:onMode = off",                     # switch off all W' decays
                            "34:onIfAny = 13,14",                  # switch on W'->munu decays
                            "34:m0 = 1000.0"]

genSeq.Pythia8.UserHook = "WprimeFlat"
genSeq.Pythia8.UserModes += ["WprimeFlat:EnergyMode = 13"]
