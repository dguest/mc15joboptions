#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.84893606E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.15959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.76900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.30485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     3.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04200148E+01   # W+
        25     1.24182019E+02   # h
        35     4.00000609E+03   # H
        36     3.99999565E+03   # A
        37     4.00096677E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01056358E+03   # ~d_L
   2000001     4.01055268E+03   # ~d_R
   1000002     4.00990892E+03   # ~u_L
   2000002     4.01084021E+03   # ~u_R
   1000003     4.01056358E+03   # ~s_L
   2000003     4.01055268E+03   # ~s_R
   1000004     4.00990892E+03   # ~c_L
   2000004     4.01084021E+03   # ~c_R
   1000005     2.32200522E+03   # ~b_1
   2000005     4.01798040E+03   # ~b_2
   1000006     3.80748354E+02   # ~t_1
   2000006     2.33016487E+03   # ~t_2
   1000011     4.00103061E+03   # ~e_L
   2000011     4.00387486E+03   # ~e_R
   1000012     3.99991790E+03   # ~nu_eL
   1000013     4.00103061E+03   # ~mu_L
   2000013     4.00387486E+03   # ~mu_R
   1000014     3.99991790E+03   # ~nu_muL
   1000015     4.00353903E+03   # ~tau_1
   2000015     4.00972399E+03   # ~tau_2
   1000016     4.00271226E+03   # ~nu_tauL
   1000021     1.96900763E+03   # ~g
   1000022     2.42120689E+02   # ~chi_10
   1000023    -2.86850260E+02   # ~chi_20
   1000025     3.06939381E+02   # ~chi_30
   1000035     2.07145810E+03   # ~chi_40
   1000024     2.83725180E+02   # ~chi_1+
   1000037     2.07162384E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.79810209E-01   # N_11
  1  2    -1.67800774E-02   # N_12
  1  3    -4.77365137E-01   # N_13
  1  4    -4.04644279E-01   # N_14
  2  1    -6.07705223E-02   # N_21
  2  2     2.53477483E-02   # N_22
  2  3    -7.01599504E-01   # N_23
  2  4     7.09522777E-01   # N_24
  3  1     6.23058589E-01   # N_31
  3  2     2.50586561E-02   # N_32
  3  3     5.29026003E-01   # N_33
  3  4     5.75588001E-01   # N_34
  4  1    -9.88106427E-04   # N_41
  4  2     9.99223691E-01   # N_42
  4  3    -3.48564292E-03   # N_43
  4  4    -3.92286824E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.93470975E-03   # U_11
  1  2     9.99987824E-01   # U_12
  2  1    -9.99987824E-01   # U_21
  2  2     4.93470975E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.54895596E-02   # V_11
  1  2    -9.98459267E-01   # V_12
  2  1    -9.98459267E-01   # V_21
  2  2    -5.54895596E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -6.25555518E-02   # cos(theta_t)
  1  2     9.98041484E-01   # sin(theta_t)
  2  1    -9.98041484E-01   # -sin(theta_t)
  2  2    -6.25555518E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999175E-01   # cos(theta_b)
  1  2    -1.28452299E-03   # sin(theta_b)
  2  1     1.28452299E-03   # -sin(theta_b)
  2  2     9.99999175E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05367574E-01   # cos(theta_tau)
  1  2     7.08841721E-01   # sin(theta_tau)
  2  1    -7.08841721E-01   # -sin(theta_tau)
  2  2    -7.05367574E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00245745E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  8.48936056E+02  # DRbar Higgs Parameters
         1    -2.76900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44822931E+02   # higgs               
         4     1.63565287E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  8.48936056E+02  # The gauge couplings
     1     3.60713346E-01   # gprime(Q) DRbar
     2     6.34947665E-01   # g(Q) DRbar
     3     1.03880389E+00   # g3(Q) DRbar
#
BLOCK AU Q=  8.48936056E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     4.86176232E-07   # A_c(Q) DRbar
  3  3     2.15959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  8.48936056E+02  # The trilinear couplings
  1  1     1.79131185E-07   # A_d(Q) DRbar
  2  2     1.79148660E-07   # A_s(Q) DRbar
  3  3     3.19982060E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  8.48936056E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.00204524E-08   # A_mu(Q) DRbar
  3  3     4.04215520E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  8.48936056E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.73233614E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  8.48936056E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86718492E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  8.48936056E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03089008E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  8.48936056E+02  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57826169E+07   # M^2_Hd              
        22    -1.68285602E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.30485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     3.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39856837E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.76136902E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.13909802E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     7.44297816E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     7.92622107E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.20583161E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.39697184E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.45841814E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.76049275E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.08787844E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.83847855E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.91486900E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.22180559E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     7.32746878E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.33167354E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.17337643E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.31895264E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     5.30545974E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.37382154E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.46335988E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.88937467E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.11671534E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.63832095E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.64323201E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.78433735E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53359923E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.97817722E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.60454828E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.18079984E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14081342E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     5.60037893E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     7.32031761E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.80208853E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.28701348E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.74860491E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.50644630E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.67955580E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.79168413E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77847643E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.40102215E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.54480700E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52912137E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58028797E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.39551370E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.05638205E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.15853996E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.96936080E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44253796E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.74878856E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.37813868E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.19115345E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.56196594E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78346986E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.27173810E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.57688305E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53138749E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51397220E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.85995810E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.36574232E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.63230304E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.74720341E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85454074E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.74860491E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.50644630E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.67955580E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.79168413E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77847643E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.40102215E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.54480700E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52912137E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58028797E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.39551370E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.05638205E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.15853996E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.96936080E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44253796E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.74878856E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.37813868E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.19115345E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.56196594E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78346986E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.27173810E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.57688305E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53138749E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51397220E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.85995810E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.36574232E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.63230304E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.74720341E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85454074E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.09374550E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.35327825E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.32204021E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.36301435E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77120590E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.50056223E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55648258E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05412510E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.09163555E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.68853902E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.87147378E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.28403148E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.09374550E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.35327825E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.32204021E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.36301435E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77120590E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.50056223E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55648258E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05412510E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.09163555E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.68853902E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.87147378E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.28403148E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06611353E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27862179E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.36125399E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.56191685E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38895137E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.49423560E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78496103E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06928150E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.23242705E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.33367071E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.36886770E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41546579E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.11763079E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83810931E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.09487690E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.08771184E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.83884255E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.54093463E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77452154E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.15976629E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53368707E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.09487690E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.08771184E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.83884255E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.54093463E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77452154E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.15976629E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53368707E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.42482645E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.83617891E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.66287236E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.01069896E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51154168E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.77958940E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00918287E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.52236739E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33663536E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33663536E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11222515E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11222515E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10227898E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.67025253E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.92422502E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.48076827E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.25855194E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.42553557E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.68168239E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.39373607E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.22080092E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.70951180E-06    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18580045E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53782961E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18580045E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53782961E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38547576E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52443855E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52443855E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48530332E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04556136E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04556136E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04556104E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.84165653E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.84165653E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.84165653E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.84165653E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.13886215E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.13886215E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.13886215E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.13886215E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.19724298E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.19724298E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.32179506E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.72689874E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.03474531E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.58230494E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.03941077E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.58230494E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.03941077E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.94852507E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     4.57561836E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     4.57561836E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.56117632E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     9.39250243E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     9.39250243E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     9.39244554E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.62926387E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.11303098E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.62926387E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.11303098E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.24263736E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     4.84323623E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.84323623E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.63171802E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     9.68019565E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     9.68019565E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     9.68019591E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.15022417E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.15022417E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.15022417E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.15022417E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.83402733E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.83402733E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.83402733E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.83402733E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.72461607E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.72461607E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.66946895E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.29498252E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.46667407E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.84360799E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.42033220E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.42033220E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.99048003E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.86507854E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.00341946E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.44913577E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.44913577E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     3.88119516E-03   # h decays
#          BR         NDA      ID1       ID2
     6.15264149E-01    2           5        -5   # BR(h -> b       bb     )
     6.63977383E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.35053718E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.99540395E-04    2           3        -3   # BR(h -> s       sb     )
     2.16976605E-02    2           4        -4   # BR(h -> c       cb     )
     7.11333465E-02    2          21        21   # BR(h -> g       g      )
     2.34199275E-03    2          22        22   # BR(h -> gam     gam    )
     1.49630532E-03    2          22        23   # BR(h -> Z       gam    )
     1.96610781E-01    2          24       -24   # BR(h -> W+      W-     )
     2.43234322E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43931053E+01   # H decays
#          BR         NDA      ID1       ID2
     3.50846890E-01    2           5        -5   # BR(H -> b       bb     )
     6.09562583E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15526565E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54930774E-04    2           3        -3   # BR(H -> s       sb     )
     7.14969052E-08    2           4        -4   # BR(H -> c       cb     )
     7.16547846E-03    2           6        -6   # BR(H -> t       tb     )
     8.19802896E-07    2          21        21   # BR(H -> g       g      )
     3.53293033E-10    2          22        22   # BR(H -> gam     gam    )
     1.84542233E-09    2          23        22   # BR(H -> Z       gam    )
     1.68698509E-06    2          24       -24   # BR(H -> W+      W-     )
     8.42908310E-07    2          23        23   # BR(H -> Z       Z      )
     6.22308524E-06    2          25        25   # BR(H -> h       h      )
     2.40330335E-24    2          36        36   # BR(H -> A       A      )
     6.74380011E-20    2          23        36   # BR(H -> Z       A      )
     1.87640964E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61859322E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61859322E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.83565145E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.95607381E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.77344051E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.08642244E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.37644424E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.68517686E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.73519257E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.00173910E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.54473276E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.80928557E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.43983957E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.43983957E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.43906317E+01   # A decays
#          BR         NDA      ID1       ID2
     3.50888588E-01    2           5        -5   # BR(A -> b       bb     )
     6.09593232E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15537232E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54984977E-04    2           3        -3   # BR(A -> s       sb     )
     7.19684914E-08    2           4        -4   # BR(A -> c       cb     )
     7.18014850E-03    2           6        -6   # BR(A -> t       tb     )
     1.47533938E-05    2          21        21   # BR(A -> g       g      )
     5.50613879E-08    2          22        22   # BR(A -> gam     gam    )
     1.62259009E-08    2          23        22   # BR(A -> Z       gam    )
     1.68362465E-06    2          23        25   # BR(A -> Z       h      )
     1.88131138E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61867549E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61867549E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.42866871E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.11980451E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.46060493E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.60596370E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.78646975E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.72055950E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.97243284E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.97594005E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.53476102E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.48642354E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.48642354E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.43083153E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.59910048E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.10665429E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15916335E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.58342123E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22320877E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51653380E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.56765458E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.68799500E-06    2          24        25   # BR(H+ -> W+      h      )
     2.09282832E-14    2          24        36   # BR(H+ -> W+      A      )
     4.99668215E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.58808466E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.83606909E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.62316809E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.42749163E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60107773E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.10765695E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.01484712E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
