#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13972904E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03983737E+01   # W+
        25     1.24322727E+02   # h
        35     3.00016571E+03   # H
        36     2.99999970E+03   # A
        37     3.00093559E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03143187E+03   # ~d_L
   2000001     3.02622575E+03   # ~d_R
   1000002     3.03051374E+03   # ~u_L
   2000002     3.02780177E+03   # ~u_R
   1000003     3.03143187E+03   # ~s_L
   2000003     3.02622575E+03   # ~s_R
   1000004     3.03051374E+03   # ~c_L
   2000004     3.02780177E+03   # ~c_R
   1000005     7.36526749E+02   # ~b_1
   2000005     3.02554721E+03   # ~b_2
   1000006     7.34517434E+02   # ~t_1
   2000006     3.01247186E+03   # ~t_2
   1000011     3.00654038E+03   # ~e_L
   2000011     3.00157086E+03   # ~e_R
   1000012     3.00514316E+03   # ~nu_eL
   1000013     3.00654038E+03   # ~mu_L
   2000013     3.00157086E+03   # ~mu_R
   1000014     3.00514316E+03   # ~nu_muL
   1000015     2.98583971E+03   # ~tau_1
   2000015     3.02206742E+03   # ~tau_2
   1000016     3.00510115E+03   # ~nu_tauL
   1000021     2.33864186E+03   # ~g
   1000022     1.50862488E+02   # ~chi_10
   1000023     3.20251424E+02   # ~chi_20
   1000025    -2.99917245E+03   # ~chi_30
   1000035     2.99973117E+03   # ~chi_40
   1000024     3.20413230E+02   # ~chi_1+
   1000037     3.00038354E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888315E-01   # N_11
  1  2    -1.15293909E-03   # N_12
  1  3     1.48271086E-02   # N_13
  1  4    -1.47804179E-03   # N_14
  2  1     1.54922099E-03   # N_21
  2  2     9.99643825E-01   # N_22
  2  3    -2.63512522E-02   # N_23
  2  4     3.92860540E-03   # N_24
  3  1    -9.41800649E-03   # N_31
  3  2     1.58683684E-02   # N_32
  3  3     7.06841728E-01   # N_33
  3  4     7.07131011E-01   # N_34
  4  1    -1.15003519E-02   # N_41
  4  2     2.14263421E-02   # N_42
  4  3     7.06725222E-01   # N_43
  4  4    -7.07070092E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99304616E-01   # U_11
  1  2    -3.72865155E-02   # U_12
  2  1     3.72865155E-02   # U_21
  2  2     9.99304616E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99984542E-01   # V_11
  1  2    -5.56014098E-03   # V_12
  2  1     5.56014098E-03   # V_21
  2  2     9.99984542E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98856716E-01   # cos(theta_t)
  1  2    -4.78044025E-02   # sin(theta_t)
  2  1     4.78044025E-02   # -sin(theta_t)
  2  2     9.98856716E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99917537E-01   # cos(theta_b)
  1  2     1.28420871E-02   # sin(theta_b)
  2  1    -1.28420871E-02   # -sin(theta_b)
  2  2     9.99917537E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06919987E-01   # cos(theta_tau)
  1  2     7.07293526E-01   # sin(theta_tau)
  2  1    -7.07293526E-01   # -sin(theta_tau)
  2  2     7.06919987E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01864207E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39729042E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44408679E+02   # higgs               
         4     7.20342088E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39729042E+03  # The gauge couplings
     1     3.61935852E-01   # gprime(Q) DRbar
     2     6.38854486E-01   # g(Q) DRbar
     3     1.02795134E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39729042E+03  # The trilinear couplings
  1  1     1.83242431E-06   # A_u(Q) DRbar
  2  2     1.83245246E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39729042E+03  # The trilinear couplings
  1  1     4.64564210E-07   # A_d(Q) DRbar
  2  2     4.64654913E-07   # A_s(Q) DRbar
  3  3     1.06207114E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39729042E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.00980904E-07   # A_mu(Q) DRbar
  3  3     1.02195047E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39729042E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53512393E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39729042E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.12712900E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39729042E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05638089E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39729042E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.49354126E+04   # M^2_Hd              
        22    -9.08329265E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41678674E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.64876353E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48243500E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48243500E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51756500E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51756500E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.86085897E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.51839111E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.92840566E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.91975522E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.03508960E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.34205207E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.03475134E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.09376512E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.75735985E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.26272563E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.59515448E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.50954663E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.98896379E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.60506815E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.84277175E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.64401167E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.17171115E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.23078810E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.91954898E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.39945937E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.01985366E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.94137811E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.71735712E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28754312E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.91481836E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.21705616E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.07111906E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.02584666E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.84150028E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63169195E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.63322782E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.94519136E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.26362756E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.71149559E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.04626498E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18772790E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58591809E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.74593712E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.86331734E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.41390759E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41407802E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.03093499E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.98347703E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62913217E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.70715539E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.67928876E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.25788971E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.24388248E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.05314037E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67685466E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.51323622E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.06600726E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.49610445E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.14205867E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54867528E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.02584666E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.84150028E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63169195E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.63322782E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.94519136E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.26362756E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.71149559E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.04626498E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18772790E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58591809E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.74593712E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.86331734E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.41390759E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41407802E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.03093499E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.98347703E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62913217E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.70715539E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.67928876E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.25788971E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.24388248E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.05314037E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67685466E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.51323622E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.06600726E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.49610445E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.14205867E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54867528E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96515735E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.78844273E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01197986E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.18355912E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.40105862E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00917571E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.43283470E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55624858E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997641E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35817063E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.27548252E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.99664614E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.96515735E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.78844273E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01197986E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.18355912E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.40105862E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00917571E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.43283470E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55624858E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997641E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35817063E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.27548252E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.99664614E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.78938294E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.48372705E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17573397E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.34053897E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.73000589E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.56624209E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14831840E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.04050988E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.18167804E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28509895E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.18339740E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96546453E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.86306064E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99972898E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.17986933E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.11059247E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01396490E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.90573648E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96546453E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.86306064E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99972898E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.17986933E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.11059247E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01396490E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.90573648E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96574360E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.86222731E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99947403E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.14921088E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.06235868E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01429731E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.88089060E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.61779329E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.62295964E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.68281623E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.23104975E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.34593916E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.92445923E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.14662525E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.75984580E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.17570253E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.89336275E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.90792798E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.50920720E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.64928303E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.32564345E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.44956257E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.72719512E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.72719512E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.14922073E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.48288718E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.64375512E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.64375512E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.24869998E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.24869998E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.60570324E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.60570324E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.56138532E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.98214064E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.46791336E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.79850301E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.79850301E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.38191686E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.70860825E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.61983881E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.61983881E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.27471823E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.27471823E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.43556055E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.43556055E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.77787726E-03   # h decays
#          BR         NDA      ID1       ID2
     6.84474301E-01    2           5        -5   # BR(h -> b       bb     )
     5.43475648E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.92394532E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.08755705E-04    2           3        -3   # BR(h -> s       sb     )
     1.76413591E-02    2           4        -4   # BR(h -> c       cb     )
     5.69640430E-02    2          21        21   # BR(h -> g       g      )
     1.92409786E-03    2          22        22   # BR(h -> gam     gam    )
     1.23022627E-03    2          22        23   # BR(h -> Z       gam    )
     1.62702607E-01    2          24       -24   # BR(h -> W+      W-     )
     2.01146500E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39661473E+01   # H decays
#          BR         NDA      ID1       ID2
     7.43946793E-01    2           5        -5   # BR(H -> b       bb     )
     1.78058487E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.29572259E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.73113711E-04    2           3        -3   # BR(H -> s       sb     )
     2.18195469E-07    2           4        -4   # BR(H -> c       cb     )
     2.17664087E-02    2           6        -6   # BR(H -> t       tb     )
     3.09426304E-05    2          21        21   # BR(H -> g       g      )
     2.53584826E-08    2          22        22   # BR(H -> gam     gam    )
     8.36222892E-09    2          23        22   # BR(H -> Z       gam    )
     3.28504708E-05    2          24       -24   # BR(H -> W+      W-     )
     1.64049797E-05    2          23        23   # BR(H -> Z       Z      )
     8.54812447E-05    2          25        25   # BR(H -> h       h      )
    -3.18405881E-23    2          36        36   # BR(H -> A       A      )
     9.98449843E-18    2          23        36   # BR(H -> Z       A      )
     2.32146216E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.12065705E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.15683735E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.23128226E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.03401392E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.06216939E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32670715E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83213881E-01    2           5        -5   # BR(A -> b       bb     )
     1.87435016E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.62724501E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.13963314E-04    2           3        -3   # BR(A -> s       sb     )
     2.29688909E-07    2           4        -4   # BR(A -> c       cb     )
     2.29003323E-02    2           6        -6   # BR(A -> t       tb     )
     6.74392937E-05    2          21        21   # BR(A -> g       g      )
     3.13755781E-08    2          22        22   # BR(A -> gam     gam    )
     6.64024202E-08    2          23        22   # BR(A -> Z       gam    )
     3.44502507E-05    2          23        25   # BR(A -> Z       h      )
     2.63816428E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21577536E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31461172E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.97511594E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.00584691E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09532271E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.47302994E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.74403065E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.01005429E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.13867772E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.05719125E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.13237909E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.55107024E-05    2          24        25   # BR(H+ -> W+      h      )
     9.39260345E-14    2          24        36   # BR(H+ -> W+      A      )
     2.05011072E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.90217776E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.42781571E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
