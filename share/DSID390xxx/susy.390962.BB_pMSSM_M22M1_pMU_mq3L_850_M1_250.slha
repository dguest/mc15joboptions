#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15954589E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04019294E+01   # W+
        25     1.25064689E+02   # h
        35     3.00012449E+03   # H
        36     2.99999987E+03   # A
        37     3.00092332E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03884235E+03   # ~d_L
   2000001     3.03374745E+03   # ~d_R
   1000002     3.03793651E+03   # ~u_L
   2000002     3.03485762E+03   # ~u_R
   1000003     3.03884235E+03   # ~s_L
   2000003     3.03374745E+03   # ~s_R
   1000004     3.03793651E+03   # ~c_L
   2000004     3.03485762E+03   # ~c_R
   1000005     9.50038279E+02   # ~b_1
   2000005     3.03278973E+03   # ~b_2
   1000006     9.45951920E+02   # ~t_1
   2000006     3.01652185E+03   # ~t_2
   1000011     3.00628606E+03   # ~e_L
   2000011     3.00204295E+03   # ~e_R
   1000012     3.00489959E+03   # ~nu_eL
   1000013     3.00628606E+03   # ~mu_L
   2000013     3.00204295E+03   # ~mu_R
   1000014     3.00489959E+03   # ~nu_muL
   1000015     2.98561882E+03   # ~tau_1
   2000015     3.02196198E+03   # ~tau_2
   1000016     3.00467837E+03   # ~nu_tauL
   1000021     2.34804329E+03   # ~g
   1000022     2.51120524E+02   # ~chi_10
   1000023     5.28374670E+02   # ~chi_20
   1000025    -2.99604580E+03   # ~chi_30
   1000035     2.99680029E+03   # ~chi_40
   1000024     5.28537659E+02   # ~chi_1+
   1000037     2.99735710E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886588E-01   # N_11
  1  2    -8.46414877E-04   # N_12
  1  3     1.49057581E-02   # N_13
  1  4    -1.97810507E-03   # N_14
  2  1     1.25774796E-03   # N_21
  2  2     9.99622269E-01   # N_22
  2  3    -2.68427834E-02   # N_23
  2  4     5.76214319E-03   # N_24
  3  1    -9.12593448E-03   # N_31
  3  2     1.49166395E-02   # N_32
  3  3     7.06861945E-01   # N_33
  3  4     7.07135349E-01   # N_34
  4  1    -1.19141232E-02   # N_41
  4  2     2.30672211E-02   # N_42
  4  3     7.06684848E-01   # N_43
  4  4    -7.07051966E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99278661E-01   # U_11
  1  2    -3.79757391E-02   # U_12
  2  1     3.79757391E-02   # U_21
  2  2     9.99278661E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99966762E-01   # V_11
  1  2    -8.15321998E-03   # V_12
  2  1     8.15321998E-03   # V_21
  2  2     9.99966762E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98507518E-01   # cos(theta_t)
  1  2    -5.46144349E-02   # sin(theta_t)
  2  1     5.46144349E-02   # -sin(theta_t)
  2  2     9.98507518E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99910512E-01   # cos(theta_b)
  1  2     1.33778919E-02   # sin(theta_b)
  2  1    -1.33778919E-02   # -sin(theta_b)
  2  2     9.99910512E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06948441E-01   # cos(theta_tau)
  1  2     7.07265086E-01   # sin(theta_tau)
  2  1    -7.07265086E-01   # -sin(theta_tau)
  2  2     7.06948441E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01776431E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.59545887E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44038656E+02   # higgs               
         4     7.51236384E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.59545887E+03  # The gauge couplings
     1     3.62381638E-01   # gprime(Q) DRbar
     2     6.37874810E-01   # g(Q) DRbar
     3     1.02498296E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.59545887E+03  # The trilinear couplings
  1  1     2.42776297E-06   # A_u(Q) DRbar
  2  2     2.42779659E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.59545887E+03  # The trilinear couplings
  1  1     6.38302514E-07   # A_d(Q) DRbar
  2  2     6.38418100E-07   # A_s(Q) DRbar
  3  3     1.42438334E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.59545887E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.33397248E-07   # A_mu(Q) DRbar
  3  3     1.34983001E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.59545887E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51929700E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.59545887E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.14165299E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.59545887E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06716286E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.59545887E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.39377616E+04   # M^2_Hd              
        22    -9.05204648E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41228840E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.67840700E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47718428E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47718428E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52281572E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52281572E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.44238501E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.06432806E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.96508709E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.82848011E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.12912733E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.78799888E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.79636009E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.62853233E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.56241227E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.95882735E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69976505E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.60490078E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.14562580E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.29226590E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.34364729E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.56488405E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.20075122E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.19792614E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.93859586E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.43534637E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.01956734E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.96304154E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.22539512E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30460512E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.87300878E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.16045197E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.97788806E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.87890919E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.96909529E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.60427396E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.55288804E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.13632391E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.20911017E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.83450009E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.12692387E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16417734E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59017894E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.40331194E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99939050E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.64053058E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40981839E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.88400593E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.07419710E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.60210789E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.23438205E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     9.46852927E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.20341996E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.14993625E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.13372466E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65855876E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.52311534E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.83575804E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.69032076E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.40878814E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54768771E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.87890919E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.96909529E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.60427396E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.55288804E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.13632391E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.20911017E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.83450009E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.12692387E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16417734E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59017894E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.40331194E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99939050E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.64053058E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40981839E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.88400593E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.07419710E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.60210789E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.23438205E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     9.46852927E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.20341996E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.14993625E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.13372466E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65855876E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.52311534E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.83575804E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.69032076E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.40878814E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54768771E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.81160202E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01269734E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00007496E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.39845966E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.36646182E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98722733E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.23926560E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54636641E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998490E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.50678651E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.34585938E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.75344433E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.81160202E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01269734E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00007496E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.39845966E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.36646182E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98722733E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.23926560E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54636641E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998490E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.50678651E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.34585938E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.75344433E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.70640951E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.56741428E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14738632E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28519940E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.64922972E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.65208034E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.11917722E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.39638826E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.55589215E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.22828830E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.58903917E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.81182306E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01820917E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98978033E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.46362995E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     8.23123875E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99201036E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.06633916E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.81182306E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01820917E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98978033E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.46362995E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     8.23123875E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99201036E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.06633916E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.81184754E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01812556E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98951294E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.24395802E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.78882807E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99234581E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.55747094E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.81226984E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.14500597E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.74099485E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.09383442E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.72077102E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.28931332E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.27354175E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.10375734E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.65401135E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.44010326E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.03386381E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.49661362E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.17166496E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.47688644E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.11400810E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.06917662E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.06917662E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.97348516E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.18070336E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.58521290E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.58521290E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.24419237E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.24419237E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.88458651E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.88458651E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.08041044E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.76468792E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.16617775E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.15256425E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.15256425E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.55245603E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.50132855E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.55251350E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.55251350E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.27403969E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.27403969E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.54904527E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.54904527E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.81722635E-03   # h decays
#          BR         NDA      ID1       ID2
     6.70277534E-01    2           5        -5   # BR(h -> b       bb     )
     5.42071663E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.91894735E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.07223294E-04    2           3        -3   # BR(h -> s       sb     )
     1.75812530E-02    2           4        -4   # BR(h -> c       cb     )
     5.72195195E-02    2          21        21   # BR(h -> g       g      )
     1.95853865E-03    2          22        22   # BR(h -> gam     gam    )
     1.30477693E-03    2          22        23   # BR(h -> Z       gam    )
     1.74948557E-01    2          24       -24   # BR(h -> W+      W-     )
     2.19035363E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40896848E+01   # H decays
#          BR         NDA      ID1       ID2
     7.37255915E-01    2           5        -5   # BR(H -> b       bb     )
     1.76495010E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.24044181E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.66326644E-04    2           3        -3   # BR(H -> s       sb     )
     2.16204180E-07    2           4        -4   # BR(H -> c       cb     )
     2.15677582E-02    2           6        -6   # BR(H -> t       tb     )
     2.98501054E-05    2          21        21   # BR(H -> g       g      )
     2.16453044E-08    2          22        22   # BR(H -> gam     gam    )
     8.32026130E-09    2          23        22   # BR(H -> Z       gam    )
     3.01025157E-05    2          24       -24   # BR(H -> W+      W-     )
     1.50327017E-05    2          23        23   # BR(H -> Z       Z      )
     7.96888361E-05    2          25        25   # BR(H -> h       h      )
    -8.24601093E-24    2          36        36   # BR(H -> A       A      )
     2.54642000E-18    2          23        36   # BR(H -> Z       A      )
     2.08471915E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.08708254E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03957980E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.80402580E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.92169750E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.64044320E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32647656E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83178683E-01    2           5        -5   # BR(A -> b       bb     )
     1.87467610E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.62839745E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.14104852E-04    2           3        -3   # BR(A -> s       sb     )
     2.29728849E-07    2           4        -4   # BR(A -> c       cb     )
     2.29043143E-02    2           6        -6   # BR(A -> t       tb     )
     6.74510159E-05    2          21        21   # BR(A -> g       g      )
     3.79805045E-08    2          22        22   # BR(A -> gam     gam    )
     6.64546700E-08    2          23        22   # BR(A -> Z       gam    )
     3.18526094E-05    2          23        25   # BR(A -> Z       h      )
     2.63927148E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21997972E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31603181E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.95508705E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.03029031E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09724342E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.41434801E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.53654568E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.02234690E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.01674595E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.03210596E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.13732831E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.10730991E-05    2          24        25   # BR(H+ -> W+      h      )
     8.57630009E-14    2          24        36   # BR(H+ -> W+      A      )
     1.95065550E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.03172503E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.98004427E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
