#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.17311980E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03995732E+01   # W+
        25     1.24804546E+02   # h
        35     3.00005562E+03   # H
        36     2.99999999E+03   # A
        37     3.00088100E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04342069E+03   # ~d_L
   2000001     3.03831760E+03   # ~d_R
   1000002     3.04251448E+03   # ~u_L
   2000002     3.03916726E+03   # ~u_R
   1000003     3.04342069E+03   # ~s_L
   2000003     3.03831760E+03   # ~s_R
   1000004     3.04251448E+03   # ~c_L
   2000004     3.03916726E+03   # ~c_R
   1000005     1.10142776E+03   # ~b_1
   2000005     3.03717546E+03   # ~b_2
   1000006     1.09781127E+03   # ~t_1
   2000006     3.01964120E+03   # ~t_2
   1000011     3.00621752E+03   # ~e_L
   2000011     3.00231839E+03   # ~e_R
   1000012     3.00482839E+03   # ~nu_eL
   1000013     3.00621752E+03   # ~mu_L
   2000013     3.00231839E+03   # ~mu_R
   1000014     3.00482839E+03   # ~nu_muL
   1000015     2.98560445E+03   # ~tau_1
   2000015     3.02186355E+03   # ~tau_2
   1000016     3.00450110E+03   # ~nu_tauL
   1000021     2.35419274E+03   # ~g
   1000022     2.00527004E+02   # ~chi_10
   1000023     4.25325811E+02   # ~chi_20
   1000025    -2.99411127E+03   # ~chi_30
   1000035     2.99478988E+03   # ~chi_40
   1000024     4.25488736E+02   # ~chi_1+
   1000037     2.99537803E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99887627E-01   # N_11
  1  2    -9.59744775E-04   # N_12
  1  3     1.48603767E-02   # N_13
  1  4    -1.72692445E-03   # N_14
  2  1     1.36215805E-03   # N_21
  2  2     9.99635385E-01   # N_22
  2  3    -2.65319245E-02   # N_23
  2  4     4.82693557E-03   # N_24
  3  1    -9.26923221E-03   # N_31
  3  2     1.53591016E-02   # N_32
  3  3     7.06852683E-01   # N_33
  3  4     7.07133272E-01   # N_34
  4  1    -1.17029925E-02   # N_41
  4  2     2.21872641E-02   # N_42
  4  3     7.06706807E-01   # N_43
  4  4    -7.07061705E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99295191E-01   # U_11
  1  2    -3.75382609E-02   # U_12
  2  1     3.75382609E-02   # U_21
  2  2     9.99295191E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99976672E-01   # V_11
  1  2    -6.83054841E-03   # V_12
  2  1     6.83054841E-03   # V_21
  2  2     9.99976672E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98413332E-01   # cos(theta_t)
  1  2    -5.63100212E-02   # sin(theta_t)
  2  1     5.63100212E-02   # -sin(theta_t)
  2  2     9.98413332E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99904011E-01   # cos(theta_b)
  1  2     1.38552801E-02   # sin(theta_b)
  2  1    -1.38552801E-02   # -sin(theta_b)
  2  2     9.99904011E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06946302E-01   # cos(theta_tau)
  1  2     7.07267224E-01   # sin(theta_tau)
  2  1    -7.07267224E-01   # -sin(theta_tau)
  2  2     7.06946302E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01740972E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.73119796E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43876418E+02   # higgs               
         4     7.84271378E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.73119796E+03  # The gauge couplings
     1     3.62655948E-01   # gprime(Q) DRbar
     2     6.38466322E-01   # g(Q) DRbar
     3     1.02313201E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.73119796E+03  # The trilinear couplings
  1  1     2.84489610E-06   # A_u(Q) DRbar
  2  2     2.84493650E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.73119796E+03  # The trilinear couplings
  1  1     7.39293859E-07   # A_d(Q) DRbar
  2  2     7.39430404E-07   # A_s(Q) DRbar
  3  3     1.66564267E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.73119796E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.58196530E-07   # A_mu(Q) DRbar
  3  3     1.60105153E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.73119796E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49746874E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.73119796E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.14516866E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.73119796E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06323395E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.73119796E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.29918190E+04   # M^2_Hd              
        22    -9.02206392E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41501274E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.86212581E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47617314E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47617314E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52382686E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52382686E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.74563153E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.49831085E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.14647248E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.70369644E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.09571225E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.00741884E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.93621156E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.90818835E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.08396415E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.02493872E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66541427E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.58970440E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.10627237E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.63361228E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.60489893E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.41934910E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.42016101E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.16785108E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.99766737E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.05926571E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.79958698E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.74280274E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     5.46978376E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.32006547E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.80485574E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.10337320E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.88781133E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.94528118E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.95107476E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62889748E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.56151623E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     7.46832766E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.25824241E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.70568912E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.05334809E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15011339E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.60790535E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.89823897E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.38257492E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.13996278E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.39209140E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.95029628E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.07049064E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62657829E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.83699540E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.15531319E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.25252521E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.33999701E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.06018452E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64277446E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.57826930E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.25216109E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.79601652E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.87213654E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54217215E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.94528118E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.95107476E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62889748E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.56151623E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     7.46832766E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.25824241E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.70568912E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.05334809E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15011339E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.60790535E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.89823897E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.38257492E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.13996278E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.39209140E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.95029628E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.07049064E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62657829E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.83699540E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.15531319E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.25252521E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.33999701E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.06018452E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64277446E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.57826930E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.25216109E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.79601652E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.87213654E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54217215E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89797952E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.96392882E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00572464E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.05862766E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.34840105E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.99788196E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.56672458E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55677635E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998196E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.79815073E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.58468644E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.46775959E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.89797952E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.96392882E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00572464E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.05862766E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.34840105E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.99788196E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.56672458E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55677635E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998196E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.79815073E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.58468644E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.46775959E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.75540008E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52700673E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16101361E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31197966E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.69695914E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.61074562E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13310760E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.57684442E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.74940309E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.25563441E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.79746203E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89824368E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00261094E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99473256E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.74069237E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.15628412E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00265631E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.15031919E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.89824368E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00261094E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99473256E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.74069237E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.15628412E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00265631E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.15031919E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89812919E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.00252922E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99446763E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.33683630E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.08238291E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00297926E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.37102783E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.30869022E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.70652095E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.69855871E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.96693456E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.72588574E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.68783008E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.39511516E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.49392665E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     8.05519651E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.25057318E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.81149535E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.51885047E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.73557790E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.51724415E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.16826046E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.45333081E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.45333081E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.86957342E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.53110575E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.52226806E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.52226806E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.22218561E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.22218561E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.91117899E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.91117899E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.64134676E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.63687698E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51048839E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.54853509E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.54853509E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.59717100E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.55164009E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.48867573E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.48867573E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.25318080E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.25318080E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.32394745E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.32394745E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.74571662E-03   # h decays
#          BR         NDA      ID1       ID2
     6.71497051E-01    2           5        -5   # BR(h -> b       bb     )
     5.49014892E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.94353636E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.12609437E-04    2           3        -3   # BR(h -> s       sb     )
     1.78162984E-02    2           4        -4   # BR(h -> c       cb     )
     5.77262059E-02    2          21        21   # BR(h -> g       g      )
     1.97101737E-03    2          22        22   # BR(h -> gam     gam    )
     1.29380408E-03    2          22        23   # BR(h -> Z       gam    )
     1.72666626E-01    2          24       -24   # BR(h -> W+      W-     )
     2.15205451E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.41136188E+01   # H decays
#          BR         NDA      ID1       ID2
     7.41333650E-01    2           5        -5   # BR(H -> b       bb     )
     1.76191727E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.22971843E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.65012146E-04    2           3        -3   # BR(H -> s       sb     )
     2.15802760E-07    2           4        -4   # BR(H -> c       cb     )
     2.15277031E-02    2           6        -6   # BR(H -> t       tb     )
     2.41173993E-05    2          21        21   # BR(H -> g       g      )
     1.22508234E-08    2          22        22   # BR(H -> gam     gam    )
     8.31310176E-09    2          23        22   # BR(H -> Z       gam    )
     2.90852785E-05    2          24       -24   # BR(H -> W+      W-     )
     1.45247057E-05    2          23        23   # BR(H -> Z       Z      )
     7.81650128E-05    2          25        25   # BR(H -> h       h      )
     2.01373931E-23    2          36        36   # BR(H -> A       A      )
     1.27900823E-19    2          23        36   # BR(H -> Z       A      )
     2.19467462E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.09808289E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.09412994E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.98358610E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.53106533E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.18278647E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33400306E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84411541E-01    2           5        -5   # BR(A -> b       bb     )
     1.86409917E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.59100000E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.09511663E-04    2           3        -3   # BR(A -> s       sb     )
     2.28432717E-07    2           4        -4   # BR(A -> c       cb     )
     2.27750880E-02    2           6        -6   # BR(A -> t       tb     )
     6.70704533E-05    2          21        21   # BR(A -> g       g      )
     3.31725550E-08    2          22        22   # BR(A -> gam     gam    )
     6.60529062E-08    2          23        22   # BR(A -> Z       gam    )
     3.06573562E-05    2          23        25   # BR(A -> Z       h      )
     2.61885021E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21078001E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30553420E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.91324706E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04583795E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10871833E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.37842229E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.40952108E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.09578633E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.94210533E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.01674999E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.20463351E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.91634686E-05    2          24        25   # BR(H+ -> W+      h      )
     6.67784496E-14    2          24        36   # BR(H+ -> W+      A      )
     1.94720290E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.57921104E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.66851131E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
