#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13080050E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.62959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.85900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.26485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     7.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04175940E+01   # W+
        25     1.25454824E+02   # h
        35     4.00000388E+03   # H
        36     3.99999672E+03   # A
        37     4.00104590E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02612839E+03   # ~d_L
   2000001     4.02278013E+03   # ~d_R
   1000002     4.02547864E+03   # ~u_L
   2000002     4.02331428E+03   # ~u_R
   1000003     4.02612839E+03   # ~s_L
   2000003     4.02278013E+03   # ~s_R
   1000004     4.02547864E+03   # ~c_L
   2000004     4.02331428E+03   # ~c_R
   1000005     2.29204764E+03   # ~b_1
   2000005     4.02578896E+03   # ~b_2
   1000006     8.02034296E+02   # ~t_1
   2000006     2.30253510E+03   # ~t_2
   1000011     4.00448593E+03   # ~e_L
   2000011     4.00372056E+03   # ~e_R
   1000012     4.00337416E+03   # ~nu_eL
   1000013     4.00448593E+03   # ~mu_L
   2000013     4.00372056E+03   # ~mu_R
   1000014     4.00337416E+03   # ~nu_muL
   1000015     4.00535310E+03   # ~tau_1
   2000015     4.00713409E+03   # ~tau_2
   1000016     4.00480307E+03   # ~nu_tauL
   1000021     1.99473610E+03   # ~g
   1000022     1.44718850E+02   # ~chi_10
   1000023    -1.96342621E+02   # ~chi_20
   1000025     2.11007101E+02   # ~chi_30
   1000035     2.06681346E+03   # ~chi_40
   1000024     1.92318397E+02   # ~chi_1+
   1000037     2.06698085E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.15326076E-01   # N_11
  1  2    -1.34479590E-02   # N_12
  1  3    -4.64101784E-01   # N_13
  1  4    -3.45936521E-01   # N_14
  2  1    -9.38112940E-02   # N_21
  2  2     2.63689271E-02   # N_22
  2  3    -6.95906985E-01   # N_23
  2  4     7.11489697E-01   # N_24
  3  1     5.71351008E-01   # N_31
  3  2     2.51381935E-02   # N_32
  3  3     5.48014799E-01   # N_33
  3  4     6.10414512E-01   # N_34
  4  1    -9.25255308E-04   # N_41
  4  2     9.99245667E-01   # N_42
  4  3    -1.66826173E-03   # N_43
  4  4    -3.87873368E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.36395015E-03   # U_11
  1  2     9.99997206E-01   # U_12
  2  1    -9.99997206E-01   # U_21
  2  2     2.36395015E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.48662794E-02   # V_11
  1  2    -9.98493711E-01   # V_12
  2  1    -9.98493711E-01   # V_21
  2  2    -5.48662794E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.54911066E-02   # cos(theta_t)
  1  2     9.96338934E-01   # sin(theta_t)
  2  1    -9.96338934E-01   # -sin(theta_t)
  2  2    -8.54911066E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999657E-01   # cos(theta_b)
  1  2    -8.28251099E-04   # sin(theta_b)
  2  1     8.28251099E-04   # -sin(theta_b)
  2  2     9.99999657E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04846731E-01   # cos(theta_tau)
  1  2     7.09359631E-01   # sin(theta_tau)
  2  1    -7.09359631E-01   # -sin(theta_tau)
  2  2    -7.04846731E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00303984E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30800497E+03  # DRbar Higgs Parameters
         1    -1.85900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43603503E+02   # higgs               
         4     1.61052611E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30800497E+03  # The gauge couplings
     1     3.62178312E-01   # gprime(Q) DRbar
     2     6.36239863E-01   # g(Q) DRbar
     3     1.02926431E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30800497E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37179617E-06   # A_c(Q) DRbar
  3  3     2.62959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30800497E+03  # The trilinear couplings
  1  1     5.03124056E-07   # A_d(Q) DRbar
  2  2     5.03171199E-07   # A_s(Q) DRbar
  3  3     9.00950934E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30800497E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.08782426E-07   # A_mu(Q) DRbar
  3  3     1.09902683E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30800497E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66458189E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30800497E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.81353085E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30800497E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03971555E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30800497E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58543496E+07   # M^2_Hd              
        22    -3.61438870E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.26485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     7.59989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40437257E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.99670644E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.17464007E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.12029179E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.20367660E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.07819700E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.80609722E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     7.73767186E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     5.91170599E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.15303503E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.51261800E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.36268415E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.77483246E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.88212122E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.33434472E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.36672795E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.44217240E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     7.57295832E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.24886146E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.07988105E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.35048251E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     4.27834305E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.25871106E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     6.11948756E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.36952673E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.79986140E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64495515E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.68517347E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.76385392E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53061547E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.76831804E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.60732379E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.41336772E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14020181E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.44901188E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.07539462E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.21103892E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.31794952E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76502472E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.76066170E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.46507552E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.56959488E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81663358E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.30277197E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.62104732E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51756192E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.59109772E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.74625790E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.94982043E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.83480076E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.63658434E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43694405E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76521637E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.49509353E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.71812949E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.10303999E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82138659E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.84449529E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.65277991E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51980140E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.52344465E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.78022359E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.29223152E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.79004639E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.88195168E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85300500E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76502472E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.76066170E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.46507552E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.56959488E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81663358E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.30277197E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.62104732E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51756192E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.59109772E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.74625790E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.94982043E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.83480076E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.63658434E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43694405E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76521637E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.49509353E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.71812949E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.10303999E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82138659E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.84449529E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.65277991E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51980140E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.52344465E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.78022359E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.29223152E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.79004639E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.88195168E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85300500E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12569485E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04504155E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.75179443E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.29799569E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76927216E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.73908993E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55207754E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08212628E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.65409183E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.78983169E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.25800523E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.62275874E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12569485E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04504155E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.75179443E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.29799569E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76927216E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.73908993E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55207754E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08212628E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.65409183E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.78983169E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.25800523E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.62275874E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09989518E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52464919E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.16705626E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.35072411E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38587092E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.43537728E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.77851243E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10252329E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.45909267E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.76757448E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.11160378E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.40827456E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.20837521E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.82343403E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12678449E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17276174E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.26487478E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.61734499E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77226105E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.08961487E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.52969781E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12678449E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17276174E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.26487478E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.61734499E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77226105E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.08961487E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.52969781E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45919740E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06044510E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95219887E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.17514572E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50805396E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.81772885E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00269150E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.75082998E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33583523E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33583523E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11195550E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11195550E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10441852E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.67745941E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.07325048E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.47178445E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     5.18670618E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.42302106E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.86985038E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.40934844E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.65049685E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.28859343E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18037434E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53124673E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18037434E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53124673E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41232931E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51191602E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51191602E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47923313E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02200263E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02200263E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02200240E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.15999414E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.15999414E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.15999414E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.15999414E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.38666708E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.38666708E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.38666708E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.38666708E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.39639889E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.39639889E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.27750016E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.64203751E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.88675015E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.63998551E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.88533704E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.63998551E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.88533704E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.42383394E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.24684183E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.24684183E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.23210547E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.54295547E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.54295547E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.54294958E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.78454583E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.90983835E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.78454583E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.90983835E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.57978165E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.12628643E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.12628643E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.03503339E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.25135662E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.25135662E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.25135665E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.31105578E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.31105578E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.31105578E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.31105578E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.43700008E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.43700008E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.43700008E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.43700008E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.37441881E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.37441881E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.67623699E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.66204380E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.35743778E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.48815389E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.40989727E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.40989727E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.59156419E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.10056517E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.04458287E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.51721722E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.51721722E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.04587398E-03   # h decays
#          BR         NDA      ID1       ID2
     5.97252993E-01    2           5        -5   # BR(h -> b       bb     )
     6.43645706E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27850493E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.83269491E-04    2           3        -3   # BR(h -> s       sb     )
     2.09859990E-02    2           4        -4   # BR(h -> c       cb     )
     6.85252878E-02    2          21        21   # BR(h -> g       g      )
     2.36079366E-03    2          22        22   # BR(h -> gam     gam    )
     1.60925589E-03    2          22        23   # BR(h -> Z       gam    )
     2.16813343E-01    2          24       -24   # BR(h -> W+      W-     )
     2.73766363E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.40669523E+01   # H decays
#          BR         NDA      ID1       ID2
     3.42091473E-01    2           5        -5   # BR(H -> b       bb     )
     6.13239006E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.16826459E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.56468341E-04    2           3        -3   # BR(H -> s       sb     )
     7.19449027E-08    2           4        -4   # BR(H -> c       cb     )
     7.21037709E-03    2           6        -6   # BR(H -> t       tb     )
     1.01880383E-06    2          21        21   # BR(H -> g       g      )
     2.36156300E-11    2          22        22   # BR(H -> gam     gam    )
     1.85119744E-09    2          23        22   # BR(H -> Z       gam    )
     2.00901122E-06    2          24       -24   # BR(H -> W+      W-     )
     1.00380970E-06    2          23        23   # BR(H -> Z       Z      )
     7.33045813E-06    2          25        25   # BR(H -> h       h      )
     2.51350833E-24    2          36        36   # BR(H -> A       A      )
     1.02528342E-20    2          23        36   # BR(H -> Z       A      )
     1.86774728E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.65615794E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.65615794E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.90805574E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.57605093E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.62012485E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.10639532E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.83694123E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.65670391E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.63893779E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.77911321E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.17485321E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     7.84853646E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.28432691E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.28432691E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.40630912E+01   # A decays
#          BR         NDA      ID1       ID2
     3.42141891E-01    2           5        -5   # BR(A -> b       bb     )
     6.13286608E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.16843120E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.56529859E-04    2           3        -3   # BR(A -> s       sb     )
     7.24045285E-08    2           4        -4   # BR(A -> c       cb     )
     7.22365102E-03    2           6        -6   # BR(A -> t       tb     )
     1.48427754E-05    2          21        21   # BR(A -> g       g      )
     4.73232859E-08    2          22        22   # BR(A -> gam     gam    )
     1.63173268E-08    2          23        22   # BR(A -> Z       gam    )
     2.00494621E-06    2          23        25   # BR(A -> Z       h      )
     1.86915986E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.65631261E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.65631261E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.51824062E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.95325222E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.31077339E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.68645288E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.58912628E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.51474467E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.84547155E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.30081236E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.78996459E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.38389676E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.38389676E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.39282009E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.44795530E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.14981883E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.17442527E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.48668838E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.23185190E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.53431549E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.47375702E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.01224714E-06    2          24        25   # BR(H+ -> W+      h      )
     3.10218790E-14    2          24        36   # BR(H+ -> W+      A      )
     5.61873174E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.48313222E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.36480871E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.66260302E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.19816474E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61347714E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00029137E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.90104962E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
