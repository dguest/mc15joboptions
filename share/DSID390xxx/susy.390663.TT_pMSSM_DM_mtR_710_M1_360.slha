#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12012881E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.52959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.73900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.05485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     7.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04204854E+01   # W+
        25     1.25636242E+02   # h
        35     4.00000949E+03   # H
        36     3.99999686E+03   # A
        37     4.00101246E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02307945E+03   # ~d_L
   2000001     4.02036925E+03   # ~d_R
   1000002     4.02243045E+03   # ~u_L
   2000002     4.02095733E+03   # ~u_R
   1000003     4.02307945E+03   # ~s_L
   2000003     4.02036925E+03   # ~s_R
   1000004     4.02243045E+03   # ~c_L
   2000004     4.02095733E+03   # ~c_R
   1000005     2.08374403E+03   # ~b_1
   2000005     4.02416410E+03   # ~b_2
   1000006     7.39107059E+02   # ~t_1
   2000006     2.09635698E+03   # ~t_2
   1000011     4.00384482E+03   # ~e_L
   2000011     4.00357546E+03   # ~e_R
   1000012     4.00273556E+03   # ~nu_eL
   1000013     4.00384482E+03   # ~mu_L
   2000013     4.00357546E+03   # ~mu_R
   1000014     4.00273556E+03   # ~nu_muL
   1000015     4.00443106E+03   # ~tau_1
   2000015     4.00802906E+03   # ~tau_2
   1000016     4.00441973E+03   # ~nu_tauL
   1000021     1.98622026E+03   # ~g
   1000022     3.41867287E+02   # ~chi_10
   1000023    -3.85153806E+02   # ~chi_20
   1000025     4.05235037E+02   # ~chi_30
   1000035     2.06526039E+03   # ~chi_40
   1000024     3.82726210E+02   # ~chi_1+
   1000037     2.06543279E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.67404637E-01   # N_11
  1  2    -1.87233382E-02   # N_12
  1  3    -4.78468086E-01   # N_13
  1  4    -4.26389317E-01   # N_14
  2  1    -4.44745451E-02   # N_21
  2  2     2.42133060E-02   # N_22
  2  3    -7.03770226E-01   # N_23
  2  4     7.08620632E-01   # N_24
  3  1     6.39617873E-01   # N_31
  3  2     2.57964078E-02   # N_32
  3  3     5.25115089E-01   # N_33
  3  4     5.60783083E-01   # N_34
  4  1    -1.05543707E-03   # N_41
  4  2     9.99198528E-01   # N_42
  4  3    -5.46838167E-03   # N_43
  4  4    -3.96394386E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.73914091E-03   # U_11
  1  2     9.99970052E-01   # U_12
  2  1    -9.99970052E-01   # U_21
  2  2     7.73914091E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.60689959E-02   # V_11
  1  2    -9.98426897E-01   # V_12
  2  1    -9.98426897E-01   # V_21
  2  2    -5.60689959E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.01027759E-01   # cos(theta_t)
  1  2     9.94883607E-01   # sin(theta_t)
  2  1    -9.94883607E-01   # -sin(theta_t)
  2  2    -1.01027759E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998771E-01   # cos(theta_b)
  1  2    -1.56780053E-03   # sin(theta_b)
  2  1     1.56780053E-03   # -sin(theta_b)
  2  2     9.99998771E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05997408E-01   # cos(theta_tau)
  1  2     7.08214417E-01   # sin(theta_tau)
  2  1    -7.08214417E-01   # -sin(theta_tau)
  2  2    -7.05997408E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00259179E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20128805E+03  # DRbar Higgs Parameters
         1    -3.73900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43849182E+02   # higgs               
         4     1.61839624E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20128805E+03  # The gauge couplings
     1     3.61755111E-01   # gprime(Q) DRbar
     2     6.35370171E-01   # g(Q) DRbar
     3     1.03122946E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20128805E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.13699956E-06   # A_c(Q) DRbar
  3  3     2.52959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20128805E+03  # The trilinear couplings
  1  1     4.18965653E-07   # A_d(Q) DRbar
  2  2     4.19004911E-07   # A_s(Q) DRbar
  3  3     7.49208336E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20128805E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.31387069E-08   # A_mu(Q) DRbar
  3  3     9.40929980E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20128805E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69058511E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20128805E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86728725E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20128805E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03223322E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20128805E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57452325E+07   # M^2_Hd              
        22    -1.31147116E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.05485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     7.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40040973E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.14134055E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.14535380E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.54374687E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.97511229E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.99670859E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.07380443E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.23033133E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     9.38358209E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.27274499E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.31753084E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.43837841E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.43540861E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.03891896E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.88617375E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.03250567E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.16286452E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.53072153E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.10126795E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     7.03036397E-05    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     6.86018560E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.50125473E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     2.00950049E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64470342E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.63627976E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83034337E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.56510747E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.26355280E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.67941350E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.50736528E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12346376E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.13351034E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.52509682E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.64640160E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.65904769E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75992291E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.40462669E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.16106963E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.87833461E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81257039E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.46304068E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.61299401E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51893480E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58620933E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.28222664E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.09825488E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.26719930E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.43172178E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44395881E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76011328E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.35588715E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.26320151E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.92366439E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81779792E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.04088137E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64546950E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52116031E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51955416E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.56419226E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.86563256E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.91570921E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.95245105E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85491433E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75992291E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.40462669E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.16106963E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.87833461E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81257039E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.46304068E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.61299401E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51893480E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58620933E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.28222664E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.09825488E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.26719930E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.43172178E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44395881E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76011328E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.35588715E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.26320151E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.92366439E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81779792E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.04088137E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64546950E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52116031E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51955416E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.56419226E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.86563256E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.91570921E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.95245105E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85491433E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11222586E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.90403740E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.23405407E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.69624357E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77492204E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.07578380E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56443605E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04936990E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.90349832E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.97498609E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.07674572E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.10308328E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11222586E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.90403740E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.23405407E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.69624357E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77492204E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.07578380E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56443605E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04936990E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.90349832E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.97498609E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.07674572E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.10308328E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07102436E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.17669922E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.46392055E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.63062105E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39520928E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.53312117E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79776628E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06570754E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.15540880E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.10160683E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.45583835E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42356359E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.00446851E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85458172E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11332440E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.05632360E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.24347730E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.79081861E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77873649E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.18701653E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54155311E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11332440E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.05632360E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.24347730E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.79081861E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77873649E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.18701653E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54155311E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.43925018E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.56633646E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.12612860E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.24433983E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51799949E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.68086326E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02158527E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.56613935E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33675842E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33675842E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11226658E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11226658E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10195000E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.65472688E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.02662833E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.44668164E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.98354702E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.38605346E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.57968605E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.38656131E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     5.92134005E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.20109145E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18579113E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53835853E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18579113E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53835853E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37739334E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52878540E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52878540E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48798355E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.05507054E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.05507054E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.05507038E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     5.53147250E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     5.53147250E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     5.53147250E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     5.53147250E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.84382580E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.84382580E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.84382580E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.84382580E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.60886585E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.60886585E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.09675116E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.53778626E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.31465004E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.94213243E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.91303653E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.94213243E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.91303653E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     8.53528459E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.97237444E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.97237444E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.97488407E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.12392175E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.12392175E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.12389630E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.95025449E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.53019315E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.95025449E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.53019315E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.48828050E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.80435286E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.80435286E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.55117557E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.16030778E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.16030778E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.16030780E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.18144526E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.18144526E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.18144526E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.18144526E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.93809687E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.93809687E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.93809687E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.93809687E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.81878958E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.81878958E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.65299163E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.20569623E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.55928304E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.68951429E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38577018E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38577018E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.85278687E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.70945203E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.02581110E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.98810276E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.98810276E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.04811848E-03   # h decays
#          BR         NDA      ID1       ID2
     5.92533198E-01    2           5        -5   # BR(h -> b       bb     )
     6.44106036E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28012660E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.83478597E-04    2           3        -3   # BR(h -> s       sb     )
     2.09987840E-02    2           4        -4   # BR(h -> c       cb     )
     6.84552251E-02    2          21        21   # BR(h -> g       g      )
     2.37852523E-03    2          22        22   # BR(h -> gam     gam    )
     1.63449740E-03    2          22        23   # BR(h -> Z       gam    )
     2.20895732E-01    2          24       -24   # BR(h -> W+      W-     )
     2.79819442E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.47483602E+01   # H decays
#          BR         NDA      ID1       ID2
     3.57599461E-01    2           5        -5   # BR(H -> b       bb     )
     6.05607645E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14128195E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53276714E-04    2           3        -3   # BR(H -> s       sb     )
     7.10368359E-08    2           4        -4   # BR(H -> c       cb     )
     7.11937000E-03    2           6        -6   # BR(H -> t       tb     )
     7.50074458E-07    2          21        21   # BR(H -> g       g      )
     1.38215647E-09    2          22        22   # BR(H -> gam     gam    )
     1.83266427E-09    2          23        22   # BR(H -> Z       gam    )
     1.74477921E-06    2          24       -24   # BR(H -> W+      W-     )
     8.71785377E-07    2          23        23   # BR(H -> Z       Z      )
     6.75364504E-06    2          25        25   # BR(H -> h       h      )
    -6.97079046E-24    2          36        36   # BR(H -> A       A      )
     5.79825839E-20    2          23        36   # BR(H -> Z       A      )
     1.86454938E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.58439989E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.58439989E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.71948117E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.01172817E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.77624072E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.09803205E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.84339344E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.50586269E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.71441348E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.16666500E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.22378692E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.36722979E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.23608407E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.23608407E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.47427191E+01   # A decays
#          BR         NDA      ID1       ID2
     3.57662013E-01    2           5        -5   # BR(A -> b       bb     )
     6.05672710E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14151032E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53345063E-04    2           3        -3   # BR(A -> s       sb     )
     7.15056324E-08    2           4        -4   # BR(A -> c       cb     )
     7.13397001E-03    2           6        -6   # BR(A -> t       tb     )
     1.46585030E-05    2          21        21   # BR(A -> g       g      )
     6.15107293E-08    2          22        22   # BR(A -> gam     gam    )
     1.61228617E-08    2          23        22   # BR(A -> Z       gam    )
     1.74128215E-06    2          23        25   # BR(A -> Z       h      )
     1.88297087E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.58447727E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.58447727E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.34371331E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.63926946E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.49570914E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.56174888E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.43398164E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.76840566E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.93233182E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.57518899E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.54582220E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.47358439E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.47358439E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.47531512E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.72367107E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.05711062E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14164592E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.66314637E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21328306E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49611342E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.64488105E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.74288625E-06    2          24        25   # BR(H+ -> W+      h      )
     2.59681043E-14    2          24        36   # BR(H+ -> W+      A      )
     4.71403929E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.18343672E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.89683323E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.58630770E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.28585827E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57437703E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.78545622E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.09749291E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
