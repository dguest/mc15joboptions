#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10049676E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.40900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     5.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04199934E+01   # W+
        25     1.24951159E+02   # h
        35     4.00000475E+03   # H
        36     3.99999580E+03   # A
        37     4.00100675E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01667179E+03   # ~d_L
   2000001     4.01532869E+03   # ~d_R
   1000002     4.01601898E+03   # ~u_L
   2000002     4.01589701E+03   # ~u_R
   1000003     4.01667179E+03   # ~s_L
   2000003     4.01532869E+03   # ~s_R
   1000004     4.01601898E+03   # ~c_L
   2000004     4.01589701E+03   # ~c_R
   1000005     2.03041818E+03   # ~b_1
   2000005     4.02074061E+03   # ~b_2
   1000006     5.30677227E+02   # ~t_1
   2000006     2.04186954E+03   # ~t_2
   1000011     4.00247004E+03   # ~e_L
   2000011     4.00364079E+03   # ~e_R
   1000012     4.00135769E+03   # ~nu_eL
   1000013     4.00247004E+03   # ~mu_L
   2000013     4.00364079E+03   # ~mu_R
   1000014     4.00135769E+03   # ~nu_muL
   1000015     4.00440087E+03   # ~tau_1
   2000015     4.00846679E+03   # ~tau_2
   1000016     4.00361613E+03   # ~nu_tauL
   1000021     1.97451725E+03   # ~g
   1000022     1.96445882E+02   # ~chi_10
   1000023    -2.51054718E+02   # ~chi_20
   1000025     2.65541248E+02   # ~chi_30
   1000035     2.06645921E+03   # ~chi_40
   1000024     2.47785394E+02   # ~chi_1+
   1000037     2.06663525E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.39521022E-01   # N_11
  1  2    -1.34747968E-02   # N_12
  1  3    -4.27288755E-01   # N_13
  1  4    -3.35331483E-01   # N_14
  2  1    -7.22101030E-02   # N_21
  2  2     2.57336742E-02   # N_22
  2  3    -6.99943821E-01   # N_23
  2  4     7.10071916E-01   # N_24
  3  1     5.38506491E-01   # N_31
  3  2     2.62315280E-02   # N_32
  3  3     5.72272098E-01   # N_33
  3  4     6.17921769E-01   # N_34
  4  1    -9.55974215E-04   # N_41
  4  2     9.99233764E-01   # N_42
  4  3    -2.75918867E-03   # N_43
  4  4    -3.90302215E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     3.90713285E-03   # U_11
  1  2     9.99992367E-01   # U_12
  2  1    -9.99992367E-01   # U_21
  2  2     3.90713285E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.52094364E-02   # V_11
  1  2    -9.98474796E-01   # V_12
  2  1    -9.98474796E-01   # V_21
  2  2    -5.52094364E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.98484873E-02   # cos(theta_t)
  1  2     9.95955445E-01   # sin(theta_t)
  2  1    -9.95955445E-01   # -sin(theta_t)
  2  2    -8.98484873E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999511E-01   # cos(theta_b)
  1  2    -9.88938704E-04   # sin(theta_b)
  2  1     9.88938704E-04   # -sin(theta_b)
  2  2     9.99999511E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05187890E-01   # cos(theta_tau)
  1  2     7.09020479E-01   # sin(theta_tau)
  2  1    -7.09020479E-01   # -sin(theta_tau)
  2  2    -7.05187890E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00275783E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.00496758E+03  # DRbar Higgs Parameters
         1    -2.40900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44293454E+02   # higgs               
         4     1.62003379E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.00496758E+03  # The gauge couplings
     1     3.61268687E-01   # gprime(Q) DRbar
     2     6.35527974E-01   # g(Q) DRbar
     3     1.03520386E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.00496758E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.46854171E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.00496758E+03  # The trilinear couplings
  1  1     2.74720581E-07   # A_d(Q) DRbar
  2  2     2.74746929E-07   # A_s(Q) DRbar
  3  3     4.90720484E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.00496758E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.08735726E-08   # A_mu(Q) DRbar
  3  3     6.14952755E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.00496758E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72131072E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.00496758E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85064319E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.00496758E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03477825E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.00496758E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58141365E+07   # M^2_Hd              
        22    -6.89137563E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     5.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40112691E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.55835858E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.85541429E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     7.28620536E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.78536161E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.02254525E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.46347260E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.02466771E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.36852333E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.39203222E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.71735972E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.47858319E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.83678717E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.86911024E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.05507001E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.46077294E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.56960900E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.15503332E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.16802552E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.62285270E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -3.48852315E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.63900319E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.62166850E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79328684E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.58623061E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.69077269E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63559542E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.24628496E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13411522E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     4.71374034E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     6.20383788E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.06919299E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.08325214E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75306673E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.87042776E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.27768274E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.44627675E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81064515E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.36437795E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.60899357E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51937694E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58215468E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.95800210E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.91998448E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.62250299E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.80352914E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43902923E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75325188E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.62706744E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.36697577E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.05182109E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81553387E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.68680524E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64102664E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52162761E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51510489E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.03313975E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.62189999E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.23514322E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.31646767E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85357233E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75306673E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.87042776E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.27768274E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.44627675E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81064515E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.36437795E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.60899357E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51937694E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58215468E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.95800210E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.91998448E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.62250299E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.80352914E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43902923E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75325188E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.62706744E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.36697577E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.05182109E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81553387E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.68680524E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64102664E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52162761E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51510489E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.03313975E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.62189999E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.23514322E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.31646767E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85357233E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11320206E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.10553345E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.20190612E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.65515637E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77127308E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.56495287E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55631943E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06667639E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.05623223E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.20447840E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.89171804E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.94774943E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11320206E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.10553345E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.20190612E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.65515637E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77127308E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.56495287E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55631943E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06667639E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.05623223E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.20447840E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.89171804E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.94774943E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08409559E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.61982329E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28428952E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.23045820E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38927649E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46552338E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78546073E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08656540E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.61530192E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.47531773E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.97357910E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41384902E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.15017030E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83472115E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11430834E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.23696891E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.28405104E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.00811678E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77442831E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.12273162E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53372327E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11430834E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.23696891E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.28405104E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.00811678E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77442831E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.12273162E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53372327E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44544702E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11872215E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.06571672E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.62498144E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51125516E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.78057595E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00880978E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.23618322E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33546066E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33546066E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11183250E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11183250E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10541368E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.68460577E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.91089855E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.18038605E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.44827916E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     4.79140084E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.39521245E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.89428617E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.38018841E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.25726537E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.01888819E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17992760E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53034723E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17992760E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53034723E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42391101E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50802891E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50802891E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47841370E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01336914E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01336914E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01336880E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.14086149E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.14086149E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.14086149E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.14086149E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.80287674E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.80287674E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.80287674E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.80287674E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.68713129E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.68713129E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.29100734E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.03031221E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.46036311E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     8.40422806E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.08698996E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     8.40422806E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.08698996E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.04319817E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.46774847E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.46774847E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.45605450E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.99285441E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.99285441E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.99284359E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.79660386E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.92438293E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.79660386E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.92438293E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.52638702E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.12898101E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.12898101E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.03536352E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.25660326E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.25660326E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.25660331E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.56166872E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.56166872E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.56166872E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.56166872E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.18720959E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.18720959E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.18720959E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.18720959E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.13016054E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.13016054E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.68573447E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.31198563E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.40435364E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.09839628E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38514411E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38514411E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.51924263E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.02052097E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.10021237E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.93219463E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.93219463E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.26117099E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.26117099E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.97155584E-03   # h decays
#          BR         NDA      ID1       ID2
     6.04692809E-01    2           5        -5   # BR(h -> b       bb     )
     6.52977671E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.31156249E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.90665860E-04    2           3        -3   # BR(h -> s       sb     )
     2.13096009E-02    2           4        -4   # BR(h -> c       cb     )
     6.88114106E-02    2          21        21   # BR(h -> g       g      )
     2.36706159E-03    2          22        22   # BR(h -> gam     gam    )
     1.56761935E-03    2          22        23   # BR(h -> Z       gam    )
     2.09039128E-01    2          24       -24   # BR(h -> W+      W-     )
     2.61927819E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.45289746E+01   # H decays
#          BR         NDA      ID1       ID2
     3.49171979E-01    2           5        -5   # BR(H -> b       bb     )
     6.08043356E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14989403E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54295414E-04    2           3        -3   # BR(H -> s       sb     )
     7.13272943E-08    2           4        -4   # BR(H -> c       cb     )
     7.14847988E-03    2           6        -6   # BR(H -> t       tb     )
     8.06421235E-07    2          21        21   # BR(H -> g       g      )
     1.29639235E-10    2          22        22   # BR(H -> gam     gam    )
     1.83861895E-09    2          23        22   # BR(H -> Z       gam    )
     1.83900676E-06    2          24       -24   # BR(H -> W+      W-     )
     9.18866495E-07    2          23        23   # BR(H -> Z       Z      )
     6.86878628E-06    2          25        25   # BR(H -> h       h      )
    -6.37694101E-25    2          36        36   # BR(H -> A       A      )
     4.08961118E-20    2          23        36   # BR(H -> Z       A      )
     1.86308926E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.62992131E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.62992131E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.57923479E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.10803714E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.50611644E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.39350471E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     8.05006264E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.00459528E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.39220467E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.90953382E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.43149120E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.94966691E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.51409393E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.51409393E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.45266822E+01   # A decays
#          BR         NDA      ID1       ID2
     3.49212521E-01    2           5        -5   # BR(A -> b       bb     )
     6.08072248E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14999450E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54348767E-04    2           3        -3   # BR(A -> s       sb     )
     7.17889239E-08    2           4        -4   # BR(A -> c       cb     )
     7.16223342E-03    2           6        -6   # BR(A -> t       tb     )
     1.47165821E-05    2          21        21   # BR(A -> g       g      )
     5.19742106E-08    2          22        22   # BR(A -> gam     gam    )
     1.61853550E-08    2          23        22   # BR(A -> Z       gam    )
     1.83527313E-06    2          23        25   # BR(A -> Z       h      )
     1.86564880E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63001759E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63001759E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.22574317E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.37952031E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.23498674E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.99562931E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.29145391E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.97479642E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.56369550E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.10283869E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.26700187E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.57246136E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.57246136E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.44355866E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.57032928E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.09243770E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15413671E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.56500768E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22035955E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51067204E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.54952085E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.84038209E-06    2          24        25   # BR(H+ -> W+      h      )
     2.55262634E-14    2          24        36   # BR(H+ -> W+      A      )
     5.89788462E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.85093284E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.96192697E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.63484099E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.99726480E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60502546E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.07320592E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.17590509E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
