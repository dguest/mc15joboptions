#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12001578E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.54959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.47900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.05485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04178640E+01   # W+
        25     1.25655580E+02   # h
        35     4.00000881E+03   # H
        36     3.99999679E+03   # A
        37     4.00102153E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02301354E+03   # ~d_L
   2000001     4.02023435E+03   # ~d_R
   1000002     4.02235962E+03   # ~u_L
   2000002     4.02124844E+03   # ~u_R
   1000003     4.02301354E+03   # ~s_L
   2000003     4.02023435E+03   # ~s_R
   1000004     4.02235962E+03   # ~c_L
   2000004     4.02124844E+03   # ~c_R
   1000005     7.72752536E+02   # ~b_1
   2000005     4.02382395E+03   # ~b_2
   1000006     7.57011570E+02   # ~t_1
   2000006     2.07093963E+03   # ~t_2
   1000011     4.00406071E+03   # ~e_L
   2000011     4.00317064E+03   # ~e_R
   1000012     4.00294613E+03   # ~nu_eL
   1000013     4.00406071E+03   # ~mu_L
   2000013     4.00317064E+03   # ~mu_R
   1000014     4.00294613E+03   # ~nu_muL
   1000015     4.00455748E+03   # ~tau_1
   2000015     4.00773331E+03   # ~tau_2
   1000016     4.00463527E+03   # ~nu_tauL
   1000021     1.97469982E+03   # ~g
   1000022     3.00180166E+02   # ~chi_10
   1000023    -3.58687981E+02   # ~chi_20
   1000025     3.70354509E+02   # ~chi_30
   1000035     2.05341019E+03   # ~chi_40
   1000024     3.56224379E+02   # ~chi_1+
   1000037     2.05357475E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.66975666E-01   # N_11
  1  2    -1.36093155E-02   # N_12
  1  3    -3.82846834E-01   # N_13
  1  4    -3.18741717E-01   # N_14
  2  1    -4.95737724E-02   # N_21
  2  2     2.45123343E-02   # N_22
  2  3    -7.03244640E-01   # N_23
  2  4     7.08793738E-01   # N_24
  3  1     4.95877597E-01   # N_31
  3  2     2.83031485E-02   # N_32
  3  3     5.99041706E-01   # N_33
  3  4     6.28055232E-01   # N_34
  4  1    -1.02159400E-03   # N_41
  4  2     9.99206117E-01   # N_42
  4  3    -4.93079646E-03   # N_43
  4  4    -3.95193599E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.97885013E-03   # U_11
  1  2     9.99975648E-01   # U_12
  2  1    -9.99975648E-01   # U_21
  2  2     6.97885013E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.59000987E-02   # V_11
  1  2    -9.98436367E-01   # V_12
  2  1    -9.98436367E-01   # V_21
  2  2    -5.59000987E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94817161E-01   # cos(theta_t)
  1  2    -1.01679969E-01   # sin(theta_t)
  2  1     1.01679969E-01   # -sin(theta_t)
  2  2     9.94817161E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999394E-01   # cos(theta_b)
  1  2    -1.10090855E-03   # sin(theta_b)
  2  1     1.10090855E-03   # -sin(theta_b)
  2  2     9.99999394E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05793719E-01   # cos(theta_tau)
  1  2     7.08417410E-01   # sin(theta_tau)
  2  1    -7.08417410E-01   # -sin(theta_tau)
  2  2    -7.05793719E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00271489E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20015780E+03  # DRbar Higgs Parameters
         1    -3.47900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43854829E+02   # higgs               
         4     1.61762560E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20015780E+03  # The gauge couplings
     1     3.61657942E-01   # gprime(Q) DRbar
     2     6.36090575E-01   # g(Q) DRbar
     3     1.03202469E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20015780E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.14482182E-06   # A_c(Q) DRbar
  3  3     2.54959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20015780E+03  # The trilinear couplings
  1  1     4.21034124E-07   # A_d(Q) DRbar
  2  2     4.21073622E-07   # A_s(Q) DRbar
  3  3     7.53381065E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20015780E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.25388927E-08   # A_mu(Q) DRbar
  3  3     9.34807918E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20015780E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69126447E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20015780E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84785395E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20015780E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03305093E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20015780E+03  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57647128E+07   # M^2_Hd              
        22    -1.14331486E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.05485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40370180E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.04796747E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43885806E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43885806E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56114194E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56114194E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.48174620E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.14161103E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.20551464E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.51807084E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.13480349E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20807711E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.29368076E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.17046349E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.62390045E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.31386067E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.93951855E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.59483225E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.11525343E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.31383185E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.94033514E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.92337515E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.36269530E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.83092549E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.78830041E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66824107E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.53123661E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79721280E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62664434E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.62284538E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.61689892E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.70633559E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13671469E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.31970841E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.76450404E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.96483957E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.13051231E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78646327E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.96160421E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.96480609E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.28431253E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79047770E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.37442683E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.56883437E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52603872E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61071829E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.13693306E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.34639813E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.34578368E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.19091139E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45038161E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78666484E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.73433247E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.88564351E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.70495697E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79552752E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.37721743E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.60103562E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52822307E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54344460E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.07902902E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.51177858E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.51017411E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.31992480E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85664410E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78646327E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.96160421E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.96480609E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.28431253E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79047770E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.37442683E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.56883437E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52603872E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61071829E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.13693306E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.34639813E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.34578368E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.19091139E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45038161E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78666484E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.73433247E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.88564351E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.70495697E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79552752E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.37721743E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.60103562E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52822307E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54344460E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.07902902E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.51177858E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.51017411E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.31992480E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85664410E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14207520E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.16514614E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.81060573E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.85241364E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77821756E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.91719843E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57083511E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05696532E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.52747608E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.44926490E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.44802553E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.73972509E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14207520E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.16514614E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.81060573E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.85241364E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77821756E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.91719843E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57083511E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05696532E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.52747608E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.44926490E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.44802553E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.73972509E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09055336E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.71166080E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.40737215E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.08747208E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40114994E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.49408184E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80957177E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08641394E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.79977307E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.14006321E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.79130437E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42891022E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.00804121E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86520190E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14313974E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.30041971E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.40069328E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.24119114E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78186088E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.15284962E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54806487E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14313974E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.30041971E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.40069328E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.24119114E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78186088E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.15284962E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54806487E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47015349E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.17838149E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.26925009E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.93703550E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52229112E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.62536816E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03039452E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.85353579E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33508977E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33508977E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11171087E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11171087E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10639872E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.83532467E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.71808134E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.58425043E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.85621029E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.19720832E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.69284864E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.46937929E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.76103583E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.72782613E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.42576333E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17761638E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52740853E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17761638E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52740853E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43652474E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50165861E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50165861E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47517022E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00046655E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00046655E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00046633E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.04745661E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.04745661E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.04745661E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.04745661E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.82486202E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.82486202E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.82486202E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.82486202E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.60208971E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.60208971E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.83322583E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.36045543E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.59027561E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.61800956E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.24334422E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.61800956E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.24334422E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.19553254E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.81738760E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.81738760E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.81154590E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.71380291E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.71380291E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.71379200E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.07148509E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.68688223E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.07148509E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.68688223E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.74761177E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.16037544E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.16037544E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.36931533E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.23138243E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.23138243E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.23138244E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.82808959E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.82808959E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.82808959E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.82808959E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     6.09357532E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     6.09357532E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     6.09357532E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     6.09357532E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.64033890E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.64033890E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.83409976E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.11740254E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.28193774E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.06251880E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.68518104E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.68518104E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.05338717E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.54258184E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.49526533E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.82172041E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.82172041E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.82979235E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.82979235E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.03799711E-03   # h decays
#          BR         NDA      ID1       ID2
     5.90720756E-01    2           5        -5   # BR(h -> b       bb     )
     6.45851894E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28630608E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84774066E-04    2           3        -3   # BR(h -> s       sb     )
     2.10540238E-02    2           4        -4   # BR(h -> c       cb     )
     6.87828541E-02    2          21        21   # BR(h -> g       g      )
     2.38614064E-03    2          22        22   # BR(h -> gam     gam    )
     1.64123417E-03    2          22        23   # BR(h -> Z       gam    )
     2.21997325E-01    2          24       -24   # BR(h -> W+      W-     )
     2.81190731E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.51526152E+01   # H decays
#          BR         NDA      ID1       ID2
     3.57074687E-01    2           5        -5   # BR(H -> b       bb     )
     6.01168515E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12558626E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51420191E-04    2           3        -3   # BR(H -> s       sb     )
     7.05196106E-08    2           4        -4   # BR(H -> c       cb     )
     7.06753325E-03    2           6        -6   # BR(H -> t       tb     )
     7.75737838E-07    2          21        21   # BR(H -> g       g      )
     9.48503536E-10    2          22        22   # BR(H -> gam     gam    )
     1.81711775E-09    2          23        22   # BR(H -> Z       gam    )
     1.79571744E-06    2          24       -24   # BR(H -> W+      W-     )
     8.97236709E-07    2          23        23   # BR(H -> Z       Z      )
     7.06182250E-06    2          25        25   # BR(H -> h       h      )
     1.69259487E-24    2          36        36   # BR(H -> A       A      )
     7.47927483E-21    2          23        36   # BR(H -> Z       A      )
     1.85126484E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.59541981E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.59541981E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.14832306E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.78514734E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.29886354E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.69272580E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.25265870E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.28118822E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.09305246E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.12226685E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.60076931E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.22567781E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.16570902E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     4.16570902E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.38375840E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.51485313E+01   # A decays
#          BR         NDA      ID1       ID2
     3.57126732E-01    2           5        -5   # BR(A -> b       bb     )
     6.01215837E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12575191E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51480811E-04    2           3        -3   # BR(A -> s       sb     )
     7.09794548E-08    2           4        -4   # BR(A -> c       cb     )
     7.08147436E-03    2           6        -6   # BR(A -> t       tb     )
     1.45506380E-05    2          21        21   # BR(A -> g       g      )
     5.94869260E-08    2          22        22   # BR(A -> gam     gam    )
     1.59969192E-08    2          23        22   # BR(A -> Z       gam    )
     1.79206523E-06    2          23        25   # BR(A -> Z       h      )
     1.86495490E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.59548848E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.59548848E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.86047560E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.57972579E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.09028956E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.28073937E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.02367076E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.40750897E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.21316847E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.70438693E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.88810860E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     4.34277913E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.34277913E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.52641382E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.73459811E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.00111856E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12184850E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.67013967E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20206711E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47303859E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.65105688E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.79031740E-06    2          24        25   # BR(H+ -> W+      h      )
     2.69059460E-14    2          24        36   # BR(H+ -> W+      A      )
     6.14496962E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.63854865E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.45068287E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.59427270E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.68661759E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57998484E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.14863642E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.19413538E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     8.49154797E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
