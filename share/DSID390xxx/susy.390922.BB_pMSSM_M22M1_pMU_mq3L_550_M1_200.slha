#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12853951E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04007548E+01   # W+
        25     1.24561151E+02   # h
        35     3.00019961E+03   # H
        36     2.99999958E+03   # A
        37     3.00095310E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02661911E+03   # ~d_L
   2000001     3.02144746E+03   # ~d_R
   1000002     3.02570144E+03   # ~u_L
   2000002     3.02328096E+03   # ~u_R
   1000003     3.02661911E+03   # ~s_L
   2000003     3.02144746E+03   # ~s_R
   1000004     3.02570144E+03   # ~c_L
   2000004     3.02328096E+03   # ~c_R
   1000005     6.26636068E+02   # ~b_1
   2000005     3.02088353E+03   # ~b_2
   1000006     6.24102123E+02   # ~t_1
   2000006     3.00659116E+03   # ~t_2
   1000011     3.00657967E+03   # ~e_L
   2000011     3.00129774E+03   # ~e_R
   1000012     3.00518541E+03   # ~nu_eL
   1000013     3.00657967E+03   # ~mu_L
   2000013     3.00129774E+03   # ~mu_R
   1000014     3.00518541E+03   # ~nu_muL
   1000015     2.98582271E+03   # ~tau_1
   2000015     3.02214039E+03   # ~tau_2
   1000016     3.00523955E+03   # ~nu_tauL
   1000021     2.33272564E+03   # ~g
   1000022     2.01577888E+02   # ~chi_10
   1000023     4.23878295E+02   # ~chi_20
   1000025    -3.00106894E+03   # ~chi_30
   1000035     3.00170556E+03   # ~chi_40
   1000024     4.24040438E+02   # ~chi_1+
   1000037     3.00231904E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99887589E-01   # N_11
  1  2    -9.62118326E-04   # N_12
  1  3     1.48627460E-02   # N_13
  1  4    -1.72719962E-03   # N_14
  2  1     1.36552443E-03   # N_21
  2  2     9.99633697E-01   # N_22
  2  3    -2.65932495E-02   # N_23
  2  4     4.83807306E-03   # N_24
  3  1    -9.27062885E-03   # N_31
  3  2     1.53946216E-02   # N_32
  3  3     7.06851792E-01   # N_33
  3  4     7.07133371E-01   # N_34
  4  1    -1.17047381E-02   # N_41
  4  2     2.22385360E-02   # N_42
  4  3     7.06705343E-01   # N_43
  4  4    -7.07061529E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99291930E-01   # U_11
  1  2    -3.76249762E-02   # U_12
  2  1     3.76249762E-02   # U_21
  2  2     9.99291930E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99976564E-01   # V_11
  1  2    -6.84630572E-03   # V_12
  2  1     6.84630572E-03   # V_21
  2  2     9.99976564E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98881368E-01   # cos(theta_t)
  1  2    -4.72864956E-02   # sin(theta_t)
  2  1     4.72864956E-02   # -sin(theta_t)
  2  2     9.98881368E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99919470E-01   # cos(theta_b)
  1  2     1.26906861E-02   # sin(theta_b)
  2  1    -1.26906861E-02   # -sin(theta_b)
  2  2     9.99919470E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06924880E-01   # cos(theta_tau)
  1  2     7.07288636E-01   # sin(theta_tau)
  2  1    -7.07288636E-01   # -sin(theta_tau)
  2  2     7.06924880E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01908727E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.28539512E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44586530E+02   # higgs               
         4     6.82362725E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.28539512E+03  # The gauge couplings
     1     3.61657881E-01   # gprime(Q) DRbar
     2     6.38085482E-01   # g(Q) DRbar
     3     1.02981055E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.28539512E+03  # The trilinear couplings
  1  1     1.53489335E-06   # A_u(Q) DRbar
  2  2     1.53491671E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.28539512E+03  # The trilinear couplings
  1  1     3.94245903E-07   # A_d(Q) DRbar
  2  2     3.94321221E-07   # A_s(Q) DRbar
  3  3     8.92233440E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.28539512E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     8.40762574E-08   # A_mu(Q) DRbar
  3  3     8.50725733E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.28539512E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.55386663E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.28539512E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.12989395E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.28539512E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06039619E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.28539512E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.76167769E+04   # M^2_Hd              
        22    -9.11184096E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41327314E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.05268739E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48254085E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48254085E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51745915E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51745915E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.93617035E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     3.07770401E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.74449062E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.94773898E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.04765778E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.22134856E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.11615666E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.25897225E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.22405427E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.62168139E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.51609415E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.00666020E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.57509114E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.90764921E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.98744997E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.52178510E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.24743148E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.87790387E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.07479853E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.72748923E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.48738970E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.76580824E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27849759E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.94770023E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.25739261E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.13089695E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.96418201E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.86518557E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.61245420E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     9.66408002E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.97895976E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.22536077E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.39789464E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.10353284E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19439291E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57280338E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.84913270E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.46218128E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.21509485E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42719368E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.96933089E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.98337370E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61014030E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.63731589E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.16978068E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.21964574E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.43015249E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.11037832E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68476412E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.47299495E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.10253155E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.33556007E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.24907987E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55269967E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.96418201E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.86518557E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.61245420E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     9.66408002E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.97895976E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.22536077E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.39789464E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.10353284E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19439291E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57280338E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.84913270E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.46218128E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.21509485E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42719368E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.96933089E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.98337370E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61014030E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.63731589E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.16978068E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.21964574E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.43015249E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.11037832E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68476412E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.47299495E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.10253155E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.33556007E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.24907987E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55269967E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89307565E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.92177084E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00713890E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.31383735E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.99347882E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00068393E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.10340792E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54753759E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998192E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.80768394E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.01633659E-12    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.89307565E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.92177084E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00713890E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.31383735E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.99347882E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00068393E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.10340792E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54753759E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998192E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.80768394E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.01633659E-12    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     2.74854425E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.51644000E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16454661E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31901339E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.69012279E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.59985443E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13682339E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.89781252E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.01702743E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26303057E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.00923954E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89334803E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.98396190E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99612765E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.99032811E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.39609085E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00547613E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.06540838E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.89334803E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.98396190E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99612765E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.99032811E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.39609085E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00547613E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.06540838E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89375797E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.98309387E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99587057E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.02533937E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.43966649E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00581802E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.00038001E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.17172775E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.83997886E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.70972677E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.27851865E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.71563403E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.76954671E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.10041747E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.60556697E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.02948846E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.21617861E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.75848832E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.52415117E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.86427750E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.33063075E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.55571315E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.57665793E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.57665793E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.58171315E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.21602066E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.66833733E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.66833733E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.25964053E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.25964053E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     6.03064087E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     6.03064087E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.77873360E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.42958883E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.20714757E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.64333718E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.64333718E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.38636247E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.83226185E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.64367846E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.64367846E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.28543179E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.28543179E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     8.99885964E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     8.99885964E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.99885964E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     8.99885964E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     8.10211861E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     8.10211861E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.83753796E-03   # h decays
#          BR         NDA      ID1       ID2
     6.82955946E-01    2           5        -5   # BR(h -> b       bb     )
     5.37900313E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.90419930E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.04408535E-04    2           3        -3   # BR(h -> s       sb     )
     1.74506639E-02    2           4        -4   # BR(h -> c       cb     )
     5.66823818E-02    2          21        21   # BR(h -> g       g      )
     1.91431158E-03    2          22        22   # BR(h -> gam     gam    )
     1.24173886E-03    2          22        23   # BR(h -> Z       gam    )
     1.64895225E-01    2          24       -24   # BR(h -> W+      W-     )
     2.04748727E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39209449E+01   # H decays
#          BR         NDA      ID1       ID2
     7.42277212E-01    2           5        -5   # BR(H -> b       bb     )
     1.78638595E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.31623384E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.75631324E-04    2           3        -3   # BR(H -> s       sb     )
     2.18944921E-07    2           4        -4   # BR(H -> c       cb     )
     2.18411769E-02    2           6        -6   # BR(H -> t       tb     )
     3.30114208E-05    2          21        21   # BR(H -> g       g      )
     4.78097355E-08    2          22        22   # BR(H -> gam     gam    )
     8.37987468E-09    2          23        22   # BR(H -> Z       gam    )
     3.42577798E-05    2          24       -24   # BR(H -> W+      W-     )
     1.71077712E-05    2          23        23   # BR(H -> Z       Z      )
     8.74242844E-05    2          25        25   # BR(H -> h       h      )
    -1.94357273E-23    2          36        36   # BR(H -> A       A      )
     2.47974487E-17    2          23        36   # BR(H -> Z       A      )
     2.23751953E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.11326249E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.11548422E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.09855048E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.14832577E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.24224228E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32103079E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82267802E-01    2           5        -5   # BR(A -> b       bb     )
     1.88240402E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.65572147E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.17460817E-04    2           3        -3   # BR(A -> s       sb     )
     2.30675856E-07    2           4        -4   # BR(A -> c       cb     )
     2.29987323E-02    2           6        -6   # BR(A -> t       tb     )
     6.77290758E-05    2          21        21   # BR(A -> g       g      )
     3.33827896E-08    2          22        22   # BR(A -> gam     gam    )
     6.67151160E-08    2          23        22   # BR(A -> Z       gam    )
     3.59617563E-05    2          23        25   # BR(A -> Z       h      )
     2.65766581E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22282793E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32487863E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.01182221E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.93162802E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.08783852E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.50462869E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85575613E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.96215549E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.20433243E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.07069853E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.08946070E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.79092094E-05    2          24        25   # BR(H+ -> W+      h      )
     1.04428329E-13    2          24        36   # BR(H+ -> W+      A      )
     2.05578646E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.79901336E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.53842437E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
