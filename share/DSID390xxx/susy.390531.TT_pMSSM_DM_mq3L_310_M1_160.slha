#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.84179270E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.86900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     3.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.30485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04170263E+01   # W+
        25     1.24926258E+02   # h
        35     4.00000212E+03   # H
        36     3.99999515E+03   # A
        37     4.00099701E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.00999207E+03   # ~d_L
   2000001     4.00975714E+03   # ~d_R
   1000002     4.00932617E+03   # ~u_L
   2000002     4.01178228E+03   # ~u_R
   1000003     4.00999207E+03   # ~s_L
   2000003     4.00975714E+03   # ~s_R
   1000004     4.00932617E+03   # ~c_L
   2000004     4.01178228E+03   # ~c_R
   1000005     3.36749361E+02   # ~b_1
   2000005     4.01635525E+03   # ~b_2
   1000006     3.25288846E+02   # ~t_1
   2000006     2.32374587E+03   # ~t_2
   1000011     4.00184366E+03   # ~e_L
   2000011     4.00217898E+03   # ~e_R
   1000012     4.00071907E+03   # ~nu_eL
   1000013     4.00184366E+03   # ~mu_L
   2000013     4.00217898E+03   # ~mu_R
   1000014     4.00071907E+03   # ~nu_muL
   1000015     4.00444554E+03   # ~tau_1
   2000015     4.00804030E+03   # ~tau_2
   1000016     4.00354550E+03   # ~nu_tauL
   1000021     1.95676836E+03   # ~g
   1000022     1.47415615E+02   # ~chi_10
   1000023    -1.96159443E+02   # ~chi_20
   1000025     2.09656606E+02   # ~chi_30
   1000035     2.05925684E+03   # ~chi_40
   1000024     1.92552300E+02   # ~chi_1+
   1000037     2.05942100E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.19524388E-01   # N_11
  1  2    -1.33444340E-02   # N_12
  1  3    -4.59912374E-01   # N_13
  1  4    -3.41587927E-01   # N_14
  2  1    -9.35787966E-02   # N_21
  2  2     2.64889141E-02   # N_22
  2  3    -6.95948014E-01   # N_23
  2  4     7.11475725E-01   # N_24
  3  1     5.65351148E-01   # N_31
  3  2     2.53726760E-02   # N_32
  3  3     5.51483538E-01   # N_33
  3  4     6.12862313E-01   # N_34
  4  1    -9.30290610E-04   # N_41
  4  2     9.99237955E-01   # N_42
  4  3    -1.69626888E-03   # N_43
  4  4    -3.89841815E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.40364771E-03   # U_11
  1  2     9.99997111E-01   # U_12
  2  1    -9.99997111E-01   # U_21
  2  2     2.40364771E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.51451259E-02   # V_11
  1  2    -9.98478350E-01   # V_12
  2  1    -9.98478350E-01   # V_21
  2  2    -5.51451259E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.97864278E-01   # cos(theta_t)
  1  2    -6.53213800E-02   # sin(theta_t)
  2  1     6.53213800E-02   # -sin(theta_t)
  2  2     9.97864278E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999834E-01   # cos(theta_b)
  1  2    -5.76194388E-04   # sin(theta_b)
  2  1     5.76194388E-04   # -sin(theta_b)
  2  2     9.99999834E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04114632E-01   # cos(theta_tau)
  1  2     7.10086322E-01   # sin(theta_tau)
  2  1    -7.10086322E-01   # -sin(theta_tau)
  2  2    -7.04114632E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00289071E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  8.41792700E+02  # DRbar Higgs Parameters
         1    -1.86900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44778460E+02   # higgs               
         4     1.62774223E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  8.41792700E+02  # The gauge couplings
     1     3.60586461E-01   # gprime(Q) DRbar
     2     6.36369961E-01   # g(Q) DRbar
     3     1.04014632E+00   # g3(Q) DRbar
#
BLOCK AU Q=  8.41792700E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     4.83113193E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  8.41792700E+02  # The trilinear couplings
  1  1     1.77095021E-07   # A_d(Q) DRbar
  2  2     1.77112228E-07   # A_s(Q) DRbar
  3  3     3.17089711E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  8.41792700E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     3.86657176E-08   # A_mu(Q) DRbar
  3  3     3.90490458E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  8.41792700E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74567244E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  8.41792700E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83188635E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  8.41792700E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03451507E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  8.41792700E+02  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58259136E+07   # M^2_Hd              
        22    -1.33912154E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     3.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.30485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40497181E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.63000703E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.46876257E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.46876257E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.53123743E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.53123743E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.98642487E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.71963253E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     7.28036747E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.39841447E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.33572290E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.20619447E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.76302271E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.84017793E-07    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.37055942E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.08428585E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.02331389E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.12105914E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.84068048E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.88491678E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.80267958E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.63621484E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.08849188E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.27529329E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.66242516E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.64148477E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.71905744E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.49720454E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.29550916E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.51507994E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.46642289E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.16103323E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.90533710E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.78729638E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.10779762E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.90726056E-08    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77790602E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.74509905E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.76359643E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.51955192E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.75935445E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.29590381E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.50650752E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53540375E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60810787E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.69925054E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.81421038E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.75603622E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.60805646E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44965685E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77810599E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.46592952E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.69818015E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.76664054E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.76409015E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.00621953E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.53822695E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53763411E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53999553E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.65216724E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.25613217E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.58187745E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.80017363E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85640335E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77790602E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.74509905E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.76359643E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.51955192E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.75935445E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.29590381E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.50650752E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53540375E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60810787E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.69925054E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.81421038E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.75603622E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.60805646E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44965685E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77810599E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.46592952E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.69818015E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.76664054E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.76409015E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.00621953E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.53822695E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53763411E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53999553E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.65216724E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.25613217E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.58187745E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.80017363E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85640335E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13146002E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04436044E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.60747318E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.11911675E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77546417E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.92094640E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56459702E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06301539E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.72229448E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.74659083E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.19023492E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.69554575E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13146002E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04436044E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.60747318E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.11911675E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77546417E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.92094640E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56459702E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06301539E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.72229448E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.74659083E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.19023492E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.69554575E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09136426E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.53000645E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.15495005E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.32252571E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39390947E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.43381362E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79468200E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09800825E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.46425177E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.74089185E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.07063015E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42207166E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.17806967E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85115027E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13254855E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17087746E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.23705983E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.45178096E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77845349E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11450928E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54197526E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13254855E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17087746E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.23705983E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.45178096E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77845349E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11450928E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54197526E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46557973E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05911164E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.92807575E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.02685315E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51578503E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.75091684E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01804557E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     6.53565721E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33612573E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33612573E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11205262E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11205262E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10364331E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.94570928E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.87586862E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.78757629E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.95208855E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.21734284E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.84856728E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.54831119E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.79924104E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.06454330E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.36655505E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18062183E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53046006E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18062183E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53046006E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41524261E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50390035E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50390035E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.46864444E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00366955E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00366955E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00366908E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.30074212E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.30074212E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.30074212E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.30074212E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.10024925E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.10024925E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.10024925E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.10024925E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.34594112E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.34594112E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.10167322E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.07231293E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.00148010E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.62852449E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.86282795E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.62852449E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.86282795E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.42951304E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.23735865E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.23735865E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.21962833E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.52331016E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.52331016E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.52329831E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.56128232E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.61684923E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.56128232E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.61684923E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.14271592E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.05720180E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.05720180E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     9.56001989E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.11256547E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.11256547E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.11256553E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.94918915E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.94918915E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.94918915E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.94918915E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.31637963E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.31637963E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.31637963E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.31637963E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.24837078E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.24837078E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.94466774E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.23223858E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.27004646E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.05773105E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.81312752E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.81312752E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.07888287E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.64600320E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.53003088E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.90797788E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.90797788E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.92396318E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.92396318E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.97178532E-03   # h decays
#          BR         NDA      ID1       ID2
     6.03040406E-01    2           5        -5   # BR(h -> b       bb     )
     6.52844156E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.31109096E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.90584475E-04    2           3        -3   # BR(h -> s       sb     )
     2.13049493E-02    2           4        -4   # BR(h -> c       cb     )
     7.10756699E-02    2          21        21   # BR(h -> g       g      )
     2.35050494E-03    2          22        22   # BR(h -> gam     gam    )
     1.56386754E-03    2          22        23   # BR(h -> Z       gam    )
     2.08548812E-01    2          24       -24   # BR(h -> W+      W-     )
     2.61096815E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.46605970E+01   # H decays
#          BR         NDA      ID1       ID2
     3.46123163E-01    2           5        -5   # BR(H -> b       bb     )
     6.06578712E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14471540E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53682892E-04    2           3        -3   # BR(H -> s       sb     )
     7.11592746E-08    2           4        -4   # BR(H -> c       cb     )
     7.13164077E-03    2           6        -6   # BR(H -> t       tb     )
     9.42117371E-07    2          21        21   # BR(H -> g       g      )
     2.03502143E-11    2          22        22   # BR(H -> gam     gam    )
     1.83182175E-09    2          23        22   # BR(H -> Z       gam    )
     1.90572586E-06    2          24       -24   # BR(H -> W+      W-     )
     9.52202768E-07    2          23        23   # BR(H -> Z       Z      )
     7.06378600E-06    2          25        25   # BR(H -> h       h      )
     1.38083687E-24    2          36        36   # BR(H -> A       A      )
     1.67304202E-20    2          23        36   # BR(H -> Z       A      )
     1.86629116E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.64692009E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.64692009E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.85030630E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.55904566E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.58330614E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.11208548E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.23338920E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.56636126E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.59329420E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.73333421E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.21601430E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     7.34523905E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.72437113E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     4.72437113E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.44999841E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.46601046E+01   # A decays
#          BR         NDA      ID1       ID2
     3.46152160E-01    2           5        -5   # BR(A -> b       bb     )
     6.06587877E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14474613E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53727880E-04    2           3        -3   # BR(A -> s       sb     )
     7.16136808E-08    2           4        -4   # BR(A -> c       cb     )
     7.14474978E-03    2           6        -6   # BR(A -> t       tb     )
     1.46806606E-05    2          21        21   # BR(A -> g       g      )
     4.70086539E-08    2          22        22   # BR(A -> gam     gam    )
     1.61375094E-08    2          23        22   # BR(A -> Z       gam    )
     1.90179867E-06    2          23        25   # BR(A -> Z       h      )
     1.86744147E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.64697183E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.64697183E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.46989107E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.93198383E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.28138526E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.69104854E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.90653297E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.43432591E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.79215580E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.25958610E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.82131069E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     4.81011923E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.81011923E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.46170173E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.53062038E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.07218463E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14697572E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.53959400E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21630309E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.50232658E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.52448608E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.90539585E-06    2          24        25   # BR(H+ -> W+      h      )
     2.43183708E-14    2          24        36   # BR(H+ -> W+      A      )
     5.60933086E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.42748316E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.26197869E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.65035936E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.01443298E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60191344E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00580332E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.00322082E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     9.46128172E-04    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
