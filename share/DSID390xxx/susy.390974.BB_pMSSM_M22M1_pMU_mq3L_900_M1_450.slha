#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16441039E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.49900000E+02   # M_1(MX)             
         2     8.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04050217E+01   # W+
        25     1.23583233E+02   # h
        35     3.00009953E+03   # H
        36     2.99999996E+03   # A
        37     3.00089385E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04052188E+03   # ~d_L
   2000001     3.03543002E+03   # ~d_R
   1000002     3.03961494E+03   # ~u_L
   2000002     3.03643942E+03   # ~u_R
   1000003     3.04052188E+03   # ~s_L
   2000003     3.03543002E+03   # ~s_R
   1000004     3.03961494E+03   # ~c_L
   2000004     3.03643942E+03   # ~c_R
   1000005     1.00614365E+03   # ~b_1
   2000005     3.03437465E+03   # ~b_2
   1000006     1.00468342E+03   # ~t_1
   2000006     3.02003971E+03   # ~t_2
   1000011     3.00625024E+03   # ~e_L
   2000011     3.00213532E+03   # ~e_R
   1000012     3.00486314E+03   # ~nu_eL
   1000013     3.00625024E+03   # ~mu_L
   2000013     3.00213532E+03   # ~mu_R
   1000014     3.00486314E+03   # ~nu_muL
   1000015     2.98545129E+03   # ~tau_1
   2000015     3.02205411E+03   # ~tau_2
   1000016     3.00459912E+03   # ~nu_tauL
   1000021     2.35025809E+03   # ~g
   1000022     4.52082779E+02   # ~chi_10
   1000023     9.35929280E+02   # ~chi_20
   1000025    -2.99515414E+03   # ~chi_30
   1000035     2.99630886E+03   # ~chi_40
   1000024     9.36090816E+02   # ~chi_1+
   1000037     2.99676564E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99879779E-01   # N_11
  1  2    -6.50746595E-04   # N_12
  1  3     1.51957790E-02   # N_13
  1  4    -3.01520366E-03   # N_14
  2  1     1.11834944E-03   # N_21
  2  2     9.99534896E-01   # N_22
  2  3    -2.88133094E-02   # N_23
  2  4     9.92648701E-03   # N_24
  3  1    -8.60195888E-03   # N_31
  3  2     1.33643459E-02   # N_32
  3  3     7.06893474E-01   # N_33
  3  4     7.07141441E-01   # N_34
  4  1    -1.28523236E-02   # N_41
  4  2     2.74037121E-02   # N_42
  4  3     7.06569528E-01   # N_43
  4  4    -7.06996009E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99168980E-01   # U_11
  1  2    -4.07596606E-02   # U_12
  2  1     4.07596606E-02   # U_21
  2  2     9.99168980E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99901386E-01   # V_11
  1  2    -1.40434458E-02   # V_12
  2  1     1.40434458E-02   # V_21
  2  2     9.99901386E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98761748E-01   # cos(theta_t)
  1  2    -4.97490777E-02   # sin(theta_t)
  2  1     4.97490777E-02   # -sin(theta_t)
  2  2     9.98761748E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99905387E-01   # cos(theta_b)
  1  2     1.37556188E-02   # sin(theta_b)
  2  1    -1.37556188E-02   # -sin(theta_b)
  2  2     9.99905387E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06971871E-01   # cos(theta_tau)
  1  2     7.07241666E-01   # sin(theta_tau)
  2  1    -7.07241666E-01   # -sin(theta_tau)
  2  2     7.06971871E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01798428E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.64410391E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44103037E+02   # higgs               
         4     7.63519153E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.64410391E+03  # The gauge couplings
     1     3.62480630E-01   # gprime(Q) DRbar
     2     6.36602422E-01   # g(Q) DRbar
     3     1.02426636E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.64410391E+03  # The trilinear couplings
  1  1     2.57451551E-06   # A_u(Q) DRbar
  2  2     2.57455091E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.64410391E+03  # The trilinear couplings
  1  1     7.34035346E-07   # A_d(Q) DRbar
  2  2     7.34152036E-07   # A_s(Q) DRbar
  3  3     1.53573260E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.64410391E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.44270129E-07   # A_mu(Q) DRbar
  3  3     1.45947549E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.64410391E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49420140E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.64410391E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.17829452E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.64410391E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.08182622E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.64410391E+03  # The soft SUSY breaking masses at the scale Q
         1     4.49900000E+02   # M_1(Q)              
         2     8.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.69806608E+04   # M^2_Hd              
        22    -9.02596408E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40652427E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.37122172E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48171839E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48171839E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51828161E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51828161E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.34399651E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     3.57131432E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     6.42868568E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     9.82873373E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.46743989E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.21232943E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.47395632E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.10751498E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.40038376E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.55088584E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.48013268E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.90805993E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.75385540E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.50039361E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.49960639E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.19714050E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.82819003E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.09323994E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.37944628E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.27114948E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.71206087E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28978588E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.93746430E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.24614765E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.08567001E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.44266785E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.20748017E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.49089860E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.40987807E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.03689866E-07    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.98250468E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.42888372E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.46451999E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.13862631E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55496975E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.67143366E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.00776076E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.12914942E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44502826E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.44814318E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.28995830E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.48900709E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.09077549E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.54084608E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.97688642E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.63392440E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.47119932E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64780333E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.40885335E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.73855509E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.72513227E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.74941936E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55911411E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.44266785E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.20748017E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.49089860E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.40987807E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.03689866E-07    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.98250468E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.42888372E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.46451999E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.13862631E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55496975E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.67143366E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.00776076E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.12914942E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44502826E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.44814318E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.28995830E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.48900709E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.09077549E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.54084608E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.97688642E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.63392440E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.47119932E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64780333E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.40885335E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.73855509E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.72513227E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.74941936E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55911411E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.33634469E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.12216836E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.96338232E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.41362630E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.38891233E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.91444876E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.79025421E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.49876230E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998928E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.06751545E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.67212451E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.60109540E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.33634469E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.12216836E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.96338232E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.41362630E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.38891233E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.91444876E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.79025421E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.49876230E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998928E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.06751545E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.67212451E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.60109540E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.44080755E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.83323069E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.05857580E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.10819351E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.39149322E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.92275658E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.02870045E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.68451139E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.85144467E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.04799833E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.91045301E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.33629438E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.12675937E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.95393534E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.04037412E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.41495306E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.91930506E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     4.14873602E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.33629438E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.12675937E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.95393534E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.04037412E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.41495306E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.91930506E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     4.14873602E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.33622104E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.12667597E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95362918E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.77091483E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.32922110E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.91967377E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.09100264E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.67419548E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.97171047E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.87656460E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.03356520E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.52204546E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.38013762E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.34126518E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.18350278E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.88284259E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.39533315E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.07369922E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.39263008E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.97794164E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.67063814E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.94526632E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.16438674E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.16438674E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.76321937E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.44147385E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.55970295E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.55970295E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.32170795E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.32170795E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.56714615E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.56714615E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.91086663E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.54966286E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.43070650E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.23148568E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.23148568E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.75876096E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.48216424E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.52051409E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.52051409E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.35003052E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.35003052E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.39241406E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.39241406E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.60605747E-03   # h decays
#          BR         NDA      ID1       ID2
     6.89707857E-01    2           5        -5   # BR(h -> b       bb     )
     5.60240625E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.98332372E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.21863157E-04    2           3        -3   # BR(h -> s       sb     )
     1.82118320E-02    2           4        -4   # BR(h -> c       cb     )
     5.79292000E-02    2          21        21   # BR(h -> g       g      )
     1.94743059E-03    2          22        22   # BR(h -> gam     gam    )
     1.19255188E-03    2          22        23   # BR(h -> Z       gam    )
     1.55380704E-01    2          24       -24   # BR(h -> W+      W-     )
     1.89861664E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39822683E+01   # H decays
#          BR         NDA      ID1       ID2
     7.49557894E-01    2           5        -5   # BR(H -> b       bb     )
     1.77849387E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.28832931E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.72208084E-04    2           3        -3   # BR(H -> s       sb     )
     2.17882646E-07    2           4        -4   # BR(H -> c       cb     )
     2.17351921E-02    2           6        -6   # BR(H -> t       tb     )
     2.34429220E-05    2          21        21   # BR(H -> g       g      )
     3.33269582E-08    2          22        22   # BR(H -> gam     gam    )
     8.38610876E-09    2          23        22   # BR(H -> Z       gam    )
     3.09447171E-05    2          24       -24   # BR(H -> W+      W-     )
     1.54532881E-05    2          23        23   # BR(H -> Z       Z      )
     8.10571455E-05    2          25        25   # BR(H -> h       h      )
    -1.52814653E-23    2          36        36   # BR(H -> A       A      )
     1.15039038E-18    2          23        36   # BR(H -> Z       A      )
     1.38999233E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.02205746E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.93478037E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.69249426E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.65447450E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.65801679E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33561980E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84772205E-01    2           5        -5   # BR(A -> b       bb     )
     1.86184271E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.58302171E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.08531763E-04    2           3        -3   # BR(A -> s       sb     )
     2.28156203E-07    2           4        -4   # BR(A -> c       cb     )
     2.27475191E-02    2           6        -6   # BR(A -> t       tb     )
     6.69892665E-05    2          21        21   # BR(A -> g       g      )
     7.71845568E-08    2          22        22   # BR(A -> gam     gam    )
     6.60349129E-08    2          23        22   # BR(A -> Z       gam    )
     3.22765008E-05    2          23        25   # BR(A -> Z       h      )
     2.55346499E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22459475E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.27371952E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.79889364E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.03565173E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.11415556E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.40182569E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.49226980E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.13058448E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.99073231E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02675412E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.24137336E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.16883796E-05    2          24        25   # BR(H+ -> W+      h      )
     7.25120619E-14    2          24        36   # BR(H+ -> W+      A      )
     1.77163621E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.99877619E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.08195944E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
