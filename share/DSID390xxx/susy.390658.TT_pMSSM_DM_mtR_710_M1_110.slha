#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12014919E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.52959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.24900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.05485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     7.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04186261E+01   # W+
        25     1.25642895E+02   # h
        35     4.00000048E+03   # H
        36     3.99999617E+03   # A
        37     4.00105010E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02310167E+03   # ~d_L
   2000001     4.02038951E+03   # ~d_R
   1000002     4.02245067E+03   # ~u_L
   2000002     4.02097910E+03   # ~u_R
   1000003     4.02310167E+03   # ~s_L
   2000003     4.02038951E+03   # ~s_R
   1000004     4.02245067E+03   # ~c_L
   2000004     4.02097910E+03   # ~c_R
   1000005     2.08350460E+03   # ~b_1
   2000005     4.02404507E+03   # ~b_2
   1000006     7.34192627E+02   # ~t_1
   2000006     2.09603297E+03   # ~t_2
   1000011     4.00386383E+03   # ~e_L
   2000011     4.00366846E+03   # ~e_R
   1000012     4.00275102E+03   # ~nu_eL
   1000013     4.00386383E+03   # ~mu_L
   2000013     4.00366846E+03   # ~mu_R
   1000014     4.00275102E+03   # ~nu_muL
   1000015     4.00537153E+03   # ~tau_1
   2000015     4.00725693E+03   # ~tau_2
   1000016     4.00445267E+03   # ~nu_tauL
   1000021     1.98623406E+03   # ~g
   1000022     9.13555820E+01   # ~chi_10
   1000023    -1.35353898E+02   # ~chi_20
   1000025     1.53143207E+02   # ~chi_30
   1000035     2.06542803E+03   # ~chi_40
   1000024     1.29921385E+02   # ~chi_1+
   1000037     2.06560099E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.50968823E-01   # N_11
  1  2     1.41317381E-02   # N_12
  1  3     5.42939458E-01   # N_13
  1  4     3.75583367E-01   # N_14
  2  1    -1.36992439E-01   # N_21
  2  2     2.72472934E-02   # N_22
  2  3    -6.84890726E-01   # N_23
  2  4     7.15133100E-01   # N_24
  3  1     6.45970656E-01   # N_31
  3  2     2.35997046E-02   # N_32
  3  3     4.85943627E-01   # N_33
  3  4     5.88237841E-01   # N_34
  4  1    -9.00223746E-04   # N_41
  4  2     9.99250185E-01   # N_42
  4  3    -4.79745496E-04   # N_43
  4  4    -3.87043476E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.82740495E-04   # U_11
  1  2     9.99999767E-01   # U_12
  2  1    -9.99999767E-01   # U_21
  2  2     6.82740495E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.47494652E-02   # V_11
  1  2    -9.98500123E-01   # V_12
  2  1    -9.98500123E-01   # V_21
  2  2    -5.47494652E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.00546844E-01   # cos(theta_t)
  1  2     9.94932325E-01   # sin(theta_t)
  2  1    -9.94932325E-01   # -sin(theta_t)
  2  2    -1.00546844E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999869E-01   # cos(theta_b)
  1  2    -5.11859339E-04   # sin(theta_b)
  2  1     5.11859339E-04   # -sin(theta_b)
  2  2     9.99999869E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03468826E-01   # cos(theta_tau)
  1  2     7.10726115E-01   # sin(theta_tau)
  2  1    -7.10726115E-01   # -sin(theta_tau)
  2  2    -7.03468826E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00321101E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20149190E+03  # DRbar Higgs Parameters
         1    -1.24900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43731124E+02   # higgs               
         4     1.60962573E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20149190E+03  # The gauge couplings
     1     3.61971106E-01   # gprime(Q) DRbar
     2     6.36603908E-01   # g(Q) DRbar
     3     1.03123377E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20149190E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.13864763E-06   # A_c(Q) DRbar
  3  3     2.52959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20149190E+03  # The trilinear couplings
  1  1     4.17072689E-07   # A_d(Q) DRbar
  2  2     4.17112122E-07   # A_s(Q) DRbar
  3  3     7.46618165E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20149190E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     8.99479791E-08   # A_mu(Q) DRbar
  3  3     9.08737984E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20149190E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69457588E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20149190E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.80861878E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20149190E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04173626E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20149190E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58680950E+07   # M^2_Hd              
        22    -6.28092278E+03   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.05485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     7.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40593495E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.15417445E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.09931466E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.20847562E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.21907063E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.02276517E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.83731664E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.39850398E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     7.55869621E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.40174256E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.52304621E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.41681655E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.31087812E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.94015464E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.83519444E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.20571513E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.52093868E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.30710396E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.81622104E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     6.48089821E-05    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     6.99323183E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.34124754E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     1.90756896E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64118038E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.83319132E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.71693619E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.42077940E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.69432676E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.59565101E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.22289594E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14283896E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.18232914E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.40141981E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.77270001E-07    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.40109850E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76104031E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.45586044E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.45611236E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.88596281E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82849932E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.30564491E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.64465188E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51395955E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58764964E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.18360131E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.05830930E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.35180241E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.49705447E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43587628E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76122787E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.16867162E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.50566079E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.63899681E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83318366E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.25116785E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.67641827E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51620761E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51997071E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.31214413E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.76316534E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.14037678E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.51827412E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85271156E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76104031E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.45586044E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.45611236E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.88596281E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82849932E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.30564491E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.64465188E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51395955E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58764964E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.18360131E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.05830930E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.35180241E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.49705447E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43587628E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76122787E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.16867162E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.50566079E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.63899681E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83318366E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.25116785E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.67641827E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51620761E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51997071E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.31214413E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.76316534E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.14037678E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.51827412E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85271156E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13043889E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.77972035E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.31951215E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.85534398E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76998832E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.79666167E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55330533E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08334716E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.64411520E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.87587671E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.16829276E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.37256521E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13043889E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.77972035E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.31951215E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.85534398E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76998832E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.79666167E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55330533E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08334716E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.64411520E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.87587671E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.16829276E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.37256521E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10294605E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.23341107E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.01678452E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66558564E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38359313E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41896315E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.77383539E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11116287E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.06981072E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.38069543E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.42585064E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41140874E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.25242403E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.82961796E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13153067E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00166399E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.68315315E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.06904772E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77281236E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.08258353E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53096151E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13153067E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00166399E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.68315315E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.06904772E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77281236E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.08258353E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53096151E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46587285E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.05422076E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.13710965E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.48593005E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50793854E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.84064798E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00261049E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.41811220E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33721750E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33721750E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11241357E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11241357E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10073786E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.70274510E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.83791458E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.45192879E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.17614786E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.42298638E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.73437901E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.38929958E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.64134780E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.24043642E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18504656E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53702011E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18504656E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53702011E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37514932E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52345146E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52345146E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48024352E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04484299E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04484299E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04484267E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.74222537E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.74222537E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.74222537E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.74222537E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.24741157E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.24741157E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.24741157E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.24741157E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.48025749E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.48025749E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.41385758E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.66135255E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.91160022E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.52714782E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.56152127E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.52714782E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.56152127E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.30027373E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.03525673E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.03525673E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.02617604E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.09588075E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.09588075E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.09587707E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     7.19535173E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     9.33331939E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     7.19535173E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     9.33331939E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.62009879E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.14017041E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.14017041E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.02093332E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.27738730E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.27738730E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.27738743E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.34375735E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.34375735E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     9.34375735E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     9.34375735E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.11453197E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.11453197E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.11453197E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.11453197E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.02543112E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.02543112E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.70140932E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.36759272E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.28534396E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.30426637E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.39193915E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.39193915E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.89388732E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.16904628E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.26130261E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89513272E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.89513272E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.07554919E-03   # h decays
#          BR         NDA      ID1       ID2
     5.94996152E-01    2           5        -5   # BR(h -> b       bb     )
     6.39963071E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26546028E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.80362248E-04    2           3        -3   # BR(h -> s       sb     )
     2.08583266E-02    2           4        -4   # BR(h -> c       cb     )
     6.80156621E-02    2          21        21   # BR(h -> g       g      )
     2.35984592E-03    2          22        22   # BR(h -> gam     gam    )
     1.62434154E-03    2          22        23   # BR(h -> Z       gam    )
     2.19626026E-01    2          24       -24   # BR(h -> W+      W-     )
     2.78164303E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.40687570E+01   # H decays
#          BR         NDA      ID1       ID2
     3.38274438E-01    2           5        -5   # BR(H -> b       bb     )
     6.13217911E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.16819001E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.56459547E-04    2           3        -3   # BR(H -> s       sb     )
     7.19473667E-08    2           4        -4   # BR(H -> c       cb     )
     7.21062396E-03    2           6        -6   # BR(H -> t       tb     )
     1.10111483E-06    2          21        21   # BR(H -> g       g      )
     3.19518144E-10    2          22        22   # BR(H -> gam     gam    )
     1.85035482E-09    2          23        22   # BR(H -> Z       gam    )
     2.10559016E-06    2          24       -24   # BR(H -> W+      W-     )
     1.05206578E-06    2          23        23   # BR(H -> Z       Z      )
     7.59848846E-06    2          25        25   # BR(H -> h       h      )
     6.95841399E-24    2          36        36   # BR(H -> A       A      )
    -6.35046078E-22    2          23        36   # BR(H -> Z       A      )
     1.86824754E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66932694E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66932694E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.40307962E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.64618152E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.70705843E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.42238819E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.48761086E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     5.11390647E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.12150977E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.40620576E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.22069828E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.04140402E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.39362357E-07    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.39362357E-07    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.40672332E+01   # A decays
#          BR         NDA      ID1       ID2
     3.38310106E-01    2           5        -5   # BR(A -> b       bb     )
     6.13239541E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.16826478E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.56510177E-04    2           3        -3   # BR(A -> s       sb     )
     7.23989729E-08    2           4        -4   # BR(A -> c       cb     )
     7.22309676E-03    2           6        -6   # BR(A -> t       tb     )
     1.48416391E-05    2          21        21   # BR(A -> g       g      )
     4.06832327E-08    2          22        22   # BR(A -> gam     gam    )
     1.63190090E-08    2          23        22   # BR(A -> Z       gam    )
     2.10122486E-06    2          23        25   # BR(A -> Z       h      )
     1.87155940E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66939436E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66939436E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.96921628E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.27699553E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.34732396E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.89871892E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.39051586E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.77679157E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.39680635E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.28841428E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.67129476E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.46864198E-07    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.46864198E-07    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.38933494E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.37922364E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.15380223E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.17583370E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.44270014E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.23264964E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.53595669E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.43087687E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.11040931E-06    2          24        25   # BR(H+ -> W+      h      )
     3.17503555E-14    2          24        36   # BR(H+ -> W+      A      )
     4.72554301E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.51109217E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.25249557E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67697964E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.93331424E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57623419E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.92612482E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.06245510E-08    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
