#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12013242E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.52959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.29900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.05485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     7.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04201527E+01   # W+
        25     1.25635882E+02   # h
        35     4.00000845E+03   # H
        36     3.99999672E+03   # A
        37     4.00101841E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02308298E+03   # ~d_L
   2000001     4.02037373E+03   # ~d_R
   1000002     4.02243379E+03   # ~u_L
   2000002     4.02096397E+03   # ~u_R
   1000003     4.02308298E+03   # ~s_L
   2000003     4.02037373E+03   # ~s_R
   1000004     4.02243379E+03   # ~c_L
   2000004     4.02096397E+03   # ~c_R
   1000005     2.08368206E+03   # ~b_1
   2000005     4.02414793E+03   # ~b_2
   1000006     7.37740285E+02   # ~t_1
   2000006     2.09628049E+03   # ~t_2
   1000011     4.00384898E+03   # ~e_L
   2000011     4.00359911E+03   # ~e_R
   1000012     4.00273935E+03   # ~nu_eL
   1000013     4.00384898E+03   # ~mu_L
   2000013     4.00359911E+03   # ~mu_R
   1000014     4.00273935E+03   # ~nu_muL
   1000015     4.00462005E+03   # ~tau_1
   2000015     4.00787827E+03   # ~tau_2
   1000016     4.00442689E+03   # ~nu_tauL
   1000021     1.98622284E+03   # ~g
   1000022     2.93622248E+02   # ~chi_10
   1000023    -3.40896034E+02   # ~chi_20
   1000025     3.58664138E+02   # ~chi_30
   1000035     2.06527738E+03   # ~chi_40
   1000024     3.38284972E+02   # ~chi_1+
   1000037     2.06544984E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.96323279E-01   # N_11
  1  2    -1.68603019E-02   # N_12
  1  3    -4.57273647E-01   # N_13
  1  4    -3.95582834E-01   # N_14
  2  1    -5.09791872E-02   # N_21
  2  2     2.46804628E-02   # N_22
  2  3    -7.02987696E-01   # N_23
  2  4     7.08943084E-01   # N_24
  3  1     6.02718277E-01   # N_31
  3  2     2.60523230E-02   # N_32
  3  3     5.44691081E-01   # N_33
  3  4     5.82549209E-01   # N_34
  4  1    -1.01857139E-03   # N_41
  4  2     9.99213632E-01   # N_42
  4  3    -4.55375896E-03   # N_43
  4  4    -3.93744124E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.44547938E-03   # U_11
  1  2     9.99979228E-01   # U_12
  2  1    -9.99979228E-01   # U_21
  2  2     6.44547938E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.56948643E-02   # V_11
  1  2    -9.98447836E-01   # V_12
  2  1    -9.98447836E-01   # V_21
  2  2    -5.56948643E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.00943352E-01   # cos(theta_t)
  1  2     9.94892175E-01   # sin(theta_t)
  2  1    -9.94892175E-01   # -sin(theta_t)
  2  2    -1.00943352E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999050E-01   # cos(theta_b)
  1  2    -1.37840455E-03   # sin(theta_b)
  2  1     1.37840455E-03   # -sin(theta_b)
  2  2     9.99999050E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05836807E-01   # cos(theta_tau)
  1  2     7.08374479E-01   # sin(theta_tau)
  2  1    -7.08374479E-01   # -sin(theta_tau)
  2  2    -7.05836807E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00269527E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20132417E+03  # DRbar Higgs Parameters
         1    -3.29900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43831910E+02   # higgs               
         4     1.61668351E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20132417E+03  # The gauge couplings
     1     3.61780644E-01   # gprime(Q) DRbar
     2     6.35507932E-01   # g(Q) DRbar
     3     1.03123057E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20132417E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.13712462E-06   # A_c(Q) DRbar
  3  3     2.52959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20132417E+03  # The trilinear couplings
  1  1     4.18423760E-07   # A_d(Q) DRbar
  2  2     4.18463090E-07   # A_s(Q) DRbar
  3  3     7.48631703E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20132417E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.26233695E-08   # A_mu(Q) DRbar
  3  3     9.35734982E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20132417E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69121322E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20132417E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85751576E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20132417E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03378904E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20132417E+03  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57758008E+07   # M^2_Hd              
        22    -1.00048001E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.05485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     7.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40103159E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.14496447E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.35681916E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.28623583E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.03238890E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.05856976E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.98041775E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.27408913E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     8.28279738E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.29257095E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.44350450E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.44701978E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.40604667E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.01491991E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.87361688E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.07619670E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.03873475E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.51737831E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.22489344E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     6.90052774E-05    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     6.89071987E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.45954411E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     1.98453501E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64425094E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.62546190E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82245939E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.58047017E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.03173866E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.67009711E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.78916770E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12599454E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.75049913E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.16849941E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.07180335E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.03540900E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76007016E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.57782344E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.96451899E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.71760054E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81442106E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.41300901E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.61666758E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51832744E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58660865E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.54677421E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.44841372E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.02157412E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.19579520E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44171643E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76025994E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.47014872E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.02961231E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.79480482E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81951724E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.25012307E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64893231E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52055664E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51966815E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.25610658E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.77995806E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.27574541E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.33844365E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85430340E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76007016E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.57782344E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.96451899E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.71760054E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81442106E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.41300901E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.61666758E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51832744E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58660865E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.54677421E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.44841372E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.02157412E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.19579520E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44171643E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76025994E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.47014872E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.02961231E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.79480482E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81951724E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.25012307E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64893231E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52055664E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51966815E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.25610658E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.77995806E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.27574541E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.33844365E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85430340E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11560136E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.72972926E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.58761778E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.92347958E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77329270E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.22859062E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56086768E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05860519E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.35364077E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.59414264E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.62041214E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.65948944E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11560136E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.72972926E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.58761778E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.92347958E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77329270E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.22859062E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56086768E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05860519E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.35364077E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.59414264E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.62041214E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.65948944E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07917440E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.34688915E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.41991622E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.47445326E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39288557E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.50827167E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79295323E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.07558522E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.33414998E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.19208578E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.27670241E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41903084E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05559373E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84534882E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11669907E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.12840247E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.46642227E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.10376410E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77687168E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.15528555E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53813236E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11669907E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.12840247E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.46642227E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.10376410E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77687168E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.15528555E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53813236E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44443487E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.02147547E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.32746888E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.62015418E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51525066E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.71617161E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01636661E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.75607686E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33618642E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33618642E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11207614E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11207614E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10347486E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.66218956E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.98718467E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.44757899E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.80930054E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.38834076E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.69625844E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.38817329E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.10682869E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.44055628E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18298424E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53467611E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18298424E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53467611E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.39743923E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52009086E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52009086E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48424789E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03775097E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03775097E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03775078E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     5.80664994E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     5.80664994E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     5.80664994E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     5.80664994E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.93555190E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.93555190E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.93555190E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.93555190E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.55681036E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.55681036E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.49624034E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.45004248E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.49438917E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     2.40344572E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     3.09798661E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.40344572E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     3.09798661E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.96489229E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     6.94935649E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     6.94935649E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     6.93633265E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.42837690E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.42837690E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.42837149E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.46245759E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.89730051E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.46245759E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.89730051E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     9.39616967E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     4.35223566E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.35223566E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.11161788E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     8.70015262E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     8.70015262E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     8.70015275E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.90190499E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.90190499E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     9.90190499E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     9.90190499E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.30059319E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.30059319E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.30059319E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.30059319E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.17920046E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.17920046E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.66056889E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.91091308E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.50907175E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.49170040E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38666138E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38666138E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.96920154E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.21451471E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.06523961E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.96866453E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.96866453E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.05266951E-03   # h decays
#          BR         NDA      ID1       ID2
     5.92987946E-01    2           5        -5   # BR(h -> b       bb     )
     6.43407470E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27765371E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.82954253E-04    2           3        -3   # BR(h -> s       sb     )
     2.09751524E-02    2           4        -4   # BR(h -> c       cb     )
     6.83782347E-02    2          21        21   # BR(h -> g       g      )
     2.37562505E-03    2          22        22   # BR(h -> gam     gam    )
     1.63259099E-03    2          22        23   # BR(h -> Z       gam    )
     2.20649704E-01    2          24       -24   # BR(h -> W+      W-     )
     2.79492800E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.46252495E+01   # H decays
#          BR         NDA      ID1       ID2
     3.54489978E-01    2           5        -5   # BR(H -> b       bb     )
     6.06972302E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14610704E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53847450E-04    2           3        -3   # BR(H -> s       sb     )
     7.11998608E-08    2           4        -4   # BR(H -> c       cb     )
     7.13570848E-03    2           6        -6   # BR(H -> t       tb     )
     7.84833520E-07    2          21        21   # BR(H -> g       g      )
     7.59264077E-10    2          22        22   # BR(H -> gam     gam    )
     1.83589771E-09    2          23        22   # BR(H -> Z       gam    )
     1.80271999E-06    2          24       -24   # BR(H -> W+      W-     )
     9.00735686E-07    2          23        23   # BR(H -> Z       Z      )
     6.89759294E-06    2          25        25   # BR(H -> h       h      )
    -1.01941170E-24    2          36        36   # BR(H -> A       A      )
    -1.66686939E-20    2          23        36   # BR(H -> Z       A      )
     1.86282859E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.60279449E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.60279449E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.67462307E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.12788939E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.70471277E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.24087917E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.77918275E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.29083219E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.57913861E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.11100364E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.67734278E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.02197651E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.54847364E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.54847364E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.46196185E+01   # A decays
#          BR         NDA      ID1       ID2
     3.54552275E-01    2           5        -5   # BR(A -> b       bb     )
     6.07037741E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14633673E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53916038E-04    2           3        -3   # BR(A -> s       sb     )
     7.16667880E-08    2           4        -4   # BR(A -> c       cb     )
     7.15004818E-03    2           6        -6   # BR(A -> t       tb     )
     1.46915402E-05    2          21        21   # BR(A -> g       g      )
     5.86829764E-08    2          22        22   # BR(A -> gam     gam    )
     1.61582633E-08    2          23        22   # BR(A -> Z       gam    )
     1.79910810E-06    2          23        25   # BR(A -> Z       h      )
     1.87405212E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.60292650E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.60292650E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.29897760E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.98553783E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.42183365E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.75860328E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.97257387E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.43461547E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.78264955E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.77643287E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.86237800E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.71166564E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.71166564E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.45969630E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.66788805E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.07444753E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14777583E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.62744525E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21675554E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.50325744E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.61024266E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.80186430E-06    2          24        25   # BR(H+ -> W+      h      )
     2.68323348E-14    2          24        36   # BR(H+ -> W+      A      )
     5.16635432E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.91546408E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.54693323E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.60570353E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.73918289E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59012158E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.56105999E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     7.45703945E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
