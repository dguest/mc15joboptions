#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11565653E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03969850E+01   # W+
        25     1.26432097E+02   # h
        35     3.00019336E+03   # H
        36     3.00000007E+03   # A
        37     3.00110714E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02045718E+03   # ~d_L
   2000001     3.01488706E+03   # ~d_R
   1000002     3.01953529E+03   # ~u_L
   2000002     3.01842257E+03   # ~u_R
   1000003     3.02045718E+03   # ~s_L
   2000003     3.01488706E+03   # ~s_R
   1000004     3.01953529E+03   # ~c_L
   2000004     3.01842257E+03   # ~c_R
   1000005     5.15028909E+02   # ~b_1
   2000005     3.01677411E+03   # ~b_2
   1000006     5.10724070E+02   # ~t_1
   2000006     3.01900031E+03   # ~t_2
   1000011     3.00753802E+03   # ~e_L
   2000011     2.99959724E+03   # ~e_R
   1000012     3.00614134E+03   # ~nu_eL
   1000013     3.00753802E+03   # ~mu_L
   2000013     2.99959724E+03   # ~mu_R
   1000014     3.00614134E+03   # ~nu_muL
   1000015     2.98632816E+03   # ~tau_1
   2000015     3.02256416E+03   # ~tau_2
   1000016     3.00674499E+03   # ~nu_tauL
   1000021     2.32504856E+03   # ~g
   1000022     1.01403488E+02   # ~chi_10
   1000023     2.16786960E+02   # ~chi_20
   1000025    -3.00840120E+03   # ~chi_30
   1000035     3.00850202E+03   # ~chi_40
   1000024     2.16945927E+02   # ~chi_1+
   1000037     3.00938653E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891912E-01   # N_11
  1  2     7.65255120E-04   # N_12
  1  3    -1.46805345E-02   # N_13
  1  4     2.45362106E-04   # N_14
  2  1    -3.82570324E-04   # N_21
  2  2     9.99660216E-01   # N_22
  2  3     2.60598809E-02   # N_23
  2  4     4.35502873E-04   # N_24
  3  1     1.02178228E-02   # N_31
  3  2    -1.87289656E-02   # N_32
  3  3     7.06778401E-01   # N_33
  3  4     7.07113226E-01   # N_34
  4  1    -1.05647361E-02   # N_41
  4  2     1.81133555E-02   # N_42
  4  3    -7.06802417E-01   # N_43
  4  4     7.07100160E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99321409E-01   # U_11
  1  2     3.68336978E-02   # U_12
  2  1    -3.68336978E-02   # U_21
  2  2     9.99321409E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999811E-01   # V_11
  1  2    -6.14298175E-04   # V_12
  2  1    -6.14298175E-04   # V_21
  2  2    -9.99999811E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98649584E-01   # cos(theta_t)
  1  2    -5.19519815E-02   # sin(theta_t)
  2  1     5.19519815E-02   # -sin(theta_t)
  2  2     9.98649584E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99712944E-01   # cos(theta_b)
  1  2    -2.39589148E-02   # sin(theta_b)
  2  1     2.39589148E-02   # -sin(theta_b)
  2  2     9.99712944E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06891207E-01   # cos(theta_tau)
  1  2     7.07322290E-01   # sin(theta_tau)
  2  1    -7.07322290E-01   # -sin(theta_tau)
  2  2    -7.06891207E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00181108E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.15656534E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44608378E+02   # higgs               
         4     1.16847619E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.15656534E+03  # The gauge couplings
     1     3.61277838E-01   # gprime(Q) DRbar
     2     6.39387568E-01   # g(Q) DRbar
     3     1.03215750E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.15656534E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.22050637E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.15656534E+03  # The trilinear couplings
  1  1     4.33182891E-07   # A_d(Q) DRbar
  2  2     4.33235885E-07   # A_s(Q) DRbar
  3  3     9.08374351E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.15656534E+03  # The trilinear couplings
  1  1     2.10797140E-07   # A_e(Q) DRbar
  2  2     2.10807683E-07   # A_mu(Q) DRbar
  3  3     2.13818521E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.15656534E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.58919693E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.15656534E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.06944759E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.15656534E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03172647E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.15656534E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -6.80501924E+04   # M^2_Hd              
        22    -9.16635090E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41921355E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.38201770E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48065054E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48065054E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51934946E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51934946E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.02030020E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.37454125E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.53472802E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.32781785E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.23937254E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.29739132E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.34912460E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.72735749E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.43694541E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.85582930E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.72246620E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.64395837E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.24078681E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.66923344E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.99328544E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.08956936E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.71110210E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.78521995E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.85017392E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -4.78722279E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.53330372E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.30076872E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.64175028E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.12630865E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.93222547E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.13511485E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.66029035E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.07671875E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.88741066E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63353775E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.08998534E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.79459193E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.26972134E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.89235335E-12    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.03786674E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.21717218E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56735419E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.27815899E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.86309652E-10    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.28999724E-10    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43264557E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.08179648E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.79016349E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63335992E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.44571129E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.07556122E-09    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.26398983E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.93322921E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.04474821E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.69278859E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.46938780E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.49616797E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.08325641E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.12315871E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55306115E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.07671875E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.88741066E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63353775E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.08998534E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.79459193E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.26972134E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.89235335E-12    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.03786674E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.21717218E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56735419E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.27815899E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.86309652E-10    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.28999724E-10    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43264557E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.08179648E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.79016349E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63335992E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.44571129E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.07556122E-09    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.26398983E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.93322921E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.04474821E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.69278859E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.46938780E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.49616797E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.08325641E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.12315871E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55306115E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.01697658E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.72273516E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00976531E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.01796118E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55388434E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999855E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.45198073E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.01697658E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.72273516E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00976531E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.01796118E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55388434E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999855E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.45198073E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.81383579E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.45531905E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18216630E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.36251464E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.75557318E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.53588646E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15521117E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.73590630E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     3.62154824E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30877641E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     4.23845254E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.01729317E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.66492721E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.01070432E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.02280296E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     4.01729317E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.66492721E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.01070432E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.02280296E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     4.01845014E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.66409358E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.01045452E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.02313612E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.10434587E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     1.00870852E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.49488845E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.57816422E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.12723324E-13    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.12723324E-13    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.79513223E-08    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.48263170E-10    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.48263170E-10    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.11479283E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.81473659E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.83349489E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.67151578E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.94961167E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.78089213E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     1.00941698E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.64924247E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.80872904E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.67129648E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.67129648E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.06934764E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.32699597E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.34876166E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34876166E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.38716468E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.38716468E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.34841750E-12    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     3.34841750E-12    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     2.75973511E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     2.75973511E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     3.34841750E-12    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     3.34841750E-12    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     2.75973511E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     2.75973511E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.30124706E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.30124706E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     8.20553274E-11    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     8.20553274E-11    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     8.20553274E-11    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     8.20553274E-11    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     4.40820405E-11    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     4.40820405E-11    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.01253904E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.00195908E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.99665935E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.65357786E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.65357786E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.12478151E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.78459039E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.25775377E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25775377E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.36565921E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.36565921E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.65785638E-12    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     3.65785638E-12    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.00876302E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.00876302E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     3.65785638E-12    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     3.65785638E-12    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.00876302E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.00876302E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.11539192E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.11539192E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     8.62272862E-11    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     8.62272862E-11    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     8.62272862E-11    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     8.62272862E-11    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     4.77766836E-11    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     4.77766836E-11    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.82697528E-03   # h decays
#          BR         NDA      ID1       ID2
     5.41983200E-01    2           5        -5   # BR(h -> b       bb     )
     6.85438392E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.42640614E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.13866448E-04    2           3        -3   # BR(h -> s       sb     )
     2.23254370E-02    2           4        -4   # BR(h -> c       cb     )
     7.38976605E-02    2          21        21   # BR(h -> g       g      )
     2.57980346E-03    2          22        22   # BR(h -> gam     gam    )
     1.85174650E-03    2          22        23   # BR(h -> Z       gam    )
     2.55431971E-01    2          24       -24   # BR(h -> W+      W-     )
     3.26298355E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.27231886E+01   # H decays
#          BR         NDA      ID1       ID2
     9.12062896E-01    2           5        -5   # BR(H -> b       bb     )
     5.82085754E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.05811613E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52736073E-04    2           3        -3   # BR(H -> s       sb     )
     7.08511212E-08    2           4        -4   # BR(H -> c       cb     )
     7.06785883E-03    2           6        -6   # BR(H -> t       tb     )
     3.41705726E-06    2          21        21   # BR(H -> g       g      )
     3.37615324E-08    2          22        22   # BR(H -> gam     gam    )
     2.89103027E-09    2          23        22   # BR(H -> Z       gam    )
     7.36495054E-07    2          24       -24   # BR(H -> W+      W-     )
     3.67793354E-07    2          23        23   # BR(H -> Z       Z      )
     4.79311026E-06    2          25        25   # BR(H -> h       h      )
     3.16456449E-25    2          36        36   # BR(H -> A       A      )
     6.89869319E-18    2          23        36   # BR(H -> Z       A      )
     7.82478278E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.62645741E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.91590118E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.38861088E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.07381776E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.32867809E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.18397957E+01   # A decays
#          BR         NDA      ID1       ID2
     9.31309831E-01    2           5        -5   # BR(A -> b       bb     )
     5.94341830E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.10144774E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.58101418E-04    2           3        -3   # BR(A -> s       sb     )
     7.28325619E-08    2           4        -4   # BR(A -> c       cb     )
     7.26151678E-03    2           6        -6   # BR(A -> t       tb     )
     2.13844709E-05    2          21        21   # BR(A -> g       g      )
     5.06014739E-08    2          22        22   # BR(A -> gam     gam    )
     2.10506484E-08    2          23        22   # BR(A -> Z       gam    )
     7.49038801E-07    2          23        25   # BR(A -> Z       h      )
     8.13301685E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.73226783E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.06999219E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.46321203E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.53102285E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.48377542E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.49022110E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.94120827E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.49615110E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.14079767E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.34698765E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.32592238E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.92867841E-07    2          24        25   # BR(H+ -> W+      h      )
     4.82885823E-14    2          24        36   # BR(H+ -> W+      A      )
     4.53592580E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.66750468E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.01177661E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
