#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16419645E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03982268E+01   # W+
        25     1.24979252E+02   # h
        35     3.00003143E+03   # H
        36     2.99999993E+03   # A
        37     3.00108966E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04040025E+03   # ~d_L
   2000001     3.03521543E+03   # ~d_R
   1000002     3.03949059E+03   # ~u_L
   2000002     3.03666201E+03   # ~u_R
   1000003     3.04040025E+03   # ~s_L
   2000003     3.03521543E+03   # ~s_R
   1000004     3.03949059E+03   # ~c_L
   2000004     3.03666201E+03   # ~c_R
   1000005     9.97803201E+02   # ~b_1
   2000005     3.03292650E+03   # ~b_2
   1000006     9.95655795E+02   # ~t_1
   2000006     3.02411293E+03   # ~t_2
   1000011     3.00649421E+03   # ~e_L
   2000011     3.00170898E+03   # ~e_R
   1000012     3.00510316E+03   # ~nu_eL
   1000013     3.00649421E+03   # ~mu_L
   2000013     3.00170898E+03   # ~mu_R
   1000014     3.00510316E+03   # ~nu_muL
   1000015     2.98605110E+03   # ~tau_1
   2000015     3.02173910E+03   # ~tau_2
   1000016     3.00499078E+03   # ~nu_tauL
   1000021     2.35014789E+03   # ~g
   1000022     2.01352229E+02   # ~chi_10
   1000023     4.27147398E+02   # ~chi_20
   1000025    -2.99594132E+03   # ~chi_30
   1000035     2.99604339E+03   # ~chi_40
   1000024     4.27310372E+02   # ~chi_1+
   1000037     2.99692831E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891538E-01   # N_11
  1  2     1.90623515E-04   # N_12
  1  3    -1.47246877E-02   # N_13
  1  4    -2.45846070E-04   # N_14
  2  1     1.95682128E-04   # N_21
  2  2     9.99654483E-01   # N_22
  2  3     2.61926578E-02   # N_23
  2  4     2.19587970E-03   # N_24
  3  1    -1.02383228E-02   # N_31
  3  2     1.69688386E-02   # N_32
  3  3    -7.06827713E-01   # N_33
  3  4     7.07108067E-01   # N_34
  4  1     1.05854458E-02   # N_41
  4  2    -2.00733076E-02   # N_42
  4  3     7.06747276E-01   # N_43
  4  4     7.07102043E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99314030E-01   # U_11
  1  2     3.70333441E-02   # U_12
  2  1    -3.70333441E-02   # U_21
  2  2     9.99314030E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995184E-01   # V_11
  1  2    -3.10346084E-03   # V_12
  2  1    -3.10346084E-03   # V_21
  2  2    -9.99995184E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98481817E-01   # cos(theta_t)
  1  2    -5.50823122E-02   # sin(theta_t)
  2  1     5.50823122E-02   # -sin(theta_t)
  2  2     9.98481817E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99709403E-01   # cos(theta_b)
  1  2    -2.41062140E-02   # sin(theta_b)
  2  1     2.41062140E-02   # -sin(theta_b)
  2  2     9.99709403E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06941098E-01   # cos(theta_tau)
  1  2     7.07272426E-01   # sin(theta_tau)
  2  1    -7.07272426E-01   # -sin(theta_tau)
  2  2    -7.06941098E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00161274E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.64196449E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43890380E+02   # higgs               
         4     1.02558466E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.64196449E+03  # The gauge couplings
     1     3.62455180E-01   # gprime(Q) DRbar
     2     6.38258863E-01   # g(Q) DRbar
     3     1.02429760E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.64196449E+03  # The trilinear couplings
  1  1     2.56492172E-06   # A_u(Q) DRbar
  2  2     2.56496075E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.64196449E+03  # The trilinear couplings
  1  1     8.97132299E-07   # A_d(Q) DRbar
  2  2     8.97240611E-07   # A_s(Q) DRbar
  3  3     1.85214883E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.64196449E+03  # The trilinear couplings
  1  1     3.98042760E-07   # A_e(Q) DRbar
  2  2     3.98062513E-07   # A_mu(Q) DRbar
  3  3     4.03649505E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.64196449E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50731030E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.64196449E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.82295965E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.64196449E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02480426E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.64196449E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.71698191E+04   # M^2_Hd              
        22    -9.03573443E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41415262E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.42669672E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47918392E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47918392E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52081608E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52081608E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.14611179E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.60010220E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.09780532E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.74218446E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.12872186E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.83283547E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.11811065E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.27116698E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.33092014E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.99640452E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.67004966E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.60228651E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.13725557E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.98679401E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.73100247E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.46306150E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.36383826E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.34345022E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.10638462E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.89733987E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.19594287E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.09635070E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     6.15694383E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.49537059E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.07105103E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.01386026E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.47198630E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.94931005E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.00685821E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62381684E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.54143562E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.09486583E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.24951063E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.88812267E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.06660305E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16410179E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59915627E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.94665743E-09    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.38119851E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.46926638E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40084339E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.95435742E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.98023532E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62293476E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.82692780E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.90943188E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.24380332E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.28050703E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.07345390E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65297297E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.55501268E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.69378760E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.67083327E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.90418635E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54449864E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.94931005E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.00685821E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62381684E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.54143562E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.09486583E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.24951063E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.88812267E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.06660305E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16410179E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59915627E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.94665743E-09    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.38119851E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.46926638E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40084339E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.95435742E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.98023532E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62293476E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.82692780E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.90943188E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.24380332E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.28050703E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.07345390E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65297297E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.55501268E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.69378760E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.67083327E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.90418635E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54449864E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89455773E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.00023875E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00179056E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.91146911E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.97934845E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.99797029E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.46353791E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55462630E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999960E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.70974860E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.55939528E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.60850587E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.89455773E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00023875E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00179056E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.91146911E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.97934845E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.99797029E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.46353791E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55462630E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999960E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.70974860E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.55939528E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.60850587E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.75137536E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.53048313E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.15803488E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31148200E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.69562633E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.61200543E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13073411E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.51055863E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.31877719E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.25682528E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.52238431E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89476070E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.98373956E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99879714E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.01555121E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.71090801E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00282876E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.77792558E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.89476070E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.98373956E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99879714E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.01555121E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.71090801E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00282876E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.77792558E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89494121E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.98289681E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99853678E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.86904828E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.52101135E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00315588E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.75271795E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.95227435E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.69297390E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.33537642E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.50491352E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.87654936E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.46893501E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.07653522E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.29833625E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.75326534E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.05998654E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99930820E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.91797763E-05    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.71683445E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.09767916E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.76057141E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.28002535E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.28002535E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.06464795E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.74409541E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.28249119E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.28249119E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.56153295E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.56153295E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.28264735E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.28264735E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.61791583E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.03916622E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.72162023E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.35347368E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.35347368E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.15441585E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.03777047E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.25328785E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25328785E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.63713809E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.63713809E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.33541780E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.33541780E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.62072424E-03   # h decays
#          BR         NDA      ID1       ID2
     5.64448860E-01    2           5        -5   # BR(h -> b       bb     )
     7.16081995E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.53495250E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.38063586E-04    2           3        -3   # BR(h -> s       sb     )
     2.33786597E-02    2           4        -4   # BR(h -> c       cb     )
     7.58931942E-02    2          21        21   # BR(h -> g       g      )
     2.59673256E-03    2          22        22   # BR(h -> gam     gam    )
     1.72249254E-03    2          22        23   # BR(h -> Z       gam    )
     2.30728308E-01    2          24       -24   # BR(h -> W+      W-     )
     2.88319951E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.51337605E+01   # H decays
#          BR         NDA      ID1       ID2
     8.95972408E-01    2           5        -5   # BR(H -> b       bb     )
     7.07787090E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.50256602E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.07316591E-04    2           3        -3   # BR(H -> s       sb     )
     8.61451804E-08    2           4        -4   # BR(H -> c       cb     )
     8.59353021E-03    2           6        -6   # BR(H -> t       tb     )
     1.21594396E-05    2          21        21   # BR(H -> g       g      )
     5.75399517E-08    2          22        22   # BR(H -> gam     gam    )
     3.51543679E-09    2          23        22   # BR(H -> Z       gam    )
     8.36948292E-07    2          24       -24   # BR(H -> W+      W-     )
     4.17958027E-07    2          23        23   # BR(H -> Z       Z      )
     5.95624943E-06    2          25        25   # BR(H -> h       h      )
    -1.66138049E-24    2          36        36   # BR(H -> A       A      )
    -3.55315748E-20    2          23        36   # BR(H -> Z       A      )
     8.80313663E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.37002754E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.40019132E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.79404225E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.24296232E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.20148461E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.43524765E+01   # A decays
#          BR         NDA      ID1       ID2
     9.16385442E-01    2           5        -5   # BR(A -> b       bb     )
     7.23882004E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.55947020E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.14356089E-04    2           3        -3   # BR(A -> s       sb     )
     8.87068325E-08    2           4        -4   # BR(A -> c       cb     )
     8.84420562E-03    2           6        -6   # BR(A -> t       tb     )
     2.60453393E-05    2          21        21   # BR(A -> g       g      )
     6.85453403E-08    2          22        22   # BR(A -> gam     gam    )
     2.56442459E-08    2          23        22   # BR(A -> Z       gam    )
     8.52803999E-07    2          23        25   # BR(A -> Z       h      )
     9.63561278E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.53622215E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.81588711E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.94255880E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.76422431E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46235392E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.60857865E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.33663222E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.35905351E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.37317909E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.82507095E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.20564550E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.79609111E-07    2          24        25   # BR(H+ -> W+      h      )
     5.37141011E-14    2          24        36   # BR(H+ -> W+      A      )
     5.29635125E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.90315228E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.08176335E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
