#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10963873E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     3.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03998427E+01   # W+
        25     1.25204978E+02   # h
        35     3.00023004E+03   # H
        36     2.99999972E+03   # A
        37     3.00098330E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.01752318E+03   # ~d_L
   2000001     3.01222965E+03   # ~d_R
   1000002     3.01660071E+03   # ~u_L
   2000002     3.01455688E+03   # ~u_R
   1000003     3.01752318E+03   # ~s_L
   2000003     3.01222965E+03   # ~s_R
   1000004     3.01660071E+03   # ~c_L
   2000004     3.01455688E+03   # ~c_R
   1000005     4.39016099E+02   # ~b_1
   2000005     3.01184519E+03   # ~b_2
   1000006     4.34636272E+02   # ~t_1
   2000006     2.99311534E+03   # ~t_2
   1000011     3.00684826E+03   # ~e_L
   2000011     3.00080186E+03   # ~e_R
   1000012     3.00545215E+03   # ~nu_eL
   1000013     3.00684826E+03   # ~mu_L
   2000013     3.00080186E+03   # ~mu_R
   1000014     3.00545215E+03   # ~nu_muL
   1000015     2.98598709E+03   # ~tau_1
   2000015     3.02227889E+03   # ~tau_2
   1000016     3.00568091E+03   # ~nu_tauL
   1000021     2.32108318E+03   # ~g
   1000022     1.51503593E+02   # ~chi_10
   1000023     3.19382009E+02   # ~chi_20
   1000025    -3.00447781E+03   # ~chi_30
   1000035     3.00502261E+03   # ~chi_40
   1000024     3.19542463E+02   # ~chi_1+
   1000037     3.00567526E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888326E-01   # N_11
  1  2    -1.15473571E-03   # N_12
  1  3     1.48262385E-02   # N_13
  1  4    -1.47795511E-03   # N_14
  2  1     1.55163327E-03   # N_21
  2  2     9.99642673E-01   # N_22
  2  3    -2.63938308E-02   # N_23
  2  4     3.93494177E-03   # N_24
  3  1    -9.41738504E-03   # N_31
  3  2     1.58940165E-02   # N_32
  3  3     7.06841099E-01   # N_33
  3  4     7.07131073E-01   # N_34
  4  1    -1.14995829E-02   # N_41
  4  2     2.14609521E-02   # N_42
  4  3     7.06724281E-01   # N_43
  4  4    -7.07069995E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99302368E-01   # U_11
  1  2    -3.73467187E-02   # U_12
  2  1     3.73467187E-02   # U_21
  2  2     9.99302368E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99984492E-01   # V_11
  1  2    -5.56910619E-03   # V_12
  2  1     5.56910619E-03   # V_21
  2  2     9.99984492E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98905203E-01   # cos(theta_t)
  1  2    -4.67802888E-02   # sin(theta_t)
  2  1     4.67802888E-02   # -sin(theta_t)
  2  2     9.98905203E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99922814E-01   # cos(theta_b)
  1  2     1.24244132E-02   # sin(theta_b)
  2  1    -1.24244132E-02   # -sin(theta_b)
  2  2     9.99922814E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06903311E-01   # cos(theta_tau)
  1  2     7.07310193E-01   # sin(theta_tau)
  2  1    -7.07310193E-01   # -sin(theta_tau)
  2  2     7.06903311E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01968371E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.09638734E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44940320E+02   # higgs               
         4     6.17248812E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.09638734E+03  # The gauge couplings
     1     3.61126696E-01   # gprime(Q) DRbar
     2     6.38498620E-01   # g(Q) DRbar
     3     1.03334258E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.09638734E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.07190714E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.09638734E+03  # The trilinear couplings
  1  1     2.67873838E-07   # A_d(Q) DRbar
  2  2     2.67927778E-07   # A_s(Q) DRbar
  3  3     6.19059243E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.09638734E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     5.79863698E-08   # A_mu(Q) DRbar
  3  3     5.86680052E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.09638734E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.58786069E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.09638734E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.11628800E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.09638734E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05448051E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.09638734E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -9.29127333E+04   # M^2_Hd              
        22    -9.16828781E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41514402E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.56237280E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48245210E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48245210E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51754790E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51754790E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.25488817E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     3.52965249E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.64703475E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.06645322E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.08533259E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.87837730E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.77900572E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.15373903E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66403407E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.52667465E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.03836161E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.75665638E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.11644745E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.88355255E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.26966197E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.84715378E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.37975037E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.29921431E-07    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.42805508E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -6.68789029E-09    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28028505E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.93329963E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.26502323E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.15154248E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.03147753E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.78082322E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62025765E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.58756733E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.41725198E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.24076481E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.57346231E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.08116923E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.21429329E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56121537E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.69914595E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.62431668E-10    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.02001181E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43878091E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.03661460E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.92177542E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61771103E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.67127351E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.68676018E-09    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.23504289E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.45910003E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.08802786E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.70198022E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.43961313E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.05189518E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.30072529E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.68224294E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55603763E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.03147753E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.78082322E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62025765E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.58756733E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.41725198E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.24076481E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.57346231E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.08116923E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.21429329E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56121537E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.69914595E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.62431668E-10    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.02001181E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43878091E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.03661460E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.92177542E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61771103E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.67127351E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.68676018E-09    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.23504289E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.45910003E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.08802786E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.70198022E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.43961313E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.05189518E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.30072529E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.68224294E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55603763E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96027233E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.75720596E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01302628E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.54123351E-11    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.01507934E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01125312E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.22642815E-10    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54883019E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997634E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.36589594E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.96027233E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.75720596E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01302628E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.54123351E-11    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.01507934E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01125312E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.22642815E-10    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54883019E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997634E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.36589594E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.78334207E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.47582782E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17837432E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.34579786E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.72402464E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.55807739E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15109392E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.13897601E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     7.20529129E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.29062569E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     6.95465108E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96059214E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.83181682E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00076428E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.82366355E-11    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.96600217E-11    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01605404E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.96059214E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.83181682E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00076428E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.82366355E-11    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.96600217E-11    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01605404E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96124146E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.83096105E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00051119E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.87649201E-11    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.61622720E-11    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01639270E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     2.55135657E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.11228754E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.65043934E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.95954584E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     7.34652003E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     9.14297277E-14    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     9.14297277E-14    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     9.21709250E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.59791190E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.03938980E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.44608915E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.79391774E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.84834254E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.94699269E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.50530073E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.14105621E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.25836592E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.23938806E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.41442435E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.41442435E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.63079853E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.35345684E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.69956342E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.69956342E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.23270882E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.23270882E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.52285091E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     4.52285091E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     4.52285091E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     4.52285091E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     8.73204833E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     8.73204833E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.05077994E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.52367837E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.35060259E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.48044157E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.48044157E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.30393921E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.45341989E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.67813003E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.67813003E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.25798275E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.25798275E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     8.97653650E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     8.97653650E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.97653650E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     8.97653650E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.10632728E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.10632728E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.98053524E-03   # h decays
#          BR         NDA      ID1       ID2
     6.76993931E-01    2           5        -5   # BR(h -> b       bb     )
     5.25288290E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.85952876E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.94523914E-04    2           3        -3   # BR(h -> s       sb     )
     1.70200912E-02    2           4        -4   # BR(h -> c       cb     )
     5.62792852E-02    2          21        21   # BR(h -> g       g      )
     1.89658914E-03    2          22        22   # BR(h -> gam     gam    )
     1.27778866E-03    2          22        23   # BR(h -> Z       gam    )
     1.71863555E-01    2          24       -24   # BR(h -> W+      W-     )
     2.15594547E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.38690606E+01   # H decays
#          BR         NDA      ID1       ID2
     7.39629764E-01    2           5        -5   # BR(H -> b       bb     )
     1.79308594E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.33992340E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.78539346E-04    2           3        -3   # BR(H -> s       sb     )
     2.19818121E-07    2           4        -4   # BR(H -> c       cb     )
     2.19282891E-02    2           6        -6   # BR(H -> t       tb     )
     3.50240506E-05    2          21        21   # BR(H -> g       g      )
     6.09771301E-08    2          22        22   # BR(H -> gam     gam    )
     8.38916197E-09    2          23        22   # BR(H -> Z       gam    )
     3.61740819E-05    2          24       -24   # BR(H -> W+      W-     )
     1.80647396E-05    2          23        23   # BR(H -> Z       Z      )
     8.99228859E-05    2          25        25   # BR(H -> h       h      )
     4.07356457E-24    2          36        36   # BR(H -> A       A      )
     5.08024405E-17    2          23        36   # BR(H -> Z       A      )
     2.34631422E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.12810759E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.16921807E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.29325165E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.31772674E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.41053521E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31350620E+01   # A decays
#          BR         NDA      ID1       ID2
     7.81017096E-01    2           5        -5   # BR(A -> b       bb     )
     1.89318770E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.69384995E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.22143779E-04    2           3        -3   # BR(A -> s       sb     )
     2.31997321E-07    2           4        -4   # BR(A -> c       cb     )
     2.31304845E-02    2           6        -6   # BR(A -> t       tb     )
     6.81170695E-05    2          21        21   # BR(A -> g       g      )
     3.16312038E-08    2          22        22   # BR(A -> gam     gam    )
     6.70867571E-08    2          23        22   # BR(A -> Z       gam    )
     3.80453624E-05    2          23        25   # BR(A -> Z       h      )
     2.67373417E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22764857E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.33233304E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.06795610E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.71939855E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.07493590E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.55934466E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     9.04921847E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.87957877E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.31801905E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.09408753E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.01549890E-01    2           6        -5   # BR(H+ -> t       bb     )
     5.14983416E-05    2          24        25   # BR(H+ -> W+      h      )
     1.24620965E-13    2          24        36   # BR(H+ -> W+      A      )
     2.12494737E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     5.09784328E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.72051877E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
