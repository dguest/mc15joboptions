#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.17312035E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.49900000E+02   # M_1(MX)             
         2     6.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04019408E+01   # W+
        25     1.24684944E+02   # h
        35     2.99999772E+03   # H
        36     2.99999994E+03   # A
        37     3.00108239E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04336380E+03   # ~d_L
   2000001     3.03820100E+03   # ~d_R
   1000002     3.04245940E+03   # ~u_L
   2000002     3.03938865E+03   # ~u_R
   1000003     3.04336380E+03   # ~s_L
   2000003     3.03820100E+03   # ~s_R
   1000004     3.04245940E+03   # ~c_L
   2000004     3.03938865E+03   # ~c_R
   1000005     1.10216773E+03   # ~b_1
   2000005     3.03546503E+03   # ~b_2
   1000006     1.10005988E+03   # ~t_1
   2000006     3.02490971E+03   # ~t_2
   1000011     3.00639037E+03   # ~e_L
   2000011     3.00197268E+03   # ~e_R
   1000012     3.00500467E+03   # ~nu_eL
   1000013     3.00639037E+03   # ~mu_L
   2000013     3.00197268E+03   # ~mu_R
   1000014     3.00500467E+03   # ~nu_muL
   1000015     2.98610999E+03   # ~tau_1
   2000015     3.02155912E+03   # ~tau_2
   1000016     3.00479920E+03   # ~nu_tauL
   1000021     2.35418047E+03   # ~g
   1000022     3.51871797E+02   # ~chi_10
   1000023     7.36127805E+02   # ~chi_20
   1000025    -2.99410323E+03   # ~chi_30
   1000035     2.99447990E+03   # ~chi_40
   1000024     7.36291474E+02   # ~chi_1+
   1000037     2.99524907E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99889495E-01   # N_11
  1  2    -5.59450251E-05   # N_12
  1  3    -1.48325940E-02   # N_13
  1  4    -9.94087064E-04   # N_14
  2  1     4.61508718E-04   # N_21
  2  2     9.99622652E-01   # N_22
  2  3     2.70052245E-02   # N_23
  2  4     5.00587418E-03   # N_24
  3  1    -9.78180678E-03   # N_31
  3  2     1.55594958E-02   # N_32
  3  3    -7.06857630E-01   # N_33
  3  4     7.07117041E-01   # N_34
  4  1     1.11848312E-02   # N_41
  4  2    -2.26374220E-02   # N_42
  4  3     7.06684514E-01   # N_43
  4  4     7.07078103E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99270594E-01   # U_11
  1  2     3.81874228E-02   # U_12
  2  1    -3.81874228E-02   # U_21
  2  2     9.99270594E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99974955E-01   # V_11
  1  2    -7.07738179E-03   # V_12
  2  1    -7.07738179E-03   # V_21
  2  2    -9.99974955E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98416161E-01   # cos(theta_t)
  1  2    -5.62598387E-02   # sin(theta_t)
  2  1     5.62598387E-02   # -sin(theta_t)
  2  2     9.98416161E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99710086E-01   # cos(theta_b)
  1  2    -2.40778726E-02   # sin(theta_b)
  2  1     2.40778726E-02   # -sin(theta_b)
  2  2     9.99710086E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06964127E-01   # cos(theta_tau)
  1  2     7.07249407E-01   # sin(theta_tau)
  2  1    -7.07249407E-01   # -sin(theta_tau)
  2  2    -7.06964127E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00158414E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.73120353E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43804884E+02   # higgs               
         4     1.01161084E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.73120353E+03  # The gauge couplings
     1     3.62631416E-01   # gprime(Q) DRbar
     2     6.37066971E-01   # g(Q) DRbar
     3     1.02308467E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.73120353E+03  # The trilinear couplings
  1  1     2.85478235E-06   # A_u(Q) DRbar
  2  2     2.85482357E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.73120353E+03  # The trilinear couplings
  1  1     1.01461785E-06   # A_d(Q) DRbar
  2  2     1.01473425E-06   # A_s(Q) DRbar
  3  3     2.04071022E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.73120353E+03  # The trilinear couplings
  1  1     4.18981167E-07   # A_e(Q) DRbar
  2  2     4.19001528E-07   # A_mu(Q) DRbar
  3  3     4.24693093E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.73120353E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49229944E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.73120353E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.73117036E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.73120353E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01319121E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.73120353E+03  # The soft SUSY breaking masses at the scale Q
         1     3.49900000E+02   # M_1(Q)              
         2     6.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.89531278E+04   # M^2_Hd              
        22    -9.01584412E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40871297E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.85344272E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47847006E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47847006E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52152994E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52152994E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.06508144E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     3.04997687E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.88006174E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.81494057E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.09913653E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.89567626E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.68946472E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.42178817E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.62174268E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.05208025E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.65599383E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.58881040E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.10081490E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.90711038E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.42399018E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.58863177E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.06896922E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.20491863E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.14096438E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.46859700E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.33984207E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.15101297E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.90998304E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.63892912E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.74206848E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.68833890E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.40267624E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.67164509E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.14781993E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.55918700E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.73847823E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     8.25273510E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.11985644E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.90374375E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.25947697E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.14020776E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58428916E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.08035711E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.55834974E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.00469115E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41571017E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.67686393E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.15223925E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.55801104E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.25240665E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.24508774E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.11420426E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.74414450E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.26625470E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.63866671E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.50475318E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.75800228E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.21162988E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.41553036E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54952450E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.67164509E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.14781993E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.55918700E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.73847823E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     8.25273510E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.11985644E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.90374375E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.25947697E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.14020776E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58428916E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.08035711E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.55834974E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.00469115E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41571017E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.67686393E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.15223925E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.55801104E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.25240665E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.24508774E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.11420426E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.74414450E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.26625470E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.63866671E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.50475318E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.75800228E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.21162988E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.41553036E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54952450E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59652685E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.06323910E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98147609E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.24175053E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.58668832E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95528420E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.38682838E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52751519E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999801E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.93467205E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.69740659E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.19756136E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.59652685E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.06323910E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98147609E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.24175053E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.58668832E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95528420E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.38682838E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52751519E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999801E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.93467205E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.69740659E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.19756136E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.58552221E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.68950396E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.10547877E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.20501727E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.53597351E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.77244251E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.07771309E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.78209568E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.55859127E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.14932920E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.81131650E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59652421E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.06314050E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97662275E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.89749060E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.38746621E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.96023652E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.41995419E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59652421E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.06314050E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97662275E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.89749060E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.38746621E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.96023652E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.41995419E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.59655660E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06305419E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97634044E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.60356320E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.33399160E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.96057875E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.64104031E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.38949566E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.23313665E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.28313012E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.51520854E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.10192093E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.54956596E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.15612960E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.37127380E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.93962309E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.24445545E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.39023184E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     5.60976816E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.24949599E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.25455523E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.43449263E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.35447068E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.35447068E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.98531144E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.13951246E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.29422508E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.29422508E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.28970960E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.28970960E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.31497035E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.31497035E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.15952075E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.70813307E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.12315687E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.42755061E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.42755061E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.32608516E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.83866471E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.25832802E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25832802E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.35979301E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.35979301E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.61300265E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.61300265E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.57309897E-03   # h decays
#          BR         NDA      ID1       ID2
     5.68039272E-01    2           5        -5   # BR(h -> b       bb     )
     7.23905367E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.56266213E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.44195141E-04    2           3        -3   # BR(h -> s       sb     )
     2.36453667E-02    2           4        -4   # BR(h -> c       cb     )
     7.63981293E-02    2          21        21   # BR(h -> g       g      )
     2.60575433E-03    2          22        22   # BR(h -> gam     gam    )
     1.70012176E-03    2          22        23   # BR(h -> Z       gam    )
     2.26263922E-01    2          24       -24   # BR(h -> W+      W-     )
     2.81564356E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.37615791E+01   # H decays
#          BR         NDA      ID1       ID2
     8.93290843E-01    2           5        -5   # BR(H -> b       bb     )
     7.36545626E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.60424932E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.19803838E-04    2           3        -3   # BR(H -> s       sb     )
     8.96445045E-08    2           4        -4   # BR(H -> c       cb     )
     8.94260785E-03    2           6        -6   # BR(H -> t       tb     )
     1.51499818E-05    2          21        21   # BR(H -> g       g      )
     6.92780320E-08    2          22        22   # BR(H -> gam     gam    )
     3.66114677E-09    2          23        22   # BR(H -> Z       gam    )
     8.62327062E-07    2          24       -24   # BR(H -> W+      W-     )
     4.30631921E-07    2          23        23   # BR(H -> Z       Z      )
     6.34892844E-06    2          25        25   # BR(H -> h       h      )
     7.38818232E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.38064798E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.69122775E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.61911156E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.20902368E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.90768642E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.30302468E+01   # A decays
#          BR         NDA      ID1       ID2
     9.13114472E-01    2           5        -5   # BR(A -> b       bb     )
     7.52859637E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.66192805E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.26940040E-04    2           3        -3   # BR(A -> s       sb     )
     9.22578450E-08    2           4        -4   # BR(A -> c       cb     )
     9.19824694E-03    2           6        -6   # BR(A -> t       tb     )
     2.70879570E-05    2          21        21   # BR(A -> g       g      )
     7.10820845E-08    2          22        22   # BR(A -> gam     gam    )
     2.66878827E-08    2          23        22   # BR(A -> Z       gam    )
     8.78198131E-07    2          23        25   # BR(A -> Z       h      )
     9.58698471E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.67529149E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.78894321E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.95682654E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.63441745E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.45847660E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.84459407E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.42008152E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.33423872E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.42222056E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.92596502E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.18427812E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.99486919E-07    2          24        25   # BR(H+ -> W+      h      )
     5.37986915E-14    2          24        36   # BR(H+ -> W+      A      )
     5.12025559E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.83732947E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.05967846E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
