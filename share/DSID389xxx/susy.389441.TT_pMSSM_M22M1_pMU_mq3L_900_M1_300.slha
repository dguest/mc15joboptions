#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16419676E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.99900000E+02   # M_1(MX)             
         2     5.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04030271E+01   # W+
        25     1.24921607E+02   # h
        35     3.00010580E+03   # H
        36     2.99999993E+03   # A
        37     3.00091016E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04044204E+03   # ~d_L
   2000001     3.03536108E+03   # ~d_R
   1000002     3.03953785E+03   # ~u_L
   2000002     3.03637458E+03   # ~u_R
   1000003     3.04044204E+03   # ~s_L
   2000003     3.03536108E+03   # ~s_R
   1000004     3.03953785E+03   # ~c_L
   2000004     3.03637458E+03   # ~c_R
   1000005     1.00220043E+03   # ~b_1
   2000005     3.03433100E+03   # ~b_2
   1000006     9.98307929E+02   # ~t_1
   2000006     3.01758479E+03   # ~t_2
   1000011     3.00624241E+03   # ~e_L
   2000011     3.00214118E+03   # ~e_R
   1000012     3.00485738E+03   # ~nu_eL
   1000013     3.00624241E+03   # ~mu_L
   2000013     3.00214118E+03   # ~mu_R
   1000014     3.00485738E+03   # ~nu_muL
   1000015     2.98555245E+03   # ~tau_1
   2000015     3.02196215E+03   # ~tau_2
   1000016     3.00459644E+03   # ~nu_tauL
   1000021     2.35015251E+03   # ~g
   1000022     3.01271629E+02   # ~chi_10
   1000023     6.31321200E+02   # ~chi_20
   1000025    -2.99532495E+03   # ~chi_30
   1000035     2.99617714E+03   # ~chi_40
   1000024     6.31484359E+02   # ~chi_1+
   1000037     2.99670134E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99885261E-01   # N_11
  1  2    -7.71294649E-04   # N_12
  1  3     1.49629042E-02   # N_13
  1  4    -2.23212828E-03   # N_14
  2  1     1.19325038E-03   # N_21
  2  2     9.99606451E-01   # N_22
  2  3    -2.72075795E-02   # N_23
  2  4     6.72808867E-03   # N_24
  3  1    -8.98827304E-03   # N_31
  3  2     1.44909890E-02   # N_32
  3  3     7.06870756E-01   # N_33
  3  4     7.07137155E-01   # N_34
  4  1    -1.21347804E-02   # N_41
  4  2     2.40074936E-02   # N_42
  4  3     7.06660876E-01   # N_43
  4  4    -7.07040872E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99258972E-01   # U_11
  1  2    -3.84903406E-02   # U_12
  2  1     3.84903406E-02   # U_21
  2  2     9.99258972E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99954689E-01   # V_11
  1  2    -9.51942971E-03   # V_12
  2  1     9.51942971E-03   # V_21
  2  2     9.99954689E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98479125E-01   # cos(theta_t)
  1  2    -5.51310887E-02   # sin(theta_t)
  2  1     5.51310887E-02   # -sin(theta_t)
  2  2     9.98479125E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99907826E-01   # cos(theta_b)
  1  2     1.35771685E-02   # sin(theta_b)
  2  1    -1.35771685E-02   # -sin(theta_b)
  2  2     9.99907826E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06957174E-01   # cos(theta_tau)
  1  2     7.07256357E-01   # sin(theta_tau)
  2  1    -7.07256357E-01   # -sin(theta_tau)
  2  2     7.06957174E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01771426E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.64196761E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43986896E+02   # higgs               
         4     7.60128597E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.64196761E+03  # The gauge couplings
     1     3.62477832E-01   # gprime(Q) DRbar
     2     6.37502164E-01   # g(Q) DRbar
     3     1.02432832E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.64196761E+03  # The trilinear couplings
  1  1     2.57371509E-06   # A_u(Q) DRbar
  2  2     2.57375018E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.64196761E+03  # The trilinear couplings
  1  1     6.90411620E-07   # A_d(Q) DRbar
  2  2     6.90532488E-07   # A_s(Q) DRbar
  3  3     1.51741529E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.64196761E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.41941169E-07   # A_mu(Q) DRbar
  3  3     1.43619313E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.64196761E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51153774E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.64196761E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.15104756E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.64196761E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07150964E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.64196761E+03  # The soft SUSY breaking masses at the scale Q
         1     2.99900000E+02   # M_1(Q)              
         2     5.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.46351219E+04   # M^2_Hd              
        22    -9.04109584E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41059053E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.40592020E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47687841E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47687841E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52312159E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52312159E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.34346328E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.60924524E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.88271070E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.85636477E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.11627578E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.82654896E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.02439599E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.08878001E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.88637884E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.98479140E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69108002E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59901394E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.12955794E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.19498961E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.97560691E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.61303113E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.08940818E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.19029000E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.92762641E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.30835247E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.29894665E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.22755421E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     6.79913264E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30566798E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.87170338E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.16307493E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.97636912E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.78597703E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.02820049E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.58332167E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.69664332E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     7.48301130E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.16727234E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.81095137E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.18912268E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15503936E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58637263E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.11420703E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.08386836E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.89669951E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41362496E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.79113451E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.12443100E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.58125420E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.49366215E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.14274782E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.16159659E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.78066420E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.19589853E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65248298E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.51001103E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.01035126E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.93738562E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.13484489E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54899821E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.78597703E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.02820049E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.58332167E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.69664332E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     7.48301130E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.16727234E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.81095137E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.18912268E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15503936E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58637263E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.11420703E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.08386836E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.89669951E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41362496E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.79113451E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.12443100E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.58125420E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.49366215E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.14274782E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.16159659E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.78066420E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.19589853E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65248298E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.51001103E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.01035126E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.93738562E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.13484489E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54899821E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.71259960E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03410027E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99281878E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.51399043E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.28157659E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97308050E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.89555721E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53767139E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998668E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.32760179E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.69645666E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.36793693E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.71259960E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03410027E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99281878E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.51399043E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.28157659E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97308050E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.89555721E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53767139E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998668E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.32760179E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.69645666E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.36793693E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.65171001E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.62036963E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.12963737E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.24999300E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.59609589E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.70614736E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.10103726E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.51207259E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.67636190E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.19232454E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.71987148E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.71276312E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03918297E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98294412E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.04446962E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.04023191E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.97787273E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.74274287E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.71276312E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03918297E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98294412E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.04446962E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.04023191E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.97787273E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.74274287E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.71272396E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03910003E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98266870E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.77295437E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     9.78810536E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.97821236E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.87738359E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.35109146E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.00488861E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.77117324E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.04840552E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.42093515E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.40299929E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.31661574E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.21100779E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.81411450E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.17530066E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.32481356E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.46751864E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.02795493E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.54235124E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.40066093E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.17858037E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.17858037E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.77515082E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.00686472E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.56531926E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.56531926E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.25450548E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.25450548E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.55484878E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.55484878E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.94040002E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.55737472E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.99331951E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.26197367E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.26197367E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.62399397E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.84041009E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.52965180E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.52965180E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.28473570E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.28473570E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.20945600E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.20945600E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.78317479E-03   # h decays
#          BR         NDA      ID1       ID2
     6.71379710E-01    2           5        -5   # BR(h -> b       bb     )
     5.45293723E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.93035887E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.09736398E-04    2           3        -3   # BR(h -> s       sb     )
     1.76901108E-02    2           4        -4   # BR(h -> c       cb     )
     5.74327248E-02    2          21        21   # BR(h -> g       g      )
     1.96290915E-03    2          22        22   # BR(h -> gam     gam    )
     1.29739671E-03    2          22        23   # BR(h -> Z       gam    )
     1.73437049E-01    2          24       -24   # BR(h -> W+      W-     )
     2.16679547E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40888438E+01   # H decays
#          BR         NDA      ID1       ID2
     7.38623261E-01    2           5        -5   # BR(H -> b       bb     )
     1.76504456E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.24077579E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.66368293E-04    2           3        -3   # BR(H -> s       sb     )
     2.16211608E-07    2           4        -4   # BR(H -> c       cb     )
     2.15684962E-02    2           6        -6   # BR(H -> t       tb     )
     2.80107878E-05    2          21        21   # BR(H -> g       g      )
     2.22355694E-08    2          22        22   # BR(H -> gam     gam    )
     8.32483373E-09    2          23        22   # BR(H -> Z       gam    )
     2.99664596E-05    2          24       -24   # BR(H -> W+      W-     )
     1.49647591E-05    2          23        23   # BR(H -> Z       Z      )
     7.92703895E-05    2          25        25   # BR(H -> h       h      )
    -5.71795402E-23    2          36        36   # BR(H -> A       A      )
     1.12168042E-18    2          23        36   # BR(H -> Z       A      )
     1.94358243E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.07254905E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.69366842E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.57124637E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.80780194E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.53371024E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32833897E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83488114E-01    2           5        -5   # BR(A -> b       bb     )
     1.87204774E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.61910419E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.12963442E-04    2           3        -3   # BR(A -> s       sb     )
     2.29406760E-07    2           4        -4   # BR(A -> c       cb     )
     2.28722016E-02    2           6        -6   # BR(A -> t       tb     )
     6.73564452E-05    2          21        21   # BR(A -> g       g      )
     4.48024754E-08    2          22        22   # BR(A -> gam     gam    )
     6.63740488E-08    2          23        22   # BR(A -> Z       gam    )
     3.16632741E-05    2          23        25   # BR(A -> Z       h      )
     2.63264425E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22136270E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31292882E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.92967416E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.03574554E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10116900E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.40162121E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.49154683E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.04747058E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.99030392E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02666599E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.16030110E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.06703917E-05    2          24        25   # BR(H+ -> W+      h      )
     7.93757374E-14    2          24        36   # BR(H+ -> W+      A      )
     1.90728307E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.63575101E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.88258720E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
