#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13937614E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03958000E+01   # W+
        25     1.25711106E+02   # h
        35     3.00012026E+03   # H
        36     2.99999996E+03   # A
        37     3.00110108E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03120388E+03   # ~d_L
   2000001     3.02582630E+03   # ~d_R
   1000002     3.03028550E+03   # ~u_L
   2000002     3.02817653E+03   # ~u_R
   1000003     3.03120388E+03   # ~s_L
   2000003     3.02582630E+03   # ~s_R
   1000004     3.03028550E+03   # ~c_L
   2000004     3.02817653E+03   # ~c_R
   1000005     7.37748313E+02   # ~b_1
   2000005     3.02532364E+03   # ~b_2
   1000006     7.35009559E+02   # ~t_1
   2000006     3.02174728E+03   # ~t_2
   1000011     3.00697324E+03   # ~e_L
   2000011     3.00079436E+03   # ~e_R
   1000012     3.00557566E+03   # ~nu_eL
   1000013     3.00697324E+03   # ~mu_L
   2000013     3.00079436E+03   # ~mu_R
   1000014     3.00557566E+03   # ~nu_muL
   1000015     2.98616069E+03   # ~tau_1
   2000015     3.02215746E+03   # ~tau_2
   1000016     3.00578143E+03   # ~nu_tauL
   1000021     2.33844805E+03   # ~g
   1000022     1.01072177E+02   # ~chi_10
   1000023     2.17151243E+02   # ~chi_20
   1000025    -3.00174873E+03   # ~chi_30
   1000035     3.00183648E+03   # ~chi_40
   1000024     2.17310835E+02   # ~chi_1+
   1000037     3.00272944E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891882E-01   # N_11
  1  2     7.64479046E-04   # N_12
  1  3    -1.46826373E-02   # N_13
  1  4     2.45397191E-04   # N_14
  2  1    -3.82182658E-04   # N_21
  2  2     9.99661001E-01   # N_22
  2  3     2.60297445E-02   # N_23
  2  4     4.34997880E-04   # N_24
  3  1     1.02192618E-02   # N_31
  3  2    -1.87073047E-02   # N_32
  3  3     7.06778962E-01   # N_33
  3  4     7.07113217E-01   # N_34
  4  1    -1.05662250E-02   # N_41
  4  2     1.80924075E-02   # N_42
  4  3    -7.06802922E-01   # N_43
  4  4     7.07100169E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99322977E-01   # U_11
  1  2     3.67911290E-02   # U_12
  2  1    -3.67911290E-02   # U_21
  2  2     9.99322977E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999812E-01   # V_11
  1  2    -6.13589192E-04   # V_12
  2  1    -6.13589192E-04   # V_21
  2  2    -9.99999812E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98597867E-01   # cos(theta_t)
  1  2    -5.29367549E-02   # sin(theta_t)
  2  1     5.29367549E-02   # -sin(theta_t)
  2  2     9.98597867E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99712748E-01   # cos(theta_b)
  1  2    -2.39670917E-02   # sin(theta_b)
  2  1     2.39670917E-02   # -sin(theta_b)
  2  2     9.99712748E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06904203E-01   # cos(theta_tau)
  1  2     7.07309301E-01   # sin(theta_tau)
  2  1    -7.07309301E-01   # -sin(theta_tau)
  2  2    -7.06904203E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00153021E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39376141E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44221714E+02   # higgs               
         4     1.08587688E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39376141E+03  # The gauge couplings
     1     3.61900538E-01   # gprime(Q) DRbar
     2     6.39658762E-01   # g(Q) DRbar
     3     1.02799741E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39376141E+03  # The trilinear couplings
  1  1     1.83466823E-06   # A_u(Q) DRbar
  2  2     1.83469710E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39376141E+03  # The trilinear couplings
  1  1     6.44042353E-07   # A_d(Q) DRbar
  2  2     6.44121453E-07   # A_s(Q) DRbar
  3  3     1.34983307E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39376141E+03  # The trilinear couplings
  1  1     3.05894636E-07   # A_e(Q) DRbar
  2  2     3.05910001E-07   # A_mu(Q) DRbar
  3  3     3.10297609E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39376141E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.54805893E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39376141E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.97531289E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39376141E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03299230E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39376141E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.65867127E+04   # M^2_Hd              
        22    -9.09595572E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42047014E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.64985740E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48032597E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48032597E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51967403E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51967403E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.54289997E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.29563224E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.02043479E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.85000199E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.19186619E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.53360186E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.50213860E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.03498474E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.20369924E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.90078803E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.70268525E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62882210E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.20618694E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.30626118E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.47200375E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.56358918E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.28921044E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.60488144E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.96135902E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -1.66155071E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.19337339E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.12927862E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -9.98372475E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.25740820E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.63861762E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.09068603E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.59147700E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.07359985E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.93347512E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64208949E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.92236042E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.75907028E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.28683370E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.80273267E-11    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.01174169E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19376417E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58722700E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.30236792E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.89793424E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.26455032E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41277265E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.07863795E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.83570264E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64191544E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.08859347E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.90936427E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.28109162E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.83858063E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.01863347E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67548941E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.52396857E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.56221079E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.40451619E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.49087289E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54760305E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.07359985E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.93347512E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64208949E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.92236042E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.75907028E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.28683370E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.80273267E-11    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.01174169E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19376417E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58722700E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.30236792E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.89793424E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.26455032E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41277265E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.07863795E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.83570264E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64191544E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.08859347E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.90936427E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.28109162E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.83858063E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.01863347E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67548941E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.52396857E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.56221079E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.40451619E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.49087289E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54760305E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.02051621E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.74593545E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00898860E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.13026290E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.20572715E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01641778E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.55629316E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55989382E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999855E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.44897641E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.02051621E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.74593545E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00898860E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.13026290E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.20572715E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01641778E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.55629316E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55989382E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999855E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.44897641E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.81854408E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.46121009E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18019937E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.35859055E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.76006257E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.54197685E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15313110E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.51510827E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     7.91418094E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30462586E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     9.18909824E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.02082278E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.68813198E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00993452E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.18169852E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.08992298E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02125226E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     8.20966082E-13    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.02082278E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.68813198E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00993452E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.18169852E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.08992298E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02125226E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     8.20966082E-13    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02143843E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.68731498E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00968408E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.31181185E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.21283056E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02158234E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.05078303E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.13170648E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.56712397E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.43436260E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.54542402E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.10482826E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.08434928E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.92257492E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.93092325E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.26422534E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.82564691E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     9.57090973E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.01273481E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.93985223E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.93250639E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.93250639E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.52196539E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.44936484E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.33556726E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.33556726E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.08467608E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.08467608E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.44965415E-12    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     3.44965415E-12    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     3.44965415E-12    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     3.44965415E-12    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     7.07338428E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     7.07338428E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.60448328E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.44397559E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.13523823E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.91145120E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.91145120E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.18719110E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.98639999E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.24006853E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.24006853E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.06119331E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.06119331E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.38159210E-12    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.38159210E-12    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     4.38159210E-12    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.38159210E-12    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.67837237E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.67837237E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.72586718E-03   # h decays
#          BR         NDA      ID1       ID2
     5.53614980E-01    2           5        -5   # BR(h -> b       bb     )
     6.99936046E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.47776055E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.25326961E-04    2           3        -3   # BR(h -> s       sb     )
     2.28259422E-02    2           4        -4   # BR(h -> c       cb     )
     7.49180906E-02    2          21        21   # BR(h -> g       g      )
     2.58555301E-03    2          22        22   # BR(h -> gam     gam    )
     1.78603362E-03    2          22        23   # BR(h -> Z       gam    )
     2.42818695E-01    2          24       -24   # BR(h -> W+      W-     )
     3.06839973E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.88998302E+01   # H decays
#          BR         NDA      ID1       ID2
     9.04313453E-01    2           5        -5   # BR(H -> b       bb     )
     6.39281981E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.26034832E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.77571025E-04    2           3        -3   # BR(H -> s       sb     )
     7.78045076E-08    2           4        -4   # BR(H -> c       cb     )
     7.76150005E-03    2           6        -6   # BR(H -> t       tb     )
     6.52422734E-06    2          21        21   # BR(H -> g       g      )
     4.60274244E-08    2          22        22   # BR(H -> gam     gam    )
     3.17552337E-09    2          23        22   # BR(H -> Z       gam    )
     7.34527208E-07    2          24       -24   # BR(H -> W+      W-     )
     3.66810576E-07    2          23        23   # BR(H -> Z       Z      )
     5.17086068E-06    2          25        25   # BR(H -> h       h      )
     7.80116065E-25    2          36        36   # BR(H -> A       A      )
     7.90991175E-19    2          23        36   # BR(H -> Z       A      )
     8.57260860E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.98453730E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.29014946E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.62080950E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.18866894E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.42851458E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.80519024E+01   # A decays
#          BR         NDA      ID1       ID2
     9.24475550E-01    2           5        -5   # BR(A -> b       bb     )
     6.53505823E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.31063719E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.83794228E-04    2           3        -3   # BR(A -> s       sb     )
     8.00827085E-08    2           4        -4   # BR(A -> c       cb     )
     7.98436738E-03    2           6        -6   # BR(A -> t       tb     )
     2.35131977E-05    2          21        21   # BR(A -> g       g      )
     5.56201804E-08    2          22        22   # BR(A -> gam     gam    )
     2.31414171E-08    2          23        22   # BR(A -> Z       gam    )
     7.47993400E-07    2          23        25   # BR(A -> Z       h      )
     8.92137922E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.10547003E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.46451030E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.70578965E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.14077834E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47354550E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.00763070E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12415168E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.43067967E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.24830909E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.56817321E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.26828388E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.88560873E-07    2          24        25   # BR(H+ -> W+      h      )
     5.14353408E-14    2          24        36   # BR(H+ -> W+      A      )
     4.95860988E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.27355564E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.06340639E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
