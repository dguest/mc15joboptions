#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871836E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.99900000E+02   # M_1(MX)             
         2     7.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04032498E+01   # W+
        25     1.24752812E+02   # h
        35     3.00002008E+03   # H
        36     2.99999994E+03   # A
        37     3.00108353E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04191304E+03   # ~d_L
   2000001     3.03674599E+03   # ~d_R
   1000002     3.04100902E+03   # ~u_L
   2000002     3.03806683E+03   # ~u_R
   1000003     3.04191304E+03   # ~s_L
   2000003     3.03674599E+03   # ~s_R
   1000004     3.04100902E+03   # ~c_L
   2000004     3.03806683E+03   # ~c_R
   1000005     1.05356247E+03   # ~b_1
   2000005     3.03430192E+03   # ~b_2
   1000006     1.05137242E+03   # ~t_1
   2000006     3.02462296E+03   # ~t_2
   1000011     3.00643531E+03   # ~e_L
   2000011     3.00183029E+03   # ~e_R
   1000012     3.00505086E+03   # ~nu_eL
   1000013     3.00643531E+03   # ~mu_L
   2000013     3.00183029E+03   # ~mu_R
   1000014     3.00505086E+03   # ~nu_muL
   1000015     2.98615923E+03   # ~tau_1
   2000015     3.02156203E+03   # ~tau_2
   1000016     3.00489482E+03   # ~nu_tauL
   1000021     2.35218552E+03   # ~g
   1000022     4.02287471E+02   # ~chi_10
   1000023     8.37563176E+02   # ~chi_20
   1000025    -2.99495832E+03   # ~chi_30
   1000035     2.99542385E+03   # ~chi_40
   1000024     8.37726600E+02   # ~chi_1+
   1000037     2.99616189E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888444E-01   # N_11
  1  2    -9.76668453E-05   # N_12
  1  3    -1.48839553E-02   # N_13
  1  4    -1.24804083E-03   # N_14
  2  1     5.13524275E-04   # N_21
  2  2     9.99605360E-01   # N_22
  2  3     2.74339338E-02   # N_23
  2  4     6.02002797E-03   # N_24
  3  1    -9.63804645E-03   # N_31
  3  2     1.51461676E-02   # N_32
  3  3    -7.06866171E-01   # N_33
  3  4     7.07119451E-01   # N_34
  4  1     1.13992817E-02   # N_41
  4  2    -2.36581749E-02   # N_42
  4  3     7.06658378E-01   # N_43
  4  4     7.07067382E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99247210E-01   # U_11
  1  2     3.87945130E-02   # U_12
  2  1    -3.87945130E-02   # U_21
  2  2     9.99247210E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99963775E-01   # V_11
  1  2    -8.51163661E-03   # V_12
  2  1    -8.51163661E-03   # V_21
  2  2    -9.99963775E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98450637E-01   # cos(theta_t)
  1  2    -5.56446356E-02   # sin(theta_t)
  2  1     5.56446356E-02   # -sin(theta_t)
  2  2     9.98450637E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99716692E-01   # cos(theta_b)
  1  2    -2.38020112E-02   # sin(theta_b)
  2  1     2.38020112E-02   # -sin(theta_b)
  2  2     9.99716692E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06966633E-01   # cos(theta_tau)
  1  2     7.07246902E-01   # sin(theta_tau)
  2  1    -7.07246902E-01   # -sin(theta_tau)
  2  2    -7.06966633E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00143470E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68718357E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43861734E+02   # higgs               
         4     1.02426605E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68718357E+03  # The gauge couplings
     1     3.62544025E-01   # gprime(Q) DRbar
     2     6.36743540E-01   # g(Q) DRbar
     3     1.02366619E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68718357E+03  # The trilinear couplings
  1  1     2.71988195E-06   # A_u(Q) DRbar
  2  2     2.71992069E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68718357E+03  # The trilinear couplings
  1  1     9.75627481E-07   # A_d(Q) DRbar
  2  2     9.75737025E-07   # A_s(Q) DRbar
  3  3     1.94512425E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68718357E+03  # The trilinear couplings
  1  1     3.96937205E-07   # A_e(Q) DRbar
  2  2     3.96956368E-07   # A_mu(Q) DRbar
  3  3     4.02294339E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68718357E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49890565E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68718357E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.73244304E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68718357E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.00958659E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68718357E+03  # The soft SUSY breaking masses at the scale Q
         1     3.99900000E+02   # M_1(Q)              
         2     7.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.04037584E+04   # M^2_Hd              
        22    -9.02414587E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40722558E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.12364795E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47874544E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47874544E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52125456E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52125456E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.59957856E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     6.52646689E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.02454411E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.32280920E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.11025722E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.78657146E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.69240007E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.42930234E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.47977933E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.03436345E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66709867E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59396064E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.11331862E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.38598444E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     8.37029229E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.31634868E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.84662210E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.22685579E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.09999921E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.27970849E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.72080157E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.51735959E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     6.25413495E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.62667788E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.80099607E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.72756948E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.40933993E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.56068982E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.19287904E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.52637190E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.26413637E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     8.33797303E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.05414518E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.53337879E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.35755271E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.14133632E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56966761E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.67241473E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.37460106E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.88278753E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43033169E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.56600581E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.20263832E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.52515324E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.50175119E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.24863743E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.04852348E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.46457762E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.36428963E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64311935E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.45869418E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.04304665E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.67900185E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.03572171E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55413039E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.56068982E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.19287904E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.52637190E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.26413637E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     8.33797303E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.05414518E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.53337879E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.35755271E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.14133632E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56966761E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.67241473E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.37460106E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.88278753E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43033169E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.56600581E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.20263832E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.52515324E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.50175119E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.24863743E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.04852348E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.46457762E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.36428963E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64311935E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.45869418E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.04304665E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.67900185E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.03572171E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55413039E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.47023371E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.09188287E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.97207950E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.89276833E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.50557492E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.93603706E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.89546583E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.51366420E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999763E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.32574779E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.01498453E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.45011857E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.47023371E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.09188287E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.97207950E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.89276833E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.50557492E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.93603706E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.89546583E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.51366420E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999763E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.32574779E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.01498453E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.45011857E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.51416188E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.75957567E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.08221455E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.15820978E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.46713454E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.84310523E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.05426554E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.71489065E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.48902677E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.10213524E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.73598064E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.47015181E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09211831E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96684698E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.68634254E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.29901418E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.94103449E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.76588477E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.47015181E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09211831E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96684698E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.68634254E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.29901418E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.94103449E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.76588477E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47024936E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09202677E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.96655763E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.48167714E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.25734027E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.94139274E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.26745628E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.37830712E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.37658872E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.29256200E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.56656712E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.45268881E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.35658591E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.11080128E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.18309068E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.75820646E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.11259333E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.54043617E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.45956383E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.39116040E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.26487559E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.49530933E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.16750249E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.16750249E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.40471638E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.88057484E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.32062385E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.32062385E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.33561615E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.33561615E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.71724149E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.71724149E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.30355601E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.14438895E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.86717950E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.23604587E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.23604587E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.33668893E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.91651329E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.28431918E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.28431918E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.40333216E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.40333216E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.98485238E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.98485238E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.57575749E-03   # h decays
#          BR         NDA      ID1       ID2
     5.66294429E-01    2           5        -5   # BR(h -> b       bb     )
     7.23718643E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.56199773E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.43996794E-04    2           3        -3   # BR(h -> s       sb     )
     2.36381389E-02    2           4        -4   # BR(h -> c       cb     )
     7.64566442E-02    2          21        21   # BR(h -> g       g      )
     2.60978156E-03    2          22        22   # BR(h -> gam     gam    )
     1.70936000E-03    2          22        23   # BR(h -> Z       gam    )
     2.27742794E-01    2          24       -24   # BR(h -> W+      W-     )
     2.83767910E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.42864011E+01   # H decays
#          BR         NDA      ID1       ID2
     8.94459894E-01    2           5        -5   # BR(H -> b       bb     )
     7.25276836E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.56440557E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.14910684E-04    2           3        -3   # BR(H -> s       sb     )
     8.82676167E-08    2           4        -4   # BR(H -> c       cb     )
     8.80525601E-03    2           6        -6   # BR(H -> t       tb     )
     1.37294638E-05    2          21        21   # BR(H -> g       g      )
     7.50633176E-08    2          22        22   # BR(H -> gam     gam    )
     3.60799886E-09    2          23        22   # BR(H -> Z       gam    )
     8.05541364E-07    2          24       -24   # BR(H -> W+      W-     )
     4.02274144E-07    2          23        23   # BR(H -> Z       Z      )
     6.17472198E-06    2          25        25   # BR(H -> h       h      )
     1.13660967E-24    2          36        36   # BR(H -> A       A      )
     6.50246348E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.23484289E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.24852909E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.45291073E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.23468183E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.97940998E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.35378384E+01   # A decays
#          BR         NDA      ID1       ID2
     9.14463081E-01    2           5        -5   # BR(A -> b       bb     )
     7.41465188E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.62164005E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.21991838E-04    2           3        -3   # BR(A -> s       sb     )
     9.08615325E-08    2           4        -4   # BR(A -> c       cb     )
     9.05903247E-03    2           6        -6   # BR(A -> t       tb     )
     2.66779837E-05    2          21        21   # BR(A -> g       g      )
     6.80948320E-08    2          22        22   # BR(A -> gam     gam    )
     2.62898930E-08    2          23        22   # BR(A -> Z       gam    )
     8.20487749E-07    2          23        25   # BR(A -> Z       h      )
     9.24606661E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.58726017E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.61819673E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.87228944E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.69326481E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46029606E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.73553694E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.38152158E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.34588322E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.39955979E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.87934453E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.19432520E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.46343979E-07    2          24        25   # BR(H+ -> W+      h      )
     5.32212790E-14    2          24        36   # BR(H+ -> W+      A      )
     4.88503740E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.12924269E-10    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07131361E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
