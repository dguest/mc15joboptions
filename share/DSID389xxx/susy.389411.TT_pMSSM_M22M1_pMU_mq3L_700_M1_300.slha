#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14500221E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.99900000E+02   # M_1(MX)             
         2     5.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04028546E+01   # W+
        25     1.24069006E+02   # h
        35     3.00015913E+03   # H
        36     2.99999975E+03   # A
        37     3.00092668E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03338826E+03   # ~d_L
   2000001     3.02833791E+03   # ~d_R
   1000002     3.03247575E+03   # ~u_L
   2000002     3.02977037E+03   # ~u_R
   1000003     3.03338826E+03   # ~s_L
   2000003     3.02833791E+03   # ~s_R
   1000004     3.03247575E+03   # ~c_L
   2000004     3.02977037E+03   # ~c_R
   1000005     7.94368981E+02   # ~b_1
   2000005     3.02756857E+03   # ~b_2
   1000006     7.92452838E+02   # ~t_1
   2000006     3.01410826E+03   # ~t_2
   1000011     3.00633546E+03   # ~e_L
   2000011     3.00169703E+03   # ~e_R
   1000012     3.00494459E+03   # ~nu_eL
   1000013     3.00633546E+03   # ~mu_L
   2000013     3.00169703E+03   # ~mu_R
   1000014     3.00494459E+03   # ~nu_muL
   1000015     2.98562027E+03   # ~tau_1
   2000015     3.02204593E+03   # ~tau_2
   1000016     3.00485006E+03   # ~nu_tauL
   1000021     2.34124796E+03   # ~g
   1000022     3.01928534E+02   # ~chi_10
   1000023     6.30233571E+02   # ~chi_20
   1000025    -2.99845316E+03   # ~chi_30
   1000035     2.99928544E+03   # ~chi_40
   1000024     6.30396355E+02   # ~chi_1+
   1000037     2.99983695E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99885196E-01   # N_11
  1  2    -7.72422346E-04   # N_12
  1  3     1.49670915E-02   # N_13
  1  4    -2.23275258E-03   # N_14
  2  1     1.19499384E-03   # N_21
  2  2     9.99605520E-01   # N_22
  2  3    -2.72397043E-02   # N_23
  2  4     6.73601952E-03   # N_24
  3  1    -8.99075602E-03   # N_31
  3  2     1.45081152E-02   # N_32
  3  3     7.06870313E-01   # N_33
  3  4     7.07137216E-01   # N_34
  4  1    -1.21381195E-02   # N_41
  4  2     2.40358360E-02   # N_42
  4  3     7.06659993E-01   # N_43
  4  4    -7.07040734E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99257221E-01   # U_11
  1  2    -3.85357708E-02   # U_12
  2  1     3.85357708E-02   # U_21
  2  2     9.99257221E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99954582E-01   # V_11
  1  2    -9.53064981E-03   # V_12
  2  1     9.53064981E-03   # V_21
  2  2     9.99954582E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98841787E-01   # cos(theta_t)
  1  2    -4.81153254E-02   # sin(theta_t)
  2  1     4.81153254E-02   # -sin(theta_t)
  2  2     9.98841787E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99914406E-01   # cos(theta_b)
  1  2     1.30836032E-02   # sin(theta_b)
  2  1    -1.30836032E-02   # -sin(theta_b)
  2  2     9.99914406E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06948187E-01   # cos(theta_tau)
  1  2     7.07265340E-01   # sin(theta_tau)
  2  1    -7.07265340E-01   # -sin(theta_tau)
  2  2     7.06948187E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01859951E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.45002212E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44335559E+02   # higgs               
         4     7.24568921E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.45002212E+03  # The gauge couplings
     1     3.62060740E-01   # gprime(Q) DRbar
     2     6.37345082E-01   # g(Q) DRbar
     3     1.02711119E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.45002212E+03  # The trilinear couplings
  1  1     1.98811776E-06   # A_u(Q) DRbar
  2  2     1.98814662E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.45002212E+03  # The trilinear couplings
  1  1     5.33562807E-07   # A_d(Q) DRbar
  2  2     5.33657436E-07   # A_s(Q) DRbar
  3  3     1.16814548E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.45002212E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.10165662E-07   # A_mu(Q) DRbar
  3  3     1.11465668E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.45002212E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52597458E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.45002212E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.15074117E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.45002212E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06986460E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.45002212E+03  # The soft SUSY breaking masses at the scale Q
         1     2.99900000E+02   # M_1(Q)              
         2     5.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.58762100E+04   # M^2_Hd              
        22    -9.06966910E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40990305E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.40442784E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48226395E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48226395E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51773605E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51773605E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.77986256E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     7.50376976E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.24962302E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.02239442E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.34073598E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.55347016E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.14122394E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.95618162E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.29535471E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.59021687E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.50200106E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.96726345E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.51064001E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.60262567E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.39737433E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.22890599E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.87281852E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.12800124E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.27139406E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.21951403E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.71430601E-10    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27878334E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.95637639E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.25897218E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.12149939E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.79404000E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.99076069E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.57648433E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.62750242E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.46918329E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.15359927E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.66319647E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.21000802E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17386410E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57155311E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.10078232E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     6.38280806E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.10401781E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42844462E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.79928582E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.08657809E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.57441268E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.74280735E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.90702819E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.14791305E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.86578880E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.21680465E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67030194E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.46566890E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.96911768E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.65746473E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.85954560E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55343247E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.79404000E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.99076069E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.57648433E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.62750242E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.46918329E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.15359927E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.66319647E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.21000802E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17386410E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57155311E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.10078232E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     6.38280806E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.10401781E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42844462E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.79928582E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.08657809E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.57441268E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.74280735E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.90702819E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.14791305E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.86578880E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.21680465E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67030194E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.46566890E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.96911768E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.65746473E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.85954560E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55343247E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.71122254E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03203970E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99351106E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.93304401E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.11038350E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97444904E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.81301493E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53376204E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998667E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.33199854E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.85041915E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.87988578E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.71122254E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03203970E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99351106E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.93304401E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.11038350E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97444904E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.81301493E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53376204E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998667E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.33199854E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.85041915E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.87988578E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.64919229E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.61529829E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13133356E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.25336815E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.59355288E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.70095697E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.10280982E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.17505102E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.31709993E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.19585096E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.33036796E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.71138507E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03712370E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98362448E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.34466106E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.43215611E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.97925175E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.85668202E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.71138507E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03712370E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98362448E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.34466106E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.43215611E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.97925175E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.85668202E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.71158274E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03703451E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98335444E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.27689297E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.28536526E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.97960367E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     7.31553603E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.33685152E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.51190807E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.78791984E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.18539554E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.20989639E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.99925209E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.18543715E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.82078003E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.35260344E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.17386803E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.28264430E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.47173557E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.52872193E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.45569649E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.11073619E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.79622947E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.79622947E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.24764031E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.89346220E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.62699453E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.62699453E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.29145192E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.29145192E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.38154862E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.38154862E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.45072948E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.06203950E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.88288849E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.86279784E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.86279784E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.52438434E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.48717427E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.59687287E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.59687287E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.31808451E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.31808451E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.34629976E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.34629976E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.72469494E-03   # h decays
#          BR         NDA      ID1       ID2
     6.86758461E-01    2           5        -5   # BR(h -> b       bb     )
     5.48459469E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.94159814E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.12670619E-04    2           3        -3   # BR(h -> s       sb     )
     1.78106378E-02    2           4        -4   # BR(h -> c       cb     )
     5.72204186E-02    2          21        21   # BR(h -> g       g      )
     1.92902293E-03    2          22        22   # BR(h -> gam     gam    )
     1.21577505E-03    2          22        23   # BR(h -> Z       gam    )
     1.59917165E-01    2          24       -24   # BR(h -> W+      W-     )
     1.96957426E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39551623E+01   # H decays
#          BR         NDA      ID1       ID2
     7.44991935E-01    2           5        -5   # BR(H -> b       bb     )
     1.78198264E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.30066477E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.73720837E-04    2           3        -3   # BR(H -> s       sb     )
     2.18363108E-07    2           4        -4   # BR(H -> c       cb     )
     2.17831307E-02    2           6        -6   # BR(H -> t       tb     )
     2.96632340E-05    2          21        21   # BR(H -> g       g      )
     4.16975055E-08    2          22        22   # BR(H -> gam     gam    )
     8.37922178E-09    2          23        22   # BR(H -> Z       gam    )
     3.27534872E-05    2          24       -24   # BR(H -> W+      W-     )
     1.63565554E-05    2          23        23   # BR(H -> Z       Z      )
     8.46903675E-05    2          25        25   # BR(H -> h       h      )
    -4.92286936E-23    2          36        36   # BR(H -> A       A      )
     8.06816111E-18    2          23        36   # BR(H -> Z       A      )
     1.96906195E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.08316978E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.82071368E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.64537473E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.97291070E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.05657568E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32735886E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83313456E-01    2           5        -5   # BR(A -> b       bb     )
     1.87342993E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.62399128E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.13563684E-04    2           3        -3   # BR(A -> s       sb     )
     2.29576140E-07    2           4        -4   # BR(A -> c       cb     )
     2.28890890E-02    2           6        -6   # BR(A -> t       tb     )
     6.74061821E-05    2          21        21   # BR(A -> g       g      )
     4.47397687E-08    2          22        22   # BR(A -> gam     gam    )
     6.64210847E-08    2          23        22   # BR(A -> Z       gam    )
     3.43056450E-05    2          23        25   # BR(A -> Z       h      )
     2.64177783E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22285972E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31748001E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.94902581E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.01231200E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09938683E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.45722876E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.68816153E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.03606465E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.10584661E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.05043683E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.15584780E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.50519404E-05    2          24        25   # BR(H+ -> W+      h      )
     8.89430726E-14    2          24        36   # BR(H+ -> W+      A      )
     1.95472326E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.71047310E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.36158324E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
