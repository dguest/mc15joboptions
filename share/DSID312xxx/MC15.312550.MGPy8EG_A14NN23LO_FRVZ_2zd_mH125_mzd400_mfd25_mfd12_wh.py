mH = 125
mfd2 = 5
mfd1 = 2
mZd = 400
nGamma = 2
avgtau = 5
decayMode = 'normal'
include("MC15JobOptions/MadGraphControl_A14N23LO_FRVZdisplaced_wh.py")
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]
