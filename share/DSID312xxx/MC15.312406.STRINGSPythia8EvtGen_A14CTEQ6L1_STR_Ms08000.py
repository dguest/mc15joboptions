evgenConfig.description = "String resonance (Ms = 8.0 TeV)"
evgenConfig.process = "STR -> 2-body"
evgenConfig.keywords = ["BSM", "exotic", "extraDimensions"]
evgenConfig.generators += ["STRINGS"]
evgenConfig.contact = ["Doug Gingrich <gingrich@ualberta.ca>"]
evgenConfig.inputfilecheck = "STR"

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py" )
include("MC15JobOptions/Pythia8_LHEF.py")

# Increase tolerance on displaced vertieces due to highly boosted heavy flavours.
testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #in mm
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000 #in mm
