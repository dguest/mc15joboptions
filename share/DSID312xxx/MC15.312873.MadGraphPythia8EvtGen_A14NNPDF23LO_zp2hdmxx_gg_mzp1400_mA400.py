include( 'MC15JobOptions/MadGraphControl_monoHiggs_zp2hdm_full_run2.py' )
evgenConfig.description = 'Simplified Model of vector mediator in 2HDM for MonoHiggs(h->gg) with mZp='+str(mZp)+'GeV and mA0='+str(mA0)+'GeV'
evgenConfig.keywords = ['BSM', 'BSMHiggs', 'Higgs', 'diphoton','simplifiedModel']
evgenConfig.contact = ['Alexander Leopold <alexander.leopold@cern.ch>']
genSeq.Pythia8.Commands += [
   '25:onMode=off',
   '25:onIfMatch = 22 22'
   ]

if not hasattr( filtSeq, 'DiPhotonFilter' ):
  from GeneratorFilters.GeneratorFiltersConf import DiPhotonFilter
  filtSeq += DiPhotonFilter()
DiPhotonFilter = filtSeq.DiPhotonFilter
DiPhotonFilter.PtCut1st = 30000.
DiPhotonFilter.PtCut2nd = 25000.
