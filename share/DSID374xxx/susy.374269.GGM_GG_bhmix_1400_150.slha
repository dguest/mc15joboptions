#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05376829E+01   # W+
        25     1.26000000E+02   # h
        35     2.00415863E+03   # H
        36     2.00000000E+03   # A
        37     2.00209649E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.99643680E+03   # ~d_L
   2000001     4.99632684E+03   # ~d_R
   1000002     4.99619111E+03   # ~u_L
   2000002     4.99624954E+03   # ~u_R
   1000003     4.99643680E+03   # ~s_L
   2000003     4.99632684E+03   # ~s_R
   1000004     4.99619111E+03   # ~c_L
   2000004     4.99624954E+03   # ~c_R
   1000005     4.99630003E+03   # ~b_1
   2000005     4.99646508E+03   # ~b_2
   1000006     5.12726939E+03   # ~t_1
   2000006     5.42532458E+03   # ~t_2
   1000011     5.00008277E+03   # ~e_L
   2000011     5.00007600E+03   # ~e_R
   1000012     4.99984123E+03   # ~nu_eL
   1000013     5.00008277E+03   # ~mu_L
   2000013     5.00007600E+03   # ~mu_R
   1000014     4.99984123E+03   # ~nu_muL
   1000015     5.00003957E+03   # ~tau_1
   2000015     5.00011983E+03   # ~tau_2
   1000016     4.99984123E+03   # ~nu_tauL
   1000021     1.52201227E+03   # ~g
   1000022     1.46522829E+02   # ~chi_10
   1000023    -1.62947308E+02   # ~chi_20
   1000025     2.94797816E+02   # ~chi_30
   1000035     3.12909424E+03   # ~chi_40
   1000024     1.60735602E+02   # ~chi_1+
   1000037     3.12909384E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     3.08720430E-01   # N_11
  1  2    -2.47369148E-02   # N_12
  1  3     6.79335804E-01   # N_13
  1  4    -6.65268853E-01   # N_14
  2  1     2.00317720E-02   # N_21
  2  2    -4.81333781E-03   # N_22
  2  3    -7.04243383E-01   # N_23
  2  4    -7.09659649E-01   # N_24
  3  1     9.50941771E-01   # N_31
  3  2     8.56866141E-03   # N_32
  3  3    -2.05702704E-01   # N_33
  3  4     2.30917136E-01   # N_34
  4  1     4.15234521E-04   # N_41
  4  2    -9.99645685E-01   # N_42
  4  3    -1.51828870E-02   # N_43
  4  4     2.18589263E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.14645781E-02   # U_11
  1  2     9.99769609E-01   # U_12
  2  1     9.99769609E-01   # U_21
  2  2     2.14645781E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.09095680E-02   # V_11
  1  2     9.99522185E-01   # V_12
  2  1     9.99522185E-01   # V_21
  2  2     3.09095680E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99808264E-01   # cos(theta_t)
  1  2    -1.95815024E-02   # sin(theta_t)
  2  1     1.95815024E-02   # -sin(theta_t)
  2  2     9.99808264E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08551728E-01   # cos(theta_b)
  1  2     9.12735167E-01   # sin(theta_b)
  2  1    -9.12735167E-01   # -sin(theta_b)
  2  2     4.08551728E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.76624338E-01   # cos(theta_tau)
  1  2     7.36328395E-01   # sin(theta_tau)
  2  1    -7.36328395E-01   # -sin(theta_tau)
  2  2     6.76624338E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90206857E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51745536E+02   # vev(Q)              
         4     3.88196696E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53146813E-01   # gprime(Q) DRbar
     2     6.29570015E-01   # g(Q) DRbar
     3     1.08619179E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02680038E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72362271E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79974855E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04066182E+06   # M^2_Hd              
        22    -5.50319056E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38111730E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.14925701E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.24100610E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.40245362E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.27161553E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     9.32483165E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.14591816E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.44409958E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.52051175E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.23072721E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.24615616E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     9.32483165E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.14591816E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.44409958E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.52051175E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.23072721E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.24615616E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.12426371E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.22855638E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     6.46871678E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.76487942E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.98031905E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     3.79847753E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.48114062E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.48114062E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.48114062E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.48114062E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.37717629E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.37717629E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.23346139E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.98069570E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.14683489E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.52940527E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.39499948E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.66833170E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.77579558E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.80901942E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     2.92525250E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.45135230E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     4.03132659E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.96119324E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.78251842E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     3.51478088E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.23993803E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.94452395E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -2.47908647E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.82645527E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -2.04027389E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.31414121E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     5.36888039E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     2.67915070E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.65537235E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.62836251E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.86377001E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.90599112E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -1.64873325E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -2.65568990E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -3.30895713E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.29427124E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.59692849E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.43728597E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.15272322E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -6.14562891E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.71918258E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.22043040E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.45492201E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     5.96325221E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.36215529E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.74510074E-05    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     9.16073321E-08    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.77763754E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.98351877E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.53516718E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.96649197E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.07481196E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.24935988E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.49951757E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.89369475E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.24784928E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.02453431E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.53003050E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.36219196E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.46500125E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.88519271E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.29038655E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.98492191E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.40336862E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.97039857E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.07529990E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.17012225E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.16597033E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.90717477E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.10075511E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.83785435E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87821571E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.36215529E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.74510074E-05    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     9.16073321E-08    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.77763754E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.98351877E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.53516718E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.96649197E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.07481196E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.24935988E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.49951757E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.89369475E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.24784928E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.02453431E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.53003050E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.36219196E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.46500125E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.88519271E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.29038655E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.98492191E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.40336862E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.97039857E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.07529990E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.17012225E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.16597033E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.90717477E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.10075511E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.83785435E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87821571E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80762189E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.54436973E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.89068051E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.04821018E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59563257E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.45659336E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19497462E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46513784E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.57614375E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.03015631E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.03835483E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.42270754E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80762189E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.54436973E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.89068051E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.04821018E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59563257E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.45659336E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19497462E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46513784E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.57614375E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.03015631E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.03835483E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.42270754E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62957272E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     6.08404407E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.84754790E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.56345349E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26706096E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.89289703E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.53593637E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36825936E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.65319092E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.61365172E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.12661839E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.05225447E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49093547E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.85125451E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.98399358E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80747303E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     2.74543678E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.80487229E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.92073843E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59774189E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.33889510E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19178217E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80747303E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     2.74543678E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.80487229E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.92073843E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59774189E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.33889510E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19178217E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.81068748E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.74229696E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.80280814E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.91854178E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59477098E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.48070926E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18584765E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.23630779E-07   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.87423205E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.36073433E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.36073433E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.12024532E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.12024532E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03765328E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.36654949E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.99042122E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.71247745E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.94612894E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.26209545E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.99431159E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     5.31602109E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     5.33915441E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.91657261E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     5.29753924E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     4.34202244E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.02292133E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.82551506E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.03309097E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.96156239E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.34663670E-04    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.26159870E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.50900390E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.65794542E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.18286043E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.01544426E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.27776316E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.65370635E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.27776316E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.65370635E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.83633603E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.77313461E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.77313461E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.52635700E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.53261751E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.53261751E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.53261751E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.36068314E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.36068314E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.36068314E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.36068314E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.86894542E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.86894542E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.86894542E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.86894542E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.36098968E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.36098968E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.63875494E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.83673722E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.19042025E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.99732959E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.99732959E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.79613542E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.17744109E-05    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.83958585E-09    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.47670304E-10    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.38084153E-12    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.36658509E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.03769479E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.93139758E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.02321514E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.94058906E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.94058906E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.71040768E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.76499841E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.63766690E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.36426602E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.01138138E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.77916558E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.22336572E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.48851440E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     4.19330002E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.33279011E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.33279011E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.44989769E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.77852900E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.80596346E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.81043274E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.61856232E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21672496E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01597694E-01    2           5        -5   # BR(h -> b       bb     )
     6.22637850E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20386654E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66295428E-04    2           3        -3   # BR(h -> s       sb     )
     2.01165019E-02    2           4        -4   # BR(h -> c       cb     )
     6.64331861E-02    2          21        21   # BR(h -> g       g      )
     2.32838015E-03    2          22        22   # BR(h -> gam     gam    )
     1.62707198E-03    2          22        23   # BR(h -> Z       gam    )
     2.16854869E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80918306E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01479506E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38942249E-03    2           5        -5   # BR(H -> b       bb     )
     2.32093744E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20537793E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05057722E-06    2           3        -3   # BR(H -> s       sb     )
     9.48136406E-06    2           4        -4   # BR(H -> c       cb     )
     9.38140977E-01    2           6        -6   # BR(H -> t       tb     )
     7.51335975E-04    2          21        21   # BR(H -> g       g      )
     2.62941400E-06    2          22        22   # BR(H -> gam     gam    )
     1.09156965E-06    2          23        22   # BR(H -> Z       gam    )
     3.16599927E-04    2          24       -24   # BR(H -> W+      W-     )
     1.57868129E-04    2          23        23   # BR(H -> Z       Z      )
     8.52372670E-04    2          25        25   # BR(H -> h       h      )
     8.40363163E-24    2          36        36   # BR(H -> A       A      )
     3.34259036E-11    2          23        36   # BR(H -> Z       A      )
     2.44234362E-12    2          24       -37   # BR(H -> W+      H-     )
     2.44234362E-12    2         -24        37   # BR(H -> W-      H+     )
     7.71013226E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.52137135E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.36059522E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.64275390E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     6.94637391E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.53643389E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.87743291E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05932535E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39235710E-03    2           5        -5   # BR(A -> b       bb     )
     2.29747941E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12241965E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07082377E-06    2           3        -3   # BR(A -> s       sb     )
     9.38383086E-06    2           4        -4   # BR(A -> c       cb     )
     9.39155051E-01    2           6        -6   # BR(A -> t       tb     )
     8.88928098E-04    2          21        21   # BR(A -> g       g      )
     2.69763200E-06    2          22        22   # BR(A -> gam     gam    )
     1.27304626E-06    2          23        22   # BR(A -> Z       gam    )
     3.08529524E-04    2          23        25   # BR(A -> Z       h      )
     5.85757394E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.28919177E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.65395732E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.84365792E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     9.65472500E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.36548090E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.93380550E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97901982E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23423440E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34630462E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29503438E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42050716E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13692270E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02355121E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40804461E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.16197817E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33823631E-12    2          24        36   # BR(H+ -> W+      A      )
     5.57917091E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.23538496E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.29286365E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
