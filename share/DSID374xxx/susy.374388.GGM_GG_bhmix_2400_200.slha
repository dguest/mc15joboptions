#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05373698E+01   # W+
        25     1.26000000E+02   # h
        35     2.00417113E+03   # H
        36     2.00000000E+03   # A
        37     2.00208904E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.89730157E+03   # ~d_L
   2000001     4.89719036E+03   # ~d_R
   1000002     4.89705305E+03   # ~u_L
   2000002     4.89711209E+03   # ~u_R
   1000003     4.89730157E+03   # ~s_L
   2000003     4.89719036E+03   # ~s_R
   1000004     4.89705305E+03   # ~c_L
   2000004     4.89711209E+03   # ~c_R
   1000005     4.89714672E+03   # ~b_1
   2000005     4.89734670E+03   # ~b_2
   1000006     5.03068944E+03   # ~t_1
   2000006     5.33465646E+03   # ~t_2
   1000011     5.00008266E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984134E+03   # ~nu_eL
   1000013     5.00008266E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984134E+03   # ~nu_muL
   1000015     5.00002622E+03   # ~tau_1
   2000015     5.00013306E+03   # ~tau_2
   1000016     4.99984134E+03   # ~nu_tauL
   1000021     2.41238029E+03   # ~g
   1000022     1.91315183E+02   # ~chi_10
   1000023    -2.16765305E+02   # ~chi_20
   1000025     2.96413797E+02   # ~chi_30
   1000035     3.12904246E+03   # ~chi_40
   1000024     2.14535721E+02   # ~chi_1+
   1000037     3.12904205E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.69225209E-01   # N_11
  1  2    -2.32974974E-02   # N_12
  1  3     6.31884920E-01   # N_13
  1  4    -6.16446573E-01   # N_14
  2  1     1.80284379E-02   # N_21
  2  2    -4.70682199E-03   # N_22
  2  3    -7.05102562E-01   # N_23
  2  4    -7.08860493E-01   # N_24
  3  1     8.82894388E-01   # N_31
  3  2     1.29546672E-02   # N_32
  3  3    -3.21417694E-01   # N_33
  3  4     3.42082361E-01   # N_34
  4  1     4.21127556E-04   # N_41
  4  2    -9.99633557E-01   # N_42
  4  3    -1.55721106E-02   # N_43
  4  4     2.21378180E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.20148781E-02   # U_11
  1  2     9.99757643E-01   # U_12
  2  1     9.99757643E-01   # U_21
  2  2     2.20148781E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.13037503E-02   # V_11
  1  2     9.99509918E-01   # V_12
  2  1     9.99509918E-01   # V_21
  2  2     3.13037503E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99612468E-01   # cos(theta_t)
  1  2    -2.78372739E-02   # sin(theta_t)
  2  1     2.78372739E-02   # -sin(theta_t)
  2  2     9.99612468E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.71078944E-01   # cos(theta_b)
  1  2     8.82091055E-01   # sin(theta_b)
  2  1    -8.82091055E-01   # -sin(theta_b)
  2  2     4.71078944E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.84679453E-01   # cos(theta_tau)
  1  2     7.28844323E-01   # sin(theta_tau)
  2  1    -7.28844323E-01   # -sin(theta_tau)
  2  2     6.84679453E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90211701E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51770398E+02   # vev(Q)              
         4     3.85436844E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53099828E-01   # gprime(Q) DRbar
     2     6.29225433E-01   # g(Q) DRbar
     3     1.07754460E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02735902E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72349993E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79965229E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.02014642E+06   # M^2_Hd              
        22    -5.39017475E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37961929E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.08394232E-02   # gluino decays
#          BR         NDA      ID1       ID2
     3.78289648E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.89342731E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.01651962E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.82916794E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.48087819E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.54575026E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.40171517E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.77510669E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.96299131E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.82916794E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.48087819E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.54575026E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.40171517E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.77510669E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.96299131E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.98545815E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.10022689E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.59556093E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.58348434E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.10927741E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     6.81643769E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.16643331E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.16643331E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.16643331E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.16643331E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.35659756E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.35659756E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.59074503E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.03202005E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.39477327E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.74632022E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.79925804E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     3.64215762E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.58306220E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.11013188E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.50103928E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.91558202E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.42515576E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     9.47553417E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.12852890E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.95852474E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -2.51809164E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.24642841E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -5.06459045E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.17331906E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -5.09710282E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.37249958E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.82345891E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     6.38976033E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     9.87462162E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.11344637E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.74860699E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.74013431E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -2.76219953E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -4.82275283E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -5.54482630E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.53055589E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.96840457E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.50491950E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.17496543E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -8.88975217E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.12506860E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.96555098E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.26852690E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     5.09865680E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.71559419E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     4.47209938E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.91449161E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.39937521E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.81496674E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.10700535E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.62925128E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.81500343E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61535416E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.40839017E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.07749428E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.96681398E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.02653232E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.36227180E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.71565775E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.32720174E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.01694338E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.48232098E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.81675141E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.04210635E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.63419755E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.81569760E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53814797E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.69776885E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.45452105E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.30405144E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.05722338E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.83256261E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.71559419E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     4.47209938E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.91449161E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.39937521E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.81496674E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.10700535E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.62925128E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.81500343E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61535416E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.40839017E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.07749428E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.96681398E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.02653232E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.36227180E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.71565775E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.32720174E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.01694338E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.48232098E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.81675141E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.04210635E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.63419755E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.81569760E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53814797E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.69776885E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.45452105E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.30405144E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.05722338E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.83256261E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80543642E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.03177297E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.04677232E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.80156911E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59484155E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.77877925E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19342859E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46528987E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.20878721E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.25796170E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.78795416E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60442042E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80543642E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.03177297E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.04677232E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.80156911E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59484155E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.77877925E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19342859E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46528987E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.20878721E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.25796170E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.78795416E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60442042E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63238582E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.35970087E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.64032377E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.72939321E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.29460017E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.92259540E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.59107269E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36677892E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64832761E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.14138875E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06878127E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.45971366E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46197434E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.84881061E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92605056E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80529044E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.74914265E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.53664668E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.62262985E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59698349E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.37060763E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19022967E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80529044E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.74914265E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.53664668E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.62262985E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59698349E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.37060763E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19022967E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80849928E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.74257399E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53489099E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.62077592E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59401632E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.51125901E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18430288E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.27149026E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.76316719E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.34387654E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34387654E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11462633E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11462633E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08271795E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.38828215E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.03051625E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48706806E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.98335759E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.91830850E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.03602707E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.91768551E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.94100294E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.96011126E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.90585537E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     9.86330348E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.01626621E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.94988495E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.02652703E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93141591E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.20570534E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     6.65601988E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.58911201E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.49053037E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.56075562E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.66108030E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.22027813E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.57941776E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.22027813E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.57941776E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.13487585E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.60412040E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.60412040E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.50093050E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.19558746E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.19558746E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.19558746E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.05909855E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.05909855E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.05909855E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.05909855E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.01969971E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.01969971E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.01969971E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.01969971E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.95845309E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.95845309E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.58722771E-03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.44571614E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.77771014E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.77771014E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.13604524E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.94423446E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.54503516E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.38831859E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.03625817E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.97362076E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.80779624E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.97931106E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.97931106E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49001771E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.49589187E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.70799232E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.00708852E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.60646861E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.91123227E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.88049467E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.81991996E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     9.37818911E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.93478870E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.93478870E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.43540825E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.72645049E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.78947537E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.93283920E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.85759961E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21713322E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01629352E-01    2           5        -5   # BR(h -> b       bb     )
     6.22586578E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20368506E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66256904E-04    2           3        -3   # BR(h -> s       sb     )
     2.01144239E-02    2           4        -4   # BR(h -> c       cb     )
     6.64291036E-02    2          21        21   # BR(h -> g       g      )
     2.32214277E-03    2          22        22   # BR(h -> gam     gam    )
     1.62689712E-03    2          22        23   # BR(h -> Z       gam    )
     2.16843687E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80891105E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01436917E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38903281E-03    2           5        -5   # BR(H -> b       bb     )
     2.32118308E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20624638E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05068737E-06    2           3        -3   # BR(H -> s       sb     )
     9.48255805E-06    2           4        -4   # BR(H -> c       cb     )
     9.38259442E-01    2           6        -6   # BR(H -> t       tb     )
     7.51400089E-04    2          21        21   # BR(H -> g       g      )
     2.63468765E-06    2          22        22   # BR(H -> gam     gam    )
     1.09159959E-06    2          23        22   # BR(H -> Z       gam    )
     3.18032745E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58582577E-04    2          23        23   # BR(H -> Z       Z      )
     8.52432114E-04    2          25        25   # BR(H -> h       h      )
     8.61420483E-24    2          36        36   # BR(H -> A       A      )
     3.39345853E-11    2          23        36   # BR(H -> Z       A      )
     2.56304399E-12    2          24       -37   # BR(H -> W+      H-     )
     2.56304399E-12    2         -24        37   # BR(H -> W-      H+     )
     7.49632868E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.87550654E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.75212494E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     4.52477768E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.45531662E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.45377480E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.11328230E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05891546E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39196641E-03    2           5        -5   # BR(A -> b       bb     )
     2.29771142E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12323990E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07093191E-06    2           3        -3   # BR(A -> s       sb     )
     9.38477849E-06    2           4        -4   # BR(A -> c       cb     )
     9.39249892E-01    2           6        -6   # BR(A -> t       tb     )
     8.89017867E-04    2          21        21   # BR(A -> g       g      )
     2.67561554E-06    2          22        22   # BR(A -> gam     gam    )
     1.27307547E-06    2          23        22   # BR(A -> Z       gam    )
     3.09918376E-04    2          23        25   # BR(A -> Z       h      )
     5.99371877E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.30136816E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.12612185E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.74034292E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.19950558E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48766051E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.69805291E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97853668E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23334866E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34658081E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29601084E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.41993917E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13752999E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02367221E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40914906E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17624109E-04    2          24        25   # BR(H+ -> W+      h      )
     1.31479110E-12    2          24        36   # BR(H+ -> W+      A      )
     1.28639573E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.26680308E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.55288086E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
