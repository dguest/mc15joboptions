#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05376817E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416821E+03   # H
        36     2.00000000E+03   # A
        37     2.00209576E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.89730172E+03   # ~d_L
   2000001     4.89719038E+03   # ~d_R
   1000002     4.89705296E+03   # ~u_L
   2000002     4.89711211E+03   # ~u_R
   1000003     4.89730172E+03   # ~s_L
   2000003     4.89719038E+03   # ~s_R
   1000004     4.89705296E+03   # ~c_L
   2000004     4.89711211E+03   # ~c_R
   1000005     4.89716323E+03   # ~b_1
   2000005     4.89733036E+03   # ~b_2
   1000006     5.03087536E+03   # ~t_1
   2000006     5.33480917E+03   # ~t_2
   1000011     5.00008277E+03   # ~e_L
   2000011     5.00007600E+03   # ~e_R
   1000012     4.99984123E+03   # ~nu_eL
   1000013     5.00008277E+03   # ~mu_L
   2000013     5.00007600E+03   # ~mu_R
   1000014     4.99984123E+03   # ~nu_muL
   1000015     5.00003957E+03   # ~tau_1
   2000015     5.00011983E+03   # ~tau_2
   1000016     4.99984123E+03   # ~nu_tauL
   1000021     2.41238088E+03   # ~g
   1000022     1.46535314E+02   # ~chi_10
   1000023    -1.62961724E+02   # ~chi_20
   1000025     2.94799659E+02   # ~chi_30
   1000035     3.12909437E+03   # ~chi_40
   1000024     1.60749931E+02   # ~chi_1+
   1000037     3.12909398E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     3.08751622E-01   # N_11
  1  2    -2.47372179E-02   # N_12
  1  3     6.79328786E-01   # N_13
  1  4    -6.65261532E-01   # N_14
  2  1     2.00314639E-02   # N_21
  2  2    -4.81340028E-03   # N_22
  2  3    -7.04243596E-01   # N_23
  2  4    -7.09659446E-01   # N_24
  3  1     9.50931651E-01   # N_31
  3  2     8.56968044E-03   # N_32
  3  3    -2.05725122E-01   # N_33
  3  4     2.30938803E-01   # N_34
  4  1     4.15251886E-04   # N_41
  4  2    -9.99645668E-01   # N_42
  4  3    -1.51832764E-02   # N_43
  4  4     2.18594117E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.14651284E-02   # U_11
  1  2     9.99769598E-01   # U_12
  2  1     9.99769598E-01   # U_21
  2  2     2.14651284E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.09102543E-02   # V_11
  1  2     9.99522164E-01   # V_12
  2  1     9.99522164E-01   # V_21
  2  2     3.09102543E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99580666E-01   # cos(theta_t)
  1  2    -2.89567291E-02   # sin(theta_t)
  2  1     2.89567291E-02   # -sin(theta_t)
  2  2     9.99580666E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08566245E-01   # cos(theta_b)
  1  2     9.12728669E-01   # sin(theta_b)
  2  1    -9.12728669E-01   # -sin(theta_b)
  2  2     4.08566245E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.76622906E-01   # cos(theta_tau)
  1  2     7.36329711E-01   # sin(theta_tau)
  2  1    -7.36329711E-01   # -sin(theta_tau)
  2  2     6.76622906E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90212286E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51750181E+02   # vev(Q)              
         4     3.87522363E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53146854E-01   # gprime(Q) DRbar
     2     6.29570241E-01   # g(Q) DRbar
     3     1.07754446E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02745669E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72384947E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79971445E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04057375E+06   # M^2_Hd              
        22    -5.37467904E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38111834E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.10969135E-02   # gluino decays
#          BR         NDA      ID1       ID2
     4.29500989E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.84001319E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     4.29674401E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     8.28849548E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.23431668E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.36204159E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.24045846E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.38487228E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.21758112E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     8.28849548E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.23431668E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.36204159E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.24045846E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.38487228E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.21758112E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.01346293E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.10693859E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     6.38439226E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.84898625E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.12402990E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     4.11008927E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     5.81046681E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     5.81046681E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     5.81046681E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     5.81046681E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.36238913E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.36238913E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.59290859E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.21137960E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.39928432E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.93623061E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.79971997E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     3.93424361E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.58460648E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.10512136E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.16318110E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     2.06592969E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.42814166E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.04206156E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.13205366E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     3.99320302E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -2.39068293E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.24797065E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -4.81229097E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.16929826E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.91445749E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.48602035E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.48956204E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     7.43172055E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     7.34518158E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.03680108E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.90741993E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.90855774E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -5.74470829E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -9.35646696E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -1.15277312E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     2.05575797E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.22289206E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     6.05922343E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.86799471E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -2.31601223E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.34344293E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.31137943E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.70582497E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     4.70030788E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.71579273E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.17349035E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.22914909E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.72623985E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.81863507E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.05944436E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.63657195E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.81398274E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61534897E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     6.10621654E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.56933972E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.76379272E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.91583608E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.36230159E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.71585608E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     7.33189053E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.89457476E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.07245024E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.82040535E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.93158295E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.64152085E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.81467888E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53814664E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.60320088E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.74585927E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.51329697E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.02815610E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.83257083E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.71579273E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.17349035E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.22914909E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.72623985E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.81863507E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.05944436E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.63657195E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.81398274E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61534897E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     6.10621654E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.56933972E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.76379272E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.91583608E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.36230159E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.71585608E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     7.33189053E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.89457476E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.07245024E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.82040535E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.93158295E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.64152085E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.81467888E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53814664E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.60320088E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.74585927E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.51329697E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.02815610E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.83257083E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80762348E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.54472644E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.89046563E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.04817414E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59563255E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.45692304E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19497470E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46513840E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.57807617E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.03003084E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.03816171E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.42324409E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80762348E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.54472644E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.89046563E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.04817414E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59563255E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.45692304E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19497470E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46513840E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.57807617E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.03003084E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.03816171E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.42324409E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62957292E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     6.08523700E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.84734082E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.56334869E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26705619E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.89289464E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.53592688E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36825925E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.65319250E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.61463549E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.12657911E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.05214067E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49094075E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.85036560E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.98400421E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80747463E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     2.74592909E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.80484645E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.92068860E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59774197E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.33895427E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19178213E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80747463E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     2.74592909E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.80484645E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.92068860E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59774197E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.33895427E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19178213E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.81068895E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.74278883E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.80278241E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.91849209E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59477117E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.48072404E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18584784E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.23960860E-07   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.87421747E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.36072751E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.36072751E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.12024304E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.12024304E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03767148E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.36663167E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.99046401E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.71247380E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.94617066E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.26255619E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.99428026E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     5.31571758E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     5.33893263E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.91627879E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     5.29732575E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     4.34278773E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.02289648E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.82561477E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.03255775E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.96208740E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.35484855E-04    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.26640733E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.50503203E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.65729089E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.18415435E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.01728861E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.27774519E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.65368291E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.27774519E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.65368291E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.83798907E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.77308091E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.77308091E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.52636370E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.53251009E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.53251009E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.53251009E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.35977991E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.35977991E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.35977991E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.35977991E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.86593465E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.86593465E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.86593465E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.86593465E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.36118056E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.36118056E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.63783823E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.83661060E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.19027943E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.99747146E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.99747146E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.79599507E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.16418507E-05    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.84038853E-09    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.47906696E-10    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.38335788E-12    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.36666721E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.03758171E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.93144006E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.02336629E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.94063106E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.94063106E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.71034441E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.76473439E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.63737584E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.36368816E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.01110112E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.77914179E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.22374362E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.48880177E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     4.19401634E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.33257487E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.33257487E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.44983729E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.77834183E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.80589873E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.81043890E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.61887258E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21694621E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01615452E-01    2           5        -5   # BR(h -> b       bb     )
     6.22615274E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20378663E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66278379E-04    2           3        -3   # BR(h -> s       sb     )
     2.01153001E-02    2           4        -4   # BR(h -> c       cb     )
     6.64319612E-02    2          21        21   # BR(h -> g       g      )
     2.32823554E-03    2          22        22   # BR(h -> gam     gam    )
     1.62698725E-03    2          22        23   # BR(h -> Z       gam    )
     2.16843523E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80903560E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01488200E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38913059E-03    2           5        -5   # BR(H -> b       bb     )
     2.32088139E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20517979E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05055101E-06    2           3        -3   # BR(H -> s       sb     )
     9.48135148E-06    2           4        -4   # BR(H -> c       cb     )
     9.38139980E-01    2           6        -6   # BR(H -> t       tb     )
     7.51304977E-04    2          21        21   # BR(H -> g       g      )
     2.62930566E-06    2          22        22   # BR(H -> gam     gam    )
     1.09154497E-06    2          23        22   # BR(H -> Z       gam    )
     3.18159142E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58645614E-04    2          23        23   # BR(H -> Z       Z      )
     8.52342137E-04    2          25        25   # BR(H -> h       h      )
     8.52921038E-24    2          36        36   # BR(H -> A       A      )
     3.38118981E-11    2          23        36   # BR(H -> Z       A      )
     2.50394712E-12    2          24       -37   # BR(H -> W+      H-     )
     2.50394712E-12    2         -24        37   # BR(H -> W-      H+     )
     7.71003322E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.52184942E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.36027321E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.64306344E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     6.94753159E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.53618459E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.87723652E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05933053E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39209993E-03    2           5        -5   # BR(A -> b       bb     )
     2.29747648E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12240929E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07082240E-06    2           3        -3   # BR(A -> s       sb     )
     9.38381889E-06    2           4        -4   # BR(A -> c       cb     )
     9.39153853E-01    2           6        -6   # BR(A -> t       tb     )
     8.88926964E-04    2          21        21   # BR(A -> g       g      )
     2.69761755E-06    2          22        22   # BR(A -> gam     gam    )
     1.27304425E-06    2          23        22   # BR(A -> Z       gam    )
     3.10050837E-04    2          23        25   # BR(A -> Z       h      )
     5.85782512E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.28939100E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.65390726E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.84522748E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     9.65778565E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.36511644E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.93376784E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97902328E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23367689E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34630172E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29502414E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42015036E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13691662E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02354999E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40802995E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17756719E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33590768E-12    2          24        36   # BR(H+ -> W+      A      )
     5.58029012E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.23543762E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.29274201E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
