#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.59300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.60000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05416151E+01   # W+
        25     1.26000000E+02   # h
        35     2.00400644E+03   # H
        36     2.00000000E+03   # A
        37     2.00151244E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.98044638E+03   # ~d_L
   2000001     4.98033678E+03   # ~d_R
   1000002     4.98020130E+03   # ~u_L
   2000002     4.98025919E+03   # ~u_R
   1000003     4.98044638E+03   # ~s_L
   2000003     4.98033678E+03   # ~s_R
   1000004     4.98020130E+03   # ~c_L
   2000004     4.98025919E+03   # ~c_R
   1000005     4.97973263E+03   # ~b_1
   2000005     4.98105192E+03   # ~b_2
   1000006     5.09568265E+03   # ~t_1
   2000006     5.38721878E+03   # ~t_2
   1000011     5.00008216E+03   # ~e_L
   2000011     5.00007612E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008216E+03   # ~mu_L
   2000013     5.00007612E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99965258E+03   # ~tau_1
   2000015     5.00050631E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.70498638E+03   # ~g
   1000022     1.64912286E+03   # ~chi_10
   1000023    -1.69955599E+03   # ~chi_20
   1000025     1.73638505E+03   # ~chi_30
   1000035     3.12668901E+03   # ~chi_40
   1000024     1.69532132E+03   # ~chi_1+
   1000037     3.12668521E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.27218819E-01   # N_11
  1  2    -3.60507750E-02   # N_12
  1  3     4.86147927E-01   # N_13
  1  4    -4.83232164E-01   # N_14
  2  1     2.57526461E-03   # N_21
  2  2    -3.21751100E-03   # N_22
  2  3    -7.07025504E-01   # N_23
  2  4    -7.07176040E-01   # N_24
  3  1     6.86398845E-01   # N_31
  3  2     4.06338949E-02   # N_32
  3  3    -5.12318571E-01   # N_33
  3  4     5.14524241E-01   # N_34
  4  1     1.66844259E-03   # N_41
  4  2    -9.98518340E-01   # N_42
  4  3    -3.61221669E-02   # N_43
  4  4     4.06636143E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.10359167E-02   # U_11
  1  2     9.98696818E-01   # U_12
  2  1     9.98696818E-01   # U_21
  2  2     5.10359167E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.74580966E-02   # V_11
  1  2     9.98347919E-01   # V_12
  2  1     9.98347919E-01   # V_21
  2  2     5.74580966E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99895837E-01   # cos(theta_t)
  1  2     1.44331268E-02   # sin(theta_t)
  2  1    -1.44331268E-02   # -sin(theta_t)
  2  2     9.99895837E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.77096790E-01   # cos(theta_b)
  1  2     7.35893971E-01   # sin(theta_b)
  2  1    -7.35893971E-01   # -sin(theta_b)
  2  2     6.77096790E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04601126E-01   # cos(theta_tau)
  1  2     7.09603589E-01   # sin(theta_tau)
  2  1    -7.09603589E-01   # -sin(theta_tau)
  2  2     7.04601126E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90201949E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.60000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52246200E+02   # vev(Q)              
         4     3.26272067E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52742148E-01   # gprime(Q) DRbar
     2     6.26917151E-01   # g(Q) DRbar
     3     1.08402980E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02504852E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71620915E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79765228E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.59300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.78142915E+05   # M^2_Hd              
        22    -7.82851650E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36933504E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.65902122E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.37332443E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.84867531E-07    2     1000023        21   # BR(~g -> ~chi_20 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.12778626E-07    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.33966210E-16    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     9.24223848E-07    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.02692964E-16    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.12778626E-07    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.33966210E-16    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     9.24223848E-07    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.02692964E-16    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.74672336E-07    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.87911020E-12    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.87911020E-12    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.87911020E-12    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.87911020E-12    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.92494489E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.69082645E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.86233267E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.33689856E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.59312643E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     8.79895241E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.12880286E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.13942014E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.67916568E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     5.17096116E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.58960965E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.49769375E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.76888574E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.83265812E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.30155884E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.75614898E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.67283202E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.22928977E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -6.05544837E-06    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.16417080E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     8.66186572E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.01245433E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.38176181E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.17403755E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.27267592E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.95122313E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.27027332E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.01145488E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.56933915E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.54260400E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.48009385E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.32870653E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.01910721E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.54360175E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.56846904E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.27516568E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.17309683E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.20105274E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.24802294E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.67798905E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.88410076E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.85217230E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.07174833E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     4.31843611E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.14527014E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.04877512E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.11709187E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.09629171E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.58886775E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.81802159E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.12884185E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60856557E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.24808791E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.97239495E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.94477130E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.01193833E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.07605427E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.40718696E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.15047176E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.04919538E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.05498819E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.39923493E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.66792345E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.68252337E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.32103375E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89918162E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.24802294E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.67798905E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.88410076E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.85217230E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.07174833E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     4.31843611E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.14527014E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.04877512E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.11709187E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.09629171E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.58886775E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.81802159E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.12884185E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60856557E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.24808791E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.97239495E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.94477130E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.01193833E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.07605427E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.40718696E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.15047176E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.04919538E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.05498819E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.39923493E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.66792345E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.68252337E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.32103375E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89918162E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.66702169E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.10502194E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.79327113E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.03272608E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.70490174E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.99073474E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.42194470E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.94176180E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.35494072E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.61392784E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.64497998E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.31601869E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.66702169E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.10502194E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.79327113E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.03272608E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.70490174E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.99073474E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.42194470E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.94176180E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.35494072E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.61392784E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.64497998E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.31601869E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.30960670E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.79340146E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.59576851E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.51457997E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.54563734E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.19933848E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.09823488E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.55721116E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.30707999E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.65268090E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.37257237E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.59167158E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57961258E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.41445060E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.16624792E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.66720020E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.15384269E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.24840552E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.76819867E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.71444546E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.79024882E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.41686465E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.66720020E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.15384269E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.24840552E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.76819867E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.71444546E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.79024882E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.41686465E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.66971386E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.15275630E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.24723009E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.76182611E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.71188969E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.72627144E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.41178396E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.39891706E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     5.72880790E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.14491741E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.14491741E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.04830697E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.04830697E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.04067045E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.84696440E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53397444E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.09117269E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46202810E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.37915432E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53358135E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     8.90761221E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.52975303E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.99744403E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.89232555E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.10230422E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.74334314E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.53406376E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.16135439E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.78359826E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.40792276E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.13061293E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.46401902E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.13061293E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.46401902E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.34405687E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.34379030E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.34379030E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.31233888E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.67787667E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.67787667E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.67787667E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.87517185E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.87517185E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.87517185E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.87517185E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.58390676E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.58390676E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.58390676E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.58390676E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.10122841E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.10122841E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.05504086E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.08597077E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.76140422E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.37877758E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.01193573E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     8.77360892E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18778201E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.40760854E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.18778201E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.40760854E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.70807718E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.42742393E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.42742393E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     5.93180772E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.90169125E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.90169125E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.90169125E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.04844209E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.65250953E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.04844209E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.65250953E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.26979866E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.05828683E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.05828683E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.96716125E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.20989317E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.20989317E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.20989317E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.27394482E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.27394482E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.27394482E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.27394482E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.24647682E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.24647682E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.24647682E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.24647682E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.20720136E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.20720136E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
     3.31449344E-08    3     1000021        -2         2   # BR(~chi_30 -> ~g      ub      u)
     8.35211705E-09    3     1000021        -1         1   # BR(~chi_30 -> ~g      db      d)
     3.31449344E-08    3     1000021        -4         4   # BR(~chi_30 -> ~g      cb      c)
     8.35211705E-09    3     1000021        -3         3   # BR(~chi_30 -> ~g      sb      s)
     5.82655253E-09    3     1000021        -5         5   # BR(~chi_30 -> ~g      bb      b)
#
#         PDG            Width
DECAY   1000035     3.84695340E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.05781513E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52126328E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.77792697E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46581776E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46581776E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.11709029E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.64478135E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.41244130E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.13138746E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.77515310E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     8.31572732E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.49339864E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.06472788E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22029410E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02078031E-01    2           5        -5   # BR(h -> b       bb     )
     6.22102163E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20197044E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65894379E-04    2           3        -3   # BR(h -> s       sb     )
     2.00996214E-02    2           4        -4   # BR(h -> c       cb     )
     6.63780762E-02    2          21        21   # BR(h -> g       g      )
     2.30564389E-03    2          22        22   # BR(h -> gam     gam    )
     1.62592083E-03    2          22        23   # BR(h -> Z       gam    )
     2.16548325E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80680737E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78099647E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46890484E-03    2           5        -5   # BR(H -> b       bb     )
     2.46428208E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71215460E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547439E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668397E-05    2           4        -4   # BR(H -> c       cb     )
     9.96067127E-01    2           6        -6   # BR(H -> t       tb     )
     7.97761935E-04    2          21        21   # BR(H -> g       g      )
     2.71604486E-06    2          22        22   # BR(H -> gam     gam    )
     1.16018775E-06    2          23        22   # BR(H -> Z       gam    )
     3.34604452E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66845922E-04    2          23        23   # BR(H -> Z       Z      )
     9.02398027E-04    2          25        25   # BR(H -> h       h      )
     7.26003908E-24    2          36        36   # BR(H -> A       A      )
     2.94599071E-11    2          23        36   # BR(H -> Z       A      )
     6.70949729E-12    2          24       -37   # BR(H -> W+      H-     )
     6.70949729E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381472E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47181985E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898229E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62268347E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677633E-06    2           3        -3   # BR(A -> s       sb     )
     9.96178561E-06    2           4        -4   # BR(A -> c       cb     )
     9.96998072E-01    2           6        -6   # BR(A -> t       tb     )
     9.43677616E-04    2          21        21   # BR(A -> g       g      )
     3.14386498E-06    2          22        22   # BR(A -> gam     gam    )
     1.35277854E-06    2          23        22   # BR(A -> Z       gam    )
     3.26075024E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472160E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35944582E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237997E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81146352E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50005669E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45695082E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731814E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402388E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34190880E-04    2          24        25   # BR(H+ -> W+      h      )
     2.77923467E-13    2          24        36   # BR(H+ -> W+      A      )
