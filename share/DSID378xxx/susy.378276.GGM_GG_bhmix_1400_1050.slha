#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.05375698E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.05000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05407640E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414482E+03   # H
        36     2.00000000E+03   # A
        37     2.00179607E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013266E+03   # ~d_L
   2000001     5.00002530E+03   # ~d_R
   1000002     4.99989264E+03   # ~u_L
   2000002     4.99994939E+03   # ~u_R
   1000003     5.00013266E+03   # ~s_L
   2000003     5.00002530E+03   # ~s_R
   1000004     4.99989264E+03   # ~c_L
   2000004     4.99994939E+03   # ~c_R
   1000005     4.99965222E+03   # ~b_1
   2000005     5.00050716E+03   # ~b_2
   1000006     4.99158038E+03   # ~t_1
   2000006     5.01286481E+03   # ~t_2
   1000011     5.00008205E+03   # ~e_L
   2000011     5.00007591E+03   # ~e_R
   1000012     4.99984203E+03   # ~nu_eL
   1000013     5.00008205E+03   # ~mu_L
   2000013     5.00007591E+03   # ~mu_R
   1000014     4.99984203E+03   # ~nu_muL
   1000015     4.99979957E+03   # ~tau_1
   2000015     5.00035901E+03   # ~tau_2
   1000016     4.99984203E+03   # ~nu_tauL
   1000021     1.40000000E+03   # ~g
   1000022     1.04324687E+03   # ~chi_10
   1000023    -1.05238194E+03   # ~chi_20
   1000025     1.06128764E+03   # ~chi_30
   1000035     3.00160442E+03   # ~chi_40
   1000024     1.05135798E+03   # ~chi_1+
   1000037     3.00160414E+03   # ~chi_2+
   1000039     4.62714837E-08   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.06098825E-01   # N_11
  1  2    -5.78866395E-03   # N_12
  1  3    -5.71408078E-01   # N_13
  1  4    -5.53266224E-01   # N_14
  2  1     2.06771513E-02   # N_21
  2  2    -1.91086842E-02   # N_22
  2  3     7.06666287E-01   # N_23
  2  4    -7.06986614E-01   # N_24
  3  1     7.95120403E-01   # N_31
  3  2     5.49181059E-03   # N_32
  3  3     4.17196665E-01   # N_33
  3  4     4.40113994E-01   # N_34
  4  1     4.63134388E-04   # N_41
  4  2    -9.99785572E-01   # N_42
  4  3    -7.90630386E-03   # N_43
  4  4     1.91333816E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.11728449E-02   # U_11
  1  2     9.99937582E-01   # U_12
  2  1     9.99937582E-01   # U_21
  2  2     1.11728449E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.70582552E-02   # V_11
  1  2    -9.99633858E-01   # V_12
  2  1     9.99633858E-01   # V_21
  2  2     2.70582552E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.08048379E-01   # cos(theta_t)
  1  2    -7.06163928E-01   # sin(theta_t)
  2  1     7.06163928E-01   # -sin(theta_t)
  2  2     7.08048379E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.61221639E-01   # cos(theta_b)
  1  2     7.50190605E-01   # sin(theta_b)
  2  1    -7.50190605E-01   # -sin(theta_b)
  2  2    -6.61221639E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03216245E-01   # cos(theta_tau)
  1  2     7.10976028E-01   # sin(theta_tau)
  2  1    -7.10976028E-01   # -sin(theta_tau)
  2  2    -7.03216245E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90195377E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.05000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51847463E+02   # vev(Q)              
         4     4.34170679E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52808735E-01   # gprime(Q) DRbar
     2     6.27339687E-01   # g(Q) DRbar
     3     1.08619171E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02648655E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72582401E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79781957E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.05375698E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.82004774E+06   # M^2_Hd              
        22    -6.51181076E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37121777E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.84522996E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.81585449E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.02420797E-01    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.49133727E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.76256666E-05    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     8.53561480E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.14943504E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.41539817E-05    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.95795800E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     6.49133727E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.76256666E-05    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     8.53561480E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.14943504E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.41539817E-05    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.95795800E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     6.85224832E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.44117306E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     8.52401493E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.09949993E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.24215210E-05    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     3.34005799E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     3.34005799E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.34005799E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     3.34005799E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.40675246E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.40675246E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.31012113E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.74178472E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.62426649E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.90815290E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.25754018E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.57949543E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.51152574E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.53772345E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.41392346E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.36942244E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.00736049E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.75833990E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.05973392E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.01543850E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.11558956E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.54689243E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.62332202E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.04105409E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.17961022E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.21696563E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.33261702E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.14952015E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.66954491E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.38736550E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.70531668E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.38040409E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79260351E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.92272586E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.57441472E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.33383851E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.15389237E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.13850688E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.43984835E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.24853867E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64332982E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.68897037E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.17697667E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.03726816E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.35312978E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.02064951E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.29805898E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55569221E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.80768487E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.66884086E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.07059609E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.57736588E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.43987930E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.01152617E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.74185583E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.45749720E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.17853402E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.76863527E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.35789833E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.02111548E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.22526135E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.01653588E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.66713883E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.89049933E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.05099225E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89088297E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.43984835E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.24853867E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64332982E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.68897037E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.17697667E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.03726816E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.35312978E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.02064951E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.29805898E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55569221E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.80768487E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.66884086E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.07059609E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.57736588E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.43987930E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.01152617E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.74185583E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.45749720E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.17853402E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.76863527E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.35789833E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.02111548E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.22526135E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.01653588E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.66713883E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.89049933E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.05099225E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89088297E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.96885222E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.77199636E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.36939486E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.23195621E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.69485104E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.50374529E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.39415243E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.26090714E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.68094492E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.27718658E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.31477693E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.61166546E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.96885222E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.77199636E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.36939486E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.23195621E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.69485104E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.50374529E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.39415243E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.26090714E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.68094492E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.27718658E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.31477693E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.61166546E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.61402299E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.04795780E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.64855598E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.40304347E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.51423719E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.14331417E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.03096903E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     6.42707143E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.62454832E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.90455705E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.36014324E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.44789403E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.54021422E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.07564251E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.08297684E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.96866627E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.24808889E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.68946972E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.17290201E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69740833E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.81959845E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.39037170E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.96866627E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.24808889E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.68946972E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.17290201E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69740833E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.81959845E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.39037170E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.97160358E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.24092444E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.68384590E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.17174264E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.69474205E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.86959613E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.38504306E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.32332913E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.45369011E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.52701560E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.04389609E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.17567867E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17468243E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.33358198E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     9.59051380E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53088740E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.49617365E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46037741E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.79383977E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53317743E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.36211379E-08    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.02344132E-13    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.18331499E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.49250138E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.32418362E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.96238957E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.64685201E-06    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.02621080E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.91232237E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.39549036E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.80685477E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18877299E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.80046111E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.12611676E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.12329509E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.24895569E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.23991472E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.23991472E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.23991472E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.32097887E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.32097887E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.40326319E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.40326319E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.17902425E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.17902425E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.91270408E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.13432013E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.72903365E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.17632736E-04    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.30192120E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.75036337E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.31439917E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.61441045E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.31139960E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.26193807E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     5.67424403E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     5.67134257E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.92263416E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.03544963E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.03544963E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.03544963E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.39975409E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.10718624E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.02588092E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.09562251E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     7.09565940E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.09055810E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.50659682E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.41695452E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.41695452E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.41695452E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.39581458E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.39581458E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.26445999E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.26445999E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.65266647E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.65266647E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.65003483E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.65003483E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.98484012E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.98484012E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     9.59077026E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.35789019E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.70689867E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.05900441E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46410056E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46410056E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.76685345E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.16204356E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.98589334E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.27563757E-09    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.03437623E-08    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     8.96531431E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.90979809E-13    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.06290058E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07908233E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17140018E-01    2           5        -5   # BR(h -> b       bb     )
     6.38505395E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26007394E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78930242E-04    2           3        -3   # BR(h -> s       sb     )
     2.06626700E-02    2           4        -4   # BR(h -> c       cb     )
     6.70925484E-02    2          21        21   # BR(h -> g       g      )
     2.30135503E-03    2          22        22   # BR(h -> gam     gam    )
     1.53997285E-03    2          22        23   # BR(h -> Z       gam    )
     2.01049331E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56586274E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78119806E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47969972E-03    2           5        -5   # BR(H -> b       bb     )
     2.46434255E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71236839E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11549124E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667043E-05    2           4        -4   # BR(H -> c       cb     )
     9.96057547E-01    2           6        -6   # BR(H -> t       tb     )
     7.97626302E-04    2          21        21   # BR(H -> g       g      )
     2.74677006E-06    2          22        22   # BR(H -> gam     gam    )
     1.15990511E-06    2          23        22   # BR(H -> Z       gam    )
     3.32659363E-04    2          24       -24   # BR(H -> W+      W-     )
     1.65876059E-04    2          23        23   # BR(H -> Z       Z      )
     9.04197142E-04    2          25        25   # BR(H -> h       h      )
     8.66671690E-24    2          36        36   # BR(H -> A       A      )
     3.49079347E-11    2          23        36   # BR(H -> Z       A      )
     4.97037439E-12    2          24       -37   # BR(H -> W+      H-     )
     4.97037439E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82384771E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48248930E-03    2           5        -5   # BR(A -> b       bb     )
     2.43896125E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62260908E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676652E-06    2           3        -3   # BR(A -> s       sb     )
     9.96169967E-06    2           4        -4   # BR(A -> c       cb     )
     9.96989471E-01    2           6        -6   # BR(A -> t       tb     )
     9.43669475E-04    2          21        21   # BR(A -> g       g      )
     2.97321560E-06    2          22        22   # BR(A -> gam     gam    )
     1.35248008E-06    2          23        22   # BR(A -> Z       gam    )
     3.24188006E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74523854E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.38325647E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238909E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81149579E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.51529562E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45686614E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08730121E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99404197E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32356579E-04    2          24        25   # BR(H+ -> W+      h      )
     6.56197891E-13    2          24        36   # BR(H+ -> W+      A      )
