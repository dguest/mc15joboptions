#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.75601371E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -2.75000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05431338E+01   # W+
        25     1.25000000E+02   # h
        35     2.00413735E+03   # H
        36     2.00000000E+03   # A
        37     2.00168388E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013257E+03   # ~d_L
   2000001     5.00002534E+03   # ~d_R
   1000002     4.99989277E+03   # ~u_L
   2000002     4.99994931E+03   # ~u_R
   1000003     5.00013257E+03   # ~s_L
   2000003     5.00002534E+03   # ~s_R
   1000004     4.99989277E+03   # ~c_L
   2000004     4.99994931E+03   # ~c_R
   1000005     4.99896202E+03   # ~b_1
   2000005     5.00119712E+03   # ~b_2
   1000006     4.97423282E+03   # ~t_1
   2000006     5.03009917E+03   # ~t_2
   1000011     5.00008188E+03   # ~e_L
   2000011     5.00007603E+03   # ~e_R
   1000012     4.99984208E+03   # ~nu_eL
   1000013     5.00008188E+03   # ~mu_L
   2000013     5.00007603E+03   # ~mu_R
   1000014     4.99984208E+03   # ~nu_muL
   1000015     4.99934705E+03   # ~tau_1
   2000015     5.00081139E+03   # ~tau_2
   1000016     4.99984208E+03   # ~nu_tauL
   1000021     2.80000000E+03   # ~g
   1000022     2.74329739E+03   # ~chi_10
   1000023    -2.75138753E+03   # ~chi_20
   1000025     2.76210486E+03   # ~chi_30
   1000035     3.00199899E+03   # ~chi_40
   1000024     2.75008687E+03   # ~chi_1+
   1000037     3.00199799E+03   # ~chi_2+
   1000039     7.35539331E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.52961669E-01   # N_11
  1  2    -4.86458350E-02   # N_12
  1  3    -5.91738300E-01   # N_13
  1  4    -5.84562024E-01   # N_14
  2  1     7.91556421E-03   # N_21
  2  2    -1.34609423E-02   # N_22
  2  3     7.06984896E-01   # N_23
  2  4    -7.07056224E-01   # N_24
  3  1     8.33156576E-01   # N_31
  3  2     3.78843236E-02   # N_32
  3  3     3.85830661E-01   # N_33
  3  4     3.94397766E-01   # N_34
  4  1     4.56684384E-03   # N_41
  4  2    -9.98006595E-01   # N_42
  4  3     3.39535376E-02   # N_43
  4  4     5.30012965E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.81471238E-02   # U_11
  1  2     9.98840255E-01   # U_12
  2  1     9.98840255E-01   # U_21
  2  2    -4.81471238E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     7.50200824E-02   # V_11
  1  2    -9.97182023E-01   # V_12
  2  1     9.97182023E-01   # V_21
  2  2     7.50200824E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07464343E-01   # cos(theta_t)
  1  2    -7.06749038E-01   # sin(theta_t)
  2  1     7.06749038E-01   # -sin(theta_t)
  2  2     7.07464343E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.89936924E-01   # cos(theta_b)
  1  2     7.23869492E-01   # sin(theta_b)
  2  1    -7.23869492E-01   # -sin(theta_b)
  2  2    -6.89936924E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05693326E-01   # cos(theta_tau)
  1  2     7.08517417E-01   # sin(theta_tau)
  2  1    -7.08517417E-01   # -sin(theta_tau)
  2  2    -7.05693326E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90193463E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -2.75000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52171257E+02   # vev(Q)              
         4     5.30860315E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52637300E-01   # gprime(Q) DRbar
     2     6.26250011E-01   # g(Q) DRbar
     3     1.07510222E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02739269E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73596624E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79453074E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.75601371E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -5.39677101E+06   # M^2_Hd              
        22    -1.23008035E+07   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36635771E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     4.60616912E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.41474230E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     6.40492001E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.52626086E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.46405014E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.03442305E-04    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.19712194E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.89680186E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.96772493E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     4.52626086E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.46405014E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.03442305E-04    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.19712194E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.89680186E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.96772493E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.93920918E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.03254675E-05    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     8.20856692E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.79575527E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.79575527E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.79575527E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.79575527E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     1.85443799E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.94245179E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.72745894E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.69693785E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.52459636E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     8.25369341E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.96927815E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.48855835E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.03932107E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.97580663E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.24286911E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.12083075E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.56712148E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.72076143E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.06906030E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.63035503E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.56395875E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.69592182E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.71918279E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.87018474E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.34597128E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.06425376E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.77828937E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.16718719E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.59252913E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.29620855E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.22877016E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.61699342E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.53281962E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.13700258E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.15795690E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.04355898E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.51402658E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.84851900E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     8.67371624E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.26731832E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.06923688E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     6.81360729E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.01391445E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.44773981E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.33051144E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.19230184E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.43091406E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.67509381E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.82061780E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61322931E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.51411540E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.41264504E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.35132490E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.43661135E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.08717489E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.80668782E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.01738781E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.44838982E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.29197176E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.06979778E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.25883139E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.88751867E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.75610596E-07    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90041882E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.51402658E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.84851900E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     8.67371624E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.26731832E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.06923688E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     6.81360729E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.01391445E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.44773981E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.33051144E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.19230184E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.43091406E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.67509381E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.82061780E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61322931E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.51411540E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.41264504E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.35132490E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.43661135E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.08717489E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.80668782E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.01738781E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.44838982E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.29197176E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.06979778E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.25883139E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.88751867E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.75610596E-07    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90041882E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.69481864E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     2.44102988E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.85239419E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.98422023E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.93354982E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.63264165E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.90731351E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.19880059E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.08296254E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.28535239E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.91623288E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.76036158E-05    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.69481864E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     2.44102988E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.85239419E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.98422023E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.93354982E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.63264165E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.90731351E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.19880059E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.08296254E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.28535239E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.91623288E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.76036158E-05    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.94566119E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.16615035E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.88778915E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.72501169E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.01688915E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.87036749E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     4.06135735E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     3.41565360E-10    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.95263247E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.07328041E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.87512379E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.77312764E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.03900908E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.88271148E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     4.10582504E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.69461210E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     4.58346195E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.12960354E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.49934859E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.96370622E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.96352469E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.88724787E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.69461210E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     4.58346195E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.12960354E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.49934859E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.96370622E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.96352469E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.88724787E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.69617040E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     4.58081286E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.12895066E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.49559218E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.96199330E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.53952482E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.88384200E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.02764754E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.62871996E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     7.93548882E-04    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#           BR         NDA      ID1       ID2       ID3
     3.61693616E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.93388654E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.20565092E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.20419354E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     8.68525353E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.58749091E-01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.47653835E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.54944766E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.49020933E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.70508356E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.51329629E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.12596263E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.79532184E-13    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.14799161E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.65747559E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.94363202E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
     9.81339193E-04    2     1000039        35   # BR(~chi_10 -> ~G        H)
     2.41087389E-02    2     1000039        36   # BR(~chi_10 -> ~G        A)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.33568619E-01    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.34362870E-09    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.51433917E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.92882805E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
     3.66593353E-04    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.49188463E-05    2     1000039        36   # BR(~chi_20 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.23006788E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.59300184E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     9.98531248E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.58582733E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     3.63934458E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.63618344E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.64696181E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.26877865E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.26877865E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.26877865E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.38764028E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.38764028E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.12921401E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.12921401E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.06101868E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.06101868E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.66656142E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.47766670E-05    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.20825287E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     5.85876499E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     5.81161703E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
     1.02292883E-07    2     1000039        35   # BR(~chi_30 -> ~G        H)
     2.97180444E-06    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     4.44006400E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.10758481E-06    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.16070266E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.10087553E-06    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.68319952E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.71077843E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.70997475E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.51759366E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.62182577E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.62182577E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.62182577E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.50716223E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.13209670E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.91315816E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.11373838E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.08377638E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.62938902E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.62857470E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.38001322E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.25426748E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.25426748E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.25426748E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.33763097E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.33763097E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.11884200E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.11884200E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.11252914E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.11252914E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.11209944E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.11209944E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.00024196E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.00024196E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.58715732E-01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.13242831E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.28043533E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.77680171E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.52934832E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.52934832E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.46577680E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.91691620E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.44911222E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.66817207E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     8.56947451E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.22418005E-12    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.83864008E-17    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.79814536E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07053185E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16407607E-01    2           5        -5   # BR(h -> b       bb     )
     6.39842971E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26480847E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79933583E-04    2           3        -3   # BR(h -> s       sb     )
     2.07061267E-02    2           4        -4   # BR(h -> c       cb     )
     6.72293229E-02    2          21        21   # BR(h -> g       g      )
     2.30829464E-03    2          22        22   # BR(h -> gam     gam    )
     1.54333855E-03    2          22        23   # BR(h -> Z       gam    )
     2.01402073E-01    2          24       -24   # BR(h -> W+      W-     )
     2.57125257E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78118189E+01   # H decays
#          BR         NDA      ID1       ID2
     1.49283159E-03    2           5        -5   # BR(H -> b       bb     )
     2.46435022E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71239552E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11549534E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666575E-05    2           4        -4   # BR(H -> c       cb     )
     9.96052711E-01    2           6        -6   # BR(H -> t       tb     )
     7.97525111E-04    2          21        21   # BR(H -> g       g      )
     2.73057168E-06    2          22        22   # BR(H -> gam     gam    )
     1.16059747E-06    2          23        22   # BR(H -> Z       gam    )
     3.32076359E-04    2          24       -24   # BR(H -> W+      W-     )
     1.65585443E-04    2          23        23   # BR(H -> Z       Z      )
     8.96890743E-04    2          25        25   # BR(H -> h       h      )
     8.27174400E-24    2          36        36   # BR(H -> A       A      )
     3.45964741E-11    2          23        36   # BR(H -> Z       A      )
     6.18152856E-12    2          24       -37   # BR(H -> W+      H-     )
     6.18152856E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82389565E+01   # A decays
#          BR         NDA      ID1       ID2
     1.49548798E-03    2           5        -5   # BR(A -> b       bb     )
     2.43893067E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62250098E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13675227E-06    2           3        -3   # BR(A -> s       sb     )
     9.96157478E-06    2           4        -4   # BR(A -> c       cb     )
     9.96976972E-01    2           6        -6   # BR(A -> t       tb     )
     9.43657644E-04    2          21        21   # BR(A -> g       g      )
     3.05754163E-06    2          22        22   # BR(A -> gam     gam    )
     1.35326174E-06    2          23        22   # BR(A -> Z       gam    )
     3.23618234E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74511794E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.41185972E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49232966E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81128567E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.53360182E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45677741E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08728355E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99404801E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.31730794E-04    2          24        25   # BR(H+ -> W+      h      )
     4.75346051E-13    2          24        36   # BR(H+ -> W+      A      )
