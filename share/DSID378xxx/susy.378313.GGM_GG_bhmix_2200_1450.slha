#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.45402667E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.45000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05415448E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414739E+03   # H
        36     2.00000000E+03   # A
        37     2.00175382E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013260E+03   # ~d_L
   2000001     5.00002531E+03   # ~d_R
   1000002     4.99989271E+03   # ~u_L
   2000002     4.99994937E+03   # ~u_R
   1000003     5.00013260E+03   # ~s_L
   2000003     5.00002531E+03   # ~s_R
   1000004     4.99989271E+03   # ~c_L
   2000004     4.99994937E+03   # ~c_R
   1000005     4.99949068E+03   # ~b_1
   2000005     5.00066862E+03   # ~b_2
   1000006     4.98750508E+03   # ~t_1
   2000006     5.01692951E+03   # ~t_2
   1000011     5.00008198E+03   # ~e_L
   2000011     5.00007594E+03   # ~e_R
   1000012     4.99984208E+03   # ~nu_eL
   1000013     5.00008198E+03   # ~mu_L
   2000013     5.00007594E+03   # ~mu_R
   1000014     4.99984208E+03   # ~nu_muL
   1000015     4.99969305E+03   # ~tau_1
   2000015     5.00046546E+03   # ~tau_2
   1000016     4.99984208E+03   # ~nu_tauL
   1000021     2.20000000E+03   # ~g
   1000022     1.44323177E+03   # ~chi_10
   1000023    -1.45200044E+03   # ~chi_20
   1000025     1.46129299E+03   # ~chi_30
   1000035     3.00150235E+03   # ~chi_40
   1000024     1.45119256E+03   # ~chi_1+
   1000037     3.00150207E+03   # ~chi_2+
   1000039     1.16148254E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.05060780E-01   # N_11
  1  2    -7.45719775E-03   # N_12
  1  3    -5.69504862E-01   # N_13
  1  4    -5.56336279E-01   # N_14
  2  1     1.49897786E-02   # N_21
  2  2    -1.73908941E-02   # N_22
  2  3     7.06822918E-01   # N_23
  2  4    -7.07017839E-01   # N_24
  3  1     7.96037977E-01   # N_31
  3  2     6.68151394E-03   # N_32
  3  3     4.19568927E-01   # N_33
  3  4     4.36166037E-01   # N_34
  4  1     5.46105264E-04   # N_41
  4  2    -9.99798632E-01   # N_42
  4  3    -5.24307233E-03   # N_43
  4  4     1.93625305E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.40447634E-03   # U_11
  1  2     9.99972586E-01   # U_12
  2  1     9.99972586E-01   # U_21
  2  2     7.40447634E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.73836276E-02   # V_11
  1  2    -9.99624998E-01   # V_12
  2  1     9.99624998E-01   # V_21
  2  2     2.73836276E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07787029E-01   # cos(theta_t)
  1  2    -7.06425878E-01   # sin(theta_t)
  2  1     7.06425878E-01   # -sin(theta_t)
  2  2     7.07787029E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.74135555E-01   # cos(theta_b)
  1  2     7.38607645E-01   # sin(theta_b)
  2  1    -7.38607645E-01   # -sin(theta_b)
  2  2    -6.74135555E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04336226E-01   # cos(theta_tau)
  1  2     7.09866523E-01   # sin(theta_tau)
  2  1    -7.09866523E-01   # -sin(theta_tau)
  2  2    -7.04336226E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90200898E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.45000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51927195E+02   # vev(Q)              
         4     4.56274492E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52751617E-01   # gprime(Q) DRbar
     2     6.26976474E-01   # g(Q) DRbar
     3     1.07892363E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02726726E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72904453E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79694771E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.45402667E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.96606044E+05   # M^2_Hd              
        22    -7.35132604E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36959719E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.29109173E-04   # gluino decays
#          BR         NDA      ID1       ID2
     2.39806258E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.77991320E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.39556441E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.88573952E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     8.69590728E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.81344946E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.27673294E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.70539894E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.02279824E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.88573952E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     8.69590728E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.81344946E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.27673294E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.70539894E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.02279824E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.06831991E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.22034930E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.85070013E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.13072450E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.45251199E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.00947405E-01    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     2.32460990E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.32460990E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.32460990E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.32460990E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.34660252E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.34660252E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.72635599E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     5.33947433E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.06560007E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.59288861E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.52158589E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.06280848E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     3.03857392E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.12233918E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.87453443E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     9.17202277E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.10056520E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.95173116E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.26112694E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.10750426E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.51735503E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.20170696E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.13409651E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.38974015E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.46561464E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.53483151E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.69112718E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.33444417E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.38808097E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.08794273E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.18856741E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.60942515E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83665188E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.51163001E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.89198794E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.47066309E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.79046006E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.89804490E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.96540224E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     9.05994361E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.70969118E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.95204057E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.94090105E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.20648316E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.88068080E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.78788401E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.81610988E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.79723557E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.10062759E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.09667606E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.16226567E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.51049870E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.96545877E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.17887873E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.28268965E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.63194396E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.94301502E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.82153581E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.88716011E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.78845778E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.74948766E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.66428742E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.85641119E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.03667010E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.85884433E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87296184E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.96540224E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.05994361E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.70969118E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.95204057E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.94090105E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.20648316E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.88068080E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.78788401E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.81610988E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.79723557E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.10062759E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.09667606E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.16226567E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.51049870E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.96545877E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.17887873E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.28268965E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.63194396E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.94301502E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.82153581E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.88716011E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.78845778E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.74948766E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.66428742E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.85641119E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.03667010E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.85884433E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87296184E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.91932909E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.23925649E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.50535376E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.15747822E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.73741853E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.15861262E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.48011120E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07422200E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.67162702E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.24847608E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.32612305E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.45632217E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.91932909E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.23925649E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.50535376E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.15747822E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.73741853E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.15861262E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.48011120E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07422200E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.67162702E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.24847608E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.32612305E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.45632217E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.49637237E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.95508964E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.73380675E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.27200628E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.58836990E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.00650729E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.17979376E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.06799293E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.50526059E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.82580585E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.14034101E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.32984251E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.60712406E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     8.47387635E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.21735028E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.91913961E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.81071890E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.74488528E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.09042172E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.74053563E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.42317516E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.47580270E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.91913961E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.81071890E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.74488528E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.09042172E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.74053563E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.42317516E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.47580270E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.92183366E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.80443914E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.74143234E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.08941631E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.73800874E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.76373958E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.47075221E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.11252084E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.28504777E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.54200570E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.03994682E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.18067505E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17963648E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.29231170E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.98925491E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53374387E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48667570E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46344948E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.91820845E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52431007E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     3.46114665E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.43156607E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.09380754E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.50917612E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.39701634E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.92642187E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.40965212E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.01456598E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.91565682E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.40209321E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.81553182E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17691995E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.80856362E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.14653120E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.14345798E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.18777072E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.28104033E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.28104033E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.28104033E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.95228382E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.95228382E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.65076138E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.65076138E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.51856704E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.51856704E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.77122629E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.01898787E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.14866413E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.97329323E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.11720705E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     9.90065092E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.32116294E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.14482883E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.31946839E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.30316695E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.34019502E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.33849046E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.90290236E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.86190572E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.86190572E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.86190572E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.17233625E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.10780021E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.71814276E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.09375482E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     9.38197889E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     9.37577601E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.45684218E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.87361678E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.87361678E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.87361678E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.62317292E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.62317292E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.47523994E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.47523994E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.41052226E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.41052226E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.40756449E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.40756449E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.65768321E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.65768321E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.98943955E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.20743585E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     6.23189963E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.06642966E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46674976E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46674976E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.16267107E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.90292774E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.10036809E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     8.32625111E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.62799051E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.98903198E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.74762138E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.74674214E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07667951E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16936619E-01    2           5        -5   # BR(h -> b       bb     )
     6.38892265E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26144332E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79220278E-04    2           3        -3   # BR(h -> s       sb     )
     2.06746958E-02    2           4        -4   # BR(h -> c       cb     )
     6.71308341E-02    2          21        21   # BR(h -> g       g      )
     2.30365306E-03    2          22        22   # BR(h -> gam     gam    )
     1.54092435E-03    2          22        23   # BR(h -> Z       gam    )
     2.01144933E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56737502E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78128374E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48338805E-03    2           5        -5   # BR(H -> b       bb     )
     2.46427165E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71211772E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11545879E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666533E-05    2           4        -4   # BR(H -> c       cb     )
     9.96052569E-01    2           6        -6   # BR(H -> t       tb     )
     7.97606046E-04    2          21        21   # BR(H -> g       g      )
     2.73785642E-06    2          22        22   # BR(H -> gam     gam    )
     1.16010393E-06    2          23        22   # BR(H -> Z       gam    )
     3.34330325E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66709294E-04    2          23        23   # BR(H -> Z       Z      )
     9.03018873E-04    2          25        25   # BR(H -> h       h      )
     8.78990704E-24    2          36        36   # BR(H -> A       A      )
     3.50160756E-11    2          23        36   # BR(H -> Z       A      )
     5.46285296E-12    2          24       -37   # BR(H -> W+      H-     )
     5.46285296E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82386831E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48618155E-03    2           5        -5   # BR(A -> b       bb     )
     2.43894811E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62256263E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676040E-06    2           3        -3   # BR(A -> s       sb     )
     9.96164601E-06    2           4        -4   # BR(A -> c       cb     )
     9.96984100E-01    2           6        -6   # BR(A -> t       tb     )
     9.43664391E-04    2          21        21   # BR(A -> g       g      )
     3.02531956E-06    2          22        22   # BR(A -> gam     gam    )
     1.35273589E-06    2          23        22   # BR(A -> Z       gam    )
     3.25820843E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74519404E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.39138259E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49236610E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81141450E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.52049639E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45683139E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729429E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402533E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34014909E-04    2          24        25   # BR(H+ -> W+      h      )
     5.82581425E-13    2          24        36   # BR(H+ -> W+      A      )
