include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")


# To modify Higgs BR:
cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL

set /Herwig/Particles/h0:NominalMass 125*GeV
set /Herwig/Particles/h0:Width 0.00403*GeV
set /Herwig/Particles/h0:WidthCut 0.00403*GeV
set /Herwig/Particles/h0:WidthLoCut 0.00403*GeV
set /Herwig/Particles/h0:WidthUpCut 0.00403*GeV
set /Herwig/Particles/h0/h0->W+,W-;:OnOff 0
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff 1
set /Herwig/Particles/h0/h0->b,bbar;:OnOff 0
set /Herwig/Particles/h0/h0->c,cbar;:OnOff 0
set /Herwig/Particles/h0/h0->g,g;:OnOff 0
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff 0
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff 0
set /Herwig/Particles/h0/h0->t,tbar;:OnOff 0
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff 0

## Z boson
set /Herwig/Particles/Z0/Z0->b,bbar;:OnOff 0
set /Herwig/Particles/Z0/Z0->c,cbar;:OnOff 0
set /Herwig/Particles/Z0/Z0->d,dbar;:OnOff 0
set /Herwig/Particles/Z0/Z0->e-,e+;:OnOff 1
set /Herwig/Particles/Z0/Z0->mu-,mu+;:OnOff 1
set /Herwig/Particles/Z0/Z0->nu_e,nu_ebar;:OnOff 0
set /Herwig/Particles/Z0/Z0->nu_mu,nu_mubar;:OnOff 0
set /Herwig/Particles/Z0/Z0->nu_tau,nu_taubar;:OnOff 0
set /Herwig/Particles/Z0/Z0->s,sbar;:OnOff 0
set /Herwig/Particles/Z0/Z0->tau-,tau+;:OnOff 0
set /Herwig/Particles/Z0/Z0->u,ubar;:OnOff 0
"""

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwigpp"]
evgenConfig.description = "aMC@NLO showered with Herwig++, mH=123 GeV"
evgenConfig.keywords += ['Higgs', 'SMHiggs', 'ttHiggs']
evgenConfig.contact  = ["Robert Harrington <roberth@cern.ch>"]
evgenConfig.inputfilecheck = '342564.tth_m123_ct10_13tev'

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
genSeq.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
