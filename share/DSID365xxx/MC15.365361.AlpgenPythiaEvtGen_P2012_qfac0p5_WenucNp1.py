#Alpgen Pythia JobOption
evgenConfig.description = "ALPGEN+Pythia W(->enu)+c process with PythiaPerugia2012C tune, Q factorization = 0.5"
evgenConfig.keywords = ["SM", "W", "electron", "jets"]
evgenConfig.minevents = 5000
evgenConfig.inputfilecheck = "AlpgenPythia_P2012_qfac0p5_WenucNp1"

if runArgs.trfSubstepName == 'generate' :
   include('MC15JobOptions/AlpgenPythia_Perugia2012_Common.py')
   include('MC15JobOptions/Pythia_Tauola.py')
   include('MC15JobOptions/Pythia_Photos.py')

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Alpgen_EvtGen.py')

