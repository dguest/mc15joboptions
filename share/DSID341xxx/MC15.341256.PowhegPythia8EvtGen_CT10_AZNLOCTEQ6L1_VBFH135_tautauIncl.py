#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 135.0
PowhegConfig.width_H = 0.00618

# CPS for the SM Higgs
PowhegConfig.complexpolescheme = 1

#
PowhegConfig.withdamp = 1

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 4.

# Generate Powheg events
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15' ]

if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  filtSeq += TauFilter()

filtSeq.TauFilter.Ntaus = 2
filtSeq.TauFilter.Ptcute = 12000.0 #MeV
filtSeq.TauFilter.Ptcutmu = 12000.0 #MeV
filtSeq.TauFilter.Ptcuthad = 20000.0 #MeV

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "VBF H->tautau->inclusive"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H(135)->tautau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "VBF", "2tau" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch' ]
evgenConfig.generators += ["Powheg", "Pythia8", "EvtGen"]
