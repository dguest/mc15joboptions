include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")

# tHjb sample with top-Yukawa coupling ct = +1
# To modify Higgs BR
cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL

do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar;
"""

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["MadGraph", "Herwigpp", "EvtGen"]
evgenConfig.description = "MadGraph5 LO showered with Herwig++"
evgenConfig.keywords += ['Higgs', 'SMHiggs', 'ttHiggs', 'tHiggs']
evgenConfig.contact  = ["Andrey Loginov <andrey.loginov@yale.edu>"]
evgenConfig.inputfilecheck = 'thjb'

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
genSeq.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
