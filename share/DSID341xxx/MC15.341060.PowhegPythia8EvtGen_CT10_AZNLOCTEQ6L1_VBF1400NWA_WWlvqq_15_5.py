#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 1400.
PowhegConfig.width_H = 0.00407

# Complex pole scheme or not (1 for NWA and 3(CPS) for SM)
PowhegConfig.bwshape = 1
# CPS for the SM Higgs
PowhegConfig.complexpolescheme = 0

#
PowhegConfig.withdamp = 1

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 2.5

# Generate Powheg events
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 24 -24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16' ]
#--------------------------------------------------------------
# filter
#--------------------------------------------------------------

include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25                
filtSeq.XtoVVDecayFilterExtended.PDGParent = 24                      
filtSeq.XtoVVDecayFilterExtended.StatusParent = 22                  
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,12,13,14,15,16]    
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [1,2,3,4,5,6]
filtSeq.Expression = "XtoVVDecayFilterExtended"


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->WW->lvqq"
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "VBF", "WW" ]
evgenConfig.contact     = [ 'Olivier.Arnaez@cern.ch' ]
