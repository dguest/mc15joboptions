from MadGraphControl.MadGraphUtils import *

evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'MadGraph_jjaH_HEFT_M125'
evgenConfig.keywords+=['Higgs', 'bottom', 'photon']
evgenConfig.inputfilecheck = 'jjaH_HEFT_M125'

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += ["25:onMode = off",
                            "25:onIfMatch = 5 5"]
