#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 1000.
PowhegConfig.width_H = 0.00407

# width settings for Higgs 
PowhegConfig.whiggsfixedwidth = 0  # use running BW width
PowhegConfig.complexpolescheme = 0 # not CPS

# Increase number of events requested to compensate for filter efficiency
# ZZ->llqq is 22% of ZZ decays (not counting Z->vv) (with 1.15x safety factor)
PowhegConfig.nEvents *= 1.15/0.22

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',#decay of Z
                             '23:mMin = 2.0',
                             '23:onIfAny = 1 2 3 4 5 6 11 13 15' ]

#--------------------------------------------------------------
# ZZ->llqq filter
#--------------------------------------------------------------
include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 23
filtSeq.XtoVVDecayFilterExtended.StatusParent = 22
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,13,15]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [1,2,3,4,5,6]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->ZZ->llqq mh=1000 GeV NWA"
evgenConfig.keywords    = [ "Higgs", "BSMHiggs", "ZZ" ]
evgenConfig.contact     = [ 'roberth@cern.ch','rdinardo@cern.ch','jochen.meyer@cern.ch','maria.hoffmann@cern.ch' ]

