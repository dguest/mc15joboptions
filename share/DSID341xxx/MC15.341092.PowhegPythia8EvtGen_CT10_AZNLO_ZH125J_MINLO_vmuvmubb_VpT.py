#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+Z+jet->vmuvmubbar production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_HZj_Common.py')

PowhegConfig.nubound=200000
PowhegConfig.xupbound=5

PowhegConfig.runningscales = 1 # 
PowhegConfig.vdecaymode = 5 # Z->vmuvmu
PowhegConfig.hdecaymode = -1

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
PowhegConfig.ptVhigh = 200 # step-wise pTV reweighting
PowhegConfig.ptVlow = 120
PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

PowhegConfig.storeinfo_rwgt = 1 # store info for PDF / scales variations reweighting
PowhegConfig.PDF = range( 10800, 10853 ) # CT10 PDF variations
PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]

#PowhegConfig.generateRunCard()
#PowhegConfig.generateEvents()
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 5 5' ]
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->vmuvmubbar production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch' ]
evgenConfig.inputconfcheck = "ZvvH125J_MINLO_VpT"
evgenConfig.minevents = 50

evgenConfig.process = "qq->ZH, H->bb, Z->vmuvmu"
