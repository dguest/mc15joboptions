include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")

# tWH sample with top-Yukawa coupling ct = +2
# To modify Higgs BR
cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL

do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma;
"""

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwigpp"]
evgenConfig.description = "aMC@NLO tWH(125GeV) H->gamgam ct=+2 showered with Herwig++"
evgenConfig.keywords += ['Higgs', 'SMHiggs', 'ttHiggs']
evgenConfig.contact  = ["Andrey Loginov <andrey.loginov@yale.edu>"]
evgenConfig.inputfilecheck = 'twh'

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
genSeq.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
