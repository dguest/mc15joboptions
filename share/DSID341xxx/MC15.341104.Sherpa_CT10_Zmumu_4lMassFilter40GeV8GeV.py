include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> mu+mu- + 0,1j@NLO + 2j@LO with 4l mass filter ( m1(ll)>40 GeV and m2(ll)>8 GeV )"
evgenConfig.keywords = [ "SM", "Z", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "antonio.salvucci@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 200
evgenConfig.inputconfcheck = "Sherpa_CT10_Zmumu"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=2; LJET:=2; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
}(run)

(processes){
  Process 93 93 -> 13 -13 93{NJET};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01;
  SCALES LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  End process;
}(processes)

(selector){
  Mass 13 -13 40.0 E_CMS
}(selector)
"""

include("MC15JobOptions/FourLeptonMassFilter.py")
filtSeq.FourLeptonMassFilter.MinPt = 4000.
filtSeq.FourLeptonMassFilter.MaxEta = 3.
filtSeq.FourLeptonMassFilter.MinMass1 = 40000.
filtSeq.FourLeptonMassFilter.MaxMass1 = 14000000.
filtSeq.FourLeptonMassFilter.MinMass2 = 8000.
filtSeq.FourLeptonMassFilter.MaxMass2 = 14000000.
filtSeq.FourLeptonMassFilter.AllowElecMu = True
filtSeq.FourLeptonMassFilter.AllowSameCharge = True
