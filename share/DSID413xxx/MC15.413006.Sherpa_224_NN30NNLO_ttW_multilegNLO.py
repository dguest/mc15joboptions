include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa ttW+0,1j@NLO+1,2j@LO with Sherpa 2.2.4"
evgenConfig.keywords = ["ttW", "SM", "multilepton"]
evgenConfig.contact = ["frank.siegert@cern.ch", "atlas-generators-sherpa@cern.ch"]
evgenConfig.inputconfcheck = "ttW"

genSeq.Sherpa_i.RunCard="""
(run){
    HARD_DECAYS=1;
    STABLE[6] = 0; WIDTH[6]=0.0;
    STABLE[23] = 0; STABLE[24] = 0;
    ACTIVE[25] = 0;
    
    CORE_SCALE VAR{H_TM2/4}
    EXCLUSIVE_CLUSTER_MODE 1;
    
    # merging setup
    QCUT:=30.;
    LJET:=3,4; NJET:=2; 
    
    ME_SIGNAL_GENERATOR Comix Amegic OpenLoops;
    INTEGRATION_ERROR=0.05;
    
    # top/W decays
    HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
    STABLE[24] 0; WIDTH[24] 0; STABLE[6] 0; WIDTH[6] 0;
}(run)

(processes){
    Process 93 93 -> 6 -6 24 93{NJET};
    Order (*,1);
    CKKW sqr(QCUT/E_CMS);
    NLO_QCD_Mode MC@NLO {LJET}; 
    ME_Generator Amegic {LJET};
    RS_ME_Generator Comix {LJET};
    Loop_Generator OpenLoops;
    End process;
    
    Process 93 93 -> 6 -6 -24 93{NJET};
    Order (*,1);
    CKKW sqr(QCUT/E_CMS);
    NLO_QCD_Mode MC@NLO {LJET}; 
    ME_Generator Amegic {LJET};
    RS_ME_Generator Comix {LJET};
    Loop_Generator OpenLoops;
    End process;

}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.0", "WIDTH[24]=0.0"]

genSeq.Sherpa_i.NCores = 240
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppwtt", "ppwttj" ]

evgenConfig.minevents = 2000
