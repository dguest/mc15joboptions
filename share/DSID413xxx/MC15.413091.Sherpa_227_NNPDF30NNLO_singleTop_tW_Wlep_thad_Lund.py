include("Sherpa_i/2.2.7_NNPDF30NNLO.py")

evgenConfig.description = "Sherpa tW production W-leptonic top-hadronic"
evgenConfig.keywords = ["SM", "W", "top" ]
evgenConfig.contact = [ "atlas-generators-sherpa@cern.ch" , "kai.chung.tam@cern.ch" ]
evgenConfig.minevents = 10000

genSeq.Sherpa_i.RunCard="""
(run){
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN
  LOOPGEN:=OpenLoops
  HARD_DECAYS On

  SCF:=1.; FSF:=SCF; RSF:=SCF; QSF:=SCF;
  scl2:=MPerp2(p[3]);
  SCALES VAR{FSF*scl2}{RSF*scl2}{QSF*scl2};

  HDH_STATUS[24,12,-11] 0
  HDH_STATUS[24,14,-13] 0
  HDH_STATUS[24,16,-15] 0
  HDH_STATUS[-24,-2,1] 0
  HDH_STATUS[-24,-4,3] 0

  PARJ(21) 0.36; PARJ(41) 0.3; PARJ(42) 0.6
  FRAGMENTATION Lund; DECAYMODEL Lund
}(run)

(processes){
  Process 93 93 -> 6 -24
  No_Decay -6  
  NLO_QCD_Mode MC@NLO
  Order (*,1)
  ME_Generator Amegic
  RS_ME_Generator Comix
  Loop_Generator LOOPGEN
  End process
}(processes)
"""
genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.OpenLoopsLibs = [ "pptw" ]
genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.", "WIDTH[24]=0.", "EW_SCHEME=3" ]
