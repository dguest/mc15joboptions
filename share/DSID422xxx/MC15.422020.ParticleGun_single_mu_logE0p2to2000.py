evgenConfig.description = "Single muons with flat eta-phi and log E in [0.2, 2000] GeV"
evgenConfig.keywords = ["singleParticle", "muon"]
	
include("MC15JobOptions/ParticleGun_Common.py")
	
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-13, 13)
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(200., 2000000.), eta=[-5.5, 5.5])
