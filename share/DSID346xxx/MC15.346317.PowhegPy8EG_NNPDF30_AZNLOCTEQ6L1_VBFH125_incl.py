#--------------------------------------------------------------
# Powheg ggH_quark_mass_effects setup
#--------------------------------------------------------------
#include('PowhegControl/PowhegControl_ggF_H_Common.py')

evgenConfig.process     = "VBFH H-> all"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->all mh=125 GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" ]
evgenConfig.contact     = [ 'roberto.di.nardo@cern.ch','hongtao.yang@cern.ch' ]
evgenConfig.minevents   = 10000
evgenConfig.inputFilesPerJob = 2


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_SMHiggs125_inc.py")
#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [ '23:mMin = 2.0',
                             '24:mMin = 2.0' ]

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']


