#--------------------------------------------------------------
# Read MadGraph output LHEF, Showering with Pythia8, AU2 tune
#--------------------------------------------------------------
include("MC15JobOptions/nonStandard/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------

evgenConfig.description = "SM p p > l nu l nu gam gam with Myy<80 GeV"
evgenConfig.keywords = ["SM","diphoton", "2lepton", "neutrino"]

evgenConfig.contact = ['Kaili Zhang <kazhang@cern.ch>']
evgenConfig.inputfilecheck = 'aMCatNLO.346024.sm_lnulnugamgam'
