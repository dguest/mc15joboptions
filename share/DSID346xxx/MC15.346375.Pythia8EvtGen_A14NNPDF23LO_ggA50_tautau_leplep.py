include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
                            "HiggsBSM:gg2A3 = on",
                            "36:m0 = 50.000",
                            "36:mWidth = 0.258362",
                            "36:onMode = off",
                            "36:onIfAny = 15",
                            "15:onMode = off",
                            "15:onIfAny = 11 13",
                            "-15:onMode = off",
                            "-15:onIfAny = 11 13"]

evgenConfig.process = "gg->A(60), A(60)->tautau"
evgenConfig.description = "Pythia 8 A->tau(lep)tau(lep) production with NNPDF23LO tune"
evgenConfig.keywords = ["BSM", "Higgs", "2tau"]
