# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.tune_commands()

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# only consider H->bb and Z->l+l- decays
Herwig7Config.add_commands("""
# force H->bb decays
do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar;
# print out decays modes and branching ratios
do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/Z0:SelectDecayModes Z0->e-,e+; Z0->mu-,mu+; Z0->tau-,tau+;
do /Herwig/Particles/Z0:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Herwig7 H+Z-jet->llbbar production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs" ]
evgenConfig.process     = "gg->ZH, H->bb, Z->l+l-"
evgenConfig.contact     = [ 'carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch', 'spyridon.argyropoulos@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Herwig7' ]
evgenConfig.tune        = "H7-MMHT2014LO"
evgenConfig.minevents   = 5000
evgenConfig.inputfilecheck = "TXT"
