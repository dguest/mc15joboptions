# EVGEN configuration
# Note:  This JO are designed to run Powheg and make an LHE file and to not run a showering
# generator afterwards. Because the current Generate_tf.py requires an output file, we
# need to fake it a bit. We therefore will run Pythia8 on the first event in the LHE file.
# Note, sence we do not intend to keep the EVNT file, the JO name doesn't include Pythia8

#--------------------------------------------------------------
# Powheg ttH setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ttH_Common.py')
PowhegConfig.topdecaymode = 22222 #ttbar we use 22222 which turns on all decay modes
PowhegConfig.hdamp        = 352.5

PowhegConfig.runningscales = 1 ## dynamic scale
PowhegConfig.storeinfo_rwgt = 1
PowhegConfig.PDF = range(260000, 260101) + range(90400, 90433) + [11068] + [25200] + [13165] + [25300] # PDF variations
PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]
PowhegConfig.nEvents *= 6000. #ensure to have enough events per LHE file to compensate for efficiency filter
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]
include("MC15JobOptions/Pythia8_SMHiggs125_inc.py")
include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = 'Powheg ttH allhad production NNPDF30ME'
evgenConfig.keywords       = [ 'SM', 'top', 'Higgs' ]
evgenConfig.contact        = [ 'antonio.salvucci@cern.ch' ]
evgenConfig.generators     = [ 'Powheg' ]
evgenConfig.minevents      = 1
evgenConfig.inputconfcheck = "ttH125_semilep_NNPDF30ME"
