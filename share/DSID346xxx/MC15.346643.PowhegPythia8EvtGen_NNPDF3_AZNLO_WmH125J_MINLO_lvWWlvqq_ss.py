#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 W- +H+jet->W-W-W+ -> lvlvqq production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs->WW at Pythia8, with W- leptonic decay, W+ hadronic decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 24 24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onPosIfAny=1 2 3 4 5',
                             '24:onNegIfAny = 11 12 13 14 15 16']
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->  W-W-W+ ->lvlvqq + jet production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs" ]
evgenConfig.contact     = [ 'ada.farilla@cern.ch' ]
evgenConfig.minevents   = 2000
evgenConfig.inputFilesPerJob = 40
evgenConfig.process = "WmH, Wm->lv, H->WmWp->lvqq"
