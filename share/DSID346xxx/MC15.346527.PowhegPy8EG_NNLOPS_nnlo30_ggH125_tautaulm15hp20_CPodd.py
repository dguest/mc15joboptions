
evgenConfig.process     = "gg->H -> tautau -> l-h+ CP odd"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN H+jet production with NNLOPS"
evgenConfig.keywords    = [ "Higgs", "1jet", "2tau", "mH125" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch' ]
evgenConfig.minevents   = 1000
#evgenConfig.inputfilecheck = "TXT"
evgenConfig.inputFilesPerJob = 3

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
else:
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'HiggsH1:parity = 2',
			     '25:onMode = off',
                             '25:onIfMatch = 15 15',
                             '15:onMode = off', # decay of taus
                             '15:onPosIfAny = 11 13', # particle
                             '15:onNegIfAny = 111 130 211 221 223 310 311 321 323' ] # antiparticle

# Set tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  taufilter = TauFilter("taufilter")
  filtSeq += taufilter

filtSeq.taufilter.UseNewOptions = True
filtSeq.taufilter.Ntaus = 2
filtSeq.taufilter.Nleptaus = 1
filtSeq.taufilter.Nhadtaus = 1
filtSeq.taufilter.EtaMaxlep = 2.6
filtSeq.taufilter.EtaMaxhad = 2.6
filtSeq.taufilter.Ptcutlep = 15000.0 #MeV
filtSeq.taufilter.Ptcutlep_lead = 15000.0 #MeV
filtSeq.taufilter.Ptcuthad = 20000.0 #MeV
filtSeq.taufilter.Ptcuthad_lead = 20000.0 #MeV
filtSeq.taufilter.filterEventNumber = 1 # keep odd EventNumber events
