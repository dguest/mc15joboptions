evgenConfig.process          = "ttH allhad H->inv"
evgenConfig.description      = 'POWHEG+Pythia8 ttH (allhad) production with A14 NNPDF2.3 tune'
evgenConfig.keywords         = [ "SM", "Higgs", "SMHiggs", "ZZ", "mH125" ]
evgenConfig.contact          = [ 'vdao@cern.ch' ]
evgenConfig.minevents        = 2000
####evgenConfig.inputFilesPerJob = 2
evgenConfig.generators       = [ 'Powheg', 'Pythia8', 'EvtGen' ]

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off', #decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 12 12',
                             '23:onIfMatch = 14 14', 
                             '23:onIfMatch = 16 16' ]


#--------------------------------------------------------------
# Missing Et filter 
#--------------------------------------------------------------
include('MC15JobOptions/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 75*GeV
filtSeq.Expression = "MissingEtFilter" 

