#--------------------------------------------------------------
# Herwig showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# only consider H->ZZ devays
Herwig7Config.add_commands("""
# force H->ZZ decays
do /Herwig/Particles/h0:SelectDecayModes h0->Z0,Z0;
# print out Higgs decays modes and branching ratios to the terminal/log.generate
do /Herwig/Particles/h0:PrintDecayModes
# force Z->ee/mumu decays
do /Herwig/Particles/Z0:SelectDecayModes Z0->e-,e+; Z0->mu-,mu+;
# print out Z decays modes and branching ratios to the terminal/log.generate
do /Herwig/Particles/Z0:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description     = 'Powheg+Herwig7 VBF H->ZZ->llll mh=125 GeV CPS'
evgenConfig.keywords        = [ "Higgs", "SMHiggs", "ZZ" ]
evgenConfig.contact         = [ 'antonio.salvucci@cern.ch', 'syed.haider.abidi@cern.ch' ]
evgenConfig.generators     += [ 'Powheg', 'Herwig7' ]
evgenConfig.minevents      = 10000
evgenConfig.inputFilesPerJob = 2
evgenConfig.tune 			= "H7UE"