include('MC15JobOptions/MadGraphControl_tHjb_NLO.py')

genSeq.Pythia8.Commands += [
    '25:onMode = off', # switch OFF all Higgs decay channels
    '25:oneChannel = 1 0.0632   100 15 -15',
    '25:addChannel = 1 0.0264   100 23 23',
    '25:addChannel = 1 0.2150   100 24 -24',  
]

evgenConfig.inputconfcheck = "tHjb"
