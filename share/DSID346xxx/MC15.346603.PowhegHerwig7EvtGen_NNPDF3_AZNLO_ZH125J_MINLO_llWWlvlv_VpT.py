#--------------------------------------------------------------
# POWHEG+MiNLO+Herwig7 Z+H+jet->llWW->lllvlv production
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.tune_commands()

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

## only consider H->WW->lvlv decays
Herwig7Config.add_commands("""
# force H->WW decays
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;
# print out decays modes and branching ratios
do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/W+:SelectDecayModes W+->nu_e,e+; W+->nu_mu,mu+; W+->nu_tau,tau+;
do /Herwig/Particles/W+:PrintDecayModes
do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;
do /Herwig/Particles/W-:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = "POWHEG+MiNLO+Pythia8 H+Z+jet->lllvlv production"
evgenConfig.keywords       = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact        = [ 'ada.farilla@roma3.infn.it' ]
evgenConfig.minevents      = 2000
evgenConfig.inputFilesPerJob = 40
evgenConfig.generators    += [ 'Powheg', 'Herwig7' ]
evgenConfig.tune        = "H7-MMHT2014LO"
evgenConfig.process        = "qq->ZH, H->WW, Z->ll"
