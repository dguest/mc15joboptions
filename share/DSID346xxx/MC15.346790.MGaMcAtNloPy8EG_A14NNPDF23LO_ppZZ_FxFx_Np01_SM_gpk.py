from MadGraphControl.MadGraphUtils import *

# General settings
nevents=10000*2
gridpack_dir=None
gridpack_mode=False
runName='run_03'

qCut=30

name='qqzz'
process="qq>zz"

gridpack_mode=True
gridpack_dir='madevent/'



stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)


fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model loop_sm-no_b_mass
define p = g u c b d s u~ c~ d~ s~ b~
define j = g u c b d s u~ c~ d~ s~ b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
generate p p > l+ l- l+ l- [QCD] @0
add process p p >  l+ l- l+ l- j  [QCD] @1
output pp_4l_NLOQCD_FxFx_2""")
fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


process_dir = new_process(grid_pack=gridpack_dir)

#Fetch default LO run_card.dat and set parameters
extras = {'lhe_version'   : '2.0',
          'ickkw'         : 3,
          'ptj'           : 10,
          'jetradius'     : 1.0,
          'maxjetflavor'  : 5,
          'pdlabel'       : "'lhapdf'",
          'lhaid'         : 90400,
          'parton_shower' :'PYTHIA8', 
          'reweight_PDF'  : 'True',
          'reweight_scale': 'True',
          'mll'           : 4.,
          'mll_sf'        : 4.,
          'ptl'           : 1.,
          'PDF_set_min'   : 90401,
          'PDF_set_max'   : 90432}

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', 
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
               extras=extras)

print_cards()


    
print_cards()
    
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=0,njobs=1,proc_dir=process_dir,run_name=runName,madspin_card_loc=None,
         grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=runArgs.randomSeed)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)




print "Now performing parton showering ..."
   
######################################################################
# End of event generation, start configuring parton shower here.
######################################################################

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

evgenConfig.description = 'aMcAtNlo_pp_4l_fxfx'
evgenConfig.keywords    = ['ZZ', 'jets']
evgenConfig.contact     = ['Roberto Di Nardo <roberto.di.nardo@cern.ch>']
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.description = """pp-> 4l 0j+1j NLO"""
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")


PYTHIA8_qCut=qCut
PYTHIA8_nJetMax=1
PYTHIA8_nJetMax=1

include("MC15JobOptions/Pythia8_FxFx.py")
genSeq.Pythia8.Commands += ['JetMatching:qCutME        = 10.0']
testSeq.TestHepMC.MaxVtxDisp = 100000000
evgenConfig.minevents  = 10000
