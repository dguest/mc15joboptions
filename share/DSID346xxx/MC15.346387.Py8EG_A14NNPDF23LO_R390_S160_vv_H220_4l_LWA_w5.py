include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.contact = ['Stefan von Buddenbrock <stef.von.b@cern.com>']
evgenConfig.description = "Event generation of gg > R > SH (H is a LW heavy scalar with width=5% of mass, R is even heavier), H -> ZZ(4l), S -> invisible"
evgenConfig.keywords = ["BSMHiggs", "4lepton", "invisible"]

genSeq.Pythia8.Commands += [
    'Higgs:useBSM = on',
    'HiggsBSM:gg2A3 = on',
    '36:m0 = 390.0',
    '36:mWidth = 0.01',
    '36:doForceWidth = yes',
    '36:addChannel = 1 1 103 35 25',
    '36:onMode = off',
    '36:onIfMatch = 25 35',
    '25:m0 = 220.0',
    '25:doForceWidth = yes',
    '25:mWidth = 11.0',
    '25:onMode = off',
    '25:onIfMatch = 23 23',
    '23:onMode = off',
    '23:onIfAny = 11 13',
    '35:m0 = 160.0',
    '35:mWidth = 0.01',
    '35:doForceWidth = on',
    '35:addChannel = 1 1 103 12 -12',
    '35:onMode = off',
    '35:onIfMatch = 12 -12'
]
