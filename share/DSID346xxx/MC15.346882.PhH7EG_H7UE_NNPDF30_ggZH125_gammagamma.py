#--------------------------------------------------------------
# Herwig showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# only consider H->gammagamma decays
Herwig7Config.add_commands("""
  # force H->yy decays                                                                                                      
  do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma;                                                                 
  # print out decays modes and branching ratios to the terminal/log.generate                                                
  do /Herwig/Particles/h0:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description     = 'Powheg+Herwig7 gg->H+Z Z->all H->gammagamma mh=125 GeV'
evgenConfig.keywords        = [ "Higgs", "SMHiggs", "diphoton" ]
evgenConfig.contact         = [ 'ana.cueto@cern.ch' ]
evgenConfig.generators     += [ 'Powheg', 'Herwig7' ]
evgenConfig.minevents      = 10000
evgenConfig.inputFilesPerJob = 5
evgenConfig.tune = "H7UE"
