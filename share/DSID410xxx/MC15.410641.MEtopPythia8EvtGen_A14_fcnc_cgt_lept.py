#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'MEtop+Pythia8 FCNC single top production cg->t with A14 tune'
evgenConfig.keywords    = [ 'FCNC', 'top', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch' ]
evgenConfig.generators += ['MEtop']

evgenConfig.inputfilecheck = "cgt"

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_LHEF.py')
