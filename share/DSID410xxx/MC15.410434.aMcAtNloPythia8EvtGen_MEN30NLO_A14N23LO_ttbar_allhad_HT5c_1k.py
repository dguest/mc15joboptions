#--------------------------------------------------------------                                                                                                                                                          
# EVGEN configuration                                                                                                                                                                                                    
#--------------------------------------------------------------                                                                                                                                                          
  
evgenConfig.description = 'MG5_aMC@NLO+Pythia8 ttbar production A14 tune NNPDF23LO EvtGen with single lepton filter from DSID 410440 LHE files with Shower Weights added, filtered on HT'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'aknue@cern.ch', 'khoo@cern.ch' ]
evgenConfig.generators += ["aMcAtNlo","Pythia8"]
evgenConfig.minevents   = 500

#if runArgs.trfSubstepName == 'generate' :
#  evgenConfig.inputfilecheck = "aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttbar_incl_LHE"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

# Configure the HT filter
include('MC15JobOptions/HTFilter.py')
filtSeq.HTFilter.MinJetPt = 35.*GeV # Min pT to consider jet in HT
filtSeq.HTFilter.MaxJetEta = 2.5 # Max eta to consider jet in HT
filtSeq.HTFilter.MinHT = 500.*GeV # Min HT to keep event
filtSeq.HTFilter.MaxHT = 1000.*GeV # Max HT to keep event
# EvtGen changes the B decays, which can cause inconsistencies
# between generator-level and DAOD-level filtering
filtSeq.HTFilter.UseNeutrinosFromWZTau = True # Include nu from the MC event in the HT
# These should be irrelevant for the sample, but kept for consistency
filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
filtSeq.HTFilter.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
filtSeq.HTFilter.MaxLeptonEta = 2.5 # Max eta to consider muon in HT

filtSeq.Expression = "((not TTbarWToLeptonFilter) and HTFilter)"
