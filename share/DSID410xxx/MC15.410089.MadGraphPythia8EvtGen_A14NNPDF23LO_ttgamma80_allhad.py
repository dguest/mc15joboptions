#--------------------------------------------------------------                                                                                                       # Configuration for EvgenJobTransforms                                                                                                                                #--------------------------------------------------------------                                                                                                        
evgenConfig.generators  += ["MadGraph", "Pythia8"]
evgenConfig.description  = 'MG5_aMC@NLO+Pythia8 ttbar + gamma allhadronic, A14 tune, with EvtGen'
evgenConfig.keywords    += [ 'SM', 'top', 'allHadronic']
evgenConfig.contact      = [ 'andrey.loginov@yale.edu', 'bnachman@cern.ch' ]
evgenConfig.minevents = 200

#--------------------------------------------------------------
# Showering with Pythia 8, A14 tune
#--------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
filtSeq += PhotonFilter("PhotonFilter") 

PhotonFilter = filtSeq.PhotonFilter
PhotonFilter.PtMin = 80000.
PhotonFilter.PtMax = 8000000.
PhotonFilter.EtaCut = 5
PhotonFilter.NPhotons = 1