#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 ttbar production with Powheg hdamp equal 1.5*top mass, H7UE tune, at least one lepton filter with b-filter, ME NNPDF30 NLO, H7UE MMHT2014 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.de','daniel.rauch@cern.ch' ]
evgenConfig.generators += ["Powheg"]

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.topdecaymode = 22222
PowhegConfig.hdamp   = 258.75
PowhegConfig.PDF     = 260000
PowhegConfig.mu_F    = 1.0
PowhegConfig.mu_R    = 1.0

PowhegConfig.nEvents     *= 35. # compensate filter efficiency
PowhegConfig.generate()
#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
include('MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3ME_LHEF_EvtGen_Common.py')
  
from Herwig7_i import config as hw
genSeq.Herwig7.Commands += hw.powhegbox_cmds().splitlines()

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

include('MC15JobOptions/TTbarPlusHFFilter.py')
filtSeq.TTbarPlusBFilter.SelectB        = True
