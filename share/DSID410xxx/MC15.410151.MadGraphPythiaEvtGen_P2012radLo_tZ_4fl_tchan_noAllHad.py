#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'MG+Pythia6  single top + Z production, scale=2.0, Perugia 2012 radLo tune'
evgenConfig.keywords    = [ 'SM','singleTop','tZ','lepton']
evgenConfig.contact     = ['alhroob@cern.ch','carlos.escobar.ibanez@cern.ch' ]
       
if runArgs.trfSubstepName == 'generate' :
  evgenConfig.inputfilecheck = "tZ_tchannel_LO_noAllHad_facsc2_rensc2_CTEQ6L1_13TeV"
       
#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/MadGraphPythia_Perugia2012radLo_Common.py')         
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')
               
#  Run EvtGen as afterburner
include ( "MC15JobOptions/Pythia_MadGraph_EvtGen.py" )
