include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")
evgenConfig.description="Sherpa ttZ, Z->nn,qq LO"
evgenConfig.keywords=["ttZ", "SM", "neutrino", "quark"]
evgenConfig.contact=["ponyisi@cern.ch"]
evgenConfig.inputconfcheck="Sherpa_NNPDF30NNLO_ttZ" # inputs are identical for all decay modes

evgenConfig.process="""
(run){
  HARD_DECAYS=1;
  STABLE[6] = 0; WIDTH[6]=0.0;
  STABLE[23] = 0; WIDTH[23]=0.0;
  STABLE[24] = 0;
  ACTIVE[25] = 0;

  # disable Z->ll in decay afterburner
  HDH_STATUS[23,11,-11]=0
  HDH_STATUS[23,13,-13]=0
  HDH_STATUS[23,15,-15]=0
}(run)

(processes){
  Process 93 93 -> 6 -6 23 93{2};
    Order (*,1);
  CKKW sqr(30./E_CMS);
  End process;
}(processes)

"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.0", "WIDTH[23]=0.0"]
