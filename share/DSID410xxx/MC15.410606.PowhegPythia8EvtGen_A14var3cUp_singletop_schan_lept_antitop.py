#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8+EvtGen single-top s-channel production (anti-top), inclusive, with Powheg hdamp equal 1.5*top mass, A14 var3c up, scale 0.5, ME NNPDF30 NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'top', 'singleTop', 'sChannel' ]
evgenConfig.contact     = [ 'timothee.theveneaux-pelzer@cern.ch', 'ian.connelly@cern.ch' ]
evgenConfig.generators += [ 'Powheg' ]
 

#--------------------------------------------------------------
# Powheg single-top s-channel setup - V1
#--------------------------------------------------------------

include('PowhegControl/PowhegControl_t_sch_Common.py')
PowhegConfig.topdecaymode = 11100  # leptonic W-from-top decays
PowhegConfig.ttype        = -1     # anti-top
PowhegConfig.hdamp        = 258.75 # 1.5 * mtop
PowhegConfig.mu_F         = 0.5    # Factorisation scale
PowhegConfig.mu_R         = 0.5    # Renormalisation scale
PowhegConfig.PDF          = 260000 # NNPDF30
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py')

include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]

#-------------------------------------------------------------
# Filters
#-------------------------------------------------------------
