evgenConfig.generators += ["MadGraph","Pythia8"]
evgenConfig.description = 'MadGraph_ttgamma_nonallhadronic'
evgenConfig.contact = ["yichen.li@cern.ch","amartya.rej@cern.ch"]
evgenConfig.keywords+=['SM','top', 'ttV', 'photon', 'lepton']

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_Var1Up_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

