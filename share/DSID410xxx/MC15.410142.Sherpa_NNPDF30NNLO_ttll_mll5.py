include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")
evgenConfig.description="Sherpa tt+ll LO"
evgenConfig.keywords=["ttZ", "SM", "multilepton"]
evgenConfig.contact=["ponyisi@cern.ch"]
evgenConfig.inputconfcheck="Sherpa_NNPDF30NNLO_ttll_mll5"

evgenConfig.process="""
(run){
  HARD_DECAYS=1;
  STABLE[6] = 0; WIDTH[6]=0.0;
  STABLE[23] = 0; STABLE[24] = 0;
  ACTIVE[25] = 0;
  MASSIVE[15] 1; MASSIVE[13] 1; MASSIVE[11] 1; 
}(run)

(processes){
  Process 93 93 -> 6 -6 11 -11 93{1};
    Order (*,2);
  CKKW sqr(30./E_CMS);
  End process;
  Process 93 93 -> 6 -6 13 -13 93{1};
    Order (*,2);
  CKKW sqr(30./E_CMS);
  End process;
  Process 93 93 -> 6 -6 15 -15 93{1};
    Order (*,2);
  CKKW sqr(30./E_CMS);
  End process;
}(processes)

(selector){
  Mass 11 -11 5 E_CMS;
  Mass 13 -13 5 E_CMS;
  Mass 15 -15 5 E_CMS;
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.0"]
