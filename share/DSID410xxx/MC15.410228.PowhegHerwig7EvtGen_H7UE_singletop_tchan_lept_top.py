#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 t-channel single top production (2->3) (top) with H7UE tune'
evgenConfig.keywords    = [ 'SM', 'top', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch' ]
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]

if runArgs.trfSubstepName == 'generate' :

  evgenConfig.inputfilecheck = "singletop_tchan2to3nlo_top_lept"

#--------------------------------------------------------------
# Herwig7 (H7-UE-MMHT) showering
#--------------------------------------------------------------
  include('MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_CT10f4ME_LHEF_EvtGen_Common.py')
