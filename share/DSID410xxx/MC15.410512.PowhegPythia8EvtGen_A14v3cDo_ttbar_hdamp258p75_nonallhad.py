#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune var 3c down, scale=2.0, at least one lepton filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch' ]

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.topdecaymode = 22222
PowhegConfig.hdamp = 258.75
PowhegConfig.PDF = 260000
PowhegConfig.mu_F = 2.0000
PowhegConfig.mu_R = 2.0000

#Information on how to run with multiple weights: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PowhegForATLAS#Running_with_multiple_scale_PDF
#PDFs - you can see a listing here: https://lhapdf.hepforge.org/pdfsets.html; picked these three as they are the inputs to the PDF4LHC2015 prescription (http://arxiv.org/pdf/1510.03865v2.pdf).

# Weight group configuration
PowhegConfig.define_event_weight_group( group_name='scales', parameters_to_vary=['mu_F','mu_R','PDF'] )

# Scale variations, NNPDF
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_NNPDF',              parameter_values=[ 2.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_NNPDF',            parameter_values=[ 0.5, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muR_NNPDF',              parameter_values=[ 1.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muR_NNPDF',            parameter_values=[ 1.0, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_0p5muR_NNPDF',     parameter_values=[ 0.5, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_2muR_NNPDF',         parameter_values=[ 2.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_2muR_NNPDF',       parameter_values=[ 0.5, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_0p5muR_NNPDF',       parameter_values=[ 2.0, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='1muF_1muR_NNPDF',         parameter_values=[ 1.0, 1.0, 260000] )

# Scale variations, MMHT2014nlo68clas118
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_MMHT',              parameter_values=[ 2.0, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_MMHT',            parameter_values=[ 0.5, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muR_MMHT',              parameter_values=[ 1.0, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muR_MMHT',            parameter_values=[ 1.0, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_0p5muR_MMHT',     parameter_values=[ 0.5, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_2muR_MMHT',         parameter_values=[ 2.0, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_2muR_MMHT',       parameter_values=[ 0.5, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_0p5muR_MMHT',       parameter_values=[ 2.0, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='1muF_1muR_MMHT',         parameter_values=[ 1.0, 1.0, 25200] )

# Scale variations, CT14nlo_as_0118
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_CT14',              parameter_values=[ 2.0, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_CT14',            parameter_values=[ 0.5, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muR_CT14',              parameter_values=[ 1.0, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muR_CT14',            parameter_values=[ 1.0, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_0p5muR_CT14',     parameter_values=[ 0.5, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_2muR_CT14',         parameter_values=[ 2.0, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_2muR_CT14',       parameter_values=[ 0.5, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_0p5muR_CT14',       parameter_values=[ 2.0, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='1muF_1muR_CT14',         parameter_values=[ 1.0, 1.0, 13165] )

# Scale variations, PDF4LHC15_nlo_30
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_PDF4LHC15',              parameter_values=[ 2.0, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_PDF4LHC15',            parameter_values=[ 0.5, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muR_PDF4LHC15',              parameter_values=[ 1.0, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muR_PDF4LHC15',            parameter_values=[ 1.0, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_0p5muR_PDF4LHC15',     parameter_values=[ 0.5, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_2muR_PDF4LHC15',         parameter_values=[ 2.0, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_2muR_PDF4LHC15',       parameter_values=[ 0.5, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_0p5muR_PDF4LHC15',       parameter_values=[ 2.0, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='1muF_1muR_PDF4LHC15',         parameter_values=[ 1.0, 1.0, 90900] )


PowhegConfig.nEvents     *= 3. # compensate filter efficiency
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_Var3cDown_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
