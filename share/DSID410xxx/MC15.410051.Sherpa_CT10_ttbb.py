include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "ttbb production with NLO+PS in the 4FS, cf. arXiv:1309.5912."
evgenConfig.keywords = ["top", "SM" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "Sherpa_CT10_ttbb"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES VAR{FSF*sqr((MPerp(p[2])+MPerp(p[3]))/2)}{RSF*sqrt(MPerp(p[2])*MPerp(p[3])*MPerp(p[4])*MPerp(p[5]))}{QSF*sqr((MPerp(p[2])+MPerp(p[3]))/2)}

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  % settings relevant for 4-flavour scheme
  MASSIVE[5]=1
  OL_PARAMETERS nq_nondecoupled 4  # for consistency with the 4F scheme 
  PDF_LIBRARY LHAPDFSherpa
  LHAPDF_GRID_PATH=./PDFsets
  PDF_SET CT10nlo_nf4       # standard CT10 4F NLO pdfs
  USE_PDF_ALPHAS=1
  CSS_ENHANCE S{G}{t}{tb} 0        # switch off g->tt shower splittings (for consistency with MEPS@LO simulation)
  CSS_ENHANCE S{G}{tb}{t} 0 
  MCATNLO_MASSIVE_SPLITTINGS=0     # deactivates g->bb first emission in MC@NLO
  EXCLUSIVE_CLUSTER_MODE=1         

  % decays
  HARD_DECAYS=1
  STABLE[6]=0
  WIDTH[6]=0
  STABLE[24]=0
  HDH_ONLY_DECAY={24,12,-11}|{-24,-14,13}
}(run)

(processes){
  Process 93 93 -> 6 -6 5 -5 ;   # pp -> t t b b 
  Order_EW 0		    	 
  NLO_QCD_Mode MC@NLO            
  Loop_Generator LOOPGEN
  ME_Generator Amegic ;
  RS_ME_Generator Comix ;
  End process;
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0" ]
