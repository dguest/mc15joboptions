evgenConfig.description = "Pythia8 W--> e,nu + 0,1,2j@NLO + 3,4j@LO with 70 GeV < max(HT, pTV) < 140 GeV, MJJ > 1000, DPHI < 2.0 "
evgenConfig.keywords = ["SM", "W", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch","mdshapiro@lbl.gov","bcarlson@cern.ch" ]
evgenConfig.minevents = 20

if runArgs.trfSubstepName == 'generate' :
   print "ERROR: These JO require an input file.  Please use the --afterburn option"
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Pythia8"]
 
## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
## Disable TestHepMC for the time being, cf.  
## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())
   
   #include("MC15JobOptions/AntiKt4TruthJets.py")
   include("MC15JobOptions/VBFMjjIntervalFilter.py")

   filtSeq.VBFMjjIntervalFilter.RapidityAcceptance = 5.0 
   filtSeq.VBFMjjIntervalFilter.MinSecondJetPT = 20.*GeV
   filtSeq.VBFMjjIntervalFilter.MinOverlapPT = 20.*GeV
   filtSeq.VBFMjjIntervalFilter.TruthJetContainerName = "AntiKt4TruthJets"
   filtSeq.VBFMjjIntervalFilter.LowMjj = 1000.*GeV
   filtSeq.VBFMjjIntervalFilter.HighMjj = 13000.*GeV
   filtSeq.VBFMjjIntervalFilter.dphijjMax = 2*rad
   
   
   filtSeq.VBFMjjIntervalFilter.TruncateAtLowMjj = True
   filtSeq.VBFMjjIntervalFilter.TruncateAtHighMjj = False
   filtSeq.VBFMjjIntervalFilter.PhotonJetOverlapRemoval = False
   filtSeq.VBFMjjIntervalFilter.ElectronJetOverlapRemoval = False
   filtSeq.VBFMjjIntervalFilter.TauJetOverlapRemoval = False
   filtSeq.VBFMjjIntervalFilter.ApplyWeighting = False;   
   filtSeq.VBFMjjIntervalFilter.ApplyDphi = True; 
   #filtSeq += VBFMjjIntervalFilter

 
