include("MC15JobOptions/MadGraphControl_monoHiggs_zp2hdm.py")

evgenConfig.description = "Simplified Model of scalar mediator for MonoHiggs(h->tautau) with mZ=900GeV and mA0=600GeV"
evgenConfig.keywords = ["BSM", "BSMHiggs", "Higgs", "tau", "Zprime"]
evgenConfig.contact = ['Julia Djuvsland <julia.djuvsland@cern.ch>']
genSeq.Pythia8.Commands += [
    '25:oneChannel = on 1.0 100 15 -15 '
]
