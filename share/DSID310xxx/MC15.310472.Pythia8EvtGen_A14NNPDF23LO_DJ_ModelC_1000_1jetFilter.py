###########################################################
# Dark Jets Event Generation
# Pythia 8: Zd --> Qd Qd --> 2j
# contact: Roland Jansky (roland.jansky@cern.ch)
#==========================================================

evgenConfig.description = "dark jet event from pair-produced dark quarks"
evgenConfig.keywords = ["exotic", "hiddenValley", "2jet"]
evgenConfig.process = "p p --> Zd --> Qd Qd --> 2DarkQcdJ"
evgenConfig.contact = ["roland.jansky@cern.ch"]

# specify PDF + tune
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

from Pythia8_i.Pythia8_iConf import CheckForFinalPartons

checkForFinalPartons = CheckForFinalPartons()
ToolSvc += checkForFinalPartons
genSeq.Pythia8.CustomInterface = checkForFinalPartons

# set sample / model parameters automatically based on jo name
m_Zd = float(runArgs.jobConfig[0].split('_')[-2])
print "VECTOR MEDIATOR MASS: %f " % m_Zd

mod = runArgs.jobConfig[0].split('_')[-3]
print "MODEL: %s " % mod
if mod == "ModelA":
    Lambda_d = 15.0
    m_q_d = 20.0
    m_pi_d = 10.0
    m_rho_d = 50.0
    nFlav = 2
    probVector = 0.173
elif mod == "ModelB":
    Lambda_d = 2.0
    m_q_d = 2.0
    m_pi_d = 2.0
    m_rho_d = 4.67
    nFlav = 6
    probVector = 0.441
elif mod == "ModelC":
    Lambda_d = 15.0
    m_q_d = 20.0
    m_pi_d = 10.0
    m_rho_d = 50.0
    nFlav = 2
    probVector = 0.173
elif mod == "ModelD":
    Lambda_d = 2.0
    m_q_d = 2.0
    m_pi_d = 2.0
    m_rho_d = 4.67
    nFlav = 6
    probVector = 0.441

print "DARK PION MASS: %f " % m_pi_d
print "DARK RHO MASS: %f " % m_rho_d
print "DARK QUARK MASS: %f " % m_q_d
print "LAMBDA: %f " % Lambda_d
print "PT MIN FSR: %f " % (Lambda_d*1.1)
print "N_DARK FLAV: %f " % nFlav

# show 5 events for testing
genSeq.Pythia8.Commands += ["Next:numberShowEvent = 5"]

# settings for dark sector 
genSeq.Pythia8.Commands += ["4900023:m0 = " + str(m_Zd), # Zd mass - variable
                            "4900023:mWidth = 0.1", # Zd width
                            "HiddenValley:spinFV = 0",
                            "4900023:rescaleBR = 0.0001",
                            "4900023:12:bratio = 0.9999",
                            "HiddenValley:Ngauge = 3" # n dark QCD colors
                            ] 
# Model settings
genSeq.Pythia8.Commands += ["4900101:m0 = " + str(m_q_d), # qd mass
                            "4900111:m0 = " + str(m_pi_d), # pi_d mass
                            "4900113:m0 = " + str(m_rho_d), # rho_d mass
                            "4900211:m0 = " + str(m_pi_d), # pi_d off-diag mass
                            "4900213:m0 = " + str(m_rho_d) # rho_d off-diag mass
                            ] 

# dark meson decays
genSeq.Pythia8.Commands += ["4900111:addChannel = 1 1.0 102 4900022 4900022",
                            "4900113:addChannel = 1 1.0 102 4900111 4900111",
                            "4900211:addChannel = 1 1.0 102 4900022 4900022",
                            "4900213:addChannel = 1 1.0 102 4900111 4900111"] 
                            
# dark photon decays
genSeq.Pythia8.Commands += ["4900022:m0 = 4.0", 
                            "4900022:0:onMode = 1", 
                            "4900022:0:bRatio = 0.22", 
                            "4900022:0:meMode = 102", 
                            "4900022:1:onMode = 1", 
                            "4900022:1:bRatio = 0.06", 
                            "4900022:1:meMode = 102", 
                            "4900022:2:onMode = 1", 
                            "4900022:2:bRatio = 0.06", 
                            "4900022:2:meMode = 102", 
                            "4900022:3:onMode = 1", 
                            "4900022:3:bRatio = 0.22", 
                            "4900022:3:meMode = 102", 
                            "4900022:5:onMode = 1", 
                            "4900022:5:bRatio = 0.17", 
                            "4900022:5:meMode = 102", 
                            "4900022:7:onMode = 1", 
                            "4900022:7:bRatio = 0.17", 
                            "4900022:7:meMode = 102", 
                            "4900022:9:onMode = 1", 
                            "4900022:9:bRatio = 0.10", 
                            "4900022:9:meMode = 102"] 

# dark jet event processes
genSeq.Pythia8.Commands += ["HiddenValley:ffbar2Zv = on"]

# Hadronization
genSeq.Pythia8.Commands += ["HiddenValley:probVector = " + str(probVector),
                            "HiddenValley:nFlav = " + str(nFlav),
                            "HiddenValley:fragment = on"]

# Shower
genSeq.Pythia8.Commands += ["HiddenValley:FSR = on",
                            "HiddenValley:alphaOrder = 1",
                            "HiddenValley:Lambda = " + str(Lambda_d),
                            "HiddenValley:pTminFSR = " + str(Lambda_d*1.1)]
                                  
## JET FILTERING ##
include("MC15JobOptions/AntiKt4TruthJets.py")

if not hasattr( filtSeq, "TruthJetFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
    filtSeq += TruthJetFilter()
    pass

filtSeq.TruthJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.TruthJetFilter.Njet = 1
filtSeq.TruthJetFilter.NjetMinPt = 300*GeV
filtSeq.TruthJetFilter.NjetMaxEta = 2.7
