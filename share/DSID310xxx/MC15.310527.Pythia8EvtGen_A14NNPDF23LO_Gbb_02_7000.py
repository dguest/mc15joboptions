######################################################################
# Graviton to b b decay with Pythia 8
######################################################################

evgenConfig.description = "RS Graviton, m = 7000. GeV, kappaMG = 1.084"
evgenConfig.keywords = ["exotic", "BSM", "graviton", "RandallSundrum"]
evgenConfig.contact = [" Nishu.Nishu@cern.ch "]
evgenConfig.process = "RS Graviton -> b b"

from EvgenProdTools.EvgenProdToolsConf import TestHepMC
testHepMC = TestHepMC()
testHepMC.MaxVtxDisp = 1500.0

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands +=[
"ExtraDimensionsG*:gg2G* = on",
"ExtraDimensionsG*:ffbar2G* = on",
"5100039:m0 = 7000.",
"5100039:onMode = off",
"5100039:onIfAny = 5",
"ExtraDimensionsG*:kappaMG = 1.084",
"5100039:mMin = 50.",
"PhaseSpace:mHatMin = 200.",
]

genSeq.Pythia8.UserModes += ["GravFlat:EnergyMode = 13"]
