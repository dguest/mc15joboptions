model       = 'dmA'
mR          = 250
mDM         = 10000
gSM         = 0.25
gDM         = 1.00
widthR      = 6.214204
xptj        = 100
filteff     = 0.03697
jetminpt    = 350
quark_decays= ['b']

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetjet_flavfilt.py")

evgenConfig.description = "Zprime sample - mR250 - model dmA"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Karol Krizka <kkrizka@cern.ch>"]
evgenConfig.minevents = 1000

