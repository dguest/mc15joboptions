## safe factor applied to nevents
safefactor=1.8

THDMparams = {}
THDMparams['gPXd'] = 1.0 # The coupling of the additional pseudoscalar mediator to dark matter (DM). This coupling is called $y_\chi$ in (2.5) of arXiv:1701.07427.                            
THDMparams['tanbeta'] =  1.0 # The ratio of the vacuum expectation values $	an eta = v_2/v_1$ of the Higgs doublets $H_2$ and $H_1$, as defined in Section 2.1 of arXiv:1701.07427.         
THDMparams['sinbma'] = 1.0 # The sine of the difference of the mixing angles $\sin (eta - lpha)$ in the scalar potential containing only the Higgs doublets.  This quantity is defined in Section 3.1 of arXiv:1701.07427.
THDMparams['lam3'] = 3.0 # The quartic coupling of the scalar doublets $H_1$ and $H_2$. This parameter corresponds to the coefficient $\lambda_3$ in (2.1) of arXiv:1701.07427.                
THDMparams['laP1'] = 3.0 # The quartic coupling between the scalar doublets $H_1$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P1}$ in (2.2) of arXiv:1701.07427.
THDMparams['laP2'] = 3.0 # The quartic coupling between the scalar doublets $H_2$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P2}$ in (2.2) of arXiv:1701.07427.
THDMparams['sinp'] = 0.35 # The sine of the mixing angle $	heta$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['MXd'] = 10. # The mass of the fermionic DM candidate denoted by $m_\chi$ in arXiv:1701.07427.
THDMparams['mh1'] = 125. # The mass of the lightest scalar mass eigenstate $h$, which is identified in arXiv:1701.07427 with the Higgs-like resonance found at the LHC. 
THDMparams['mh2'] = 1300. # The mass of the heavy scalar mass eigenstate $H$. See Section 2.1 of arXiv:1701.07427 for further details.                        
THDMparams['mh3'] = 1300. # The mass of the heavy pseudoscalar mass eigenstate $A$. See Section 2.1 of arXiv:1701.07427 for further details.  
THDMparams['mhc'] = 1300. # The mass of the charged scalar eigenstate $H^\pm$. See Section 2.1 of arXiv:1701.07427 for further details.                            
THDMparams['mh4'] = 150. # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin 	heta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.

include("MC15JobOptions/MadGraphControl_Pythia8EvtGen_A14NNPDF23LO_Pseudoscalar_2HDM_monoVhad.py")

## MET filter
include("MC15JobOptions/MissingEtFilter.py")
filtSeq.MissingEtFilter.METCut = 50*GeV

## B filter/veto
include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter

filtSeq.Expression = "(MissingEtFilter) and (not HeavyFlavorBHadronFilter)"
