include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", 
                            "23:onMode = off",                 
                            "23:onIfAny = 13", 
                            "PhaseSpace:mHatMin = 15000."]

# EVGEN configuration
evgenConfig.description = 'Pythia 8 Zprime decaying to two muons'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.generators += [ 'Pythia8' ]
