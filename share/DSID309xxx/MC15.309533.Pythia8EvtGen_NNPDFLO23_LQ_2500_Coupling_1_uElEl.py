
include("MC15JobOptions/nonStandard/Pythia8_A14_NNPDF23LO_Common.py")
include("MC15JobOptions/Pythia8_EvtGen.py")

genSeq.Pythia8.Commands += ["LeptoQuark:qg2LQl = on",
                            "LeptoQuark:kCoup = 1",
                            "42:0:products = 2 11",
                            "42:m0 = 2500"]

evgenConfig.description = 'Pythia8 Single LQ Production (LQ mass = 2500 GeV, Coupling=1, Channel=uElEl) '
evgenConfig.keywords    = [ 'BSM','exotic','leptoquark','scalar' ]
evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.contact = ["Holger Herr <hherr@cern.ch>"]
evgenConfig.process = 'pp -> LQ l'

