#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += [ 'CompHep', 'Pythia8' ]
evgenConfig.description = 'E6 IsoSinglet Quark Pair Production'
evgenConfig.keywords = ["exotic"]
evgenConfig.contact = ['aytul.adiguzel@cern.ch']
evgenConfig.inputfilecheck = 'HbbarZlljj1600GeV'

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_LHEF.py')

genSeq.Pythia8.Commands += [ '25:onMode = off','25:onIfAny = 5',         # decay of Higgs
                             '23:onMode = off','23:onIfAny = 11 13' ]    # decay of Z

