evgenConfig.description = 'Pythia 8 Zprime decaying to tau tau'
evgenConfig.contact = ["Lino Gerlach <lino.oscar.gerlach@cern.ch>"]
evgenConfig.keywords    = [ 'BSM', 'Zprime', 'heavyBoson', 'SSM', 'resonance', 'electroweak', '2tau' ]
evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.process = "pp>Zprime>tautau"
evgenConfig.minevents = 5000
evgenConfig.maxeventsfactor = 2.0

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on"]  # create Z' bosons
genSeq.Pythia8.Commands += ["Zprime:gmZmode = 3"]                  # Z',Z,g with interference
genSeq.Pythia8.Commands += ["32:onMode = off"]                     # switch off all Z decays
genSeq.Pythia8.Commands += ["32:onIfAny  = 15 15"]                    # switch on Z->tautau decays
genSeq.Pythia8.Commands += ["32:mWidth = 0.1"]
genSeq.Pythia8.Commands += ["32:m0 = 1000"]
genSeq.Pythia8.Commands += ["32:doForceWidth = on"]
