#--------------------------------------------------------------
# Herwig 7 showering setup
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering

include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop No")
Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC15_nlo_30_pdfas")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune = "H7.1-Default"

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

#HW7 settings and Higgs BR
Herwig7Config.add_commands ("""
#set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
#set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
set /Herwig/Shower/ShowerHandler:SpinCorrelations Yes 
do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma; h0->b,bbar;
do /Herwig/Particles/h0:PrintDecayModes
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio  0.5
""")

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators    += ["Powheg", "Herwig7"]
evgenConfig.description    = "SM diHiggs production, decay to bbgammagamma, with Powheg-Box-V2, at NLO + full top mass."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "bbbar","bottom"]
evgenConfig.contact        = ['Jannicke Pearkes <Jannicke.Pearkes@cern.ch>']
evgenConfig.minevents      = 10000
evgenConfig.maxeventsfactor = 1.0
evgenConfig.inputFilesPerJob = 4

# run Herwig7
Herwig7Config.run()

#---------------------------------------------------------------------------------------------------
# Filter for bbyy
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("HyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq.Expression = "HbbFilter and HyyFilter"
