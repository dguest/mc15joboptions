include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa tautaugamma + 0,1,2,3j@LO with 7<pT_y<15."
evgenConfig.keywords = ["SM", "2tau", "photon" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "tautaugamma_LO_pty_7_15"

genSeq.Sherpa_i.RunCard = """
(run){
  % tags for process setup
  NJET:=3; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix;

  % tau decay spin correlations
  SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
  Process 93 93 -> 22 15 -15 93{NJET}
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.99 {5,6}
  End process
}(processes)

(selector){
  PT 22  7  15
  IsolationCut  22  0.1  2  0.10
  DeltaR  22  90  0.1 1000.0
  Mass  15  -15  10.0  E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 24
