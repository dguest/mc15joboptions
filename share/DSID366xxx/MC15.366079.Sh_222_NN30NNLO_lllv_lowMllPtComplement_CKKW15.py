include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "3 charged leptons + neutrino with 0j@NLO+1,2j@LO and mll>2*ml+250MeV, (pTl1>20GeV OR MET>50GeV) AND (pTl2<5GeV OR ONE M(SFOS)<4GeV), CKKW down variation."
evgenConfig.keywords = ["SM", "diboson", "3lepton", "neutrino", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 50
evgenConfig.inputconfcheck = "lllv_lowMllPtComplement_CKKW15"

Sherpa_iRunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=2; LJET:=4; QCUT:=15.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0
  SOFT_SPIN_CORRELATIONS=1
  EXCLUSIVE_CLUSTER_MODE=1

  %integration settings
  PSI_ITMIN=50000

  %settings for MAXMLLPT cut
  SHERPA_LDADD=SherpaMAXMLLPT
}(run)

(processes){
  Process 93 93 -> 90 90 90 91 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {4,5};
  Integration_Error 0.99 {6,7,8};
  End process;
}(processes)

(selector){
  "PT" 90 2.0,E_CMS:2.0,E_CMS [PT_UP]
  Mass 11 -11 0.25 E_CMS
  Mass 13 -13 0.4614 E_CMS
  Mass 15 -15 3.804 E_CMS
  MAXMLLPT 4.0
}(selector)
"""

Sherpa_iOpenLoopsLibs = [ "ppllll", "ppllllj", "ppllll2", "ppllllj2" ]
Sherpa_iNCores = 96
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
Sherpa_iExtraFiles = [ "libSherpaMAXMLLPT.so" ]


