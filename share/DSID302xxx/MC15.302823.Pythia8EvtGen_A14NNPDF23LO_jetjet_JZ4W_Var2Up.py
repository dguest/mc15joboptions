# JO for Pythia 8 jet jet JZ4W slice

evgenConfig.description = "Dijet truth jet slice JZ4W, with the A14 NNPDF23 LO Var2Up tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

evgenConfig.contact = ["marco.vanadia@cern.ch"]
evgenConfig.process = "QCD dijet"
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_Var2Up_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 150."]

include("MC15JobOptions/JetFilter_JZ4W.py")
evgenConfig.minevents = 500
