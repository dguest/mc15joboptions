evgenConfig.description = "Protos XX pair production (X[+5/3] in doublet, M = 1100 GeV)"
evgenConfig.keywords = ["BSM","exotic","top","quark"]
evgenConfig.contact  = ["Mark Cooke <mark.stephen.cooke@cern.ch>"]
evgenConfig.inputfilecheck = "protoslhef"
evgenConfig.process = "pp>XX"

include("MC15JobOptions/ProtosLHEF_Common.py") 
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")
