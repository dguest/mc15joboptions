evgenConfig.description = "UEH subgroup; prompt leptonjet gun samples"
evgenConfig.keywords = ["BSM", "exotic", "darkPhoton"]
evgenConfig.generators += ["ParticleDecayer"]
evgenConfig.process = 'sd->2zd_decay'
evgenConfig.contact = ["Bernhard.Meirose@cern.ch"]
include("MC15JobOptions/ParticleGun_Common.py")


import ParticleGun as PG
pg = PG.ParticleGun()

pg.sampler.pid = 999
pg.sampler.mom = PG.PtEtaMPhiSampler(pt=[12000, 500000], eta=[-2.5,2.5], phi=[-3.14,3.14])

from ParticleDecayer.ParticleDecayerConf import ParticleDecayer
genSeq += ParticleDecayer()
genSeq.ParticleDecayer.OutputLevel = INFO
genSeq.ParticleDecayer.McEventCollection = "GEN_EVENT"


genSeq.ParticleDecayer.LJType = 2
genSeq.ParticleDecayer.OutputLevel = FATAL
genSeq.ParticleDecayer.ScalarMass = 12000
genSeq.ParticleDecayer.ScalarPDGID = 700021
genSeq.ParticleDecayer.ParticleID = 999
genSeq.ParticleDecayer.ParticleMass = 50
genSeq.ParticleDecayer.ParticleLifeTime = 0
genSeq.ParticleDecayer.ParticlePolarization = 0
genSeq.ParticleDecayer.ParticlePDGID = 700022
genSeq.ParticleDecayer.DecayBRElectrons = 1.00
genSeq.ParticleDecayer.DecayBRMuons     = 0.00
genSeq.ParticleDecayer.DecayBRPions     = 0.00
genSeq.ParticleDecayer.DoUniformDecay               = False
genSeq.ParticleDecayer.DoExponentialDecay           = True
genSeq.ParticleDecayer.ExpDecayDoVariableLifetime   = False
#genSeq.ParticleDecayer.ExpDecayPercentageToKeep     = 0.8
genSeq.ParticleDecayer.ExpDecayDoTruncateLongDecays = False
genSeq.ParticleDecayer.BarrelRadius         = 8.e3
genSeq.ParticleDecayer.EndCapDistance       = 11.e3
genSeq.ParticleDecayer.ThetaEndCapBarrel    = 0.628796286
