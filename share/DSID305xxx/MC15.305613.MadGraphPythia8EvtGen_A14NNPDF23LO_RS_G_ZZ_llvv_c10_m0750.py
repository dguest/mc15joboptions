include("MC15JobOptions/MadGraphControl_BulkRS.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "MadGraph+PYTHIA8+EVTGEN, RS G*->ZZ->llvv mh=750 GeV"
evgenConfig.keywords    = [ "exotic", "BSM", "RandallSundrum", "warpedED", "graviton" ]
evgenConfig.contact     = [ "Mike Hance <michael.hance@cern.ch>" ]
