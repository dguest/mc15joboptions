mchi = 100
mphi = 1000
gx = 1.97094276543
filter_string = "T"
evt_multiplier = 15
include("MC15JobOptions/MadGraphControl_bFDMmodels.py")
evgenConfig.minevents = 500
evgenConfig.keywords = ['exotic','BSM','WIMP']
