LQmass = 1500

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["LeptoQuark:gg2LQLQbar = on",
                            "LeptoQuark:qqbar2LQLQbar = on",
                            "LeptoQuark:kCoup = 1.",                            
                            "42:0:all = 2 0.5 0 2 11",  
                            "42:addChannel = 3 0.5 0 1 12",
                            "42:m0 = "+ str(LQmass),
                            "42:mMin ="+ str(LQmass-200),
                            "42:mMax ="+ str(LQmass+200)]
# EVGEN configuration
evgenConfig.description = "Pythia 8 Pair produced scalar leptoquarks (M = "+str(LQmass)+" GeV) to electron-neutrino jet channel, A14 tune and NNPDF23LO PDF"
evgenConfig.contact = ["Georgios Zacharis <georgios.zacharis@cern.ch>"]
evgenConfig.keywords    = [ 'exotic', 'BSM', 'leptoquark', 'scalar', 'electron', '2jet' ]
evgenConfig.process = "pp>LQLQ>eudnu"




