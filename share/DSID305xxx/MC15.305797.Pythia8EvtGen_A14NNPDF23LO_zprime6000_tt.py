# zprime mass in GeV
m_zprime=6000.0

evgenConfig.description = "SSM Z prime ("+str(m_zprime)+") to ttbar"
evgenConfig.process = "pp>Zprime>ttbar"
evgenConfig.keywords = ["BSM", "Zprime" ,"resonance", "ttbar"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact = ["Jiahang Zhong <jiahang.zhong@cern.ch>"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")


# turn on the Z' process
genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on"]
# set mass and disable all decay modes except Z' -> ttbar
genSeq.Pythia8.Commands += ["32:m0 ="+str(m_zprime) ]
genSeq.Pythia8.Commands += ["32:onMode = off"]
genSeq.Pythia8.Commands += ["32:onIfAny = 6 -6"]

# only Z'
genSeq.Pythia8.Commands += ["Zprime:gmZmode= 3"]
