mchi = 50
mphi = 1200
gx = 3.33108898803
filter_string = "T"
evt_multiplier = 25
include("MC15JobOptions/MadGraphControl_bFDMmodels.py")
evgenConfig.minevents = 500
evgenConfig.keywords = ['exotic','BSM','WIMP']
