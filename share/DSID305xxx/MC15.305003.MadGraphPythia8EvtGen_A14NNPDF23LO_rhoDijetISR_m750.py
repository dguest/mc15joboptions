
#based on the work of: MadGraph/MadGraphControl_HAHM.py

proc_name = "CRES_m100"

proc_card = """
import model cres_UFO 
define j = j b b~
define qh = dh dh~ uh uh~ sh sh~ ch ch~ bh bh~

#Calculate widths first:
generate p p > rho j, rho > j j
"""
param_card_extras = { 
                "MASS" : { "MGH" : 750 } ,
                "FRBLOCK" : {"GghuuL": '1e-2',
                "GghddL":'1e-2',
                "GghuuR":'1e-2',
                "GghddR":'1e-2'},
                      "DECAY" : {  "wt" : "Auto" , "wz" : "Auto" , "ww" : "Auto" , "wh" : "Auto" , 
                                   "wdhe" : "Auto", "wuhe" : "Auto", "wshe" : "Auto" , "wche" : "Auto" , "wbhe" : "Auto", "wthe" : "Auto" ,
"wgh" : "Auto" , "wsh" : "Auto" }
}

run_card_extras =  { 'lhe_version':'3.0',
           'cut_decays':'T',
           'scale':'91.1880',
           'dsqrt_q2fact1':'91.1880',
           'dsqrt_q2fact2':'91.1880',      
           'parton_shower':'PYTHIA8',
           'ptj':'20',
           'ptb':'0',
           'pta':'20',
           'ptl':'10',
           'ptjmax':'-1',
           'ptbmax':'-1',
           'ptamax':'-1',
           'etaj':'-1',
           'etab':'-1',
           'etaa':'-1',
           'etajmin':'0',
           'etabmin':'0',
           'etaamin':'0',
           'mmaa':'0',
           'mmaamax':'-1',
           'mmbb':'0',
           'mmbbmax':'-1',
           'drjj':'0',
           'drbb':'0',
           'draa':'0',
           'drbj':'0',
           'draj':'0',
           'drab':'0',
           'drjjmax':'-1',
           'drbbmax':'-1',
           'draamax':'-1',
           'drbjmax':'-1',
           'drajmax':'-1',
           'drabmax':'-1',
           'gridpack':'false',
           'asrwgtflavor':'5',
           'sys_alpsfact':'0.5 1 2'}

evgenConfig.contact = ["katherine.pachal@cern.ch"]
evgenConfig.description = "CRES gluon partner plus ISR decaying to three jets"
evgenConfig.keywords = ["exotic","BSM"]
evgenConfig.process = "CRES_rhoj_jjj"

include("MC15JobOptions/MadGraphControl_Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
