# Initialise Herwig7 for LHEF showering
include("MC15JobOptions/Herwig7_LHEF.py")
include("MC15JobOptions/Herwig7_701_StripWeights.py")

## evtgen
include("MC15JobOptions/Herwig7_EvtGen.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7+EvtGen Wt production (top), DR scheme, inclusive, with Powheg hdamp equal 1.5*top mass, H7UE tune, ME NNPDF30 NLO, H7UE MMHT2014 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt', 'inclusive']
evgenConfig.contact     = [ 'timothee.theveneaux-pelzer@cern.ch', 'ian.connelly@cern.ch', 'kevin.finelli@cern.ch' ]
evgenConfig.generators += [ 'Powheg','Herwig7', 'EvtGen' ]
evgenConfig.tune = "MMHT2014"

## Configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
""")

## run generator
Herwig7Config.run()
