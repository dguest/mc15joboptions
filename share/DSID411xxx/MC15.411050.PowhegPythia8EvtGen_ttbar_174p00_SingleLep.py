#--------------------------------------------------------------
# EVGEN configuration
# Note:  These JO are designed to run Powheg and make an LHE file and to not run a showering
# generator afterwards.  Because the current Generate_tf.py requires an output file, we
# need to fake it a bit.  We therefore will run Pythia8 on the first event in the LHE file.
# Note, sence we do not intend to keep the EVNT file, the JO name doesn't include Pythia8
# The Powheg configuration is identical to DSID 410501
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch','mshapiro@lbl.gov','steffen.henkelmann@cern.ch']
evgenConfig.generators += [ 'Powheg' ]

# This is to fake the system into not spending all its time running Pythia8, since we intend
# to throw out the EVNT file
from PowhegControl import ATLASCommonParameters
ATLASCommonParameters.mass_t  = 174.0
ATLASCommonParameters.width_t = 1.360

include('PowhegControl/PowhegControl_tt_Common.py')
# Initial settings
PowhegConfig.topdecaymode = 22222                                         # Inclusive
PowhegConfig.hdamp        = 261                                        # 1.5 * mtop
PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
PowhegConfig.PDF          = [260000, 25200, 13165, 90900, 265000, 266000, 303400] # NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119, NNPDF31_nlo_as_0118 - PDF variations with nominal scale variation
PowhegConfig.PDF.extend(range(260001, 260101))                          # Include the NNPDF error set
#PowhegConfig.PDF.extend(range(25201, 25251))                            # Include the MMHT2014nlo68clas118 error set
PowhegConfig.PDF.extend(range(90901, 90931))                            # Include the PDF4LHC15_nlo_30 error set

#Information on how to run with multiple weights: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PowhegForATLAS#Running_with_multiple_scale_PDF
#PDFs - you can see a listing here: https://lhapdf.hepforge.org/pdfsets.html; picked these three as they are the inputs to the PDF4LHC2015 prescription (http://arxiv.org/pdf/1510.03865v2.pdf).

# Define a weight group configuration for scale variations with different PDFs
# Nominal mu_F = mu_R = 1.0 is not required as this is captured by the PDF variation above
PowhegConfig.define_event_weight_group( group_name='scales_pdf', parameters_to_vary=['mu_F','mu_R','PDF'] )

# Scale variations, MMHT2014nlo68clas118
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_MMHT',                   parameter_values=[ 2.0, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_MMHT',                 parameter_values=[ 0.5, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_MMHT',                   parameter_values=[ 1.0, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_MMHT',                 parameter_values=[ 1.0, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_MMHT',          parameter_values=[ 0.5, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_MMHT',              parameter_values=[ 2.0, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_MMHT',            parameter_values=[ 0.5, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_MMHT',            parameter_values=[ 2.0, 0.5, 25200] )

## Scale variations, CT14nlo_as_0118
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_CT14',                   parameter_values=[ 2.0, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_CT14',                 parameter_values=[ 0.5, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_CT14',                   parameter_values=[ 1.0, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_CT14',                 parameter_values=[ 1.0, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_CT14',          parameter_values=[ 0.5, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_CT14',              parameter_values=[ 2.0, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_CT14',            parameter_values=[ 0.5, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_CT14',            parameter_values=[ 2.0, 0.5, 13165] )

# Scale variations, PDF4LHC15_nlo_30
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_PDF4LHC15_NLO_30',              parameter_values=[ 2.0, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_PDF4LHC15_NLO_30',            parameter_values=[ 0.5, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_PDF4LHC15_NLO_30',              parameter_values=[ 1.0, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_PDF4LHC15_NLO_30',            parameter_values=[ 1.0, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_PDF4LHC15_NLO_30',     parameter_values=[ 0.5, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_PDF4LHC15_NLO_30',         parameter_values=[ 2.0, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_PDF4LHC15_NLO_30',       parameter_values=[ 0.5, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_PDF4LHC15_NLO_30',       parameter_values=[ 2.0, 0.5, 90900] )

# Scale variations, NNPDF30_nlo_as_0117
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_NNPDF_NLO_0117',                   parameter_values=[ 2.0, 1.0, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_NNPDF_NLO_0117',                 parameter_values=[ 0.5, 1.0, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_NNPDF_NLO_0117',                   parameter_values=[ 1.0, 2.0, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_NNPDF_0117',                     parameter_values=[ 1.0, 0.5, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_NNPDF_NLO_0117',          parameter_values=[ 0.5, 0.5, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_NNPDF_NLO_0117',              parameter_values=[ 2.0, 2.0, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_NNPDF_NLO_0117',            parameter_values=[ 0.5, 2.0, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_NNPDF_NLO_0117',            parameter_values=[ 2.0, 0.5, 265000] )

# Scale variations, NNPDF30_nlo_as_0119
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_NNPDF_NLO_0119',                  parameter_values=[ 2.0, 1.0, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_NNPDF_NLO_0119',                 parameter_values=[ 0.5, 1.0, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_NNPDF_NLO_0119',                   parameter_values=[ 1.0, 2.0, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_NNPDF_NLO_0119',                 parameter_values=[ 1.0, 0.5, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_NNPDF_NLO_0119',          parameter_values=[ 0.5, 0.5, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_NNPDF_NLO_0119',              parameter_values=[ 2.0, 2.0, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_NNPDF_NLO_0119',            parameter_values=[ 0.5, 2.0, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_NNPDF_NLO_0119',            parameter_values=[ 2.0, 0.5, 266000] )

# Scale variations, NNPDF31_nlo_as_0118
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_NNPDF31_NLO_0118',                  parameter_values=[ 2.0, 1.0, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_NNPDF31_NLO_0118',                 parameter_values=[ 0.5, 1.0, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_NNPDF31_NLO_0118',                   parameter_values=[ 1.0, 2.0, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_NNPDF31_NLO_0118',                 parameter_values=[ 1.0, 0.5, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_NNPDF31_NLO_0118',          parameter_values=[ 0.5, 0.5, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_NNPDF31_NLO_0118',              parameter_values=[ 2.0, 2.0, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_NNPDF31_NLO_0118',            parameter_values=[ 0.5, 2.0, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_NNPDF31_NLO_0118',            parameter_values=[ 2.0, 0.5, 303400] )

PowhegConfig.nEvents *= 3
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

