#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'MEtop+Pythia8 FCNC single top production ug->t with A14 tune and pT(match)=15 GeV'
evgenConfig.keywords    = [ 'FCNC', 'top', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch' ]
evgenConfig.generators += ['MEtop']

evgenConfig.inputfilecheck = "ugt"

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_LHEF.py')

genSeq.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 1',
                             'TimeShower:pTmaxMatch = 1'  ]

include("MC15JobOptions/Pythia8_ShowerWeights.py")

