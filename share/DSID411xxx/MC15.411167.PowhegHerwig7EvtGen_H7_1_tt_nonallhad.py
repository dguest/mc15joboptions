# based on the JobOptions MC15.429304
 
# Provide config information
evgenConfig.generators    += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.tune           = "H7-UE-MMHT"
evgenConfig.description    = "PowhegBox+Herwig7 7.1 ttbar production with Powheg hdamp equal top mass, H7-UE-MMHT tune, at least one lepton filter, with EvtGen"
evgenConfig.keywords       = ['SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact        = ['simone.amoroso@cern.ch','marco.vanadia@cern.ch']
 
#include('PowhegControl/PowhegControl_tt_Common.py')
## PowhegConfig.topdecaymode = 22222
##PowhegConfig.decay_mode = "t t~ > all"
##PowhegConfig.hdamp        = 172.5
#PowhegConfig.hdamp   = 258.75
#PowhegConfig.PDF     = 260000
#PowhegConfig.mu_F    = 1.0
#PowhegConfig.mu_R    = 1.0
## compensate filter efficiency
#PowhegConfig.nEvents     *= 3.
## PowhegConfig.generateRunCard()
## PowhegConfig.generateEvents()
#PowhegConfig.generate()
 
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")
 
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
 
# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")
 
# run Herwig7
Herwig7Config.run()
 
## NonAllHad filter
include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #(non-all had)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0
