from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigMerging import Hw7ConfigMerging

genSeq += Herwig7()

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "MMHT2014"
evgenConfig.description = "Herwig7 ttbar Multijet merging"
evgenConfig.keywords    = ["SM","top"]
evgenConfig.contact     = ["David Yallup (dyallup@cern.ch)"]
#evgenConfig.inputconfcheck = "group.phys-gener.H7.414002.MMHT2014nlo68cl_MatchBox_IncTTbar_TopMTScale_13TeV.TXT.mc15_v1._00001.tar.gz"

## initialize generator configuration object
generator = Hw7ConfigMerging(genSeq, runArgs, run_name="HerwigMerging", beams="pp")

## configure generator
generator.me_pdf_commands(order="NLO", name="MMHT2014nlo68cl")
generator.tune_commands()


generator.add_commands("""
##################################################
## Process selection
##################################################

## Model assumptions
read Matchbox/StandardModelLike.in
read Matchbox/DiagonalCKM.in

## Set the hard process
cd /Herwig/Merging
set MergingFactory:OrderInAlphaS 2
set MergingFactory:OrderInAlphaEW 0
do MergingFactory:Process p p -> t tbar [ j j ]

set /Herwig/MatrixElements/Matchbox/Amplitudes/MadGraph:ProcessPath MGExtra

#Set order (above born process) of processes to use NLO ME for
set MergingFactory:NLOProcesses 2

# Set the merging scale dividing the parton shower
# from the matrix element region in phase space.
set Merger:MergingScale 20.*GeV
set Merger:MergingScaleSmearing 0.1

read Matchbox/OnShellTopProduction.in



##################################################
## Scale choice
## See the documentation for more options
##################################################

#cd /Herwig/MatrixElements/Matchbox/Scales/
#ls
#set /Herwig/Merging/MergingFactory:ScaleChoice /Herwig/MatrixElements/Matchbox/Scales/TopPairMassScale
#set /Herwig/Merging/MergingFactory:ScaleChoice /Herwig/MatrixElements/Matchbox/Scales/SHatScale
#set /Herwig/Merging/MergingFactory:ScaleChoice /Herwig/MatrixElements/Matchbox/Scales/HTScale
#set /Herwig/Merging/MergingFactory:ScaleChoice /Herwig/MatrixElements/Matchbox/Scales/TopMTScale
set /Herwig/Merging/MergingFactory:ScaleChoice /Herwig/MatrixElements/Matchbox/Scales/TopPairMTScale



##################################################
##Semi Leptonic Lepton Filter 
##################################################
#set /Herwig/Particles/t:Synchronized Not_synchronized
#set /Herwig/Particles/tbar:Synchronized Not_synchronized
#do /Herwig/Particles/t:SelectDecayModes /Herwig/Particles/t/t->nu_e,e+,b; /Herwig/Particles/t/t->nu_mu,mu+,b;  
#do /Herwig/Particles/tbar:SelectDecayModes /Herwig/Particles/tbar/tbar->b,bbar,cbar; /Herwig/Particles/tbar/tbar->bbar,cbar,d; /Herwig/Particles/tbar/tbar->bbar,cbar#,s; /Herwig/Particles/tbar/tbar->bbar,s,ubar; /Herwig/Particles/tbar/tbar->bbar,ubar,d; 


##################################################
##Di-Lepton Filter
##################################################
#do /Herwig/Particles/t:SelectDecayModes /Herwig/Particles/t/t->nu_e,e+,b; /Herwig/Particles/t/t->nu_mu,mu+,b; 
#do /Herwig/Particles/tbar:SelectDecayModes /Herwig/Particles/tbar/tbar->nu_ebar,e-,bbar; /Herwig/Particles/tbar/tbar->nu_mubar,mu-,bbar;  


##################################################
##Print decay modes
##################################################
do /Herwig/Particles/t:PrintDecayModes
do /Herwig/Particles/tbar:PrintDecayModes

##################################################
##Re-weight x-section for the change in BR
##taken from : https://twiki.cern.ch/twiki/bin/view/AtlasProtected/Herwig7ForAtlas
##################################################
create Herwig::BranchingRatioReweighter /Herwig/Generators/BRReweighter
insert /Herwig/Generators/EventGenerator:EventHandler:PostHadronizationHandlers 0 /Herwig/Generators/BRReweighter


##################################################
## Matrix element library selection
##################################################

# read Matchbox/MadGraph-GoSam.in
read Matchbox/MadGraph-MadGraph.in
# read Matchbox/MadGraph-NJet.in
# read Matchbox/MadGraph-OpenLoops.in
# read Matchbox/HJets.in
# read Matchbox/VBFNLO.in


##################################################
## Cut selection
## See the documentation for more options
##################################################

## cuts on additional jets

cd /Herwig/Cuts

#set /Herwig/Cuts/LeptonPairMassCut:MinMass 60*GeV
#set /Herwig/Cuts/LeptonPairMassCut:MaxMass 120*GeV

read Matchbox/DefaultPPJets.in

#insert JetCuts:JetRegions 0 FirstJet
#insert JetCuts:JetRegions 1 SecondJet
# insert JetCuts:JetRegions 2 ThirdJet
# insert JetCuts:JetRegions 3 FourthJet

#set /Herwig/Cuts/FirstJet:PtMin 20.*GeV
#set /Herwig/Cuts/SecondJet:PtMin 20.*GeV   

cd /Herwig/MatrixElements/Matchbox/Utility
insert DiagramGenerator:ExcludeInternal 0 /Herwig/Particles/gamma

##################################################
## PDF choice
##################################################

read Matchbox/FiveFlavourNoBMassScheme.in
## required for dipole shower and fixed order in five flavour scheme
# read Matchbox/FiveFlavourNoBMassScheme.in
set /Herwig/Shower/ShowerHandler:SpinCorrelations Yes
""")

## CAUTION: Extremely crude sampling for testing purposes
#generator.sampler_commands("CellGridSampler", 1000, 2, 1000, 1, 100)
#generator.sampler_commands("MonacoSampler", 20000, 4, 50000, 1, 100)
# generator.sampler_commands("FlatBinSampler", 1000, 2, 1000, 1, 100)

#Replicate authors NLO sampler commands in Interface
generator.sampler_commands("MonacoSampler", 20000, 4, 50000, 1, 100)


generator.merging_weight(2,0,"No")

if runArgs.generatorRunMode == 'build':
  # use the --generatorJobNumber command line parameter to dynamically
  # specify the total number of parallel integration jobs
  generator.do_build(runArgs.generatorJobNumber)
 
elif runArgs.generatorRunMode == 'integrate':
  # use the --generatorJobNumber command line parameter to dynamically
  # specify which specific integration job is to be run
  generator.do_integrate(runArgs.generatorJobNumber)

elif runArgs.generatorRunMode == 'mergegrids':
  # combine integration grids and prepare a gridpack
  # use the --generatorJobNumber command line parameter to dynamically
  # specify the total number of parallel integration jobs
  generator.do_mergegrids(integration_jobs=runArgs.generatorJobNumber, gridpack="group.phys-gener.H7.414002.MMHT2014nlo68cl_MatchBox_IncTTbar_TopMTScale_13TeV.TXT.mc15_v1._00001.tar.gz")

elif runArgs.generatorRunMode == 'run':
  # generate events using the specified gridpack
  generator.do_run(gridpack="group.phys-gener.H7.414002.MMHT2014nlo68cl_MatchBox_IncTTbar_TopMTScale_13TeV.TXT.mc15_v1._00001.tar.gz")
  #generator.do_run()

elif runArgs.generatorRunMode == 'batchrun':
  generator.do_run_existing_runfile()
