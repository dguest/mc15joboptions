evgenConfig.description = 'POWHEL+Pythia8 ttbarbbbar production in allhad channel with Powhel hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO from Powhel LHE files with Shower Weights added '
evgenConfig.keywords    = [ 'SM', 'top']
evgenConfig.contact     = [ 'sven.menke@cern.ch']
evgenConfig.minevents   = 2000
#evgenConfig.inputfilecheck="ttbb"

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 4' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
  print "Pyhtia8 uses UserHooks:",genSeq.Pythia8.UserHooks
  print "Pyhtia8 uses Commands:",genSeq.Pythia8.Commands
else:
  print "Pyhtia8 uses UserHook:",genSeq.Pythia8.UserHook
  print "Pyhtia8 uses Commands:",genSeq.Pythia8.Commands

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
filtSeq.Expression = "(not TTbarWToLeptonFilter)"
