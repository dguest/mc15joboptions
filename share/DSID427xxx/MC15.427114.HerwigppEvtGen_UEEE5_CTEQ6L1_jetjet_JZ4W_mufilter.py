## Job options file for Herwig++, QCD jet slice production
include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py")

evgenConfig.description = "QCD dijet production JZ4W with PDF and UE-EE5 tune"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact  = [ "ruth.magdalena.jacobs@cern.ch" ]

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 150*GeV
"""

genSeq.Herwigpp.Commands += cmds.splitlines()

include('MC15JobOptions/Herwigpp_EvtGen.py')
include("MC15JobOptions/JetFilter_JZ4W.py")
include("MC15JobOptions/LowPtMuonFilter.py")

evgenConfig.minevents = 10

