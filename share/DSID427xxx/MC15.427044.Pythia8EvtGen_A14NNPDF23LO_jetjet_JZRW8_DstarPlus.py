# JO for Pythia 8 jet jet JZ8W slice

evgenConfig.description = "Dijet truth jet slice JZ8W, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/JetFilter_JZ8W.py")
include("MC15JobOptions/DstarPlusFilter.py")

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 1500.",
                            "PhaseSpace:bias2Selection = on",
                            "PhaseSpace:bias2SelectionRef = "+str(minDict[8]) ,
                            "PhaseSpace:bias2SelectionPow = 5.0"
                           ]

filtSeq.QCDTruthJetFilter.DoShape = False
filtSeq.UseEventWeight = True

evgenConfig.minevents = 5000

