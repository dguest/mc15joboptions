evgenConfig.description = "MadGraph+PYTHIA8 gg->pseudoscalar X(18.5)->Upsilon(1S)mumu->mumu + mumu with A14 NNPDF23LO by requiring 3 lepton pt>2GeV within eta 2.8"
evgenConfig.keywords = ["heavyFlavour","Upsilon","4muon"]
evgenConfig.inputfilecheck = 'madgraph'
evgenConfig.contact = [ 'Tiesheng.Dai@cern.ch' ]
evgenConfig.minevents = 5000
evgenConfig.generators += ["MadGraph", "Pythia8"]

##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# Upsilon -> mumu
##############################################################
f = open("MY_UPSILON_USER_FORCED.DEC","w")
f.write("Decay Upsilon\n")
f.write("1.0000    mu+  mu-             PHOTOS  VLL; #[Reconstructed PDG2011]\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################


include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_LHEF.py")

genSeq.Pythia8.Commands += [
                            'SpaceShower:pTmaxMatch = 1',  # no power shower, just wimpy showers
                            '25:onMode = off'    #decay of Higgs
                            '553:onMode = off'   #decay of Upsilon
                           ]
genSeq.EvtInclusiveDecay.userDecayFile = "MY_UPSILON_USER_FORCED.DEC"
#
### 3 lepton filter
#
include("MC15JobOptions/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.NLeptons = 3
filtSeq.MultiLeptonFilter.Ptcut = 2000.0
filtSeq.MultiLeptonFilter.Etacut = 2.8
