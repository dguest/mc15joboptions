#################################################################################
# job options fragment for hf->NegDs10->etapmunu->mu4mu2mu1X 
# For HF tau3mu based on Meson Decay(MD) study for run2
################################################################################# 
# All production channels of bb and cc are included in this fragment.
# D_s meson force decaying into semileptonic channel munu and eta' which also must decay into dimuon+X.
# D_s pT is limited as high because we can get only boosted signature, at least.
# thresholds: mu1>4GeV, mu2>2GeV, mu3>1GeV, and D_s>10GeV
# original version from Dai Kobayashi
#################################################################################

f = open("NegDs_EtapMuNu_3MuX_USER.DEC", "w")
f.write("Alias myEta' eta'\n")
f.write("Decay D_s-\n")
f.write("1.0000   myEta'   mu-  anti-nu_mu   PHOTOS ISGW2;\n")
f.write("Enddecay\n")
f.write("Decay myEta'\n")
f.write("1.0000   gamma   mu+  mu-          PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

#################################################################################

include("MC15JobOptions/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.description = "hf->NegDs->etapmunu->3muX inclusive production"
evgenConfig.keywords = ["charmonium", "3muon", "heavyFlavour", "SM", "singlyResonant"]
evgenConfig.minevents = 200
evgenConfig.contact = ['marcus.matthias.morgenstern@cern.ch']
evgenConfig.process = "hf>NegDs>eta'munu>3muX"

genSeq.Pythia8B.Commands += ['HardQCD:all = on']
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.']
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = True
genSeq.Pythia8B.QuarkPtCut = 10.0
genSeq.Pythia8B.AntiQuarkPtCut = 10.0
genSeq.Pythia8B.QuarkEtaCut = 4.5
genSeq.Pythia8B.AntiQuarkEtaCut = 4.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = True

genSeq.Pythia8B.NHadronizationLoops = 2
genSeq.Pythia8B.NDecayLoops = 1

genSeq.EvtInclusiveDecay.userDecayFile = "NegDs_EtapMuNu_3MuX_USER.DEC"

from GeneratorFilters.GeneratorFiltersConf import TripletChainFilter
filtSeq += TripletChainFilter()

TripletChainFilter = filtSeq.TripletChainFilter
TripletChainFilter.NTriplet = 1
TripletChainFilter.PdgId1 = 13
TripletChainFilter.PdgId2 = -13
TripletChainFilter.PdgId3 = 13
TripletChainFilter.NStep1 = 2
TripletChainFilter.NStep2 = 2
TripletChainFilter.NStep3 = 1
TripletChainFilter.PtMin1 = 4000
TripletChainFilter.PtMin2 = 2000
TripletChainFilter.PtMin3 = 1000
TripletChainFilter.EtaMax1 = 3.0
TripletChainFilter.EtaMax2 = 3.0
TripletChainFilter.EtaMax3 = 3.0
TripletChainFilter.TripletPdgId = -431
TripletChainFilter.TripletPtMin = 10000
TripletChainFilter.TripletEtaMax = 3.0
TripletChainFilter.TripletMassMin = 0
TripletChainFilter.TripletMassMax = 10000000
TripletChainFilter.DoubletPdgId = 331
TripletChainFilter.DoubletPtMin = 0
TripletChainFilter.DoubletEtaMax = 100
TripletChainFilter.DoubletMassMin = 0
TripletChainFilter.DoubletMassMax = 10000000
