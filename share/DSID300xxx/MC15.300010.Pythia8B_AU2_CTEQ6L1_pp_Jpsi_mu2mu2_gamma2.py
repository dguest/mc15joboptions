evgenConfig.description = "Inclusive pp->J/psi(mu2p0mu2p0) gamma2p0 production with Photos"
evgenConfig.keywords = ["charmonium","Jpsi","2muon","inclusive"]
evgenConfig.minevents = 10000

include('MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py')
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_Charmonium_Common.py")

genSeq.Pythia8B.Commands += ['Charmonium:all = off']

genSeq.Pythia8B.SuppressSmallPT = False
genSeq.Pythia8B.Commands =  [x for x in genSeq.Pythia8B.Commands if 'pTHatMin' not in x]

genSeq.Pythia8B.Commands += ['PhaseSpace:mHatMin  force= 0.0'] 
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMinDiverge force= 0.0'] 
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin  = 0.'] 

genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [443,   -13,    13  ]
genSeq.Pythia8B.SignalPtCuts   = [  0.0,   2.0,   2.0]
genSeq.Pythia8B.SignalEtaCuts  = [102.5, 102.5, 102.5]

gammaFilter = CfgMgr.DirectPhotonFilter()
gammaFilter.Ptmin    = 2e3
gammaFilter.Ptmax    = 1e30
gammaFilter.NPhotons = 1
gammaFilter.Etacut   = 400

genSeq += gammaFilter

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [0.0]
genSeq.Pythia8B.TriggerStateEtaCut = 100
genSeq.Pythia8B.MinimumCountPerCut = [0]
 
genSeq.Pythia8B.Commands+=['Charmonium:gg2ccbar(3S1)[3S1(1)]gm = on,on']
