f = open("MYDECAY_BcJpsiK.DEC","w")
f.write("Alias      MyJ/psi    J/psi\n")
f.write("ChargeConj MyJ/psi    MyJ/psi\n")
f.write("Decay B_c+\n")
f.write("  1.000     MyJ/psi   K+                SVS;\n")
f.write("Enddecay\n")
f.write("Decay MyJ/psi\n")
f.write("  1.000     mu+       mu-                VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

evgenConfig.description = "Bc+->J/psi K+ sample with BCVEGPY"
evgenConfig.keywords = ["exclusive","Jpsi","2muon"]
evgenConfig.minevents = 5000
evgenConfig.inputfilecheck = "BcPlus"

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_BCVEGPY.py")
include("MC15JobOptions/Pythia8_BcStates.py")

genSeq.EvtInclusiveDecay.userDecayFile = "MYDECAY_BcJpsiK.DEC"
evgenConfig.auxfiles += ['inclusiveP8_BcPDG18.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8_BcPDG18.pdt"

include("MC15JobOptions/BSignalFilter.py")

filtSeq.BSignalFilter.LVL1MuonCutOn  = True
filtSeq.BSignalFilter.LVL2MuonCutOn  = True
filtSeq.BSignalFilter.LVL1MuonCutPT  = 3500.0
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.8
filtSeq.BSignalFilter.LVL2MuonCutPT  = 3500.0
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.8

filtSeq.BSignalFilter.B_PDGCode = 541
