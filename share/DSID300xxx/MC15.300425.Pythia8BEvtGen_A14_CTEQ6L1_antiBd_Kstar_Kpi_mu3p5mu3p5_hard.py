##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# anti-B0 -> anti-K*0 (K-pi+) mu3.5mu3.5 (flat angles)
##############################################################
f = open("B0_KSTAR_MUMU_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")
f.write("Alias my_anti-K*0   anti-K*0\n")
f.write("Decay anti-B0\n")
f.write("1.0000  my_anti-K*0     mu+   mu-       PHOTOS BTOSLLBALL;\n")
f.write("Enddecay\n")
f.write("Decay my_anti-K*0\n")
f.write("1.0000    K-    pi+           VSS;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

##############################################################

evgenConfig.description = "Exclusive anti-B0->anti-K*0_Kpi_mu3p5mu3p5 production"
evgenConfig.keywords    = ["exclusive","B0","2muon"]
evgenConfig.minevents   = 200

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 12.']

genSeq.Pythia8B.QuarkPtCut      = 12.0
genSeq.Pythia8B.AntiQuarkPtCut  = 0.0
genSeq.Pythia8B.QuarkEtaCut     = 2.6
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 4

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [-511]
genSeq.EvtInclusiveDecay.userDecayFile = "B0_KSTAR_MUMU_USER.DEC"

filtSeq.BSignalFilter.LVL1MuonCutOn  = True
filtSeq.BSignalFilter.LVL2MuonCutOn  = True
filtSeq.BSignalFilter.LVL1MuonCutPT  = 3500
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter.LVL2MuonCutPT  = 3500
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.6

filtSeq.BSignalFilter.B_PDGCode = -511
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 3500.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.6
