f = open("Bc_USER.DEC", "w")
f.write("Alias myJ/psi J/psi\n")
f.write("Alias myD*-   D*- \n")
f.write("Alias myanti-D0    anti-D0 \n")
f.write("Decay B_c-\n")
f.write("1.0000    myJ/psi myD*-         SVV_HELAMP 1.0 0.0 0.0 0.0 0.0 0.0;\n")
f.write("Enddecay\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+ mu-               VLL;\n")
f.write("Enddecay\n")
f.write("Decay myD*-\n")
f.write("1.0000    myanti-D0 pi-         VSS;\n")
f.write("Enddecay\n")
f.write("Decay myanti-D0\n")
f.write("1.0000    K+  pi-               PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

#################################################################################

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_BCVEGPY.py")
include("MC15JobOptions/Pythia8_BcStates.py")

evgenConfig.description = "Bc- -> J/psi(mu3p5mu3p5) D*-(antiD0(K+ pi-) pi-) in H100"
evgenConfig.keywords = ["exclusive", "2muon", "Jpsi"]
evgenConfig.minevents = 5000
evgenConfig.inputfilecheck = "BcMinus"

genSeq.EvtInclusiveDecay.userDecayFile = "Bc_USER.DEC"
evgenConfig.auxfiles += ['inclusiveP8_BcPDG18.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8_BcPDG18.pdt"

include("MC15JobOptions/BSignalFilter.py")

filtSeq.BSignalFilter.LVL1MuonCutOn  = True
filtSeq.BSignalFilter.LVL2MuonCutOn  = True
filtSeq.BSignalFilter.LVL1MuonCutPT  = 3500.0
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.7
filtSeq.BSignalFilter.LVL2MuonCutPT  = 3500.0
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.7

filtSeq.BSignalFilter.B_PDGCode = -541
