#--------------------------------------------------------------
# JO  fragment for pp->J/psi(mu2p5mu2p5)X, Pythia 8B
#--------------------------------------------------------------

evgenConfig.description = "Inclusive pp->J/psi(mu3p5mu3p5) production with Photos"
evgenConfig.keywords = ["charmonium","Jpsi","2muon","inclusive"]
evgenConfig.minevents = 5000

include('MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py')
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_Charmonium_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 4.'] 
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [3.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [2]
