##############################################################
# B0d -> J/psi (mu3.5mu3.5) K0*(K+ pi-)
##############################################################

evgenConfig.description = "Exclusive B0d -> J/psi (mu3.5mu3.5) K0*(K+ pi-)"
evgenConfig.keywords    = ["exclusive", "B0", "2muon", "Jpsi"]
evgenConfig.minevents   = 200

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.']

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 8.0
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.Commands      += ['511:onIfMatch = 443 313']
genSeq.Pythia8B.Commands      += ['443:onMode = off']
genSeq.Pythia8B.Commands      += ['443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [ 511, 443, -13, 13, 313, 321, -211 ]
# Select the required method in the userselections file
genSeq.Pythia8B.UserSelection = 'BDJPSIKSTAR_TRANS'
# Pass the requested physical parameters to the algorithm
genSeq.Pythia8B.UserSelectionVariables = [ 1.0, 1.2, 1.0, 0.519, 0.252, 0.0, 0.659, 0.0 ,0.507, 0.0, 3.02, -2.87, 0.86]

genSeq.Pythia8B.NHadronizationLoops = 4

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [3.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [2]
