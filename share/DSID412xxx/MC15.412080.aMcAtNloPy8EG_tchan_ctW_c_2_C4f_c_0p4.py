evgenConfig.description = 'MadGraph5_aMC@NLO++Pythia8+EvtGen single-top-quark t-channel (2->3) production, MadSpin, A14 tune, ME NNPDF3.04f NLO, A14 NNPDF23 LO'
evgenConfig.keywords = [ 'top', 'singleTop', 'tChannel', 'lepton' ]
evgenConfig.contact = ["Marc de Beurs <marcus.de.beurs@cern.ch>"]
evgenConfig.process = "singleTop"

#Common MG_Control file
include("MC15JobOptions/MadGraphControl_tchan_ctW_c-2_C4f-c-0p4_Pythia8_A14.py")

#Below is showering
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")
