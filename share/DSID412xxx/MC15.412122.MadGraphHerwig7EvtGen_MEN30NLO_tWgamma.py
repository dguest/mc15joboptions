from MadGraphControl.MadGraphUtils import *

# General settings
nevents=int(2*1.1*runArgs.maxEvents)
name='ttgamma_nonallhad'
stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

defs = """
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define uc = u c
define uc~ = u~ c~
define ds = d s
define ds~ = d~ s~
"""

mcprod = """
generate p p > ttbar w a
"""
mcprod_maddec = defs+"""
define w+child = l+ vl uc ds~
define w-child = l- vl~ ds uc~
decay t > w+ b, w+ > w+child w+child
decay t~ > w- b~, w- > w-child w-child
decay w+ > w+child w+child
decay w- > w-child w-child
"""

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define w = w+ w-
define ttbar = t t~
"""+mcprod+"""
output -f
""")
fcard.close()

# Decay with MadSpin
madspin_card_loc='madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event
set Nevents_for_max_weigth 500
set BW_cut 15
set seed %i
%s
launch
"""%(runArgs.randomSeed, mcprod_maddec))
mscard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#Fetch default LO run_card.dat and set parameters
extras = {'lhe_version'   :'3.0',
           'lhaid'         :260000,
           'pdlabel'       :"'lhapdf'",
           'maxjetflavor'  :5,
           'cut_decays'    :'T',
           'ptl'           :5.,
           'pta'           :15.,
           'ptj'           :1.,
           'xptl'          :15.,
           'etal'          :5.0,
           'etaa'          :5.0,
           'etaj'          :-1,
           'etab'          :-1,
           'drjj'          :0.0,
           'drjl'          :0.0,
           'drll'          :0.0,
           'draa'          :0.0,
           'draj'          :0.2,
           'dral'          :0.2,
           'use_syst'      :'T',
           'sys_scalefact' :'1 0.5 2',
           'dynamical_scale_choice':'3',
           'sys_pdf'       :'NNPDF30_nlo_as_0118'}
 
process_dir = new_process()
# Run card
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',  nevts=nevents, rand_seed=runArgs.randomSeed, beamEnergy=beamEnergy, xqcut=0., extras=extras)

# Print cards
print_cards()
# set up
generate(run_card_loc='run_card.dat', param_card_loc=None, madspin_card_loc=madspin_card_loc, mode=0, proc_dir=process_dir)
# run
outputDS=arrange_output(proc_dir=process_dir,outputDS=stringy+'._00001.tar.gz',lhe_version=3)

# Go to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

## pythia shower
keyword=['SM','top', 'ttV', 'photon']
evgenConfig.generators += ["MadGraph", "Herwig7", "EvtGen"]
evgenConfig.description = 'MadGraph_'+str(name)+'_GamFromProd'
evgenConfig.tune = "H7.1-Default"
evgenConfig.keywords += keyword
evgenConfig.contact = ["amartya.rej@cern.ch"]
runArgs.inputGeneratorFile=outputDS

# Striping PDF and scale factor weights
include("MC15JobOptions/Herwig7_701_StripWeights.py")

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED 
""")

# run Herwig7
Herwig7Config.run()
