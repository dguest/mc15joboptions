#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'MadGraph5_aMC@NLO+Herwig7+EvtGen single-top-quark s-channel production (top), inclusive, H7UE tune, ME NNPDF30 NLO, H7UE MMHT2014 LO'
evgenConfig.keywords = [ 'top', 'singleTop', 'sChannel', 'lepton' ]
evgenConfig.contact = [ '"Marc de Beurs <marcus.de.beurs@cern.ch>' ]
evgenConfig.process = 'singleTop'
evgenConfig.tune = "MMHT2014"
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]

#--------------------------------------------------------------
# Common MG_Control file
#--------------------------------------------------------------
include("MC15JobOptions/MadGraphControl_schan_NLO_Herwig7_A14.py")

#--------------------------------------------------------------
# Herwig7 showering with the H7UE MMHT2014 tune
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")
#include("MC15JobOptions/Herwig7_701_StripWeights.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
""")

# run Herwig7
Herwig7Config.run()
