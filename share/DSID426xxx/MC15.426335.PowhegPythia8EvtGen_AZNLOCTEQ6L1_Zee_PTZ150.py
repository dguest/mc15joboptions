#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.vdecaymode = 1   # ee

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 500 # this is the number for 150 GeV pTZ
PowhegConfig.running_width = 1

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->ee production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Christopher Young <christopher.young@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2electron' ]
evgenConfig.minevents = 500

from GeneratorFilters.GeneratorFiltersConf import DiLeptonMassFilter
filtSeq += DiLeptonMassFilter()
filtSeq.DiLeptonMassFilter.MinPt = 5000.
filtSeq.DiLeptonMassFilter.MaxEta = 5.0
filtSeq.DiLeptonMassFilter.MinMass = 20000.
filtSeq.DiLeptonMassFilter.MaxMass = 14000000.
filtSeq.DiLeptonMassFilter.AllowSameCharge = False
filtSeq.DiLeptonMassFilter.MinDilepPt = 150000.
filtSeq.Expression = "DiLeptonMassFilter"

