evgenConfig.description = "Single antin with flat eta-phi and log Pt in [100, 2000] GeV"
evgenConfig.keywords = ["singleParticle", "antineutron"]
 
include("MC15JobOptions/ParticleGun_Common.py")
 
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = -2112
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=PG.LogSampler(100, 2000000.), eta=[-2.4, 2.4])

