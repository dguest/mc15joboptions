evgenConfig.description = "a8 Z'->ttbar with A14 tune and NNPDF23LO PDF"
evgenConfig.process = "Z' -> t + tbar"
evgenConfig.contact = ["fdibello@cern.ch"] 
evgenConfig.keywords    = [ 'BSM', 'Zprime', 'heavyBoson', 'SSM', 'resonance', 'electroweak']
evgenConfig.generators += [ 'Pythia8' ]

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.UserHooks += ["ZprimeFlatpT"]

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",  # create Z bosons
"PhaseSpace:mHatMin = 25.0",         # minimum inv.mass cut
"32:onMode = off",                     # switch off all Z' decays
"32:m0 = 4000.0"]


genSeq.Pythia8.Commands += ['32:oneChannel = on 1 100 -6 6'] # onMode bRatio meMode products setting ttbar only decay
genSeq.Pythia8.Commands += ["24:onMode = off"]# Turn off all W decays
genSeq.Pythia8.Commands += ["24:onIfAny = 1 2 3 4 5"]# Turn on hadronic W  


#Only Z' - no gamma/Z
genSeq.Pythia8.Commands += ["Zprime:gmZmode= 3",
                            "ZprimeFlatpT:MaxSHat=13000.",
                            "ZprimeFlatpT:DoDecayWeightBelow=2000."]
