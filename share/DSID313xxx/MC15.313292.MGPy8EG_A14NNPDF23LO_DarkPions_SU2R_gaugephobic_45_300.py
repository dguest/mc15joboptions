dmmodel = 'Gaugephobic_SU2R'
dpionmass = 300.0
fermioneta = 0.45
ndark = 4

include("MC15JobOptions/MadGraphControl_DarkPions_Pythia8EvtGen_A14_NNPDF23LO.py")
