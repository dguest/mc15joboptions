model="LightVector"
mDM1 = 5.
mDM2 = 630.
mZp = 600.
mHD = 125.
widthZp = 2.842817e+00
widthN2 = 2.766014e-01
filteff = 8.116883e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
