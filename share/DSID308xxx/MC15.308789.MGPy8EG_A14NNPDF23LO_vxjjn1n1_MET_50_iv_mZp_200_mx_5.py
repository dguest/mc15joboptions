model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 230.
mZp = 200.
mHD = 125.
widthZp = 7.957744e-01
widthN2 = 6.644102e-03
filteff = 5.488474e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
