model="LightVector"
mDM1 = 500.
mDM2 = 2000.
mZp = 1000.
mHD = 125.
widthZp = 4.770293e+00
widthN2 = 6.569598e+01
filteff = 9.940358e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
