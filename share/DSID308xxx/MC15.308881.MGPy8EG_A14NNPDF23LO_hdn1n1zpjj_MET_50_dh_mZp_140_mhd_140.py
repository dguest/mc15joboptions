model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 140.
mHD = 140.
widthZp = 5.570415e-01
widthhd = 5.556195e-02
filteff = 7.776050e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
