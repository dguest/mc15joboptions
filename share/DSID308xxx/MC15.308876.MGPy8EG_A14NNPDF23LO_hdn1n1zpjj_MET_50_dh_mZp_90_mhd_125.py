model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 90.
mHD = 125.
widthZp = 3.580954e-01
widthhd = 2.570046e-02
filteff = 6.973501e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
