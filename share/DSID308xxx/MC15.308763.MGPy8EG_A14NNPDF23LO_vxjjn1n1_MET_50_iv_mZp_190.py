model="InelasticVectorEFT"
mDM1 = 95.
mDM2 = 380.
mZp = 190.
mHD = 125.
widthZp = 7.559856e-01
widthN2 = 2.514338e-02
filteff = 9.07441e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
