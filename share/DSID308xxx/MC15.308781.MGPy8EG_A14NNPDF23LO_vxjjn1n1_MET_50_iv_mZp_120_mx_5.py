model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 150.
mZp = 120.
mHD = 125.
widthZp = 4.774635e-01
widthN2 = 1.795121e-03
filteff = 4.325260e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
