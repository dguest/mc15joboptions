model="LightVector"
mDM1 = 85.
mDM2 = 340.
mZp = 170.
mHD = 125.
widthZp = 6.764080e-01
widthN2 = 1.116832e+01
filteff = 9.025271e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
