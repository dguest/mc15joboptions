model="LightVector"
mDM1 = 5.
mDM2 = 100.
mZp = 70.
mHD = 125.
widthZp = 2.785143e-01
widthN2 = 1.753105e+00
filteff = 2.733734e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
