model="LightVector"
mDM1 = 5.
mDM2 = 930.
mZp = 900.
mHD = 125.
widthZp = 4.291147e+00
widthN2 = 1.880998e-01
filteff = 8.726003e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
