model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 130.
mHD = 130.
widthZp = 5.172525e-01
widthhd = 5.157210e-02
filteff = 7.751938e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
