model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 380.
mZp = 350.
mHD = 125.
widthZp = 1.468759e+00
widthN2 = 3.023944e-02
filteff = 7.235890e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
