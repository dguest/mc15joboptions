#Alpgen Pythia JobOption
evgenConfig.description = "ALPGEN+Pythia Z(->mumu)+jets process with PythiaPerugia2012C tune - HT filtered"
evgenConfig.keywords = ["SM", "Z", "muon", "jets"]
evgenConfig.minevents = 200
evgenConfig.inputfilecheck = "AlpgenPythia_P2012_ZmumuNp3"

if runArgs.trfSubstepName == 'generate' :
   include('MC15JobOptions/AlpgenPythia_Perugia2012_Common.py')
   include('MC15JobOptions/Pythia_Tauola.py')
   include('MC15JobOptions/Pythia_Photos.py')

   # Configure the HT filter
   include('MC15JobOptions/HTFilter.py')
   filtSeq.HTFilter.MinHT = 500.*GeV # Min HT to keep event
   filtSeq.HTFilter.MaxHT = 14000.*GeV # Max HT to keep event

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Alpgen_EvtGen.py')


