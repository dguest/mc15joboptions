#--------------------------------------------------------------
# Configuration for EvgenJobTransforms            
#--------------------------------------------------------------
evgenConfig.generators  += ["MadGraph", "Pythia8"]
evgenConfig.description  = 'MG5_aMC@NLO+Pythia8 WWbb, A14 tune, with EvtGen'
evgenConfig.keywords    += [ 'SM', 'top', 'lepton','Wt','singleTop']
evgenConfig.contact      = [ 'bnachman@cern.ch' ]
evgenConfig.inputfilecheck="MGPy8EG_A14N23LO_WWbbDR_doubleres_2L_MET200_400_13TeV"

#--------------------------------------------------------------
# Showering with Pythia 8, A14 tune
#--------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")
