include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "TGC ZZ with 0,1j@LO and mll>2*ml+250 MeV, pTl1>5 GeV, pTl2>5 GeV."
evgenConfig.keywords = ["diboson", "ZZ", "jets", "tripleGaugeCoupling" ]
evgenConfig.contact  = ["atlas-generators-sherpa@cern.ch", "maurice.becker@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Sherpa_CT10_ZZ_TGC0"

evgenConfig.process="""
(run){
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  ACTIVE[25]=0
  ERROR=0.02
  ME_SIGNAL_GENERATOR=Amegic
  MASSIVE[15]=1
  EXCLUSIVE_CLUSTER_MODE=1
  METS_CLUSTER_MODE=16
}(run)

(model){
  MODEL = SM+AGC
  F4_GAMMA=0.1
  F5_GAMMA=0.0
  F4_Z=0.0
  F5_Z=0.0

  UNITARIZATION_SCALE = 16000.0
  UNITARIZATION_N = 0
}(model)

(processes){
  Process 93 93 -> 90 90 90 90 93{1};
  Integration_Error 0.05 {5,6,7,8};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;
}(processes)

(selector){
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
  Mass 11 -11 0.25 E_CMS
  Mass 13 -13 0.4614 E_CMS
  Mass 15 -15 3.804 E_CMS
}(selector)
"""
