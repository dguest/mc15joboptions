##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B+ -> J/psi(mu3.5mu3.5) K+
##############################################################
f = open("BU_JPSI_K_USER.DEC","w")
f.write("Particle  B+ 5.27929 0.0\n") # PDG2014 mass
f.write("Particle  B- 5.27929 0.0\n") # PDG2014 mass
f.write("Define dm_incohMix_B0 0.0\n") #disable neutral meson mixing
f.write("Define dm_incohMix_B_s0 0.0\n") #same, LEAVE AS B_s0
f.write("Alias myJ/psi J/psi\n")
f.write("Decay B+\n")
f.write("1.0000  myJ/psi   K+             SVS;\n")
f.write("Enddecay\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+  mu-             VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################
from MadGraphControl.MadGraphUtils import *

safefactor=800
nevents=5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

mode=0
runName='runTest'     

#Defaults for run_card.dat
extras = { 'lhe_version':'3.0', 
           'cut_decays':'F', 
           'pdlabel':"'lhapdf'",
           'lhaid'         : 303400,
           'drjj'          :'0.1',
           'drbb'          :'0.1',
           'drbj'          :'0.1',
           'ickkw'         :'0',
           'ptj'           :'10.',
           'ptgmin'        :'5.0',
           'R0gamma'       :'0.1',
           'epsgamma'      :'0.1',
           'xn'            :'2.0',
           'isoEM'          :'False',
           'fixed_ren_scale' :'.false.',
           'fixed_fac_scale' :'.false.',
           'maxjetflavor'  : '5'
           }  

#import model loop_sm-no_b_mass
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define q = u d s b u~ d~ s~ b~
    define bs = b b~
    define cs = c c~
    define l+ = e+ mu+
    define l- = e- mu-
    generate j j > a j @0
    add process j j > a j j @1
    add process j j > a j j j @2
    output -f""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process()

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,extras=extras)

print_cards()
    
generate(run_card_loc='run_card.dat',param_card_loc='aMcAtNlo_param_card_loopsmnobmass.dat',mode=2,njobs=10,proc_dir=process_dir,run_name=runName)

outputDS = arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3.0)  

evgenConfig.contact = [ "josu.cantero.garcia@cern.ch" ]
evgenConfig.description = 'MadGraph_SM_photonBp'
evgenConfig.keywords = [ "photon", "LO", "QCD", "SM" ]
evgenConfig.minevents = 2000
runArgs.inputGeneratorFile=outputDS

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

#Let EvtGen do the B decay 
genSeq.EvtInclusiveDecay.outputKeyName = "GEN_EVENT"
genSeq.EvtInclusiveDecay.readExisting = True
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
genSeq.EvtInclusiveDecay.whiteList+=[521]
genSeq.EvtInclusiveDecay.userDecayFile = "BU_JPSI_K_USER.DEC"
evgenConfig.auxfiles += [ 'BU_JPSI_K_USER.DEC' ]

##Filter for Bp
include("MC15JobOptions/BSignalFilter.py")
filtSeq.BSignalFilter.LVL1MuonCutOn = True
filtSeq.BSignalFilter.LVL2MuonCutOn = True
filtSeq.BSignalFilter.LVL1MuonCutPT = 3500
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter.LVL2MuonCutPT = 3500
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.6

filtSeq.BSignalFilter.B_PDGCode = 521
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT = 900.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta = 2.6

##photon filter
include("MC15JobOptions/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.Ptmin = 10*GeV
filtSeq.DirectPhotonFilter.Etacut = 2.5
filtSeq.DirectPhotonFilter.Ptmax = 25.*GeV
