include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z(->qq)Z(->ll) with 0,1j@NLO + 2,3j@LO."
evgenConfig.keywords = ["SM", "diboson", "2lepton", "2jet", "NLO", "systematic" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "Sherpa_CT10_ZqqZll"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=4.00;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])/4.0}

  %tags for process setup
  NJET:=3; LJET:=2,3; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1
  METS_CLUSTER_MODE=16

  % decay setup
  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0
}(run)

(processes){
  Process 93 93 -> 23 23 93{NJET};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[23]=0" ]

from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
decaysfilter = DecaysFinalStateFilter()
filtSeq += decaysfilter

filtSeq.DecaysFinalStateFilter.PDGAllowedParents = [ 23 ]
filtSeq.DecaysFinalStateFilter.NQuarks = 2
filtSeq.DecaysFinalStateFilter.NChargedLeptons = 2
