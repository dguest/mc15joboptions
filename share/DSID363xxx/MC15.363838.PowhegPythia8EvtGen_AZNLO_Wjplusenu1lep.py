#--------------------------------------------------------------
# Powheg Wj setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Wj_Common.py')

PowhegConfig.foldx=10
PowhegConfig.foldy=10
PowhegConfig.foldphi=10
PowhegConfig.bornktmin=0.
PowhegConfig.bornsuppfact=0.
PowhegConfig.PDF=13000
PowhegConfig.nEvents *= 1.9
# PowhegConfig.NNLO_reweighting_inputs = { 'DYNNLO':'Wp_CM8_MMHT14NNLO_11.top' }
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_SM_Common.py')
#include('MC15JobOptions/Pythia8_Powheg.py')

#include('MC15JobOptions/Pythia8_Powheg_Main31.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')
genSeq.Photospp.PhotonSplitting = True
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 W+j production with A14 NNPDF2.3 tune'
evgenConfig.keywords    = [ 'SM', 'W', '1jet' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch' ]

include('MC15JobOptions/OneLeptonFilter.py')
