include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += [
    "PDF:useHard = on",
    "PDF:pHardSet = LHAPDF6:NNPDF31_nlo_as_0118_luxqed",
    "SpaceShower:pTdampMatch = 1",
    "PhotonCollision:gmgm2tautau= on", # gg->mumu
    "PhaseSpace:mHatMin = 200.", # lower invariant mass
    "PhaseSpace:mHatMax = 600." # upper invariant mass
]

include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.NLeptons = 2

evgenConfig.description = "gammagamma -> mumu production with NNPDF31NLOLUXQED, 20<M<60GeV"
evgenConfig.contact = ["Simone Amoroso <simone.amoroso@cern.ch>"]
evgenConfig.keywords = ["SM", "drellYan", "electroweak", "2tau"]
evgenConfig.generators += ["Pythia8"]
