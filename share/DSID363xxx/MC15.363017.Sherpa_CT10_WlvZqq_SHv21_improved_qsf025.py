include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "W(->lv)Z(->qq) with 0j@NLO + 1,2,3j@LO."
evgenConfig.keywords = ["SM", "diboson", "1lepton", "neutrino", "2jet", "NLO", "systematic" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Sherpa_CT10_WlvZqq"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=0.25;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])/4.0}

  %tags for process setup
  NJET:=3; LJET:=2; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1
  METS_CLUSTER_MODE=16

  % decay setup
  HARD_DECAYS=1
  STABLE[24]=0
  WIDTH[24]=0
  STABLE[23]=0
  WIDTH[23]=0
  HDH_ONLY_DECAY={-24,-12,11}|{-24,-14,13}|{-24,-16,15}|{24,12,-11}|{24,14,-13}|{24,16,-15}|{23,1,-1}|{23,2,-2}|{23,3,-3}|{23,4,-4}|{23,5,-5}
}(run)

(processes){
  Process 93 93 -> 24 23 93{NJET};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;

  Process 93 93 -> -24 23 93{NJET};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[24]=0", "WIDTH[23]=0" ]
