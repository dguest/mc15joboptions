include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "llgjj(j)-EW production (EW(5))"
evgenConfig.keywords = ["SM", "diboson", "2lepton", "VBS"]
evgenConfig.contact  = ["louis.helary@cern.ch"]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "tautaugjj_EW5"

evgenConfig.process="""
(run){
  EW_SCHEME 3;
  ACTIVE[25]=1;
  MASS[25]=125.0;
  WIDTH[25]=0.00418;
  MASSIVE[11]=1;
  MASSIVE[13]=1;
  MASSIVE[15]=1;
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;

  %scales, tags for scale variations
  EXCLUSIVE_CLUSTER_MODE=1;
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])/4.0}

  %solves problem with dipole QED modeling
  ME_QED_CLUSTERING_THRESHOLD=5;

  % improve integration performance
  PSI_ITMIN=25000;
  CDXS_VSOPT=5;
  INTEGRATION_ERROR 0.05;

}(run)


(processes){
  Process 93 93 -> 15 -15 22 93 93;
  Order (EW,5);
  CKKW sqr(20/E_CMS);
  End process;
}(processes)

(selector){
  PT 991 5 E_CMS;
  NJetFinder 2 15. 0. 0.4 -1;
  PT 22  10 E_CMS;
  PseudoRapidity 22 -3	3;
  Mass 15 -15 10 E_CMS
  DeltaR 22 90 0.1 1000
  IsolationCut  22  0.1  2  0.10
# IsolationCut 22 <dR> <exponent> <epsilon>
}(selector)
"""
