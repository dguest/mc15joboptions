##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B+ -> J/psi(mu3.5mu3.5) K+ 
##############################################################
f = open("BU_JPSI_K_USER.DEC","w")
f.write("Particle  B+ 5.27929 0.0\n") # PDG2014 mass
f.write("Particle  B- 5.27929 0.0\n") # PDG2014 mass
f.write("Define dm_incohMix_B0 0.0\n") #disable neutral meson mixing
f.write("Define dm_incohMix_B_s0 0.0\n") #same, LEAVE AS B_s0
f.write("Alias myJ/psi J/psi\n")
f.write("Decay B+\n")
f.write("1.0000  myJ/psi   K+             SVS;\n")
f.write("Enddecay\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+  mu-             VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################

evgenConfig.description = "Exclusive Bplus->Jpsi_mu3p5mu3p5_Kplus production"
evgenConfig.keywords = ["exclusive","Bplus","Jpsi","2muon"]
evgenConfig.minevents = 2000

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/Pythia8B_ShowerWeights.py")

### Put the content of MC15JobOptions/Pythia8B_exclusiveB_Common.py
### except actual closing B decays

# Monash
genSeq.Pythia8B.Commands += [ 'Tune:ee = 7' ]
genSeq.Pythia8B.Commands += [ 'Tune:pp = 14' ]
genSeq.Pythia8B.Commands += [ 'PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed' ]

# Set B+ mass in Pythia equal to that in EvtGen 
genSeq.Pythia8B.Commands += ['521:m0 = 5.27929'] # PDG2014 mass

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")
###
###

include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 11.']

genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 7.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 5

#Fragmentation function choice
genSeq.Pythia8B.Commands += ['StringZ:usePetersonB=on']

# Make Pythia select the events with B+
genSeq.Pythia8B.SignalPDGCodes = [ 521 ]

genSeq.EvtInclusiveDecay.userDecayFile = "BU_JPSI_K_USER.DEC"

# Final state selections
filtSeq.BSignalFilter.LVL1MuonCutOn = True
filtSeq.BSignalFilter.LVL2MuonCutOn = True
filtSeq.BSignalFilter.LVL1MuonCutPT = 5500 
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter.LVL2MuonCutPT = 5500
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.6

filtSeq.BSignalFilter.B_PDGCode = 521
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT = 900.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta = 2.6

#Jet filter
include("MC15JobOptions/JetFilterAkt4.py")
filtSeq.QCDTruthJetFilter.MinPt = 15*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 13000*GeV
