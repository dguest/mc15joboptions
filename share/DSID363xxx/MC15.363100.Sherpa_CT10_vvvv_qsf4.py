include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "4 neutrinos with 0j@NLO + 1,2,3j@LO."
evgenConfig.keywords = ["SM", "diboson", "neutrino", "NLO", "systematic" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Sherpa_CT10_vvvv"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=4.00;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1
  METS_CLUSTER_MODE=16

  SOFT_SPIN_CORRELATIONS=0 # due to stalling events, reenable with Sherpa 2.2.0
}(run)

(processes){
  Process 93 93 -> 91 91 91 91 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;
}(processes)
"""
