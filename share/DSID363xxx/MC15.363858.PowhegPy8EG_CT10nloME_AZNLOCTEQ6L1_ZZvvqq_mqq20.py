#--------------------------------------------------------------
# Pythia8 showering with main31 and AZNLO CTEQ6L1 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_Var1Down_EvtGen_Common.py')
genSeq.Pythia8.UserModes += ['Main31:NFinal = 2']
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Diboson ZZ->vvqq production with AZNLO CTEQ6L1 tune and mqqmin20'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'ZZ', '2jet', 'neutrino' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'christian.johnson@cern.ch','carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch' ]
evgenConfig.minevents   = 5000
evgenConfig.inputfilecheck = "TXT"
