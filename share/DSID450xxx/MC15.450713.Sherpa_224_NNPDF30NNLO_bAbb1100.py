include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa 5FS pp-> bA+X with A-> b ~b with up to 4 jets in ME+PS"
evgenConfig.keywords = [ "BSMHiggs", "bHiggs","bbbar"]
evgenConfig.contact  = [ "timb@slac.stanford.edu" ]
evgenConfig.minevents = 10000 

genSeq.Sherpa_i.RunCard="""
(run){
  MASS[25]=1100.; WIDTH[25]=0.;
  YUKAWA[5] 4.8;
  PARTICLE_CONTAINER 98 B 5 -5;
  CSS_IS_AS_FAC 1;
  HARD_DECAYS On;
  HDH_STATUS[25,5,-5]=2;
  HDH_BR_WEIGHTS=1;
}(run);

(processes){
  Process 93 93 -> 25 98 93{1};
  CKKW sqr(20.0/E_CMS);
  Order (*,1);
  End process;
}(processes);

(selector){
  PT 98 1 E_CMS;
}(selector);
"""

genSeq.Sherpa_i.Parameters += [ "HDH_WIDTH[25,5,-5]=35.276" ]
genSeq.Sherpa_i.Parameters += [ "HDH_WIDTH[25,15,-15]=2.333" ]
genSeq.Sherpa_i.Parameters += [ "HDH_WIDTH[25,6,-6]=0.081" ]
genSeq.Sherpa_i.Parameters += [ "HDH_WIDTH[25,24,-24]=0." ]
genSeq.Sherpa_i.Parameters += [ "HDH_WIDTH[25,23,23]=0." ]
genSeq.Sherpa_i.Parameters += [ "HDH_WIDTH[25,13,-13]=0." ]
genSeq.Sherpa_i.Parameters += [ "HDH_WIDTH[25,4,-4]=0." ]
genSeq.Sherpa_i.Parameters += [ "HDH_WIDTH[25,3,-3]=0." ]
genSeq.Sherpa_i.Parameters += [ "HDH_WIDTH[25,21,21]=0." ]
genSeq.Sherpa_i.Parameters += [ "HDH_WIDTH[25,22,22]=0." ]
