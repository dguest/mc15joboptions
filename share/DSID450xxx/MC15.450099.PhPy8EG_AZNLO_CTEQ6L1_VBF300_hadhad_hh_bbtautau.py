
#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

H_Mass = 300.0
H_Width = 0.00407

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = H_Mass
PowhegConfig.width_H = H_Width

# CPS for the BSM Higgs
PowhegConfig.higgsfixedwidth = 0
PowhegConfig.complexpolescheme = 0 

 # Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 8.

# Generate Powheg events
PowhegConfig.generate()

#PowhegConfig.generateRunCard()
#PowhegConfig.generateEvents()
#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
infile = glob.glob("*.events")[0]
f1 = open( infile )
newfile = infile+'.temp'
f2 = open(newfile,'w')
for line in f1:
    if line.startswith('      25     1'):
        f2.write(line.replace('      25     1','      35     1'))
    else:
        f2.write(line)
f1.close()
f2.close()
os.system('mv %s %s '%(infile, infile+'.old') )
os.system('mv %s %s '%(newfile, infile) )


#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
#genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]
genSeq.Pythia8.UserHooks += [ 'PowhegMain31']
genSeq.Pythia8.Commands += [ 'POWHEG:nFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 
                             'Higgs:useBSM = on',

                             '35:m0 = '+str(H_Mass),
                             '35:mWidth = '+str(H_Width),
                             '35:doForceWidth = on',
                             '35:onMode = off',
                             '35:onIfMatch = 25 25', # H->hh

                             '25:onMode = off',
                             '25:oneChannel = 1 0.5 100 5 -5', #h->bb
                             '25:addChannel = 1 0.5 100 15 -15', #h->tautau
                             '25:m0 125.0', #scalar mass
                             '25:mMin 124.5', #scalar mass
                             '25:mMax 125.5', #scalar mass
                             '25:mWidth 0.01', # narrow width
                             '25:tau0 0', #scalarlife time
                             ]

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H(300GeV)->hh->bbtautau, hadhad"
evgenConfig.generators = [ "Powheg", "Pythia8", "EvtGen"] 
evgenConfig.keywords    = [  "BSMHiggs", "VBF" ]
evgenConfig.contact     = [ 'Danilo Ferreira de Lima <dferreir@cern.ch>']
evgenConfig.inputconfcheck = "VBF300_hadhad_hh_bbtautau"


if not hasattr(filtSeq, "XtoVVDecayFilter"):
    from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilter
    filtSeq += XtoVVDecayFilter("scalarFilter")
filtSeq.scalarFilter.PDGGrandParent = 35
filtSeq.scalarFilter.PDGParent = 25
filtSeq.scalarFilter.StatusParent = 22
filtSeq.scalarFilter.PDGChild1 = [15]
filtSeq.scalarFilter.PDGChild2 = [5]




#------------------------------------------------------------
#XtoVVDecayFilterExtended
#------------------------------------------------------------
include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]

#---------------------------------------------------------------------------------------------------
# Filter for 2 leptons (inc tau(had)) with pt cuts on e/mu and tau(had)
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("TauPtFilter")
filtSeq.TauPtFilter.IncludeHadTaus = True
filtSeq.TauPtFilter.NLeptons = 2
filtSeq.TauPtFilter.MinPt = 13000.
filtSeq.TauPtFilter.MinVisPtHadTau = 15000.
filtSeq.TauPtFilter.MaxEta = 3.

#---------------------------------------------------------------------------------------------------
# Leading tau filter
#---------------------------------------------------------------------------------------------------
filtSeq += MultiElecMuTauFilter("LeadTauPtFilter")
filtSeq.LeadTauPtFilter.IncludeHadTaus = True
filtSeq.LeadTauPtFilter.NLeptons = 1
filtSeq.LeadTauPtFilter.MinPt = 13000.
filtSeq.LeadTauPtFilter.MinVisPtHadTau = 35000.
filtSeq.LeadTauPtFilter.MaxEta = 3.

filtSeq.Expression = "scalarFilter and XtoVVDecayFilterExtended and TauPtFilter and LeadTauPtFilter"
