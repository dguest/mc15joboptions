include('MC15JobOptions/MadGraphControl_loopZp2Hy.py')

evgenConfig.description = "Loop induced Zprime decay model for Spin-1 qq->Zprime->Higgs+gamma 800 GeV narrow width resonance in b-bbar decay model"
evgenConfig.keywords = ["exotic", "Higgs", "photon", "LO", "bbbar", "spin1"]
