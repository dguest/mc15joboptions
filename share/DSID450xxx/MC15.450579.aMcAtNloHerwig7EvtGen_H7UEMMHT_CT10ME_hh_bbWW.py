#--------------------------------------------------------------
# Herwig 7 showering setup
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering

include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop No")
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune = "H7-UE-MMHT"

# add EvtGen 
include("MC15JobOptions/Herwig7_EvtGen.py")

#HW7 settings and Higgs BR
Herwig7Config.add_commands ("""
#set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
#set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-; h0->b,bbar;
do /Herwig/Particles/h0:PrintDecayModes
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio  0.5
""")

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
if (runArgs.runNumber == 450579):
    evgenConfig.description = "SM diHiggs production, decay to bbWW, with MG5_aMC@NLO, inclusive of box diagram FF."
    evgenConfig.keywords = ["SM", "SMHiggs", "nonResonant", "WW", "bbbar"]

evgenConfig.contact = ['Arnaud Ferrari <Arnaud.Ferrari@cern.ch>']
#evgenConfig.inputfilecheck = 'hh_NLO_EFT_FF_HERWIGPP_CT10' 
evgenConfig.inputfilecheck = 'hh_NLO_EFT_FF_HERWIG7_CT10'
evgenConfig.minevents   = 10000

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter 
filtSeq += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("hWWFilter", PDGParent = [25], PDGChild = [24])

from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter

filtSeq += DecaysFinalStateFilter("OneLepFilter", PDGAllowedParents = [24, -24], NQuarks = 2, NChargedLeptons = 1, NNeutrinos = 1)

filtSeq.Expression = "hbbFilter and hWWFilter and OneLepFilter"
#filtSeq.Expression = "hbbFilter and hWWFilter"

# run Herwig7 
Herwig7Config.run()
