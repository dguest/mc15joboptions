job_option_name = runArgs.jobConfig[0]
check_tag_list = job_option_name.split('_')[3:-1]
file_check_name = '_'.join(check_tag_list)
include('MC15JobOptions/MCFM_Pythia8_A14_NNPDF23LO_Common.py')
evgenConfig.inputfilecheck = file_check_name
