#--------------------------------------------------------------
# Powheg ggH_quark_mass_effects setup
#--------------------------------------------------------------

def decaywidth(cAA = 1, MALP = 1):
    import math

    sw2=0.23
    cBB = cAA*(1 - sw2)
    cWW = cAA*sw2
    aEWM1=127.9
    aEW=1/aEWM1
    ee = 2*math.sqrt(aEW)*math.sqrt(math.pi)
    Lambda=1000

    return (pow(MALP,2)*((8*cBB*cBB*pow(ee,4)*pow(MALP,4))/pow(Lambda,2) + (16*cBB*cWW*pow(ee,4)*pow(MALP,4))/pow(Lambda,2) + (8*cWW*cWW*pow(ee,4)*pow(MALP,4))/pow(Lambda,2)))/(32.*math.pi*pow(MALP,3))

def lifetime(width=1):
    return 3.0E11*6.582E-16/float(width)/1E9 ##mm


include('PowhegControl/PowhegControl_ggF_H_Common.py')

config_names = runArgs.jobConfig[0].split('.')[2].split('_')
A_Mass = float(config_names[4][2:].replace('p','.'))
Cyy = float(config_names[5][3:].replace('p','.'))
Czh = float(config_names[6][3:].replace('p','.'))


A_Width = decaywidth(Cyy,A_Mass)
A_Ctau = lifetime(A_Width)
print '================================================================='
print 'Mass of ALP: ',A_Mass,' Cyy: ',Cyy,' Czh: ',Czh,' Ctau: ',A_Ctau
print '================================================================='



# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 125.
PowhegConfig.width_H = 0.00407

# Turn on the heavy quark effect
#PowhegConfig.use_massive_b = True
#PowhegConfig.use_massive_c = True

# Complex pole scheme or not (1 for NWA and 3(CPS) for SM)
PowhegConfig.bwshape = 3

# Dynamical scale (sqrt(pT(H)^2+mH^2) real emission contributions)
# Note: r2330 does not support this option. please use newer versions.
PowhegConfig.runningscale = 2

# EW correction
if PowhegConfig.mass_H <= 1000.:
  PowhegConfig.ew = 1
else:
  PowhegConfig.ew = 0

# Set scaling and masswindow parameters
hfact_scale    = 1.2
masswindow_max = 30.

# Calculate an appropriate masswindow and hfact
masswindow = masswindow_max
if PowhegConfig.mass_H <= 700.:
  masswindow = min( (1799.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
else:
  masswindow = min( (1999.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
PowhegConfig.masswindow = masswindow
PowhegConfig.hfact = PowhegConfig.mass_H / hfact_scale

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 2.
# PowhegConfig.nEvents *= 10. Could x10 as in 2mu2tau case

PowhegConfig.generate()

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys
infile = 'PowhegOTF._1.events'
f1 = open( infile )
newfile = infile+'.temp'
f2 = open(newfile,'w')
for line in f1:
  if line.startswith('      25     1'):
    f2.write(line.replace('      25     1','      35     1'))
  else:
    f2.write(line)
f1.close()
f2.close()
os.system('mv %s %s '%(infile, infile+'.old') )
os.system('mv %s %s '%(newfile, infile) )


#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

# Can instead not use EvtGen. From James' D*gamma search.
# He doesn't know if mistake or because inclusive production made it necessary
# #--------------------------------------------------------------
# # Pythia8 showering
# # note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_Common.py
# #--------------------------------------------------------------
# include('MC15JobOptions/nonStandard/Pythia8_AZNLO_CTEQ6L1_Common.py')


#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 1' ] # these user modes not present in the Haa2mu2tau JOs for some reason. This is the number of outgoing particles from the Born-level process. 2 by default, 1 in most other files
genSeq.Pythia8.UserModes += [ 'WeakZ0:gmZmode = 2'] #
# Or use BSM style decay
H_Mass = 125.0
H_Width = 0.00407
A_MassMin = float(A_Mass) - float(A_Width)
A_MassMax = float(A_Mass) + float(A_Width)
genSeq.Pythia8.Commands += [
  'Higgs:useBSM = on',
  '35:m0 = '+str(H_Mass),
  '35:mWidth = '+str(H_Width),
  '35:doForceWidth = on',
  '35:onMode = off',
  '35:onIfMatch = 23 36', # h->Za
  '23:onMode = off',
  '23:onIfAny = 11 13 15',
  '36:onMode = off', # decay of the a
  '36:onIfAny = 22', # decay of the yy
  '36:m0 = '+str(A_Mass), #scalar mass
  '36:mWidth = '+str(A_Width), # narrow width
  #'36:mMin = '+str(A_MassMin), # narrow width
  #'36:mMax = '+str(A_MassMax), # narrow width
  '36:tau0 = '+str(A_Ctau), #scalar lifetime [mm/c]
  'ParticleDecays:tau0Max = 10000.0', #some very large value
  'ParticleDecays:limitTau0 = off',
  ]

testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->a0(yy)Z(ll), mH=125 GeV, mA0="+str(A_Mass)+" GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs","Z", "mH125" ]
evgenConfig.contact     = [ 'renjie.wang@cern.ch' ]
evgenConfig.generators  = [ "Pythia8", "Powheg", "EvtGen" ]


