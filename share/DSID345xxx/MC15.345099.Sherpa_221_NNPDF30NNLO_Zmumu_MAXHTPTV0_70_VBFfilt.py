include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> mu mu + 0,1,2j@NLO + 3,4j@LO with 0 GeV < max(HT, pTV) < 70 GeV with VBF filter."
evgenConfig.keywords = ["SM", "Z", "2muon", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "xin.chen@cern.ch" ]
evgenConfig.minevents = 50
evgenConfig.inputconfcheck = "Zmumu_MAXHTPTV0_70"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=4; LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  %settings for MAX(HT,PTV) slicing
  HTMIN:=0
  HTMAX:=70
}(run)

(processes){
  Process 93 93 -> 13 -13 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  Enhance_Factor 0.0 {6}
  End process;
}(processes)

(selector){
  Mass 13 -13 40.0 E_CMS
  FastjetMAXHTPTV  HTMIN  HTMAX  antikt  20.0  0.0  0.4
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "SHERPA_LDADD=SherpaFastjetMAXHTPTV" ]

include("MC15JobOptions/AntiKt4TruthJets_pileup.py")

# Set up VBF filters
if not hasattr(filtSeq, "VBFForwardJetsFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
  VBFfilter = VBFForwardJetsFilter("VBFfilter")
  filtSeq += VBFfilter

filtSeq.VBFfilter.JetMinPt=20.*GeV
filtSeq.VBFfilter.JetMaxEta=5.0
filtSeq.VBFfilter.NJets=2
filtSeq.VBFfilter.Jet1MinPt=30.*GeV
filtSeq.VBFfilter.Jet1MaxEta=5.0
filtSeq.VBFfilter.Jet2MinPt=20.*GeV
filtSeq.VBFfilter.Jet2MaxEta=5.0
filtSeq.VBFfilter.UseOppositeSignEtaJet1Jet2=False
filtSeq.VBFfilter.TruthJetContainer="AntiKt4TruthJets"
filtSeq.VBFfilter.LGMinPt=15.*GeV
filtSeq.VBFfilter.LGMaxEta=2.5
filtSeq.VBFfilter.DeltaRJLG=0.05
filtSeq.VBFfilter.RatioPtJLG=0.3
filtSeq.VBFfilter.UseLeadingJJ=True
filtSeq.VBFfilter.MassJJ = 300.*GeV
filtSeq.VBFfilter.DeltaEtaJJ = 3.0
