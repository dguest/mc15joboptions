#--------------------------------------------------------------      
# POWHEG+MiNLO+Herwig7 H+Z+jet->vvbbar production                  
#--------------------------------------------------------------      
#--------------------------------------------------------------      
# Herwig7 showering                                                  
#--------------------------------------------------------------      
include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3ME_LHEF_EvtGen_Common.py")
include("MC15JobOptions/Herwig7_701_StripWeights.py")

from Herwig7_i import config as hw

genSeq.Herwig7.Commands += hw.powhegbox_cmds().splitlines()

## only consider H->bb decays                                        
genSeq.Herwig7.Commands += [
  '## force H->bb decays',
  'do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar;',
  'do /Herwig/Particles/h0:PrintDecayModes' # print out decays modes and branching ratios to the terminal/log.generate                                                         
]

#--------------------------------------------------------------      
# EVGEN configuration                                                
#--------------------------------------------------------------      
evgenConfig.description = "POWHEG+MiNLO+Herwig7 H+Z+jet->vvbbar production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Herwig7' ]
#evgenConfig.inputconfcheck = "integration_grids"                    
evgenConfig.minevents   = 50
evgenConfig.inputfilecheck = "TXT"

evgenConfig.process = "qq->ZH, H->bb, Z->vv"
