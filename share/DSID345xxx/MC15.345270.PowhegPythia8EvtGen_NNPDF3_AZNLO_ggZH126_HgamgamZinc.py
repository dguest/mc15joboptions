#--------------------------------------------------------------
# POWHEG+Pythia8 gg->H+Z->gamgamqqbar production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggF_HZ_Common.py')

PowhegConfig.runningscales = 1 # 
PowhegConfig.vdecaymode = 10 # Z->inc
PowhegConfig.hdecaymode = -1 #Pythia will handle h decays
PowhegConfig.mass_Z_low = 10

PowhegConfig.mass_H  = 126
PowhegConfig.width_H = 0.00407

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
#PowhegConfig.ptVhigh = 200 # step-wise pTV reweighting
#PowhegConfig.ptVlow = 120
##PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

PowhegConfig.storeinfo_rwgt = 1 # store info for PDF / scales variations reweighting
PowhegConfig.PDF = range(260000, 260101) + range( 90400, 90433 )#  PDF variations
PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]
PowhegConfig.kappa_ghz = [1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0]
PowhegConfig.kappa_ght = [1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0]
PowhegConfig.kappa_ghb = [1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0]

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2']
#--------------------------------------------------------------
# Higgs->gamgam at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 22 22' ]
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 gg->H+Z->gamgamqqbar production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZHiggs" ]
evgenConfig.contact     = [ 'cyril.becot@cern.ch', 'carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch' ]
evgenConfig.process     = "gg->ZH, H->gamgam, Z->inc"
evgenConfig.inputconfcheck = "ggZH126_Zinc_NNPDF3"
evgenConfig.minevents   = 2000
