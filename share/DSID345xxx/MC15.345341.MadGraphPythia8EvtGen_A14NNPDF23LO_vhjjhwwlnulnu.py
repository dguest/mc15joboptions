from MadGraphControl.MadGraphUtils import *

#Some setup variables.
mode=0

from os import environ,path
environ['ATHENA_PROC_NUMBER'] = '1'

#Need extra events to avoid Pythia8 problems.
safefactor=50.0
nevents=5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

######################################################################
# Configure event generation.
######################################################################

runName='run_01'     

#Set input file for Pythia8.
runArgs.inputGeneratorFile = runName+'._00001.events.tar.gz'

#Make process card.
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model HC_NLO_X0_UFO-no_b_mass
define p = g d d~ u u~ s s~ c c~ b b~                                   
define j = g d d~ u u~ s s~ c c~ b b~                                 
define v = w+ w- z
generate p p > x0 v / a [QCD]
output ppx0_4l_NLOQCD_FxFx_MadSpin""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#Define process directory.
process_dir = new_process()

#Fetch default run_card.dat and set parameters
extras = {'lhe_version'   :'3.0',
          'parton_shower' :'PYTHIA8',
          'reweight_scale':'True',
          'maxjetflavor'  :'5',
          'pdlabel'       :"'lhapdf'",
          'lhaid'         :260000,
          'pdfwgt'        :'T',
          'use_syst'      :'T',
          'sys_scalefact' :'None',
          'sys_alpsfact'  :'None',
          'sys_matchscale':'None',
          'sys_pdf'       :'NNPDF30_nlo_as_0118'}

#Set couplings here.
parameters={'frblock': {
        'Lambda': 1000.0,
        'cosa'  : 1.0,
        'kSM'   : 1.0,
        'kHtt'  : 0.0,
        'kAtt'  : 0.0,
        'kHll'  : 0.0,
        'kAll'  : 0.0,
        'kHaa'  : 0.0,
        'kAaa'  : 0.0,
        'kHza'  : 0.0,
        'kAza'  : 0.0,
        'kHzz'  : 0.0,
        'kAzz'  : 0.0,
        'kHww'  : 0.0,
        'kAww'  : 0.0,
        'kHda'  : 0.0,
        'kHdz'  : 0.0,
        'kHdwR' : 0.0,
        'kHdwI' : 0.0
        }}

#Create parame card
build_param_card(param_card_old=path.join(process_dir,'Cards/param_card.dat'),param_card_new='param_card_new.dat',
                 masses={'25': '1.250000e+02'},params=parameters)

#Create run card.
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

#Create MadSpin decay card.
madspin_card_loc='madspin_card.dat'

#NOTE: Can't set random seed in due to crashes when using "set spinmode none".
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
 set spinmode none
#
# specify the decay for the final state particles
define l- = e- mu- ta-
define l+ = e+ mu+ ta+
define v = w+ w- z
decay v > j j
decay x0 > w+ w- > l+ vl l- vl~
# running the actual code
launch""")
mscard.close()

print_cards()

#Run the event generation!
generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runArgs.inputGeneratorFile,lhe_version=3,saveProcDir=True)  

######################################################################
# End of event generation, start configuring parton shower here.
######################################################################

print "Now performing parton showering ..."
   
#### Shower 
environ['ATHENA_PROC_NUMBER'] = '1'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

#--------------------------------------------------------------
# DF Dilepton filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter,MultiElectronFilter,MultiMuonFilter
filtSeq += MultiLeptonFilter("Multi1TLeptonFilter")
filtSeq += MultiLeptonFilter("Multi2LLeptonFilter")
filtSeq += MultiElectronFilter("Multi1ElectronFilter")
filtSeq += MultiMuonFilter("Multi1MuonFilter")

Multi1TLeptonFilter = filtSeq.Multi1TLeptonFilter
Multi1TLeptonFilter.Ptcut = 10000.
Multi1TLeptonFilter.Etacut = 2.7
Multi1TLeptonFilter.NLeptons = 1

Multi2LLeptonFilter = filtSeq.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 5000.
Multi2LLeptonFilter.Etacut = 2.7
Multi2LLeptonFilter.NLeptons = 2

Multi1ElectronFilter = filtSeq.Multi1ElectronFilter
Multi1ElectronFilter.Ptcut  = 5000.
Multi1ElectronFilter.Etacut = 2.7
Multi1ElectronFilter.NElectrons = 1

Multi1MuonFilter = filtSeq.Multi1MuonFilter
Multi1MuonFilter.Ptcut  = 5000.
Multi1MuonFilter.Etacut = 2.7
Multi1MuonFilter.NMuons = 1

filtSeq.Expression = "(Multi1TLeptonFilter) and (Multi2LLeptonFilter) and (Multi1ElectronFilter) and (Multi1MuonFilter)"

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = "VH(had) 125 GeV Higgs production in the Higgs Characterization model decaying to wwlnulnu."
evgenConfig.keywords = ['BSM','Higgs','mH125','BSMHiggs','2lepton','dijet','resonance','WW']

evgenConfig.contact = ['Adam Kaluza <Adam.Kaluza@cern.ch>']
evgenConfig.inputfilecheck = runName
