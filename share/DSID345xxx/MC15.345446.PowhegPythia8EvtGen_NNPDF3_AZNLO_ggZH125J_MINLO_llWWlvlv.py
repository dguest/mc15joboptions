#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 gg->Z+H+jet->llWW->lllvlv production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2']
#--------------------------------------------------------------
# Higgs->WW->lvlv at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 24 24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onMode = off',
                             '24:onIfAny = 11 12 13 14 15 16']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->H+Z+jet->lllvlv production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'yongke.zhao@cern.ch', 'weimin.song@cern.ch' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.process = "gg->ZH, H->WW, Z->ll"
