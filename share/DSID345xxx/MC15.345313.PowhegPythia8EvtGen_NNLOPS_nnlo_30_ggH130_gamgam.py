#--------------------------------------------------------------
# Powheg HJ setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Hj_Common.py')


# 90400 PDF4LHC15_nnlo_30_pdfas 33

  # NNPDF30_nnlo central+PDF4LHC15_nnlo central +  all 101 NNPDF30_nlo variations + all 30 PDF variations + alphaS (31 + 32) for PDF4LHC15_nlo
PowhegConfig.PDF = [261000]+[91400]+range(260000,260101)+range(90400,90433)+[11000]+[25200]+[13100]
PowhegConfig.mass_H = 130.0
PowhegConfig.width_H = 0.00407

  # Powheg QCD scale variations
  #   The frist weight is the nominal. (mu0 = mH, I think)
  #   Second will be stored as 1001, i.e. mu_fact = mu0 * 0.5, mu_ren = mu0 * 0.5
  #   Third  will be stored as 1002, i.e. mu_fact = mu0 * 0.5, mu_ren = mu0 * 1.0, etc
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]

  # which quark mass varations to save.
PowhegConfig.define_event_weight_group( group_name='quark_mass_variation', parameters_to_vary=['bmass_in_minlo','quarkmasseffects'] )

  # mtmb means both top and b masses are considered in the loop. This is the nominal choice.
  # mtinf is 'EFT', meaning the top quark mass is set to infinity, and mb=0 (ignored)
  # mtmb-bminlo is an alternative treatment of the b-mass
  # these weghts are stored in a separate weight group, and I think we will have:
  # 3001 = mtmb (copy of nominal Powheg weight = weight 0), 3002 = mtinf, 3003 mtmb-bminlo
PowhegConfig.add_weight_to_group( group_name='quark_mass_variation', weight_name='mtmb', parameter_values=[0,1] )
PowhegConfig.add_weight_to_group( group_name='quark_mass_variation', weight_name='mtinf', parameter_values=[0,0] )
PowhegConfig.add_weight_to_group( group_name='quark_mass_variation', weight_name='mtmb-bminlo', parameter_values=[1,1] )

  # NNLO reweighing. Input from HNNLO, produced by Kathrin Becker
  # HH = ren and fact set to Half higgs mass (nominal choice for Run-II)
  # 11 = ren and fact QCD scales set to the Higgs mass.  QQ => mu=mH/4.  22 => mu=2*mH
#PowhegConfig.NNLO_reweighting_inputs['nnlo-mtinf'] = 'H1250-CM13-NNPDF3-APX0-HH.top'
#PowhegConfig.NNLO_reweighting_inputs['nnlo'] = 'H1250-CM13-NNPDF3-APX2-HH.top'
PowhegConfig.NNLO_reweighting_inputs['nnlo-pdflhc'] = 'H1300_CM13_PDF4LHC30-APX2-HH.top'
#PowhegConfig.NNLO_reweighting_inputs['nnlo-QCDdn'] = 'H1250-CM13-NNPDF3-APX2-QQ.top'
#PowhegConfig.NNLO_reweighting_inputs['nnlo-QCDup'] = 'H1250-CM13-NNPDF3-APX2-11.top'
  # first 3 quark mass weights
  # Nominal, EFT (consistently in HNNLO and Powheg) then alterntive b-mass treatement (Powheg variation)
  # 
#PowhegConfig.NNLO_output_weights["nnlops-nominal"] = "combine 'nnlo-pdflhc' and '0'"
PowhegConfig.NNLO_output_weights["nnlops-nominal-pdflhc"] = "combine 'nnlo-pdflhc' and '2001'"
#PowhegConfig.NNLO_output_weights["nnlops-mtinf"] = "combine 'nnlo-mtinf' and '3002'"
PowhegConfig.NNLO_output_weights["nnlops-bminlo"] = "combine 'nnlo-pdflhc' and '3003'"
  # nominal NNLO scale (mH/2 in HNNLO), with varied Powheg scales
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgDnDn"] = "combine 'nnlo-pdflhc' and '1001'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgDnNom"] = "combine 'nnlo-pdflhc' and '1002'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgDnUp"] = "combine 'nnlo-pdflhc' and '1003'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgNomDn"] = "combine 'nnlo-pdflhc' and '1004'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgNomUp"] = "combine 'nnlo-pdflhc' and '1005'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgUpDn"] = "combine 'nnlo-pdflhc' and '1006'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgUpNom"] = "combine 'nnlo-pdflhc' and '1007'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgUpUp"] = "combine 'nnlo-pdflhc' and '1008'"
  # low NNLO scale (mH/4 in HNNLO), with varied Powheg scales
#PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgNomNom"] = "combine 'nnlo-QCDdn' and '0'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgDnDn"] = "combine 'nnlo-QCDdn' and '1001'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgDnNom"] = "combine 'nnlo-QCDdn' and '1002'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgDnUp"] = "combine 'nnlo-QCDdn' and '1003'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgNomDn"] = "combine 'nnlo-QCDdn' and '1004'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgNomUp"] = "combine 'nnlo-QCDdn' and '1005'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgUpDn"] = "combine 'nnlo-QCDdn' and '1006'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgUpNom"] = "combine 'nnlo-QCDdn' and '1007'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgUpUp"] = "combine 'nnlo-QCDdn' and '1008'"
# # high NNLO scale (mu=mH0) and varied powheg scales
#PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgNomNom"] = "combine 'nnlo-QCDup' and '0'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgDnDn"] = "combine 'nnlo-QCDup' and '1001'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgDnNom"] = "combine 'nnlo-QCDup' and '1002'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgDnUp"] = "combine 'nnlo-QCDup' and '1003'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgNomDn"] = "combine 'nnlo-QCDup' and '1004'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgNomUp"] = "combine 'nnlo-QCDup' and '1005'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgUpDn"] = "combine 'nnlo-QCDup' and '1006'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgUpNom"] = "combine 'nnlo-QCDup' and '1007'"
#PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgUpUp"] = "combine 'nnlo-QCDup' and '1008'"

PowhegConfig.generate()

  # AZNLO tune
include("MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py")

  # use "Main31", i.e. "Power-shower + jet veto"
include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

  # Decay to diphoton
genSeq.Pythia8.Commands  += [ '25:onMode = off', '25:onIfMatch = 22 22' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune mh = 130 GeV'
evgenConfig.keywords    = [ 'Higgs', '1jet' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'kathrin.becker@cern.ch']
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
evgenConfig.inputconfcheck = "ggH130_HJ_NNLOPS"
evgenConfig.minevents   = 2000

#
#----
