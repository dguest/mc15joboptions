#--------------------------------------------------------------          
# POWHEG+Herwig7 gg->H+Z->l+l-ccbar production                       
#--------------------------------------------------------------          
#--------------------------------------------------------------          
# Herwig7 showering                                                      
#--------------------------------------------------------------          
include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3ME_LHEF_EvtGen_Common.py")
include("MC15JobOptions/Herwig7_701_StripWeights.py")

from Herwig7_i import config as hw

genSeq.Herwig7.Commands += hw.powhegbox_cmds().splitlines()

## only consider H->cc decays                                            
genSeq.Herwig7.Commands += [
  '## force H->cc decays',
  'do /Herwig/Particles/h0:SelectDecayModes h0->c,cbar;',
  'do /Herwig/Particles/h0:PrintDecayModes' # print out decays modes and branching ratios to the terminal/log.generate              
]

#--------------------------------------------------------------          
# EVGEN configuration                                                    
#--------------------------------------------------------------          
evgenConfig.description = "POWHEG+Herwig7 gg->H+Z->l+l-ccbar production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs" ]
evgenConfig.contact     = [ 'adrian.buzatu@cern.ch', 'carlo.enrico.pandini@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Herwig7' ]
#evgenConfig.inputconfcheck = "integration_grids"                        
evgenConfig.minevents   = 5000
evgenConfig.inputfilecheck = "TXT"

evgenConfig.process = "gg->ZH, H->cc, Z->ll"

