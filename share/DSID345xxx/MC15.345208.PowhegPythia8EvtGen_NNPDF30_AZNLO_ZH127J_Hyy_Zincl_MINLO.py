#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+Z+jet-> gamgam+inc production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_HZj_Common.py')

PowhegConfig.mass_H  = 127
PowhegConfig.width_H = 0.00407

PowhegConfig.runningscales = 1 #
PowhegConfig.vdecaymode = 10 # Z->all
PowhegConfig.mass_Z_low = 10



PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

#NNPDF3.0 and PDF4LHC PDF variations
PowhegConfig.PDF = range(260000, 260101) + range(90400, 90433)
# scale variations: first pair is the nominal setting
PowhegConfig.mu_F =  [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R =  [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]
PowhegConfig.generate()


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
#--------------------------------------------------------------
# H->GammaGamma decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 22 22' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet production: Z->all, H->gammagamma"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZHiggs" ]
evgenConfig.contact     = [ 'cyril.becot@cern.ch.ch' ]
evgenConfig.inputconfcheck = "ZH127J_Hyy_Zincl_MINLO"
evgenConfig.process = "qq->ZH, H->GammaGamma, Z->all"
evgenConfig.minevents   = 100
