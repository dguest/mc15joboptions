
evgenConfig.process     = "qq->ZH, Z->all, H->mutau"
evgenConfig.description = "POWHEG+MiNLO+PYTHIA8, H+Z+jet, Z->all, H->mutau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZHiggs", "mH125" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch' ]
evgenConfig.minevents   = 100
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:oneChannel = 1 0.5 100 15 -13',
	                     '25:addChannel = 1 0.5 100 13 -15' ]
