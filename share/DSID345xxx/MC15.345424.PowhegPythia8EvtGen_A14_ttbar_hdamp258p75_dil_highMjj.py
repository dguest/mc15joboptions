#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, dilepton filter, ME NNPDF30 NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch']
evgenConfig.minevents   = 500

include('PowhegControl/PowhegControl_tt_Common.py')
# Initial settings
PowhegConfig.topdecaymode = 22200    # Dilepton
PowhegConfig.hdamp        = 258.75   # 1.5 * mtop

#PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
#PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
#PowhegConfig.PDF          = [260000, 25200, 13165, 90900]                 # NNPDF30, MMHT, CT14, PDF4LHC - PDF variations with nominal scale variation
#PowhegConfig.PDF.extend(range(260001, 260101))                            # Include the NNPDF error set
#PowhegConfig.PDF.extend(range(90901 , 90931 ))                            # Include the PDF4LHC error set

#Information on how to run with multiple weights: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PowhegForATLAS#Running_with_multiple_scale_PDF
#PDFs - you can see a listing here: https://lhapdf.hepforge.org/pdfsets.html; picked these three as they are the inputs to the PDF4LHC2015 prescription (http://arxiv.org/pdf/1510.03865v2.pdf).

# Define a weight group configuration for scale variations with different PDFs
# Nominal mu_F = mu_R = 1.0 is not required as this is captured by the PDF variation above
#PowhegConfig.define_event_weight_group( group_name='scales_pdf', parameters_to_vary=['mu_F','mu_R','PDF'] )

# Scale variations, MMHT2014nlo68clas118
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_MMHT',                   parameter_values=[ 2.0, 1.0, 25200] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_MMHT',                 parameter_values=[ 0.5, 1.0, 25200] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_MMHT',                   parameter_values=[ 1.0, 2.0, 25200] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_MMHT',                 parameter_values=[ 1.0, 0.5, 25200] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_MMHT',          parameter_values=[ 0.5, 0.5, 25200] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_MMHT',              parameter_values=[ 2.0, 2.0, 25200] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_MMHT',            parameter_values=[ 0.5, 2.0, 25200] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_MMHT',            parameter_values=[ 2.0, 0.5, 25200] )

# Scale variations, CT14nlo_as_0118
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_CT14',                   parameter_values=[ 2.0, 1.0, 13165] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_CT14',                 parameter_values=[ 0.5, 1.0, 13165] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_CT14',                   parameter_values=[ 1.0, 2.0, 13165] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_CT14',                 parameter_values=[ 1.0, 0.5, 13165] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_CT14',          parameter_values=[ 0.5, 0.5, 13165] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_CT14',              parameter_values=[ 2.0, 2.0, 13165] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_CT14',            parameter_values=[ 0.5, 2.0, 13165] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_CT14',            parameter_values=[ 2.0, 0.5, 13165] )

# Scale variations, PDF4LHC15_nlo_30
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_PDF4LHC15',              parameter_values=[ 2.0, 1.0, 90900] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_PDF4LHC15',            parameter_values=[ 0.5, 1.0, 90900] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_PDF4LHC15',              parameter_values=[ 1.0, 2.0, 90900] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_PDF4LHC15',            parameter_values=[ 1.0, 0.5, 90900] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_PDF4LHC15',     parameter_values=[ 0.5, 0.5, 90900] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_PDF4LHC15',         parameter_values=[ 2.0, 2.0, 90900] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_PDF4LHC15',       parameter_values=[ 0.5, 2.0, 90900] )
#PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_PDF4LHC15',       parameter_values=[ 2.0, 0.5, 90900] )

#Safety factor
PowhegConfig.nEvents *= 500.

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
# No TTbarWToLeptonFilter required as Powheg controls final state decay
#--------------------------------------------------------------
# VBF Mjj Interval filter
#--------------------------------------------------------------
include("MC15JobOptions/VBFMjjIntervalFilter.py")
filtSeq.VBFMjjIntervalFilter.RapidityAcceptance = 5.0
filtSeq.VBFMjjIntervalFilter.MinSecondJetPT = 15.*GeV
filtSeq.VBFMjjIntervalFilter.MinOverlapPT = 15.*GeV
filtSeq.VBFMjjIntervalFilter.TruthJetContainerName = "AntiKt4TruthJets"
filtSeq.VBFMjjIntervalFilter.NoJetProbability  = -1.0
filtSeq.VBFMjjIntervalFilter.OneJetProbability = -1.0
filtSeq.VBFMjjIntervalFilter.LowMjjProbability = 0.00502
filtSeq.VBFMjjIntervalFilter.HighMjjProbability = 1.0
filtSeq.VBFMjjIntervalFilter.LowMjj = 500.*GeV
filtSeq.VBFMjjIntervalFilter.TruncateAtLowMjj = True
filtSeq.VBFMjjIntervalFilter.HighMjj = 2500.*GeV
filtSeq.VBFMjjIntervalFilter.TruncateAtHighMjj = False
filtSeq.VBFMjjIntervalFilter.PhotonJetOverlapRemoval = True
filtSeq.VBFMjjIntervalFilter.ElectronJetOverlapRemoval = True
filtSeq.VBFMjjIntervalFilter.TauJetOverlapRemoval = True
