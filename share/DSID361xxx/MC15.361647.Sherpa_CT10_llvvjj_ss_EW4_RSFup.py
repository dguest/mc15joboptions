include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "llvvjj(j) like-sign QCD production with renormalization scale * 4"
evgenConfig.keywords = ["SM", "diboson", "2lepton", "VBS"]
evgenConfig.contact  = ["philipp.anger@cern.ch", "alexander.melzer@cern.ch", "frank.siegert@cern.ch", "atlas-generators-sherpa@cern.ch"]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Sherpa_CT10_llvvjj_ss_EW4"

evgenConfig.process="""
(run){
  EW_SCHEME 3;
  ACTIVE[25]=1;
  MASS[25]=126.0;
  WIDTH[25]=0.00418;
  MASSIVE[11]=1;
  MASSIVE[13]=1;
  MASSIVE[15]=1;
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;

  %scales, tags for scale variations
  FSCF:=1.0; RSCF:=4.0; QSCF:=1.0;
  EXCLUSIVE_CLUSTER_MODE=1;
  SCALES=STRICT_METS{FSCF*MU_F2}{RSCF*MU_R2}{QSCF*MU_Q2};
  CORE_SCALE=VAR{Abs2(p[2]+p[3]+p[4]+p[5])};
  
  %solves problem with dipole QED modeling
  ME_QED_CLUSTERING_THRESHOLD=5;

  % improve integration performance
  PSI_ITMIN=25000;
  CDXS_VSOPT=5;
  INTEGRATION_ERROR 0.05;
}(run)


(processes){
  Process 93 93 -> 11 11 -12 -12 93 93 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;

  Process 93 93 -> 13 13 -14 -14 93 93 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;

  Process 93 93 -> 15 15 -16 -16 93 93 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;

  Process 93 93 -> 11 13 -12 -14 93 93 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;

  Process 93 93 -> 11 15 -12 -16 93 93 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;

  Process 93 93 -> 13 15 -14 -16 93 93 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;

  Process 93 93 -> -11 -11 12 12 93 93 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;

  Process 93 93 -> -13 -13 14 14 93 93 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;

  Process 93 93 -> -15 -15 16 16 93 93 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;

  Process 93 93 -> -11 -13 12 14 93 93 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;

  Process 93 93 -> -11 -15 12 16 93 93 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;

  Process 93 93 -> -13 -15 14 16 93 93 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  End process;
}(processes)

(selector){
  PT 991 5 E_CMS;
  NJetFinder 2 15. 0. 0.4 -1;
  Mass 11 -11 0.1 E_CMS;
}(selector)
"""
