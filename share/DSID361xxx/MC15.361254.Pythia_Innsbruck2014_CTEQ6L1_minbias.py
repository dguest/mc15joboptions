evgenConfig.description = 'Inelastic minbias with IBK CTEQ6LL tune'
evgenConfig.contact = ['Wolfgang Lukas <Wolfgang.Lukas@cern.ch>']
evgenConfig.keywords = ['minBias', 'QCD', 'SM']

include("MC15JobOptions/Pythia_Base_Fragment.py")
include("MC15JobOptions/Pythia_Innsbruck_Common.py")

## Inelastic minbias processes
genSeq.Pythia.PythiaCommand += [
    "pysubs msel 0",
    "pysubs msub 11 1",
    "pysubs msub 12 1",
    "pysubs msub 13 1",
    "pysubs msub 28 1",
    "pysubs msub 53 1",
    "pysubs msub 68 1",
    "pysubs msub 95 1",
    "pysubs msub 92 1",
    "pysubs msub 93 1",
    "pysubs msub 94 1"]

## --- PY6.4.28
## --- switches & parameters for high energy pp -> inelastic ---
genSeq.Pythia.PythiaCommand += [
    "pypars mstp 3 1",        # different Lambda_QCD
    "pydat1 mstu 112 5",      # N_flavors = 5
    "pypars mstp 61 2",       # ISR (QCD+QED) switch
    "pypars mstp 71 1",       # FSR (QCD+QED) switch
    "pypars mstp 81 21",      # MPI & BR master switch
    "pypars mstp 111 1",      # hadronis+decay
    "pypars mstp 64 2",       # alpha_s(Q**2) choice in ISR (def=2)
    "pypars mstp 70 2",       # ISR regularisation (def=1)
    "pypars mstp 72 2",       # ptmax scale for rad betw ISR partons (def=1)
    "pypars mstp 82 5",       # MPI structure: matter overlap (def=4)
    "pypars mstp 88 0",       # collapse of junction configur. (def=1)
    "pypars mstp 95 6",       # CR: annealing model (def=1)
##
    "pypars parp 61 0.190",   # Lam_QCD for ISR
    "pypars parp 64 1.0",     # K-factor in alpha_s for ISR (def=1.0)
    "pypars parp 67 1.0",     # max.virt. scale factor for ISR (def=4.0)
    "pypars parp 71 1.0",     # max.virt. scale factor for FSR (def=4.0)
    "pypars parp 77 0.90",    # CR suppression for fast moving strings (def=0.0)
    "pypars parp 89 7000.0",  # PT0 reference Ecm (def=1800 GeV)
    "pypars parp 79 1.50",    # beam remnant x enhancement (def=2.0)
    "pypars parp 80 0.06",    # beam remnant breakup suppression (def=0.1)
    "pypars parp 91 2.0",     # intrinsic kT width (def=2.0)
    "pypars parp 93 10.0"]    # intrinsic kT cutoff (def=5.0)

## --- Fragmentation tune (02 Jul 14) to e+e- -> hadrons data (ALEPH) at Z0 pole,
## --- for the pT-ordered parton shower
genSeq.Pythia.PythiaCommand += [
    "pydat1 mstj 11 5",      # Lund+Bowler scheme for HQ fragment.
    "pydat1 mstj 12 2",      # old baryon model
    "pydat1 mstj 41 12",     # 2=PYSHOW  12=PYPTFS for gluon and photon emiss.
    "pydat1 parj 81 0.257",  # Lambda_LLA
    "pydat1 parj 82 0.90",   # p_tmin cutoff (set by hand)
    "pydat1 parj 21 0.331",  # sigma_pt
    "pydat1 parj 41 0.273",  # A of LSFF
    "pydat1 parj 42 1.431",  # B of LSFF
    "pydat1 parj 46 1.51",   # r_c
    "pydat1 parj 47 0.962",  # r_b
    "pydat1 parj 11 0.520",  # V_u,d
    "pydat1 parj 12 0.481",  # V_s
    "pydat1 parj 13 0.512",  # V_c,b
    "pydat1 parj 17 0.20",   # L=1 mesons rates
    "pydat1 parj 14 0.12",   #
    "pydat1 parj 15 0.04",   #
    "pydat1 parj 16 0.12",   #
    "pydat1 parj 26 0.216",  # eta-prime suppr.
    "pydat1 parj 2 0.266",   # s/u
    "pydat1 parj 1 0.120",   # qq/q
    "pydat1 parj 3 0.737",   # su/du
    "pydat1 parj 4 0.05",    # (qq)_1/(qq)_0
    "pydat1 parj 19 0.583",  # end-point baryon suppress.
    "pydat1 parj 45 1.664"]  # delta(A)_qq
