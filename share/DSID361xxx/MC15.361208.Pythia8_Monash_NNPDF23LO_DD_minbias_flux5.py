###############################################################
# Pythia8 minimum bias (DD) samples
#==============================================================

evgenConfig.description = "DD Minbias with the MBR model by Dinos Goulianos et al tuned on CDF data"
evgenConfig.keywords = ["minBias"]
evgenConfig.generators = ["Pythia8"]

# ... Main generator : Pythia8
include ("MC15JobOptions/nonStandard/Pythia8_Monash_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:doubleDiffractive = on"]

# this uses the MBR model by Dino Goulianos etc. tuned on CDF data
genSeq.Pythia8.Commands += ["Diffraction:PomFlux = 5"]

