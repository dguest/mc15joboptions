# Pythia8 minimum bias DD with A3

evgenConfig.description = "DD Minbias with Pythia8 A3"
evgenConfig.keywords = ["minBias","diffraction","DD"]
evgenConfig.generators = ["Pythia8"]

# include ("../Pythia8_A2_MSTW2008LO_Common.py")
include("MC15JobOptions/nonStandard/Pythia8_A3_ALT_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:doubleDiffractive = on"]


