evgenConfig.description = "Single K- with flat eta-phi and log E in [2, 200] GeV"
evgenConfig.keywords = ["singleParticle", "Kminus"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = -321
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(2000., 200000.), eta=[-2.5, 2.5])
