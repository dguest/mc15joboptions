evgenConfig.description = "gammagamma->mumu with Budnev parameterization, central lepton filter pt>10 GeV"
evgenConfig.keywords = ["lepton", "exclusive", "2photon","2muon"]
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch"]

include("MC15JobOptions/Herwigpp_Base_Fragment.py")
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(runArgs.ecmEnergy) + hw.base_cmds() + hw.pdf_gammagamma_cmds()
genSeq.Herwigpp.Commands += cmds.splitlines()
del cmds
#from Herwigpp_i import config as hw

cmds = """\

# Cuts
cd /Herwig/Cuts
set QCDCuts:ScaleMin 0.0*GeV
set QCDCuts:X1Min 0
set QCDCuts:X2Min 0
set QCDCuts:X1Max 1.
set QCDCuts:X2Max 1.
erase QCDCuts:MultiCuts 0
set QCDCuts:MHatMin 9.8*GeV
set QCDCuts:MHatMax 40*GeV

# Selected the hard process

cd /Herwig/MatrixElements
insert SimpleQCD:MatrixElements 0 /Herwig/MatrixElements/MEgg2ff
set /Herwig/MatrixElements/MEgg2ff:Process Muon
"""

genSeq.Herwigpp.Commands += cmds.splitlines()

include("MC15JobOptions/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 5000.
filtSeq.LeptonFilter.Etacut = 2.5

evgenConfig.minevents = 5000


# To avoid warning from displaced vertices, bugfix needed in herwig++
#from TruthExamples.TruthExamplesConf import TestHepMC
#testHepMC = TestHepMC()
testSeq.TestHepMC.MaxTransVtxDisp = 1000000 
testSeq.TestHepMC.MaxVtxDisp      = 1000000000 
#testSeq += testHepMC
