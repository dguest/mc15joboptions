#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+HerwigPP Diboson WZ->qqll production mllmin20'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'WZ', '2lepton', '2jet' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'christian.johnson@cern.ch','carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch' ]
evgenConfig.minevents   = 5000
evgenConfig.inputfilecheck = "TXT"
evgenConfig.generators += ["Powheg"]

# Herwig showering                                                                                                                                                    
#--------------------------------------------------------------                                                                                                  
include('MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py')
#--------------------------------------------------------------                                                 

cmds = """                                                                                                                                                                     
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General                                                                                                        
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost                                                                                            
                                                                                                                                                                               
"""

# print checks                                                                                                                                                                 
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')
genSeq.Herwigpp.Commands += cmds.splitlines()

# clean up                                                                                                                                                                     
del cmds
