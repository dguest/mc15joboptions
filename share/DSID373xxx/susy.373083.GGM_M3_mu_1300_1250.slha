#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.24900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.30000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.25000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05410195E+01   # W+
        25     1.26000000E+02   # h
        35     2.00402179E+03   # H
        36     2.00000000E+03   # A
        37     2.00150697E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00370330E+03   # ~d_L
   2000001     5.00359399E+03   # ~d_R
   1000002     5.00345891E+03   # ~u_L
   2000002     5.00351667E+03   # ~u_R
   1000003     5.00370330E+03   # ~s_L
   2000003     5.00359399E+03   # ~s_R
   1000004     5.00345891E+03   # ~c_L
   2000004     5.00351667E+03   # ~c_R
   1000005     5.00313424E+03   # ~b_1
   2000005     5.00416446E+03   # ~b_2
   1000006     5.12384545E+03   # ~t_1
   2000006     5.41767343E+03   # ~t_2
   1000011     5.00008220E+03   # ~e_L
   2000011     5.00007609E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008220E+03   # ~mu_L
   2000013     5.00007609E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99974600E+03   # ~tau_1
   2000015     5.00041289E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.42934099E+03   # ~g
   1000022     1.28280371E+03   # ~chi_10
   1000023    -1.33135658E+03   # ~chi_20
   1000025     1.36999952E+03   # ~chi_30
   1000035     3.12827765E+03   # ~chi_40
   1000024     1.32798214E+03   # ~chi_1+
   1000037     3.12827576E+03   # ~chi_2+
   1000039     1.88944444E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.19376796E-01   # N_11
  1  2    -2.91906850E-02   # N_12
  1  3     4.92553114E-01   # N_13
  1  4    -4.88913447E-01   # N_14
  2  1     3.28645564E-03   # N_21
  2  2    -3.48262483E-03   # N_22
  2  3    -7.06988888E-01   # N_23
  2  4    -7.07208443E-01   # N_24
  3  1     6.94611475E-01   # N_31
  3  2     3.17724526E-02   # N_32
  3  3    -5.06726541E-01   # N_33
  3  4     5.09640680E-01   # N_34
  4  1     1.05995669E-03   # N_41
  4  2    -9.99062704E-01   # N_42
  4  3    -2.80420144E-02   # N_43
  4  4     3.29580859E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.96325380E-02   # U_11
  1  2     9.99214322E-01   # U_12
  2  1     9.99214322E-01   # U_21
  2  2     3.96325380E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.65859974E-02   # V_11
  1  2     9.98914283E-01   # V_12
  2  1     9.98914283E-01   # V_21
  2  2     4.65859974E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99967857E-01   # cos(theta_t)
  1  2     8.01779065E-03   # sin(theta_t)
  2  1    -8.01779065E-03   # -sin(theta_t)
  2  2     9.99967857E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.68542964E-01   # cos(theta_b)
  1  2     7.43673521E-01   # sin(theta_b)
  2  1    -7.43673521E-01   # -sin(theta_b)
  2  2     6.68542964E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03860099E-01   # cos(theta_tau)
  1  2     7.10338624E-01   # sin(theta_tau)
  2  1    -7.10338624E-01   # -sin(theta_tau)
  2  2     7.03860099E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90201799E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.25000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52153621E+02   # vev(Q)              
         4     3.43935566E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52784035E-01   # gprime(Q) DRbar
     2     6.27182799E-01   # g(Q) DRbar
     3     1.08739696E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02512779E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71791925E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79805685E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.24900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.30000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.29746295E+06   # M^2_Hd              
        22    -6.93135626E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37052113E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.66399098E-08   # gluino decays
#          BR         NDA      ID1       ID2
     5.32350402E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.53604244E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     4.23237152E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     2.88710776E-03    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     7.50488162E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     6.53555861E-08    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     7.07525295E-05    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.27564322E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.09961906E-08    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.71596121E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     7.50488162E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     6.53555861E-08    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     7.07525295E-05    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.27564322E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.09961906E-08    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.71596121E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     7.32628987E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.92156236E-05    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     7.36450246E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     4.69557051E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     4.69557051E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     4.69557051E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     4.69557051E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.15831147E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.89553243E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.01104034E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.38547387E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.43717652E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     8.00775293E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.83792818E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.11783380E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.25875511E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     6.21264360E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.86786315E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.65957449E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.90088776E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.85079659E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.10449501E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.78289484E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.28158041E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.17425961E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.95733286E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.39298043E-06    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.61775343E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     3.21080268E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.55569918E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.21182177E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.39173964E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.19043086E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.15498359E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.97474701E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.32736879E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.58952836E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.69558039E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.19400849E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.38518424E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.66399939E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.51328781E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.35251792E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.04993141E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.14164156E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.41237237E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.64713016E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.56396013E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.76147960E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.91165269E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.94156611E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.82390104E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.09823557E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.28687923E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.10212069E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.33983275E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.92129416E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.94549022E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59765398E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.41242155E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.84295411E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.13773265E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.50694494E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.91441569E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.12907247E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.82829064E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.09865243E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.21791691E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.41881390E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.11871571E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.95268534E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.01522954E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89628384E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.41237237E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.64713016E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.56396013E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.76147960E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.91165269E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.94156611E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.82390104E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.09823557E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.28687923E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.10212069E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.33983275E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.92129416E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.94549022E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59765398E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.41242155E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.84295411E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.13773265E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.50694494E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.91441569E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.12907247E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.82829064E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.09865243E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.21791691E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.41881390E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.11871571E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.95268534E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.01522954E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89628384E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.71396886E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.86805705E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.66152013E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.10077008E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.66161945E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.95639563E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.33122419E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.14024863E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.22472024E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.07862994E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.77516708E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.81374979E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.71396886E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.86805705E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.66152013E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.10077008E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.66161945E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.95639563E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.33122419E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.14024863E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.22472024E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.07862994E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.77516708E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.81374979E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.43205902E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.87402156E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.70197923E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.68132580E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.46772945E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.23472783E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.93987390E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     4.14270255E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.43048618E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.72073362E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.82659136E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.75687962E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.50339852E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     8.87867422E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.01127379E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.71404865E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.18393031E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.76848392E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.94469688E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.66744370E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.70286101E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.32695084E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.71404865E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.18393031E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.76848392E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.94469688E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.66744370E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.70286101E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.32695084E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.71682384E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.18272095E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.76667744E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.93658149E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.66471895E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.72037554E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.32152154E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.18204899E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     5.56423414E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33615646E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33615646E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11205344E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11205344E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10357462E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.84103787E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53102362E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.12214378E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46284069E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.34509608E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53889583E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.50670442E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.68650357E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     8.40704197E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00204605E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88613889E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.11815065E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.37396396E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.53943814E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.31433728E-12    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.85777547E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.55741244E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18569067E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53526051E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18569067E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53526051E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40056673E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50613875E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50613875E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47134315E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00186677E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00186677E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00186677E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.17469138E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.17469138E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.17469138E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.17469138E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.91563816E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.91563816E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.91563816E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.91563816E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     9.88441712E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     9.88441712E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.15913531E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.89397526E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.84739737E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.77973480E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.45356853E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     6.96708870E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.04636177E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.24290524E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.04636177E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.24290524E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.36259660E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.15551819E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.15551819E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.87294999E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.09054448E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.09054448E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.09054448E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.43363874E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.15113775E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.43363874E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.15113775E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.73422821E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.19640029E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.19640029E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.09605607E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.43713450E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.43713450E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.43713450E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.32774460E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.32774460E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.32774460E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.32774460E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.42580896E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.42580896E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.42580896E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.42580896E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.38672283E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.38672283E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.84106349E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.54896102E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51374139E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.75251325E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46375426E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46375426E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14886837E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.60458471E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.37753438E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     6.01148990E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.90529383E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.73285567E-14    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.63545412E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.92049393E-16    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21886515E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01924674E-01    2           5        -5   # BR(h -> b       bb     )
     6.22312594E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20271528E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66051976E-04    2           3        -3   # BR(h -> s       sb     )
     2.01064333E-02    2           4        -4   # BR(h -> c       cb     )
     6.63999428E-02    2          21        21   # BR(h -> g       g      )
     2.30702192E-03    2          22        22   # BR(h -> gam     gam    )
     1.62643735E-03    2          22        23   # BR(h -> Z       gam    )
     2.16640327E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80775806E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78103365E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47116291E-03    2           5        -5   # BR(H -> b       bb     )
     2.46427722E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71213743E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547100E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668027E-05    2           4        -4   # BR(H -> c       cb     )
     9.96063884E-01    2           6        -6   # BR(H -> t       tb     )
     7.97767693E-04    2          21        21   # BR(H -> g       g      )
     2.71240985E-06    2          22        22   # BR(H -> gam     gam    )
     1.16000585E-06    2          23        22   # BR(H -> Z       gam    )
     3.34563307E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66825389E-04    2          23        23   # BR(H -> Z       Z      )
     9.03442921E-04    2          25        25   # BR(H -> h       h      )
     7.52549895E-24    2          36        36   # BR(H -> A       A      )
     3.00278287E-11    2          23        36   # BR(H -> Z       A      )
     6.99415658E-12    2          24       -37   # BR(H -> W+      H-     )
     6.99415658E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382323E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47406023E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897686E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62266427E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677380E-06    2           3        -3   # BR(A -> s       sb     )
     9.96176344E-06    2           4        -4   # BR(A -> c       cb     )
     9.96995852E-01    2           6        -6   # BR(A -> t       tb     )
     9.43675515E-04    2          21        21   # BR(A -> g       g      )
     3.17092141E-06    2          22        22   # BR(A -> gam     gam    )
     1.35257483E-06    2          23        22   # BR(A -> Z       gam    )
     3.26029821E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472403E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36442473E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237153E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81143370E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50324322E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693437E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731487E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402432E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34142332E-04    2          24        25   # BR(H+ -> W+      h      )
     2.72935083E-13    2          24        36   # BR(H+ -> W+      A      )
