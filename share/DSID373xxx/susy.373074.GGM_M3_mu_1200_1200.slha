#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.20000000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.20000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05409189E+01   # W+
        25     1.26000000E+02   # h
        35     2.00402435E+03   # H
        36     2.00000000E+03   # A
        37     2.00150706E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.01048217E+03   # ~d_L
   2000001     5.01037295E+03   # ~d_R
   1000002     5.01023798E+03   # ~u_L
   2000002     5.01029570E+03   # ~u_R
   1000003     5.01048217E+03   # ~s_L
   2000003     5.01037295E+03   # ~s_R
   1000004     5.01023798E+03   # ~c_L
   2000004     5.01029570E+03   # ~c_R
   1000005     5.00993394E+03   # ~b_1
   2000005     5.01092260E+03   # ~b_2
   1000006     5.13110861E+03   # ~t_1
   2000006     5.42496841E+03   # ~t_2
   1000011     5.00008220E+03   # ~e_L
   2000011     5.00007608E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008220E+03   # ~mu_L
   2000013     5.00007608E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99975935E+03   # ~tau_1
   2000015     5.00039955E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.33582136E+03   # ~g
   1000022     1.23047843E+03   # ~chi_10
   1000023    -1.27863445E+03   # ~chi_20
   1000025     1.31765668E+03   # ~chi_30
   1000035     3.12845049E+03   # ~chi_40
   1000024     1.27535443E+03   # ~chi_1+
   1000037     3.12844876E+03   # ~chi_2+
   1000039     2.00000000E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.17028817E-01   # N_11
  1  2    -2.84803928E-02   # N_12
  1  3     4.94356077E-01   # N_13
  1  4    -4.90581911E-01   # N_14
  2  1     3.42143327E-03   # N_21
  2  2    -3.52432032E-03   # N_22
  2  3    -7.06981416E-01   # N_23
  2  4    -7.07215067E-01   # N_24
  3  1     6.97034408E-01   # N_31
  3  2     3.07499365E-02   # N_32
  3  3    -5.05027416E-01   # N_33
  3  4     5.08079506E-01   # N_34
  4  1     1.00132935E-03   # N_41
  4  2    -9.99115052E-01   # N_42
  4  3    -2.71414060E-02   # N_43
  4  4     3.21162517E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.83608532E-02   # U_11
  1  2     9.99263952E-01   # U_12
  2  1     9.99263952E-01   # U_21
  2  2     3.83608532E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.53975860E-02   # V_11
  1  2     9.98968998E-01   # V_12
  2  1     9.98968998E-01   # V_21
  2  2     4.53975860E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99970129E-01   # cos(theta_t)
  1  2     7.72923720E-03   # sin(theta_t)
  2  1    -7.72923720E-03   # -sin(theta_t)
  2  2     9.99970129E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.66906694E-01   # cos(theta_b)
  1  2     7.45141236E-01   # sin(theta_b)
  2  1    -7.45141236E-01   # -sin(theta_b)
  2  2     6.66906694E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03717963E-01   # cos(theta_tau)
  1  2     7.10479436E-01   # sin(theta_tau)
  2  1    -7.10479436E-01   # -sin(theta_tau)
  2  2     7.03717963E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90200623E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.20000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52138779E+02   # vev(Q)              
         4     3.46638282E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52790995E-01   # gprime(Q) DRbar
     2     6.27226830E-01   # g(Q) DRbar
     3     1.08870358E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02503359E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71811502E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79812251E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.20000000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.43530555E+06   # M^2_Hd              
        22    -6.82397948E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37071785E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.95051504E-08   # gluino decays
#          BR         NDA      ID1       ID2
     6.97455268E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.47048417E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     4.25419262E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     6.27652162E-03    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.97007247E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.59837695E-08    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.70786548E-07    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.51073841E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.25325013E-08    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.56439314E-06    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     4.97007247E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.59837695E-08    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.70786548E-07    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.51073841E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.25325013E-08    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.56439314E-06    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.74833463E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     8.73611421E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.97750676E-07    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.16729115E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.16729115E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.16729115E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.16729115E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.21662335E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.90124904E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.00518001E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.31993078E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.40278754E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.54500463E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.77141258E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.13937333E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.23787412E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     6.25754308E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.92966461E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.64874233E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.85560520E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.80596548E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.08534940E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.77376076E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.23680415E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.19350227E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.44823014E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.57226633E-06    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.37982563E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     2.94568738E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.60613686E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.16428900E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.31960037E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.21250287E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.13024846E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.83323739E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.27666653E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.61148488E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.75276429E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.14501942E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.34064670E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.65836421E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.49608706E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.34907107E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.01410564E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.15094176E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.46248525E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.62887218E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.58996144E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.73779902E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.86590554E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.77441826E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.73229748E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.11239283E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.33720728E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.07038095E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.66582317E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.91981289E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.70649603E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60097578E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.46253096E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.80371184E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.16729604E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.65977902E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.86850136E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.98107977E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.73656486E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.11280373E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.26730776E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.33562704E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.20244058E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.94759764E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.39800501E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89716651E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.46248525E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.62887218E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.58996144E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.73779902E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.86590554E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.77441826E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.73229748E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.11239283E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.33720728E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.07038095E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.66582317E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.91981289E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.70649603E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60097578E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.46253096E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.80371184E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.16729604E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.65977902E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.86850136E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.98107977E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.73656486E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.11280373E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.26730776E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.33562704E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.20244058E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.94759764E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.39800501E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89716651E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.72003880E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.91940386E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.60839144E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.11356203E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.65612928E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.85065879E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.31984563E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.16529718E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.18876966E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.16930355E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.81110917E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.24583659E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.72003880E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.91940386E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.60839144E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.11356203E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.65612928E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.85065879E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.31984563E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.16529718E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.18876966E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.16930355E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.81110917E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.24583659E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.44760527E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.87200668E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.70796459E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.71322348E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.45818549E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.13294873E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.92054686E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     3.67392716E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.44615796E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.71674740E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.89097699E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.78795897E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49448686E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     7.04089544E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.99321171E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.72010541E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.18353142E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.86536620E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     8.13101341E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.66160246E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.59165996E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.31566163E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.72010541E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.18353142E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.86536620E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     8.13101341E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.66160246E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.59165996E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.31566163E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.72291381E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.18231074E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.86344227E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.12262712E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.65885730E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.61924640E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.31019044E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.14540642E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.18136033E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33619588E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33619588E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11206659E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11206659E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10347087E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.81901214E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53074172E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.13064292E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46294315E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.33632524E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53934696E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.25087488E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.27230338E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     6.07320972E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.99057432E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.89697787E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.12447811E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.32136790E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.44565155E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.17128928E-12    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.40665709E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.44522275E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18584205E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53544358E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18584205E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53544358E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.39873075E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50649640E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50649640E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47128764E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00254180E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00254180E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00254180E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.06016799E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.06016799E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.06016799E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.06016799E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.53389351E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.53389351E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.53389351E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.53389351E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.15810780E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.15810780E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.22708205E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.77282009E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.60707969E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.98986549E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.74333212E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     4.92328584E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     9.56804070E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.13444321E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.56804070E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.13444321E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.04815733E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.95581469E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.95581469E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.36009233E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.56860842E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.56860842E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.56860842E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.46279693E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.18886577E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.46279693E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.18886577E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.77432498E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.28243717E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.28243717E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.18242572E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.45430776E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.45430776E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.45430776E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.32357253E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.32357253E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.32357253E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.32357253E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.41190204E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.41190204E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.41190204E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.41190204E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.37346585E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.37346585E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.81904055E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.22333035E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51235662E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     9.45013453E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46358510E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46358510E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.15724911E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.74080000E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.36814261E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.39896565E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.71074716E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.50400135E-14    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.23075641E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.13851016E-16    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21860543E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01897171E-01    2           5        -5   # BR(h -> b       bb     )
     6.22348722E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20284315E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66079063E-04    2           3        -3   # BR(h -> s       sb     )
     2.01077028E-02    2           4        -4   # BR(h -> c       cb     )
     6.64039561E-02    2          21        21   # BR(h -> g       g      )
     2.30727703E-03    2          22        22   # BR(h -> gam     gam    )
     1.62653156E-03    2          22        23   # BR(h -> Z       gam    )
     2.16656817E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80793093E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78102507E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47163022E-03    2           5        -5   # BR(H -> b       bb     )
     2.46428984E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71218205E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547655E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668012E-05    2           4        -4   # BR(H -> c       cb     )
     9.96063814E-01    2           6        -6   # BR(H -> t       tb     )
     7.97769191E-04    2          21        21   # BR(H -> g       g      )
     2.71159179E-06    2          22        22   # BR(H -> gam     gam    )
     1.15998017E-06    2          23        22   # BR(H -> Z       gam    )
     3.34207638E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66648037E-04    2          23        23   # BR(H -> Z       Z      )
     9.03577140E-04    2          25        25   # BR(H -> h       h      )
     7.47675937E-24    2          36        36   # BR(H -> A       A      )
     3.01235077E-11    2          23        36   # BR(H -> Z       A      )
     7.02858271E-12    2          24       -37   # BR(H -> W+      H-     )
     7.02858271E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382367E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47451642E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897658E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62266329E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677367E-06    2           3        -3   # BR(A -> s       sb     )
     9.96176230E-06    2           4        -4   # BR(A -> c       cb     )
     9.96995738E-01    2           6        -6   # BR(A -> t       tb     )
     9.43675407E-04    2          21        21   # BR(A -> g       g      )
     3.17770694E-06    2          22        22   # BR(A -> gam     gam    )
     1.35254078E-06    2          23        22   # BR(A -> Z       gam    )
     3.25681184E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472512E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36539883E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237092E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81143154E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50386664E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693300E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731459E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402789E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33785011E-04    2          24        25   # BR(H+ -> W+      h      )
     2.73016117E-13    2          24        36   # BR(H+ -> W+      A      )
