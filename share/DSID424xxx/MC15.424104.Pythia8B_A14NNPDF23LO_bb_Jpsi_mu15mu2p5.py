#################################################################################
# job options fragment for bb->Jpsi->mu15mu2p5 for trigger study for run2
#################################################################################

include("MC15JobOptions/nonStandard/Pythia8B_A14_NNPDF23LO_Common.py")
evgenConfig.description  = "bb->Jpsi->mu15mu2p5 production"
evgenConfig.keywords     = [ "bottom", "charmonium", "muon", "SM" ]
evgenConfig.minevents    = 500
evgenConfig.contact      = [ 'dai.kobayashi@cern.ch' ]
evgenConfig.process      = "pp>bb>Jpsi>mumu"

genSeq.Pythia8B.Commands       += [ 'HardQCD:all = on' ]
genSeq.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 10.' ]
genSeq.Pythia8B.Commands       += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands       += [ 'HadronLevel:all = off' ]
genSeq.Pythia8B.SelectBQuarks      = True
genSeq.Pythia8B.SelectCQuarks      = False
genSeq.Pythia8B.QuarkPtCut         = 10.0
genSeq.Pythia8B.AntiQuarkPtCut     = 0.0
genSeq.Pythia8B.QuarkEtaCut        = 4.5
genSeq.Pythia8B.AntiQuarkEtaCut    = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents  = True
genSeq.Pythia8B.NHadronizationLoops = 1

# B0
genSeq.Pythia8B.Commands += ['511:onMode = off' ]
genSeq.Pythia8B.Commands += ['511:onIfAny = 443']
genSeq.Pythia8B.Commands += ['511:onIfAny = 100443']
genSeq.Pythia8B.Commands += ['511:onIfAny = 445']
genSeq.Pythia8B.Commands += ['511:onIfAny = 10441']
genSeq.Pythia8B.Commands += ['511:onIfAny = 10443']
genSeq.Pythia8B.Commands += ['511:onIfAny = 20443']
# B+/-
genSeq.Pythia8B.Commands += ['521:onMode = off' ]
genSeq.Pythia8B.Commands += ['521:onIfAny = 443']
genSeq.Pythia8B.Commands += ['521:onIfAny = 100443']
genSeq.Pythia8B.Commands += ['521:onIfAny = 445']
genSeq.Pythia8B.Commands += ['521:onIfAny = 10441']
genSeq.Pythia8B.Commands += ['521:onIfAny = 10443']
genSeq.Pythia8B.Commands += ['521:onIfAny = 20443']
# Bs
genSeq.Pythia8B.Commands += ['531:onMode = off' ]
genSeq.Pythia8B.Commands += ['531:onIfAny = 443']
genSeq.Pythia8B.Commands += ['531:onIfAny = 100443']
genSeq.Pythia8B.Commands += ['531:onIfAny = 445']
genSeq.Pythia8B.Commands += ['531:onIfAny = 10441']
genSeq.Pythia8B.Commands += ['531:onIfAny = 10443']
genSeq.Pythia8B.Commands += ['531:onIfAny = 20443']
# Bc
genSeq.Pythia8B.Commands += ['541:onMode = off' ]
genSeq.Pythia8B.Commands += ['541:onIfAny = 443']
genSeq.Pythia8B.Commands += ['541:onIfAny = 100443']
genSeq.Pythia8B.Commands += ['541:onIfAny = 445']
genSeq.Pythia8B.Commands += ['541:onIfAny = 10441']
genSeq.Pythia8B.Commands += ['541:onIfAny = 10443']
genSeq.Pythia8B.Commands += ['541:onIfAny = 20443']
# LambdaB
genSeq.Pythia8B.Commands += ['5122:onMode = off' ]
genSeq.Pythia8B.Commands += ['5122:onIfAny = 443']
genSeq.Pythia8B.Commands += ['5122:onIfAny = 100443']
genSeq.Pythia8B.Commands += ['5122:onIfAny = 445']
genSeq.Pythia8B.Commands += ['5122:onIfAny = 10441']
genSeq.Pythia8B.Commands += ['5122:onIfAny = 10443']
genSeq.Pythia8B.Commands += ['5122:onIfAny = 20443']
# Xb+/-
genSeq.Pythia8B.Commands += ['5132:onMode = off' ]
genSeq.Pythia8B.Commands += ['5132:onIfAny = 443']
genSeq.Pythia8B.Commands += ['5132:onIfAny = 100443']
genSeq.Pythia8B.Commands += ['5132:onIfAny = 445']
genSeq.Pythia8B.Commands += ['5132:onIfAny = 10441']
genSeq.Pythia8B.Commands += ['5132:onIfAny = 10443']
genSeq.Pythia8B.Commands += ['5132:onIfAny = 20443']
# Xb
genSeq.Pythia8B.Commands += ['5232:onMode = off' ]
genSeq.Pythia8B.Commands += ['5232:onIfAny = 443']
genSeq.Pythia8B.Commands += ['5232:onIfAny = 100443']
genSeq.Pythia8B.Commands += ['5232:onIfAny = 445']
genSeq.Pythia8B.Commands += ['5232:onIfAny = 10441']
genSeq.Pythia8B.Commands += ['5232:onIfAny = 10443']
genSeq.Pythia8B.Commands += ['5232:onIfAny = 20443']
# Omega_b+/-
genSeq.Pythia8B.Commands += ['5332:onMode = off' ]
genSeq.Pythia8B.Commands += ['5332:onIfAny = 443']
genSeq.Pythia8B.Commands += ['5332:onIfAny = 100443']
genSeq.Pythia8B.Commands += ['5332:onIfAny = 445']
genSeq.Pythia8B.Commands += ['5332:onIfAny = 10441']
genSeq.Pythia8B.Commands += ['5332:onIfAny = 10443']
genSeq.Pythia8B.Commands += ['5332:onIfAny = 20443']

genSeq.Pythia8B.BPDGCodes = [511,521,531,541,5122,5132,5232,5332,-511,-521,-531,-541,-5122,-5132,-5232,-5332]

genSeq.Pythia8B.Commands += [ '443:onMode = off' ]
genSeq.Pythia8B.Commands += [ '443:2:onMode = on' ]
genSeq.Pythia8B.SignalPDGCodes = [ 443, -13, 13 ]

genSeq.Pythia8B.TriggerPDGCode      = 13
genSeq.Pythia8B.TriggerStateEtaCut  = 2.7
genSeq.Pythia8B.TriggerStatePtCut   = [ 2.5, 15 ]
genSeq.Pythia8B.MinimumCountPerCut  = [   2,  1 ]
