import os
import sys, shutil
import subprocess
import argparse

def testJO(name):
	try:		
		dsid     = name.split(".",2)[1]    # split of the first to parts separated by points
		descript = name.split(".",3)[2]
		number= int(dsid)
	except Exception:
		dsid = 0
		descript = 0

	return dsid, descript

def checkJO(new, git_list, git_path):
	comp             = ""
	dsid, description = testJO(new)
	if dsid == 0:
		print new, "is not a JO"
		return ""

	if new  not in git_list:
		# check if the same DSID was already used
		match = filter(lambda s: s.find(dsid) != -1  ,[s for s in git_list])
		# omit message for files ~ susy.dsid.<something>
		match = filter(lambda s: s[:2].find("MC") != -1  ,[s for s in match])
		print match
		if len(match) > 0:
			print "DSID", dsid, "already exists with a different file name:"
			print "		Proposed name:", new
			print "		Old name:     ", match[0] 
			if not args.replace_duplicate:
				return  match[0]
			# remove old from git
			print "remove old from git"
			ToGit("rm", match[0], git_path)
		
		# check if same description field was already used
		match = filter(lambda s: s.find(description) != -1  ,[s for s in git_list])
		# omit message for files ~ susy.dsid.<something>
		match = filter(lambda s: s[:2].find("MC") != -1  ,[s for s in match])
		print match
		if len(match) > 0:
			print "Description", description, "already exists for a different DSID:"
			print "		Proposed name:", new
			print "		Old name:     ", match[0] 
			if not args.replace_duplicate:
				return  match[0]
			# remove old from git
			ToGit("rm", match[0], git_path)
				
		else:	
			return ""
	return new

def finalize(new, git_path):
	if not args.copy and not args.update:
		print "file {} not found in {}".format(new,git_path)
		return
	print "copying {} to {}".format(new,git_path)
	shutil.copyfile(new, os.path.join(git_path,new))
	ToGit("add",    new,      git_path)

def checkfile(new, git_list, git_path):
	if new[:2] == "MC":
		return ""
	if new not in git_list:
		finalize(new, git_path)
		return ""
	return new

def ToGit(action, new, git_path):
	if not args.copy and not args.update:
		# print "Nothing will be added, since this is a test run (option -c)"
		return
	# add/delete file to/from git
	current_dir = os.getcwd()
	os.chdir(git_path)
	proc = subprocess.Popen( ["git", action, os.path.join(git_path, new)] )
	proc.communicate()
	os.chdir(current_dir)	

def main():
	"""
          given path can be the path to the directory containing the file or to the file itself
	  first argument: path to JOs to commit
          second argument: path to JOs contained in git
        """

	new      = os.listdir(args.new) if os.path.isdir(args.new) else [args.new]
	git      = os.listdir(args.git) if os.path.isdir(args.git) else [args.git]
	base_dir = os.getcwd()
	git_path = os.path.join(base_dir,args.git )
	JO_path  = args.new

	for n in new:
		# change to directory containing new JOs, if not already there
		if os.getcwd().find(JO_path) == -1:
			  os.chdir( os.path.join( base_dir, JO_path) )
		if git_path.find("share") != -1:
			#print "Looking for JOs"
			comp = checkJO(n,git,git_path)
		else: 
			comp = checkfile(n,git,git_path)
		if comp == "" or args.update:
			finalize(n, git_path)
			continue

		comp = git[git.index(n)] if comp == "" else comp
		cmd  = ['diff', n, "{}/{}".format(git_path,comp)]
		print cmd
		proc = subprocess.Popen(cmd)
		proc.communicate()


if __name__ == '__main__':
	parser   = argparse.ArgumentParser(description='Options')
	parser.add_argument('new', action='store')
	parser.add_argument('git', action='store')
	parser.add_argument('--replace-duplicate', '-r', action='store_true',   help='replace JOs with same DSID but different description field')
	parser.add_argument('--copy',              '-c', action='store_true',   help='copy all files from the new folder to git that dont already exist there')
	parser.add_argument('--update',            '-u', action='store_true',   help='update all files from the folder to git, even if they allready exist')
	args     = parser.parse_args()
	print args
	main()
		
