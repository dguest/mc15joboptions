# Pythia8 Showering with A14_NNPDF23LO
#---------------------------------------------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")


#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["aMcAtNlo", "Pythia8", "EvtGen"]
evgenConfig.description = "Vh 125 GeV Higgs production in the Higgs Characterization model. kSM=1, Lambda=1 TeV, cosalpha=1, A14_NNPDF23LO_Var3cUp"
evgenConfig.keywords = ['Higgs','mH125','BSMHiggs','bbbar']

evgenConfig.contact = ['Paolo Francavilla <paolo.francavilla@cern.ch>']
evgenConfig.inputfilecheck = "TXT"
genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch = 1",                                                                                                                  
                            "SpaceShower:pTmaxFudge = 1",                                                                                                                  
                            "SpaceShower:MEcorrections = off",                                                                                                             
                            "TimeShower:pTmaxMatch = 1",                                                                                                                   
                            "TimeShower:pTmaxFudge = 1",                                                                                                                   
                            "TimeShower:MEcorrections = off",                                                                                                              
                            "TimeShower:globalRecoil = on",                                                                                                                
                            "TimeShower:limitPTmaxGlobal = on",                                                                                                            
                            "TimeShower:nMaxGlobalRecoil = 1",                                                                                                             
                            "TimeShower:globalRecoilMode = 2",                                                                                                             
                            "TimeShower:nMaxGlobalBranch = 1.",                                                                                                            
                            "Check:epTolErr = 1e-2" ]
genSeq.Pythia8.Commands += ["25:onMode=off",
                            "25:onIfAny=5"]
