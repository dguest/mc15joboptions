# in comments it is listed which generators got updated in a given release
AtlasProduction, 19.2.5.37
MCProd,          19.2.5.36.5
MCProd,          20.7.9.9.27

# it is recommended to use:
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PhysicsModellingGroup#MC_Software
to find latest pcaches and generator versions present in given pcaches
#comments
#AtlasProduction, 19.2.4.5 to be used for the Pythia 8.2
#AtlasProduction, 19.2.4.11 to be used for Pythia8.21, Powheg
#AtlasProduction, 19.2.4.12 to be used for Herwigpp and Sherpa
#AtlasProduction, 19.2.4.15 VBFMjjIntervalFilter new functionalities added
#AtlasProduction, 19.2.4.16 EvgenJobTransform fixed to allow for correct .TXT output handling
#AtlasProduction, 19.2.4.17 to be used for MG 2.2 (reduced size of produced directory)
#AtlasProduction, 19.2.5.2 to be used for MG 2.3, Powheg (fixed grid-pack issue)
#AtlasProduction, 19.2.5.5 Powheg most recent version, Starlight 1.1, fixed bug in XtoVVFilterExtended
#AtlasProduction, 19.2.5.7 Sherpa2.2, Rivet2.4, GeneratorFilters (corrected FourLeptonMassFilter)
#AtlasProduction, 19.2.5.9 Pythia8.212, Rivet2.4.2, YODA 1.5.9, GeneratorFilters (added 
TransverseMassVVFilter)
#AtlasProduction, 19.2.5.10 Powheg-00-03-07, PowhegControl updates, MadGraphControl (gfortran 
protection, addition of new SUSY gentype)
#AtlasProduction, 19.2.5.11 MadGraphControl update for Z+jets MG5_aMC+Py8 FxFx sample
#AtlasProduction, 19.2.5.13 QCDTrithJetFilter extra parameter added - build on request of HI  
#AtlasProduction, 19.2.5.15 Sherpa2.2.1
#AtlasProduction, 19.2.5.16 fixed ParentTwoChildrenFilter,env. var. fixes in Herwig7_i,MadGraphControl-AGENE-1240
#AtlasProduction, 19.2.5.19 edited HeavyFlavorHadronFilter, MGControl patch to arrange output, Tauolapp
#AtlasProduction, 19.2.5.20 MadGraphControl and Powheg update
#AtlasProduction, 19.2.5.21 ParticleGun ATLMCPROD-4664
#AtlasProduction, 19.2.5.22 Fix small bug in MadGraphUtils. AGENE-1359,patch to MadGraphControl generate_from_gridpack. 
AGENE-1361, BSignalFilter update (cut on invariant mass with a different mass hypothesis). AGENE-1363 ParentChildFilter extension with cut for parent min rapidity. AGENE-1377
- Herwig7 packages update. AGENE-1378
#AtlasProduction, 19.2.5.24 MadGraphControl cleanup to prevent errors (AGENE-1403), Modifying the 
initialization of Chargino ConstructProcess (AGENE-3231)
#AtlasProduction, 19.2.5.25 Add option for Charginos to decay into an electron or a muon plus a 
Neutralino, in addition to the ability for them to decay into a charged pion plus a Neutralino 
(ATLASSIM 3401)
#AtlasProduction, 19.2.5.26 New LeadingChargedParticleFilter AGENE-1405, Modifying Chargino BR. 
ATLASSIM-3401
#AtlasProduction, 19.2.5.30 first cache with new V2 version of Powheg
#AtlasProduction, 19.2.5.31 first cache with MG 2.60
#AtlasProduction, 19.2.5.32 first cache with Powheg-00-03-11 and PowhegControl-00-03-10
#AtlasProduction, 19.2.5.33 new GeneratorFilters and meta data info added

#MCProd,          19.2.4.10.2 to be used for MG2.3 validation
#MCProd,          19.2.4.12.1 to be used for Sherpa 2.2 validation
#MCProd,          19.2.4.16.3 MG2.3.2 p.1 - ATLMCPROD-2493
#MCProd,          19.2.4.16.5 Pythia8.2.12 validation
#MCProd,          19.2.4.16.6 validation Photos3.61
#MCProd,          19.2.5.1.1  validation Pythia8.15
#MCProd,          19.2.5.3.1  Pythia8.212, copilertime switch set to LHAPDF6
#MCProd,          19.2.5.9.1  validation Pythia8.219
#MCProd,          19.2.5.12.1 pcache for generation with MC15 and sim/reco rel.17 ATLASSIM-3025
#MCProd,          19.2.5.12.2 validation of Sherpa_i 2.2.1 and  workaround for https://sherpa.hepforge.org/trac/ticket/365
#MCProd,          19.2.5.12.3 for testing the EvgenJobTransforms changes ATLASJT-301
#MCProd,          19.2.5.12.2 validation MadGraph2.4.3
#MCProd,          19.2.5.14.3 MG5-2.43, MGControl-00-05-51, Powheg-00-03-08, and Pythia8_i allowing 
for mass reweight (for the tau CP group)
#MCProd,          19.2.5.14.4 MGControl updated to MadGraphControl-00-05-63 and Herwig7_i-00-00-01-01
#MCProd,          19.2.5.16.1 Sherpa for multicore testing, fixed JetSimTools (ATLMCPROD-3731), MG2.51
#MCProd,          19.2.5.19.1 MadGraph 2.5.2 validation
#MCProd,          19.2.5.19.5 Pythia8.219pdf6plugin, Herwig7.04 validation
#MCProd,          19.2.5.20.3 Sherpa for multicore testing
#MCProd,          19.2.5.21.1 validation of Pythia8.226, updated H7 interface and satelite packages
#MCProd,          19.2.5.21.3 updated with Pythia8-02-30-01
#MCProd,          19.2.5.26.1 Sherpa2.2.4, new Powheg, MC2.6, Pythia8.212
#MCProd,          19.2.5.31.1 Powheg-00-03-11, MG 2.61, Py8.230, metadata improvements from Jose, not to be used for H7
#MCProd,          19.2.5.32.2 Pythia8.235 validation
#MCProd,          19.2.5.33.1 Sherpa225 validation
#MCProd,          17.2.14.13.2 Photospp_i updates (option to set ME corrections for Wt for Z and W true)
#MCProd,          17.2.14.13.3 W mass production (Lhapdf6, Pythia8.2 , Photospp3.61 and modified Photospp_i)

