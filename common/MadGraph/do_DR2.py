# Script to perform Diagram Removal 2
# Works for tW, tWZ and tWH. Removes the overlap (and interference) with tt, ttZ and ttH respectively.
# Does not work for tWgamma and tWll.
# Contact: olga.bylund@cern.ch
# Please let me know if you have any questions or problems.

import os
import re

import DR_functions

pdir=process_dir+'/SubProcesses/'
DR_functions.do_fks_hacks(pdir)
DR_functions.do_coupl_hacks(pdir) 


P_folders= glob.glob(pdir+"P*_*") #find relevant directories
mfiles = DR_functions.find_matrix_files(P_folders)

for mfile in mfiles:
    matrix_idx =  re.compile("matrix_(\d+).f").search(mfile).group(1)
    DR_functions.make_support_file(mfile, "the_process.txt", "Process:")

    with open("the_process.txt","r") as f:
        myline = f.readline()
        m2 = myline.split("Process: ")[1]
        the_process = m2.split(" WEIGHTED")[0]

        bindex, windex = DR_functions.return_out_index(the_process)  


    redefine_twidth=True #regularises integral for MadGraph part, makes sure it does not diverge
    DRmode=2
    to_replace = DR_functions.find_W_prepare_DRXhack(mfile,bindex,windex,redefine_twidth,DRmode)


    if os.path.getsize("mytmp.txt")>0: #if there are hacks to make
        DR_functions.find_jamp(mfile,mfile+".jamp1","JAMP(1)","JAMP(2)")
        DR_functions.find_jamp(mfile,mfile+".jamp2","JAMP(2)","MATRIX")
        DR_functions.do_DR2_hack(mfile, "mytmp.txt","_"+matrix_idx,to_replace,1)

        os.remove(mfile+".jamp1")
        os.remove(mfile+".jamp2")

os.remove("the_process.txt")
