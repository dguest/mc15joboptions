include ( 'MC15JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

# specifying SUSY masses
mSquark = float(runArgs.jobConfig[0].split('_')[4])
mCN = float(runArgs.jobConfig[0].split('_')[5])
mStau = float(runArgs.jobConfig[0].split('_')[6])
mLSP = float(runArgs.jobConfig[0].split('_')[7].rstrip('.py'))

# only consider sq_L
masses['1000001'] = mSquark
masses['1000002'] = mSquark
masses['1000003'] = mSquark
masses['1000004'] = mSquark
masses['1000023'] = mCN
masses['1000024'] = mCN
masses['1000015'] = mStau
masses['1000016'] = mStau
masses['1000022'] = mLSP

# specifying the process
gentype = str(runArgs.jobConfig[0].split('_')[2])
decaytype = str(runArgs.jobConfig[0].split('_')[3])

process = '''
generate p p > susylq susylq~ $ go susyweak @1
add process p p > susylq susylq~ j $ go susyweak @2
add process p p > susylq susylq~ j j $ go susyweak @3
'''

njets = 2

# filter efficiency depends on mass splitting
if mSquark-mLSP <= 50 and '_J85' in runArgs.jobConfig[0]:
    evt_multiplier = 40.
elif mSquark-mLSP <= 240 and '_J85' in runArgs.jobConfig[0]:
    evt_multiplier = 4.

# recommended by https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYMcRequestProcedure
extras['event_norm']='sum'
extras['use_syst']='F'
# now centrally set
#extras['pdgs_for_merging_cut']='1, 2, 3, 4, 21'

include ( 'MC15JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

# Pythia8 merging
if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
    else:
        genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'


filters = []

# truth jet filter for compressed points
if '_J85' in runArgs.jobConfig[0]:
    filters.append("TruthJetFilter")
    evgenLog.info('Adding truth jet filter, Pt > 85 GeV')    
    include("MC15JobOptions/AntiKt4TruthJets.py")    
    from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
    filtSeq += TruthJetFilter("TruthJetFilter")
    filtSeq.TruthJetFilter.jet_pt1 = 85000.
    filtSeq.TruthJetFilter.NjetMinPt = 0.
    filtSeq.TruthJetFilter.NjetMaxEta = 2.8
    filtSeq.TruthJetFilter.Njet = 1

# truth tau filter
if '1tau' in runArgs.jobConfig[0].split('_')[-1]:
    filters.append("MultiElecMuTauFilter")
    evgenLog.info('Adding 1 tau filter')
    include("MC15JobOptions/MultiElecMuTauFilter.py")
    MultiElecMuTauFilter = filtSeq.MultiElecMuTauFilter
    MultiElecMuTauFilter.NLeptons = 1
    MultiElecMuTauFilter.MinPt = 1e8
    MultiElecMuTauFilter.MaxEta = 2.8
    MultiElecMuTauFilter.MinVisPtHadTau =  10000
    MultiElecMuTauFilter.IncludeHadTaus = 1

if filters:
   filtSeq.Expression = ' and '.join(filters)


evgenLog.info('Registered generation of squark grid '+str(runArgs.runNumber))
evgenConfig.contact  = [ "martindl@cern.ch" ]
evgenConfig.keywords += [ 'squark' ]
evgenConfig.description = 'squark simplified model, two-step decays via C1/N2 then stau/sneutrino, m_sq = %s GeV, m_C1 = m_N2 = %s GeV, m_stau = m_snu = %s GeV, m_N1 = %s GeV'%(masses['1000001'],masses['1000024'],masses['1000015'],masses['1000022']) 
