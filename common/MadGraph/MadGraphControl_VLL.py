from MadGraphControl.MadGraphUtils import *
import math
import os

# Compute decay widths as a function of m_taup for use in param card
def taup_decay_width(m_taup):
    epsilon=0.04
    mZ=91.187
    mH=125.1
    rz=mZ**2 / m_taup**2
    rh=mH**2 / m_taup**2

    decaywidth = (1-rz)**2 * (2+1/rz) * rz
    decaywidth += (1-rh)**2
    decaywidth *= m_taup * epsilon**2
    decaywidth /= 64 * math.pi
    return decaywidth

def vtaup_decay_width(m_taup):
    epsilon=0.04
    mW=80.385
    rw=mW**2 / m_taup**2

    decaywidth = m_taup * (1-rw)**2 * (2+1/rw) * epsilon**2 * rw / (32*math.pi)
    return decaywidth

# Define constants
mode=0

dsid_to_mtaup_nlep = { 
    312403 : [ 130, 2 ],
    312402 : [ 130, 3 ],
    312401 : [ 130, 4 ],
    312400 : [ 200, 2 ],
    312399 : [ 200, 3 ],
    312398 : [ 200, 4 ],
    312397 : [ 300, 2 ],
    312396 : [ 300, 3 ],
    312395 : [ 300, 4 ],
    312394 : [ 400, 2 ],
    312393 : [ 400, 3 ],
    312392 : [ 400, 4 ],
    312391 : [ 500, 2 ],
    312390 : [ 500, 3 ],
    312389 : [ 500, 4 ],
    312388 : [ 600, 2 ],
    312387 : [ 600, 3 ],
    312386 : [ 600, 4 ],
    312385 : [ 700, 2 ],
    312384 : [ 700, 3 ],
    312383 : [ 700, 4 ],
    312382 : [ 800, 2 ],
    312381 : [ 800, 3 ],
    312380 : [ 800, 4 ],
    312379 : [ 900, 2 ],
    312378 : [ 900, 3 ],
    312377 : [ 900, 4 ],
    312376 : [ 1000, 2 ],
    312375 : [ 1000, 3 ],
    312374 : [ 1000, 4 ],
}

# Get arguments
DSID=-1
beamEnergy=-1
randSeed=-1
nevents=-1
if hasattr(runArgs,'runNumber'):
    DSID=runArgs.runNumber
else:
    raise RuntimeError("Run number (DSID) not given.")

if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2
else:
    raise RuntimeError("Center of mass energy not given.")

if hasattr(runArgs,'randomSeed'):
    randSeed = runArgs.randomSeed % (30081*30081) # MG5 random seed must be less than 30081*30081
else:
    raise RuntimeError("Random seed not given.")

if hasattr(runArgs,'maxEvents'):
    nevents=runArgs.maxEvents
else:
    nevents=5000

try:
    mtaup = dsid_to_mtaup_nlep.get(DSID)[0]
    nlep = dsid_to_mtaup_nlep.get(DSID)[1]
except (KeyError, IndexError) as e:
    raise RuntimeError("VLL joboptions run with invalid runNumber: {}".format(DSID))

evt_multiplier = 25.05
if mtaup <= 300:
    evt_multiplier *= 2

# Write MG5 process card
proc_card = open('proc_card_mg5.dat','w')
proc_card.write("""
                import model sm
                define l+ e+ mu+ ta+
                define l- e- mu- ta-
                import model Doublet_Vector_like_Leptons_Model_UFO
                generate p p > taup taup~, taup > z ta-, taup~ > z ta+
                add process p p > taup taup~, taup > h ta-, taup~ > h ta+
                add process p p > taup taup~, taup > z ta-, taup~ > h ta+
                add process p p > taup taup~, taup > h ta-, taup~ > z ta+
                add process p p > taup vtaup~, taup > h ta-, vtaup~ > w- ta+
                add process p p > vtaup taup~, vtaup > w+ ta-, taup~ > h ta+
                add process p p > taup vtaup~, taup > z ta-, vtaup~ > w- ta+
                add process p p > vtaup taup~, vtaup > w+ ta-, taup~ > z ta+
                add process p p > vtaup vtaup~, vtaup > w+ ta-, vtaup~ > w- ta+
                output -f
                """)
proc_card.close()

process_dir = new_process()

# Set up the run card with mostly default values
run_card_extras = {
#    'pdlabel' : 'lhapdf',
#    'lhaid' : 247000,
    'pdlabel' : 'nn23lo1',
    'sys_pdf' : 'cteq6l1',
    'scale' : str(mtaup),
    'dsqrt_q2fact1' : str(mtaup),
    'dsqrt_q2fact2' : str(mtaup)
}

# Set up the param card, varying the taup mass and decay widths based on the DSID
param_card_extras = {
    'MASS' : {
        'MTAUP' : mtaup,
        'MVTAUP' : mtaup
    },
    'VECTORLIKE' : {
        'EPSILON' : 0.04
    },
    'DECAY' : {
        'WTAUP' : str(taup_decay_width(mtaup)),
        'WVTAUP' : str(vtaup_decay_width(mtaup))
    }
}

ret_flag = build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir), run_card_new='run_card.dat', nevts=nevents*evt_multiplier, rand_seed=randSeed, beamEnergy=beamEnergy, extras=run_card_extras)
if ret_flag == -1:
    raise RuntimeError("Error building run_card.dat.")

ret_flag = build_param_card(param_card_old='{}/Cards/param_card.dat'.format(process_dir), param_card_new='param_card.dat', params=param_card_extras)
if ret_flag == -1:
    raise RuntimeError("Error building param_card.dat.")

print_cards()

# Generate LHE file from MG5
generate(run_card_loc='run_card.dat', param_card_loc='param_card.dat', mode=mode, proc_dir=process_dir, cluster_type=None, cluster_queue=None)

output_DS_prefix = 'madgraph.'+str(DSID)+'.MadGraph_VLL_mtaup'+str(mtaup)
output_DS = output_DS_prefix+'._00001.events.tar.gz'
arrange_output(proc_dir=process_dir, outputDS=output_DS)

# Change to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    if hasattr(opts,'nprocs'):
        opts.nprocs = 0

# Shower
evgenConfig.description = 'MadGraph5+Pythia8 for Vectorlike leptons of mass {} GeV, {} lepton final state'.format(mtaup,nlep)
evgenConfig.contact = ['Daniel Wilbern <daniel.wilbern@cern.ch>']
evgenConfig.keywords += ['exotic', 'multilepton', 'tau']
evgenConfig.generators += ['MadGraph','Pythia8']
runArgs.inputGeneratorFile=output_DS

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_MadGraph.py')

# Filters
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonWithParentFilter
if nlep == 2:
    filtSeq += MultiLeptonWithParentFilter('LightFilter')
    LightFilter = filtSeq.LightFilter
    LightFilter.PDGOrigin = [9000005, 9000006]
    LightFilter.PDGIntermediate = [23, 24, 25]
    LightFilter.MinPt = 18000.
    LightFilter.MaxEta = 2.8
    LightFilter.NLeptonsMin = 2
    LightFilter.NLeptonsMax = 2
    LightFilter.IncludeLepTaus = True
    LightFilter.IncludeHadTaus = False
    LightFilter.VetoHadTaus = True
elif nlep == 3:
    filtSeq += MultiLeptonWithParentFilter('LeptonFilter')
    LeptonFilter = filtSeq.LeptonFilter
    LeptonFilter.PDGOrigin = [9000005, 9000006]
    LeptonFilter.PDGIntermediate = [23, 24, 25]
    LeptonFilter.MinPt = 15000.
    LeptonFilter.MaxEta = 2.8
    LeptonFilter.NLeptonsMin = 3
    LeptonFilter.NLeptonsMax = 3
    LeptonFilter.IncludeLepTaus = True
    LeptonFilter.IncludeHadTaus = True
    LeptonFilter.VetoHadTaus = False

    filtSeq += MultiLeptonWithParentFilter('LightFilter')
    LightFilter = filtSeq.LightFilter
    LightFilter.PDGOrigin = [9000005, 9000006]
    LightFilter.PDGIntermediate = [23, 24, 25]
    LightFilter.MinPt = 18000.
    LightFilter.MaxEta = 2.8
    LightFilter.NLeptonsMin = 2
    LightFilter.NLeptonsMax = 3
    LightFilter.IncludeLepTaus = True
    LightFilter.IncludeHadTaus = False
    LightFilter.VetoHadTaus = False
elif nlep == 4:
    filtSeq += MultiLeptonWithParentFilter('LeptonFilter')
    LeptonFilter = filtSeq.LeptonFilter
    LeptonFilter.PDGOrigin = [9000005, 9000006]
    LeptonFilter.PDGIntermediate = [23, 24, 25]
    LeptonFilter.MinPt = 15000.
    LeptonFilter.MaxEta = 2.8
    LeptonFilter.NLeptonsMin = 4
    LeptonFilter.NLeptonsMax = 10
    LeptonFilter.IncludeLepTaus = True
    LeptonFilter.IncludeHadTaus = True
    LeptonFilter.VetoHadTaus = False

    filtSeq += MultiLeptonWithParentFilter('LightFilter')
    LightFilter = filtSeq.LightFilter
    LightFilter.PDGOrigin = [9000005, 9000006]
    LightFilter.PDGIntermediate = [23, 24, 25]
    LightFilter.MinPt = 18000.
    LightFilter.MaxEta = 2.8
    LightFilter.NLeptonsMin = 2
    LightFilter.NLeptonsMax = 10
    LightFilter.IncludeLepTaus = True
    LightFilter.IncludeHadTaus = False
    LightFilter.VetoHadTaus = False
