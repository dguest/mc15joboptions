from MadGraphControl.MadGraphUtils import *
import math

#---------------------------------------------------------------------------------------------------
#General Settings
#---------------------------------------------------------------------------------------------------
minevents=5000
nevents=5500

#mode=0
# gridpack_dir='madevent/'
# gridpack_mode=True
gridpack_dir=None
gridpack_mode=False
safefactor=1.1

DSID = runArgs.runNumber

# CP even samples (Verena, May 2019): one restrict model per coupling, param card is the same as the restrict model.
if DSID == 346579:
    modelExtension="-SMlimit_massless_SMEFT_H4l"
elif DSID == 346580:
    modelExtension="-cHW0p5_massless_SMEFT_H4l"
elif DSID == 346581: 
    modelExtension="-cHB0p4_massless_SMEFT_H4l"
elif DSID == 346582:
    modelExtension="-cHWB0p5_massless_SMEFT_H4l"

# CP odd samples (Antoine, June 2019): single restrict model + build param card on the fly for each coupling.
# Need the definition of the coupling dictionnary (below).
elif 346608 <= DSID and DSID <= 346631:
    modelExtension="-massless_cHWtil_cHBtil_cHWBtil_SMEFT_H4l"

else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)


if   DSID == 346608: params = {'cHWtil':  2.000000e+00}
elif DSID == 346609: params = {'cHWtil':  4.000000e+00}
elif DSID == 346610: params = {'cHWtil':  1.000000e+01}
elif DSID == 346611: params = {'cHWtil':  2.500000e+01}
elif DSID == 346612: params = {'cHWtil': -2.000000e+00}
elif DSID == 346613: params = {'cHWtil': -4.000000e+00}
elif DSID == 346614: params = {'cHWtil': -1.000000e+01}
elif DSID == 346615: params = {'cHWtil': -2.500000e+01}
elif DSID == 346616: params = {'cHWtil':  1.758252e+00, 'cHBtil':  5.042086e-01, 'cHWBtil':  1.883110e+00} # tCzz 1
elif DSID == 346617: params = {'cHWtil':  3.516503e+00, 'cHBtil':  1.008417e+00, 'cHWBtil':  3.766220e+00} # tCzz 2
elif DSID == 346618: params = {'cHWtil':  8.791258e+00, 'cHBtil':  2.521043e+00, 'cHWBtil':  9.415549e+00} # tCzz 5
elif DSID == 346619: params = {'cHWtil':  2.109902e+01, 'cHBtil':  6.050503e+00, 'cHWBtil':  2.259732e+01} # tCzz 12
elif DSID == 346620: params = {'cHWtil': -1.758252e+00, 'cHBtil': -5.042086e-01, 'cHWBtil': -1.883110e+00} # tCzz -1
elif DSID == 346621: params = {'cHWtil': -3.516503e+00, 'cHBtil': -1.008417e+00, 'cHWBtil': -3.766220e+00} # tCzz -2
elif DSID == 346622: params = {'cHWtil': -8.791258e+00, 'cHBtil': -2.521043e+00, 'cHWBtil': -9.415549e+00} # tCzz -5
elif DSID == 346623: params = {'cHWtil': -2.109902e+01, 'cHBtil': -6.050503e+00, 'cHWBtil': -2.259732e+01} # tCzz -12
elif DSID == 346624: params = {'cHWtil':  1.567366e+00, 'cHBtil': -1.567366e+00, 'cHWBtil': -2.087551e+00} # tCza 2
elif DSID == 346625: params = {'cHWtil':  3.918414e+00, 'cHBtil': -3.918414e+00, 'cHWBtil': -5.218878e+00} # tCza 5
elif DSID == 346626: params = {'cHWtil':  9.404194e+00, 'cHBtil': -9.404194e+00, 'cHWBtil': -1.252531e+01} # tCza 12
elif DSID == 346627: params = {'cHWtil':  1.567366e+01, 'cHBtil': -1.567366e+01, 'cHWBtil': -2.087551e+01} # tCza 20
elif DSID == 346628: params = {'cHWtil': -1.567366e+00, 'cHBtil':  1.567366e+00, 'cHWBtil':  2.087551e+00} # tCza -2
elif DSID == 346629: params = {'cHWtil': -3.918414e+00, 'cHBtil':  3.918414e+00, 'cHWBtil':  5.218878e+00} # tCza -5
elif DSID == 346630: params = {'cHWtil': -9.404194e+00, 'cHBtil':  9.404194e+00, 'cHWBtil':  1.252531e+01} # tCza -12
elif DSID == 346631: params = {'cHWtil': -1.567366e+01, 'cHBtil':  1.567366e+01, 'cHWBtil':  2.087551e+01} # tCza -20
# If this is not job that expects a params dict (ie. all but 346579, 346580, 346581, 346582)
# the job will later due to 'params' being undefined.

#---------------------------------------------------------------------------------------------------
# generating process cards
#---------------------------------------------------------------------------------------------------

fcard = open('proc_card_mg5.dat','w')

#---------------------------------------------------------------------------------------------------
# generating VBFVH to H4l
#---------------------------------------------------------------------------------------------------

fcard.write("""
import model SMEFTsim_A_U35_MwScheme_UFO"""+modelExtension+"""
set gauge unitary

define v = a z w+ w-
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~

generate p p > j j h QCD=0 NP=1, h > l+ l- l+ l- NP=1

output -f
""")
fcard.close()


#---------------------------------------------------------------------------------------------------
# require beam energy to be set as argument
#---------------------------------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")



#---------------------------------------------------------------------------------------------------
# creating run_card.dat for VBF
#---------------------------------------------------------------------------------------------------

extras = {'lhe_version'    :'3.0',
          'pdlabel'        : "'nn23lo1'",
          'lhaid'          : 230000,
          'parton_shower'  :'PYTHIA8',
          'event_norm'     : 'sum',
          'cut_decays'     : 'T',
          'drll'           : '0.05',
          'bwcutoff'       : '15.0',
          'pta'            : '0.',
		  'ptl'            : '0.',
          'etal'           : '-1.0',
          'drjj'           : '0.0',
          'draa'           : '0.0',
          'etaj'           : '-1.0',
          'draj'           : '0.0',
          'drjl'           : '0.0',
          'dral'           : '0.0',
          'etaa'           : '-1.0',
          'ptj'            : '10.',
          'ptj1min'        : '0.',
          'ptj1max'        : '-1.0',
          'mmjj'           : '3',
          'mmjjmax'        : '-1'
          }

process_dir = new_process(grid_pack=gridpack_dir)

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=int(runArgs.maxEvents*safefactor),rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
print_cards()

# Legacy for CP even samples from Verena (May 2019)
if DSID in [346579, 346580, 346581, 346582]:
    param_card_loc='restrict_'+modelExtension[1:]+'.dat'
    paramcard = subprocess.Popen(['get_files','-data',param_card_loc])
    paramcard.wait()
    if not os.access(param_card_loc,os.R_OK):
        raise RuntimeError("ERROR: Could not get %s"%param_card_loc)
# new method: build on the fly from H4l SM SMEFT restrict model (MadGraphModels/SMEFTsim_A_U35_MwScheme_UFO/...)
else:
    param_card_base='restrict_SMlimit_massless_SMEFT_H4l.dat'
    paramcard = subprocess.Popen(['get_files','-data',param_card_base])
    paramcard.wait()
    if not os.access(param_card_base,os.R_OK):
        raise RuntimeError("ERROR: Could not get baseline param card: %s"%param_card_base)
    param_card_loc = build_param_card(param_card_old=param_card_base, params={'frblock': params})

runName='run_01'

generate(run_card_loc='run_card.dat',
        param_card_loc=param_card_loc,
        mode=0,
        grid_pack=gridpack_mode,
        gridpack_dir=gridpack_dir,
        njobs=1,
        run_name=runName,
        proc_dir=process_dir)

#---------------------------------------------------------------------------------------------------
# Multi-core capability
#---------------------------------------------------------------------------------------------------
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

#---------------------------------------------------------------------------------------------------
#### Showering with Pythia 8
#---------------------------------------------------------------------------------------------------
evgenConfig.description = "VBFVH-Had 125 GeV Higgs production in the SMEFT model decaying to zz4l."
evgenConfig.keywords = ["Higgs", "VBF", "BSM", "mH125"]
evgenConfig.process = "generate p p > j j h QCD=0 NP=1, h > l+ l- l+ l- NP=1"
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.minevents = minevents
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
evgenConfig.contact = ["Verena Maria Walbrecht <vwalbrec@cern.ch>", "Antoine Laudrain <antoine.laudrain@cern.ch>"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
