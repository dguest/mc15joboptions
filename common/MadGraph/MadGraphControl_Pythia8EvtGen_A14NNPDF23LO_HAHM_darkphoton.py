from MadGraphControl.MadGraphUtils import *

## safe factor applied to nevents, to account for the filter efficiency
nevents=5000
WHS=5.237950e-01

path = os.getcwd()
fcard = open('proc_card_mg5.dat', 'w')
fcard.write("""
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/HAHM_darkphoton_iDM_UFO
define p = u c s d b u~ c~ s~ d~ b~ g
define j = u c s d b u~ c~ s~ d~ b~ g
generate p p > zp h2, h2 > zp zp
add process p p > zp h2 j, h2 > zp zp
output -f
""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else: 
  raise RuntimeError("No center of mass energy found.")

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents)*safefactor
else: nevents = nevents*safefactor
skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 run Card
#---------------------------------------------------------------------------

extras = {
    'pdlabel'       : "'lhapdf'",
    'lhaid'         : "262000",
    'lhe_version': "3.0",
    'maxjetflavor'  : "4",
    'asrwgtflavor'  : "5",
    'auto_ptj_mjj'  : "False",
    'cut_decays': 'F',
    'use_syst': 'T',
    'sys_alpsfact': '0.5 1 2',
    'sys_pdf':"NNPDF30_lo_as_0118.LHgrid"
}

process_dir = new_process()

build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=randomSeed,beamEnergy=beamEnergy, extras=extras)

#---------------------------------------------------------------------------
# MG5 param Card
#---------------------------------------------------------------------------

## params is a dictionary of dictionaries (each dictionary is a 
## separate block)
params={}
mhd={}
mhd['mZDinput']=mZDinput
mhd['MHSinput']=MHSinput
mhd['epsilon']="1.000000e-02" ## default
mhd['kap']="1.000000e-09" ## default
mhd['aXM1']="1.000000" ## default
params['hidden']=mhd

build_param_card(param_card_old=os.path.join(process_dir,'Cards/param_card.dat'),
                 param_card_new='param_card_old.dat',
                 params=params)

fp = open ('param_card.dat','w')
lines = open('param_card_old.dat').readlines()
for s in lines:
    fp.write( s.replace('5.237950e+00','5.237950e-01'))
fp.close()

str_param_card='param_card.dat'

print_cards()

runName='run_01'

#---------------------------------------------------------------------------
# creating mad spin card
#---------------------------------------------------------------------------

madspin_card_loc='madspin_card.dat'
mscard = open(madspin_card_loc,'w')                                                                                               

mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
set spinmode none
define fall = u c s d b u~ c~ s~ d~ b~ e- e+ ta- ta+ mu- mu+ ve vm vt ve~ vm~ vt~
define l = mu- mu+ e- e+
#decay zp > fall fall
decay zp > fall fall; decay zp > l l; decay zp > l l
# running the actual code
launch"""%runArgs.randomSeed)  

mscard.close()

generate(run_card_loc='run_card.dat',param_card_loc=str_param_card,madspin_card_loc=madspin_card_loc,mode=2,njobs=1,run_name=runName,proc_dir=process_dir)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz', saveProcDir=True)                                                                         
import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts                                                                                                                                                                       
#### Shower                                                                                                                                                             
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = "Test HAHM_darkphoton_iDM_darkhiggs_UFO"
evgenConfig.keywords = ["exotic","BSM"]
evgenConfig.process = "p p > zp h2"
evgenConfig.contact = ["Mingyi Liu <mingyi.liu@cern.ch>"]
evgenConfig.inputfilecheck = runName

runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
                                                                                                             
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

