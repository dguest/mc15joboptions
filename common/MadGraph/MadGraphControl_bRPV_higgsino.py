#-------------------------------------------------------------
# JobOptions for bRPV model
#--------------------------------------------------------------

from MadGraphControl.MadGraphUtils import *

#turn on to True if you want to save MG diagrams, process xsecs etc. 
keepOutput=True

# e.g. MC15.999000.MGPy8EG_A14N23LO_bRPV_mu200_tb5_prC1C1.py
dsid        = str(runArgs.jobConfig[0].split('.')[1]) 
splitConfig = runArgs.jobConfig[0].rstrip('.py').split('_')
mu          = splitConfig[3][2:]
tb          = splitConfig[4][2:] 
proc        = splitConfig[5][2:] 
gentype     = 'pMSSM'

#--------------------------------------------------------------
# Process lines
#--------------------------------------------------------------
# Particle naming at /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/bRPV/particles.py
# susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
# p = g u c d s u~ c~ d~ s~
# j = g u c d s u~ c~ d~ s~
# jb = g u c d s b u~ c~ d~ s~ b~
# e4=C1m, nu4=N1, nu5=N2
# proc: C1C1, N1C1m, N1C1p, N2C1m, N2C1p, N1N2, N1N1, N2N2 
process = '''
import model bRPV --modelname
define j = g u1 u2 d1 d2 u1bar u2bar d1bar d2bar 
define p = g u1 u2 d1 d2 u1bar u2bar d1bar d2bar 
define susystrong = go su1 su4 sd1 sd4 su2 su5 sd2 sd5 su3 su6 sd3 sd6 su1c su4c sd1c sd4c su2c su5c sd2c sd5c su3c su6c sd3c sd6c
'''

if proc=="C1C1":
    process+='''
generate p p > e4 e4bar / susystrong @11
add process p p > e4 e4bar j / susystrong @21
add process p p > e4 e4bar j j / susystrong @31
'''
elif proc=="N1C1m":
    process+='''
generate p p > nu4 e4 / susystrong @12
add process p p > nu4 e4 j / susystrong @22
add process p p > nu4 e4 j j / susystrong @32
'''
elif proc=="N1C1p":
    process+='''
generate p p > nu4 e4bar / susystrong @13
add process p p > nu4 e4bar j / susystrong @23
add process p p > nu4 e4bar j j / susystrong @33
'''
elif proc=="N2C1m":
    process+='''
generate p p > nu5 e4 / susystrong @14
add process p p > nu5 e4 j / susystrong @24
add process p p > nu5 e4 j j / susystrong @34
'''
elif proc=="N2C1p":
    process+='''
generate p p > nu5 e4bar / susystrong @15
add process p p > nu5 e4bar j / susystrong @25
add process p p > nu5 e4bar j j / susystrong @35
'''
elif proc=="N1N2":
    process+='''
generate p p > nu4 nu5 / susystrong @16
add process p p > nu4 nu5 j / susystrong @26
add process p p > nu4 nu5 j j / susystrong @36
'''
elif proc=="N1N1":
    process+='''
generate p p > nu4 nu4 / susystrong @17
add process p p > nu4 nu4 j / susystrong @27
add process p p > nu4 nu4 j j / susystrong @37
'''
elif proc=="N2N2":
    process+='''
generate p p > nu5 nu5 / susystrong @18
add process p p > nu5 nu5 j / susystrong @28
add process p p > nu5 nu5 j j / susystrong @38
'''
else:
    print "ERROR: No such process", proc

process+='''
output -f
'''

njets = 2

#--------------------------------------------------------------
# Event filters and multipliers
#--------------------------------------------------------------

evt_multiplier = 2
if proc == "C1C1":
    evt_multiplier = 20
    if mu<=500:
        evt_multiplier = 90
if proc == "N2N2" and mu==700:
    evt_multiplier = 20

if '2L10' in str(runArgs.jobConfig[0]):
    evgenLog.info('2leptons10 filter is applied')

    include ( 'MC15JobOptions/MultiElecMuTauFilter.py' )
    filtSeq.MultiElecMuTauFilter.NLeptons  = 2
    filtSeq.MultiElecMuTauFilter.MinPt = 10000.         # pt-cut on the lepton
    filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
    filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0    # don't include hadronic taus
    evt_multiplier = 5

#-------------------------------------------------------------- 
# Define parameter cards
#--------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

#defining input slha file
# e.g. susy.999000.bRPV_mu200_tb5.slha
param_card_in = 'susy.%s.bRPV_mu%s_tb%s.slha' % (dsid,mu,tb)
log.info("Using paramCard %s" % param_card_in)

#Creating final parameter card
from MadGraphControl.MadGraphUtils import build_param_card
build_param_card( param_card_old = param_card_in, param_card_new = 'param_card.dat' )

beamEnergy = 6500.
if hasattr(runArgs,'ecmEnergy'): beamEnergy = runArgs.ecmEnergy / 2.

rand_seed=1234
if hasattr(runArgs, "randomSeed"): rand_seed=runArgs.randomSeed

extras = { 'lhe_version'   : '3.0',
           'cut_decays'    : 'F',
           'pdlabel'       : "'lhapdf'",
           'lhaid'         : '263400',
           'bwcutoff'      : '1000',
           'use_syst'      : 'False',
           'drjj'          :  -1.0,
           'drll'          :  -1.0,
           'draa'          :  -1.0,
           'draj'          :  -1.0,
           'drjl'          :  -1.0,
           'dral'          :  -1.0,
           'etal'          :  -1.0,
           'etaj'          :  -1.0,
           'etaa'          :  -1.0,
         }

thedir = new_process(card_loc=process)

build_run_card(run_card_old=get_default_runcard(proc_dir=thedir), 
               run_card_new='run_card.dat', 
               nevts=runArgs.maxEvents * evt_multiplier, 
               rand_seed=runArgs.randomSeed, 
               beamEnergy=beamEnergy, 
               xqcut=100., extras=extras)

generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=thedir)

# Move output files into the appropriate place, with the appropriate name
the_spot = arrange_output(run_name='Test',proc_dir=thedir,outputDS='madgraph_OTF._00001.events.tar.gz')
outputDS = the_spot
runArgs.inputGeneratorFile = outputDS
runArgs.qcut = 100.

shutil.rmtree(thedir,ignore_errors=True)

#-------------------------------------------------------------- 
# Pass everything to the PostInclude JO file
#--------------------------------------------------------------

evgenConfig.contact = ["judita.mamuzic@cern.ch"]
evgenConfig.keywords += ['gaugino', 'chargino', 'neutralino']
evgenConfig.description = 'bRPV model with higgsino production, mu = %s GeV, tb = %s GeV, proc = %s' % (mu,tb,proc)

#extra settings needed to tell Pythia8 about the sparticle spin
if njets>0:
    genSeq.Pythia8.Commands += [ "Merging:Process = pp>{e4,-1000024}{e4bar,1000024}",
                                 "Merging:Process = pp>{nu4,1000022}{e4,-1000024}",
                                 "Merging:Process = pp>{nu4,1000022}{e4bar,1000024}",
                                 "Merging:Process = pp>{nu5,1000023}{e4,-1000024}",
                                 "Merging:Process = pp>{nu5,1000023}{e4bar,1000024}",
                                 "Merging:Process = pp>{nu4,1000022}{nu5,1000023}",
                                 "Merging:Process = pp>{nu4,1000022}{nu4,1000022}",
                                 "Merging:Process = pp>{nu5,1000023}{nu5,1000023}"]

# extra settings needed to tell Pythia8 about the sparticle spin
#if njets>0:
#    # use "guess" option for merging of processes,
#    # works with Pythia 8.230 or higher, use 19.2.5.33.4
#    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
#    if "UserHooks" in genSeq.Pythia8.__slots__.keys():
#        genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
#    else:
#        genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'
#    genSeq.Pythia8.Commands += ["1000024:spinType = 1",
#                                "1000023:spinType = 1",
#                                "1000022:spinType = 1"]
