import re
import os
import math
import shutil
import subprocess

from MadGraphControl.MadGraphUtils import *

nevents = 5500
mode = 0


job_option_name = runArgs.jobConfig[0]

generation = 0
quark_flavour = job_option_name.split('U1T')[-1][0]
quark_flavours = ['c','c~', 'b', 'b~' ]
quark_flavour_index = quark_flavours.index(quark_flavour) + 1
if quark_flavour not in quark_flavours:
    raise RuntimeError("Cannot determine quark flavour from job option name: {:s}.".format(job_option_name))
if "Wtau" in job_option_name:
    generation = 1
elif "LQnu" in job_option_name:
    generation = 3
elif "LQtau" in job_option_name:
    generation = 3
else:
    raise RuntimeError("Cannot determine LQ generation from job option name: {:s}.".format(job_option_name))

matches = re.search("M([0-9]+).*\.py", job_option_name)
if matches is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    lqmass = float(matches.group(1))

#matches = re.search("l([0-9]_[0-9]+)\.py", job_option_name)
#if matches is None:
    #raise RuntimeError("Cannot find coupling string in job option name: {:s}.".format(job_option_name))
#else:
    #coupling = float(matches.group(1).replace("_", "."))

fcard = open('proc_card_mg5.dat', 'w')
fcard.write("""
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model sm
define p = g u c d s b u~ c~ d~ s~ b~
define vl = ve vm vt
define vl~ = ve~ vm~ vt~\n""")

if generation == 1:
    #fcard.write("""import model ./LO_LQ_S1Tilde\n""")
    fcard.write("""generate p p > ta- vt~ {:s} \n""".format(quark_flavour))
    fcard.write("""output -f\n""")
    fcard.close()

elif generation == 3:
    fcard.write("""import model ./vector_LQ_UFO\n""")
    fcard.write("""define p = g u c d s b u~ c~ d~ s~ b~\n""")
    fcard.write("""generate p p >  ta- vt~ {:s} \n""".format(quark_flavour))
    fcard.write("""add process p p > ta+ vt {:s}~ \n""".format(quark_flavour))
    fcard.write("""output -f""")
    fcard.close()

beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process()
extras = {'pdlabel': "'lhapdf'",
	  'lhaid': 260000,
          'ktdurham': lqmass * 0.25}

try:
    os.remove('run_card.dat')
except OSError:
    pass

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir), run_card_new='run_card.dat',
               nevts=nevents, rand_seed=runArgs.randomSeed, beamEnergy=beamEnergy, extras=extras)

if os.path.exists("param_card.dat"):
    os.remove("param_card.dat")


param_card_name = 'MadGraph_param_card_TBU1_LO.dat'
param_card = subprocess.Popen(['get_files', '-data', param_card_name])
param_card.wait()
if not os.access(param_card_name, os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open(param_card_name, 'r')

    newcard = open('param_card.dat', 'w')
    for line in oldcard:
        if '# MVLQ' in line:
            newcard.write('  9000007 {:e} # vlq  \n'.format(lqmass))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
 
print_cards()

runName = 'run_01'
process_dir = new_process()
generate(run_card_loc='run_card.dat',
         param_card_loc='param_card.dat',
         mode=mode,
         proc_dir=process_dir,
         run_name=runName)

arrange_output(run_name=runName, proc_dir=process_dir, outputDS=runName + '._00001.events.tar.gz', lhe_version=3,
               saveProcDir=True)

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = 'Vector LO single production of U1, generation: {0:d}, mLQ={1:d}'.format(
    int(generation), int(lqmass))
evgenConfig.keywords += ['BSM', 'exotic', 'leptoquark']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> vlq ta- -> nu c ta-'
evgenConfig.contact = ["Patrick Bauer <patrick.bauer@cern.ch>"]
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile = runName + '._00001.events.tar.gz'
