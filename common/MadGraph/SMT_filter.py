from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("Wetau")
filtSeq.Wetau.PDGParent  = [24]
filtSeq.Wetau.PDGChild = [11,15]

filtSeq += ParentChildFilter("Wmu")
filtSeq.Wmu.PDGParent  = [24]
filtSeq.Wmu.PDGChild = [13]

filtSeq += ParentChildFilter("Zetau")
filtSeq.Zetau.PDGParent  = [23]
filtSeq.Zetau.PDGChild = [11,11,15,15]

filtSeq += ParentChildFilter("Zmu")
filtSeq.Zmu.PDGParent  = [23]
filtSeq.Zmu.PDGChild = [13,13]
 
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("OneMuonFilter")
filtSeq.OneMuonFilter.Ptcut = 3250.
filtSeq.OneMuonFilter.Etacut = 2.8
filtSeq.OneMuonFilter.NMuons = 1

filtSeq += MultiMuonFilter("TwoMuonsFilter")
filtSeq.TwoMuonsFilter.Ptcut = 3250.
filtSeq.TwoMuonsFilter.Etacut = 2.8
filtSeq.TwoMuonsFilter.NMuons = 2

filtSeq += MultiMuonFilter("ThreeMuonsFilter")
filtSeq.ThreeMuonsFilter.Ptcut = 3250.
filtSeq.ThreeMuonsFilter.Etacut = 2.8
filtSeq.ThreeMuonsFilter.NMuons = 3

filtSeq += MultiMuonFilter("FourMuonsFilter")
filtSeq.FourMuonsFilter.Ptcut = 3250.
filtSeq.FourMuonsFilter.Etacut = 2.8
filtSeq.FourMuonsFilter.NMuons = 4

from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
filtSeq += ElectronFilter("ElectronFilter")
filtSeq.ElectronFilter.Ptcut = 20000.
filtSeq.ElectronFilter.Etacut = 2.8

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
filtSeq += MuonFilter("MuonFilter")
filtSeq.MuonFilter.Ptcut = 20000.
filtSeq.MuonFilter.Etacut = 2.8

filtSeq.Expression=" (ElectronFilter or MuonFilter) and ( (Wmu and ( (Zmu and FourMuonsFilter) or (Zetau and TwoMuonsFilter) )) or (Wetau and ( (Zmu and ThreeMuonsFilter) or (Zetau and OneMuonFilter) )) )"
