from MadGraphControl.MadGraphUtils import *
import re


reMassTp = int(re.findall(r'\d+',re.findall(r'\d+_S',runArgs.jobConfig[0])[0])[0])
reMassS  = int(re.findall(r'\d+',re.findall(r'\d+_Za',runArgs.jobConfig[0])[0])[0])
reKL     = int(re.findall(r'\d+',re.findall(r'\d+KB',runArgs.jobConfig[0])[0])[0])
reKBB    = int(re.findall(r'\d+',re.findall(r'\d+KW',runArgs.jobConfig[0])[0])[0])
reKWW    = int(re.findall(r'\d+', runArgs.jobConfig[0])[-1])


runArgs.massTp = reMassTp
runArgs.massS  = reMassS*100
runArgs.TlSt   = reKL*0.001
runArgs.KSWW   = reKWW*0.0000001
runArgs.KSBB   = reKBB*0.0000001

print ("Mass of Tp:  ", runArgs.massTp)
print ("Mass of S:  ", runArgs.massS)
print ("TLSt coupling:  ", runArgs.TlSt)
print ("SWW coupling:  ", runArgs.KSWW)
print ("SBB coupling:  ", runArgs.KSBB)
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model ./VLQ_scalar_diagonalCKM_5FS_UFO

define bb = b b~
define WW = w+ w-
define tt = t t~
define ferm = ve vm vt ve~ vm~ vt~ mu- ta- e- mu+ ta+ e+ u c d s u~ c~ d~ s~
define p = 21 2 4 1 3 -2 -4 -1 -3 5 -5 # pass to 5 flavors
define j = p
generate p p > tp tp~, (tp > eta t, (eta > z a, (z > l+ l-)), (t > w+ \                                                                                                                                             
b, (w+ > ferm ferm))), (tp~ > eta t~, (eta > z a, (z > l+ l-)), (t~ > w- b~,\                                                                                                                                             
 (w- > ferm ferm)))                                                                                                                                                                                                    
output -f
""")



fcard.flush()


process_dir = new_process()
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 6500.

# allow usage of all PDF sets
os.environ['LHAPATH']=os.environ["LHAPDF_DATA_PATH"]=(os.environ['LHAPATH'].split(':')[0])+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
# especially 263400 = NNPDF30_lo_as_0130_nf_4

extras = { 'lhe_version'   : '3.0',
           'cut_decays'    : 'F',
           'pdlabel'       : "'lhapdf'",
           'lhaid'         : '303400',
           'bwcutoff'      : '15',
           'event_norm'    : 'average',
           'use_syst'      : 'False',
           'drjj'          :  -1.0,
           'drll'          :  -1.0,
           'draa'          :  -1.0,
           'draj'          :  -1.0,
           'drjl'          :  -1.0,
           'dral'          :  -1.0,
           'etal'          :  -1.0,
           'etaj'          :  -1.0,
           'etaa'          :  -1.0,


 
}
safe_factor = 1.1
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir), run_card_new='run_card.dat', nevts=runArgs.maxEvents * safe_factor, rand_seed=runArgs.randomSeed, beamEnergy=beamEnergy, xqcut=0., extras=extras)


if not os.access(process_dir+'/Cards/param_card.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open(process_dir+'/Cards/param_card.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if ' # MTP ' in line:
            newcard.write(' 6000006 %i # MTP \n'%(runArgs.massTp))
        elif ' # Meta ' in line:
            newcard.write(' 6000025 %i # Meta \n'%(runArgs.massS))
        elif ' # KetaVLTL ' in line:
            newcard.write(' 1 %e # KetaVLTL \n'%(runArgs.TlSt))
        
        elif ' # KetaWW ' in line:
            newcard.write(' 2 %e # KetaWW \n'%(runArgs.KSWW))
        elif ' # KetaBB ' in line:
            newcard.write(' 3 %e # KetaBB \n'%(runArgs.KSBB))
        elif ' # WTP ' in line:
            newcard.write('DECAY 6000006 auto # WTP \n')
        elif ' # Weta ' in line:
            newcard.write('DECAY 6000025 auto # Weta \n')
        # set some sm parameters to atlas defaults
        elif ' # MB ' in line:
            newcard.write('    5 4.950000e+00 # MB \n')
        elif ' # MT ' in line:
            newcard.write('    6 1.725000e+02 # MT \n')
        elif ' # WT ' in line:
            newcard.write('DECAY   6 1.350000e+00 # WT \n')
        elif ' # WZ ' in line:
            newcard.write('DECAY  23 2.495200e+00 # WZ \n')
        elif ' # WW ' in line:
            newcard.write('DECAY  24 2.085000e+00 # WW \n')
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()


runName='run_01'
generate(run_card_loc='./run_card.dat',param_card_loc='./param_card.dat',run_name=runName,proc_dir=process_dir)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')  


runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")


evgenConfig.description = "MadGraph+Pythia8 production JO with NNPDF30LN and A15NNPDF23LO for VLQ decay to scalar and scalar decaying to Za"

evgenConfig.keywords = ["BSM", "BSMtop", "exotic"]

evgenConfig.process = "TZa"

evgenConfig.contact =  ['venugopal.ellajosyula@cern.ch']

evgenConfig.minevents = 5000

