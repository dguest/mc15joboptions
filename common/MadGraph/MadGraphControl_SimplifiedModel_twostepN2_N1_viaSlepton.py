# Generator transform pre-include
#  Gets us ready for on-the-fly SUSY SM generation
include ( 'MC15JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

gentype=runArgs.jobConfig[0].split('SM')[1].split('_')[1]
if 'SLN1' in runArgs.jobConfig[0]: decaytype='twostepN2_SLN1'


mass_string = runArgs.jobConfig[0].replace('.py','').split('N1_')[1]
deltaM = 0 # mchi20 - mchi10 

#--------------------------------------------------------------
# MadGrpah configuration
#--------------------------------------------------------------
if gentype=='GG':
# Direct gluino decay to LSP (0-lepton, grid 1 last year)
    masses['1000021'] = float( mass_string.split('_')[0] )  #gluino
    masses['1000022'] = float( mass_string.split('_')[1] )  #chi10
    masses['1000023'] = 0.5*(masses['1000021']+masses['1000022'])  #chi20
    deltaM = 0.5*(masses['1000021'] - masses['1000022']) # (mgluino - mchi10) / 2 = mchi20 - mchi10 

    if 'SL' in decaytype:
        masses['1000011'] = 0.5*(masses['1000022']+masses['1000023'])  #slepton
        masses['1000012'] = 0.5*(masses['1000022']+masses['1000023'])  #slepton
        masses['1000013'] = 0.5*(masses['1000022']+masses['1000023'])  #slepton
        masses['1000014'] = 0.5*(masses['1000022']+masses['1000023'])  #slepton
        masses['1000015'] = 0.5*(masses['1000022']+masses['1000023'])  #slepton
        masses['1000016'] = 0.5*(masses['1000022']+masses['1000023'])  #slepton
    else:
        masses['1000011'] = 4.5e5 #slepton
        masses['1000012'] = 4.5e5 #slepton
        masses['1000013'] = 4.5e5 #slepton
        masses['1000014'] = 4.5e5 #slepton
        masses['1000015'] = 4.5e5 #slepton
        masses['1000016'] = 4.5e5 #slepton
    process = '''
    generate p p > go go
    add process p p > go go j
    '''

evgenConfig.contact  = [ "arka.santra@cern.ch" ]
if 'GG' in gentype: evgenConfig.keywords += ['simplifiedModel','gluino']

if 'SL' in decaytype:
    evgenConfig.description = 'SUSY Simplified Model with gluino production and decays via sleptons with MadGraph/Pythia8, m_glu = %s GeV, m_N2 = %s GeV, m_slep = %s GeV, m_N1 = %s GeV'%(masses['1000021'],masses['1000023'],masses['1000011'],masses['1000022'])
    evgenConfig.keywords += ['slepton']
    
#--------------------------------------------------------------
# Pythia configuration
#--------------------------------------------------------------
# This comes after all Simplified Model setup files
evgenLog.info('Will use Pythia8...')

pythia = genSeq.Pythia8

pythia.MaxFailures = 100

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
filters=[]
# Two-lepton+MET filter
if '2LMET100' in runArgs.jobConfig[0]:
    evt_multiplier = 20
    include('MC15JobOptions/MultiLeptonFilter.py')
    MultiLeptonFilter = filtSeq.MultiLeptonFilter
    filtSeq.MultiLeptonFilter.Ptcut = 5000.
    filtSeq.MultiLeptonFilter.Etacut = 2.8
    filtSeq.MultiLeptonFilter.NLeptons = 2  
    
    include("MC15JobOptions/MissingEtFilter.py")
    filtSeq.MissingEtFilter.METCut = 100*GeV            # MET > 100 GeV
  
    filtSeq.Expression = "(MultiLeptonFilter) and (MissingEtFilter)"
    
    
# two lepton filter
elif '2L' in runArgs.jobConfig[0]:
    evt_multiplier = 20
    include('MC15JobOptions/MultiLeptonFilter.py')
    MultiLeptonFilter = filtSeq.MultiLeptonFilter
    filtSeq.MultiLeptonFilter.Ptcut = 5000.
    filtSeq.MultiLeptonFilter.Etacut = 2.8
    filtSeq.MultiLeptonFilter.NLeptons = 2  
    

# only MET filter
elif 'MET100' in runArgs.jobConfig[0]:
    evt_multiplier *= 2
    include ( 'MC15JobOptions/MissingEtFilter.py' )
    MissingEtFilter = filtSeq.MissingEtFilter
    filtSeq.MissingEtFilter.METCut = 100*GeV

njets = 1
include('MC15JobOptions/MadGraphControl_SimplifiedModelPostInclude.py')

    
if njets>0:
    genSeq.Pythia8.Commands += [ "Merging:Process = guess" ]
    
    ### needed to use "guess" option of Pythia.
    if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
    else:
        genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'
