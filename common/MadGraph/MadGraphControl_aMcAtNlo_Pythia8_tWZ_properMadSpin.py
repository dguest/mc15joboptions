from MadGraphControl.MadGraphUtils import *
import fileinput
import shutil
import subprocess
import os
#############
# General settings
nevents=int(1.1*runArgs.maxEvents)
mode=0
# MG Particle cuts
mllcut=-1
maxjetflavor=5
parton_shower='PYTHIA8'

madspin_dir = 'my_madspin'
### DSID list
tWZ_DR1_dil_P8  = [412118]
tWZ_DR2_dil_P8  = [412119]

mgproc="""generate p p > ttbar w Z [QCD]"""


if runArgs.runNumber==412118:
    DR_mode='DR1'
elif runArgs.runNumber==412119:
    DR_mode='DR2'
else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

name='tWZ_min'+DR_mode+'_NLO__P8'

zdecay="decay z > l+ l-"
stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define w = w+ w-
define ttbar = t t~
"""+mgproc+"""
output -f
""")
fcard.close()
process_dir = new_process()

# fetch helpers required by do_+DR_mode+.py
DR_helper = 'DR_functions.py'
subprocess.Popen(['get_files','-data',DR_helper]).communicate()
if not os.access(DR_helper, os.R_OK):
    raise RuntimeError("Could not get_file required for automated diagram removal: '%s"%DR_helper)


#Call DR code
if DR_mode!='':
    include('MC15JobOptions/do_'+DR_mode+'.py')
else:
    raise RuntimeError("No DR mode found, this calculation will not converge!")


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")
lhaid=260000
pdflabel='lhapdf'
#Fetch default LO run_card.dat and set parameters
extras = { 'lhaid'         : lhaid,
           'pdlabel'       : "'"+pdflabel+"'",
           'maxjetflavor'  : maxjetflavor,
           'parton_shower' : parton_shower,
           'fixed_ren_scale' : "True",
           'fixed_fac_scale' : "True",
           'muR_ref_fixed' : 172.5,
           'muF1_ref_fixed' : 172.5,
           'muF2_ref_fixed' : 172.5,
           'fixed_QES_scale' : "True",
           'QES_ref_fixed' : 172.5
        }
doSyst=True
if doSyst:
    lhe_version=3
    extras.update({'reweight_scale': '.true.',
                   'rw_Rscale_down':  0.5,
                   'rw_Rscale_up'  :  2.0,
                   'rw_Fscale_down':  0.5,
                   'rw_Fscale_up'  :  2.0,
                   'reweight_PDF'  : '.true.',
                   'PDF_set_min'   : 260001,
                   'PDF_set_max'   : 260100,
                   'mll_sf'        : mllcut})
else:
    lhe_version=1
    extras.update({'reweight_scale': '.false.',
                   'reweight_PDF'  : '.false.'})
madspin_card_loc='madspin_card.dat'



mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
 #*                        MadSpin                           *
 #*                                                          *
 #*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
 #*                                                          *
 #*    Part of the MadGraph5_aMC@NLO Framework:              *
 #*    The MadGraph5_aMC@NLO Development Team - Find us at   *
 #*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
 #*                                                          *
 #************************************************************
 #Some options (uncomment to apply)
 #
 # set seed 1
 # set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
 # set BW_cut 15                # cut on how far the particle can be off-shell
 # set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
  set max_weight_ps_point 500
  set Nevents_for_max_weigth 500
  set BW_cut 50
  set ms_dir %s
 #set use_old_dir True
 #
 set seed %i
 # specify the decay for the final state particles
 define q = u d s c b
 define q~ = u~ d~ s~ c~ b~
 define l+ = e+ mu+ ta+
 define l- = e- mu- ta-
 define v = ve vm vt
 define v~ = ve~ vm~ vt~
 decay t > w+ b, w+ > all all
 decay t~ > w- b~, w- > all all
 decay w+ > all all
 decay w- > all all
 %s
 # running the actual code
 launch"""%(madspin_dir,runArgs.randomSeed,zdecay))
mscard.close()

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
               extras=extras)
print_cards()
param_card_loc='aMcAtNlo_param_card_loopsmnobmass.dat'
paramcard = subprocess.Popen(['get_files','-data',param_card_loc])
paramcard.communicate()
if not os.access(param_card_loc,os.R_OK):
    raise RuntimeError("ERROR: Could not get %s"%param_card.loop_sm-no_b_mass.dat)
generate(run_card_loc='run_card.dat',param_card_loc=param_card_loc,mode=mode,proc_dir=process_dir,madspin_card_loc=madspin_card_loc)


msfile=process_dir+'/Cards/madspin_card.dat'
mstilde=process_dir+'/Cards/madspin_card.dat~'
shutil.copyfile(msfile,mstilde)
with open(msfile,"w") as myfile, open(mstilde,'r') as f:
    for line in f:
        if '#set use_old_dir True' in line:
            line = line.replace('#',' ') #uncomment
        myfile.write(line)
os.remove(mstilde)


#MadSpin DR hack and compile
MadSpin_DR_hack = 'do_MadSpin_'+DR_mode+'.py'

subprocess.Popen(['get_files','-data',MadSpin_DR_hack]).communicate()
if not os.access(MadSpin_DR_hack, os.R_OK):
    raise RuntimeError("Could not get_file required for MadSpin DR hack: '%s"%MadSpin_DR_hack)

full_madspin_dir = process_dir+'/'+madspin_dir

do_hack_MS = subprocess.Popen(['python', MadSpin_DR_hack, full_madspin_dir], stdout=subprocess.PIPE, cwd=".")
hack_output = do_hack_MS.communicate() #instead of wait() to avoid the pipe buffer filling up and blocking

do_hack_MS=subprocess.Popen(["make", "clean"], stdout=subprocess.PIPE, cwd=process_dir)
hack_output = do_hack_MS.communicate()
do_hack_MS=subprocess.Popen("make", stdout=subprocess.PIPE, cwd=process_dir) #The first compilation attempt fails, with errors written to the log
hack_output = do_hack_MS.communicate()
do_hack_MS=subprocess.Popen("make", stdout=subprocess.PIPE, cwd=process_dir) #The second compilation attempt works, producing a new tarfile in the process directory
hack_output = do_hack_MS.communicate()

do_hack_MS=subprocess.Popen(['bin/aMCatNLO', 'decay_events', 'Test', '-f'], stdout=subprocess.PIPE, cwd=process_dir)
hack_output = do_hack_MS.communicate()

outputDS=arrange_output(proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz', lhe_version=lhe_version,saveProcDir=True)

#### Shower
## run Pythia8 on-the-fly -----------------------------------------------------
## Provide config information
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]
evgenConfig.description = "MG5aMCatNLO/Pythia8 tWZ_Ztoll "+DR_mode
evgenConfig.keywords    = ["SM","top"]
evgenConfig.contact     = ["olga.bylund@cern.ch"]
runArgs.inputGeneratorFile=outputDS
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")
